﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using DataLayer;
using System.Web;
  
namespace BusinessLayer
{

    public class ManageCSR
    {
        public int intGroupId, intSquence, intTask, intBranchID, intSequence, Tasks, intFyId, intDepartmentId, intProjectId;
        public string strGroupName, strDisplayLavel, strFormDate, strToDate, strGenerateFormat, strformatebyFirstEnd, strFormatByName, strFormatBySymbol, FromDate, Todate;

        public string ManagetblCSRGroupMaster()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@strGroupName", strGroupName);
                parm[i++] = new SqlParameter("@intSquence", intSquence);
                parm[i++] = new SqlParameter("@strDisplayLavel", strDisplayLavel);
                parm[i++] = new SqlParameter("@intCreatedId", intCreatedId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCSRGroupMaster", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public string GetDeletetblCSRGroupMaster()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@Task", intTask);

                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("GetDeletetblCSRGroupMaster", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public DataTable GetCSRGroupDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@Task", intTask);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetDeletetblCSRGroupMaster", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intGroupDetailsId;
        public string strGroupDetailsName;
        public string ManagetblCSRGroupDetails()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupDetailsId", intGroupDetailsId);
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@strGroupDetailsName", strGroupDetailsName);
                parm[i++] = new SqlParameter("@intSequence", intSquence);
                parm[i++] = new SqlParameter("@strDisplayLevel", strDisplayLavel);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCSRGroupDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }


        public DataTable GetCSRtblCSRGroupDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@intGroupDetailsId", intGroupDetailsId);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblCSRGroupDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }
        public DataTable GetProjectTaskDetails()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intProjectTypeMaster", intProjectTypeMaster);
                parm[i++] = new SqlParameter("@strTaskName", strTaskName);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("GetProjectTaskDetails_New", parm);
            }
            catch (Exception exp)
            {
                dt = null;
                ExceptionHandler.LogError(exp);
            }
            return dt;
        }

        public string DeletetblCSRGroupDetails()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupDetailsId", intGroupDetailsId);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCSRGroupDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public int intSubgroupDetailsId, intUOMId;
        public decimal dblRate;
        public string strSubGroupName, strSubTotalFlag, strGoodValueFlag;
        public string MANAGEtblCSRSubGroupDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                  
                SqlParameter[] parm = new SqlParameter[11];
                int i = 0;
                parm[i++] = new SqlParameter("@intSubgroupDetailsId", intSubgroupDetailsId);
                parm[i++] = new SqlParameter("@intGroupDetailsId", intGroupDetailsId);
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@strSubGroupName", strSubGroupName);
                parm[i++] = new SqlParameter("@intUOMId", intUOMId);
                parm[i++] = new SqlParameter("@dblRate", dblRate);
                parm[i++] = new SqlParameter("@strSubTotalFlag", strSubTotalFlag);
                parm[i++] = new SqlParameter("@intsequence", intSquence);
                parm[i++] = new SqlParameter("@strDisplayLavel", strDisplayLavel);
                parm[i++] = new SqlParameter("@strGoodValueFlag", strGoodValueFlag);
                parm[i++] = new SqlParameter("@Tasks", intTask);

                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("MANAGEtblCSRSubGroupDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public string DeletetblCSRSubGroupDetails()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intSubgroupDetailsId", intSubgroupDetailsId);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("MANAGEtblCSRSubGroupDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public DataTable GetMANAGEtblCSRSubGroupDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intSubgroupDetailsId", intSubgroupDetailsId);
                parm[i++] = new SqlParameter("@intGroupDetailsId", intGroupDetailsId);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("MANAGEtblCSRSubGroupDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intFYID;
        public DataTable GetYearandMonth()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intFYID", intFYID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetYearandMonth", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strMonthDate;
        public int intBranchId, intMonthId;
        public decimal dblTargetQuantity, dblPCTargetValues;
        public string MANAGEtblCSRTargetValues()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@strMonthDate", strMonthDate);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFYID);
                parm[i++] = new SqlParameter("@dblTargetQuantity", dblTargetQuantity);
                parm[i++] = new SqlParameter("@intSubgroupDetailsId", intSubgroupDetailsId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intCreatedId", intCreatedId);
                parm[i++] = new SqlParameter("@intMonthId", intMonthId);
                parm[i++] = new SqlParameter("@dblPCTargetValues", dblPCTargetValues);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("MANAGEtblCSRTargetValues", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public decimal dblValues;
        public string strRemarks;
        public string ManagetblCSREntryDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@strMonthDate", strMonthDate);
                parm[i++] = new SqlParameter("@intSubgroupDetailsId", intSubgroupDetailsId);
                parm[i++] = new SqlParameter("@dblValues", dblValues);
                parm[i++] = new SqlParameter("@intFYId", intFYID);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@intCreatedId", intCreatedId);
                parm[i++] = new SqlParameter("@intMonthId", intMonthId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCSREntryDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }





        //----DTM---\\

        public int intProjectTypeMaster;
        public string strProjecttype, strProjecttypeShortName, strFor, strProjectTypeOthers;
        public string ManagetblDTMProjectTypeMaster()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intProjectTypeMaster", intProjectTypeMaster);
                parm[i++] = new SqlParameter("@strProjecttype", strProjecttype);
                parm[i++] = new SqlParameter("@strProjecttypeShortName", strProjecttypeShortName);
                parm[i++] = new SqlParameter("@strFor", strFor);
                parm[i++] = new SqlParameter("@strProjectTypeOthers", strProjectTypeOthers);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblDTMProjectTypeMaster", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }


        public DataTable GettblDTMProjectTypeMaster()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intProjectTypeMaster", intProjectTypeMaster);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblDTMProjectTypeMaster", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string DeletetblDTMProjectTypeMaster()
        {
            string aReturnValue = string.Empty;
            try
            {


                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intProjectTypeMaster", intProjectTypeMaster);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblDTMProjectTypeMaster", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intprojectId, intProjecttype, intCustomerId, intProjectInchargeId, intTaskFormatId;
        public string strshortName, strProjectName, strProjectDesc, strInhouseflag, strProjectCode, strProjectstartDate, strTargetDate, strProjectFlag, strTaskType;
        //strOthersFlag,strIdleTImeFlag, strNoPerformanceFlag;
        public decimal dblPlanedhours, dblPlanedBudget;
        public string ManagetblDTMProjectMaster()
        {
            string aReturnValue = string.Empty;
            try
            {

                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[24];
                int i = 0;
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@intBracnchId", intBranchId);
                parm[i++] = new SqlParameter("@intcompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@strProjectCode", strProjectCode);
                parm[i++] = new SqlParameter("@strshortName", strshortName);
                parm[i++] = new SqlParameter("@strProjectName", strProjectName);
                parm[i++] = new SqlParameter("@intProjecttype", intProjecttype);
                parm[i++] = new SqlParameter("@strProjectDesc", strProjectDesc);
                //parm[i++] = new SqlParameter("@strInhouseflag",strInhouseflag);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                parm[i++] = new SqlParameter("@strProjectstartDate", strProjectstartDate);
                parm[i++] = new SqlParameter("@strTargetDate", strTargetDate);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@dblPlanedhours", dblPlanedhours);
                parm[i++] = new SqlParameter("@dblPlanedBudget", dblPlanedBudget);
                parm[i++] = new SqlParameter("@intProjectInchargeId", intProjectInchargeId);
                parm[i++] = new SqlParameter("@inrcreatedById", intCreatedId);

                parm[i++] = new SqlParameter("@strProjectFlag", strProjectFlag);
                parm[i++] = new SqlParameter("@strTaskType", strTaskType);
                parm[i++] = new SqlParameter("@intTaskFormatId", intTaskFormatId);
                parm[i++] = new SqlParameter("@strGenerateFormat", strGenerateFormat);
                parm[i++] = new SqlParameter("@strformatebyFirstEnd", strformatebyFirstEnd);
                parm[i++] = new SqlParameter("@strFormatByName", strFormatByName);
                parm[i++] = new SqlParameter("@strFormatBySymbol", strFormatBySymbol);

                //parm[i++] = new SqlParameter("@strOthersFlag", strOthersFlag);
                //parm[i++] = new SqlParameter("@strIdleTImeFlag", strOthersFlag);
                //parm[i++] = new SqlParameter("@strNoPerformanceFlag", strNoPerformanceFlag);

                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblDTMProjectMaster", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }


        public string DeletetblDTMProjectMaster()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("DeletetblDTMProjectMaster", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }
        
        public DataTable GetManagetblDTMProjectMaster()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetManagetblDTMProjectMaster", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intPdId, intsubdepartmentId;
        public string ManagetblDTMProjectDepartmentDetails()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intPdId", intPdId);
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                parm[i++] = new SqlParameter("@intsubdepartmentId", intsubdepartmentId);
                parm[i++] = new SqlParameter("@dblPlanedhours", dblPlanedhours);
                parm[i++] = new SqlParameter("@dblPlanedBudget", dblPlanedBudget);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblDTMProjectDepartmentDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public string DeleteDTMProjectDepartmentDetails()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intPdId", intPdId);
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblDTMProjectDepartmentDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public DataTable GetManagetblDTMProjectDepartmentDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intPdId", intPdId);
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblDTMProjectDepartmentDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intPTDId, intPrevioustask;
        public decimal dblPlanedbudget;
        public string strTaskName, strTaskshortName, strTaskDetail, strTaskstartdate, strTaskTargetDate, strSequenceParallalFlag;
        public string MANAGEtblDTMProjecttaskDetails()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[13];
                int i = 0;
                parm[i++] = new SqlParameter("@intPTDId", intPTDId);
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                parm[i++] = new SqlParameter("@strTaskName", strTaskName);
                parm[i++] = new SqlParameter("@strTaskshortName", strTaskshortName);
                parm[i++] = new SqlParameter("@strTaskDetail", strTaskDetail);
                parm[i++] = new SqlParameter("@dblPlanedhours", dblPlanedhours);
                parm[i++] = new SqlParameter("@dblPlanedbudget", dblPlanedbudget);
                parm[i++] = new SqlParameter("@strTaskstartdate", strTaskstartdate);
                parm[i++] = new SqlParameter("@strTaskTargetDate", strTaskTargetDate);
                parm[i++] = new SqlParameter("@strSequenceParallalFlag", strSequenceParallalFlag);
                parm[i++] = new SqlParameter("@intPrevioustask", intPrevioustask);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("MANAGEtblDTMProjecttaskDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public string DeleteMANAGEtblDTMProjecttaskDetails()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intPTDId", intPTDId);
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("MANAGEtblDTMProjecttaskDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public DataTable GetMANAGEtblDTMProjecttaskDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intPTDId", intPTDId);
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("MANAGEtblDTMProjecttaskDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intPTSID;
        public string ManagetblDTMProjectSubTaskDetails()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[14];
                int i = 0;
                parm[i++] = new SqlParameter("@intPTSID", intPTSID);
                parm[i++] = new SqlParameter("@intPTDId", intPTDId);
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                parm[i++] = new SqlParameter("@strTaskName", strTaskName);
                parm[i++] = new SqlParameter("@strTaskshortName", strTaskshortName);
                parm[i++] = new SqlParameter("@strTaskDetail", strTaskDetail);
                parm[i++] = new SqlParameter("@dblPlanedhours", dblPlanedhours);
                parm[i++] = new SqlParameter("@dblPlanedbudget", dblPlanedbudget);
                parm[i++] = new SqlParameter("@strTaskstartdate", strTaskstartdate);
                parm[i++] = new SqlParameter("@strTaskTargetDate", strTaskTargetDate);
                parm[i++] = new SqlParameter("@strSequenceParallalFlag", strSequenceParallalFlag);
                parm[i++] = new SqlParameter("@intPrevioustask", intPrevioustask);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblDTMProjectSubTaskDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public string DeletetblDTMProjectSubTaskDetails()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intPTSID", intPTSID);
                parm[i++] = new SqlParameter("@intPTDId", intPTDId);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblDTMProjectSubTaskDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public DataTable GetManagetblDTMProjectSubTaskDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intPTSID", intPTSID);
                parm[i++] = new SqlParameter("@intPTDId", intPTDId);
                parm[i++] = new SqlParameter("@Tasks", intTask);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblDTMProjectSubTaskDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string FromTime, ToTimeTime, FromActualTime, ToActualTime;
        public DataTable DTMCheckDateandTimeNotBigger()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@FromTime", FromTime);
                parm[i++] = new SqlParameter("@ToTimeTime", ToTimeTime);
                parm[i++] = new SqlParameter("@FromActualTime", FromActualTime);
                parm[i++] = new SqlParameter("@ToActualTime", ToActualTime);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("DTMCheckDateandTimeNotBigger", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetDTMEmployeeDepartment()
        {

            DataTable ds = new DataTable();
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intUserId", intCreatedId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetDTMEmployeeDepartment", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strTaskDate, strFromTime, strToTime, strtaskdetails;
        public decimal dblhours;
        public int intSubDepartmentId;
        public string ManagetblDTMProjectTaskEntryDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intProjectId", intprojectId);
                parm[i++] = new SqlParameter("@strTaskDate", strTaskDate);
                parm[i++] = new SqlParameter("@strFromTime", strFromTime);
                parm[i++] = new SqlParameter("@strToTime", strToTime);
                parm[i++] = new SqlParameter("@dblhours", dblhours);
                parm[i++] = new SqlParameter("@strtaskdetails", strtaskdetails);
                parm[i++] = new SqlParameter("@intSubDepartmentId", intSubDepartmentId);
                parm[i++] = new SqlParameter("@CreatedId", intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblDTMProjectTaskEntryDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public string strcurrentStatus, strCurrentStatusDate;
        public string ManageDTMStatusofProject()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strcurrentStatus", strcurrentStatus);
                parm[i++] = new SqlParameter("@intCurrentStatusByid", intCreatedId);
                parm[i++] = new SqlParameter("@strCurrentStatusDate", strCurrentStatusDate);
                parm[i++] = new SqlParameter("@intProjectId", intprojectId);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageDTMStatusofProject", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }
        
        public int intBreakDownID;
        public DataTable GetBDDetailsForReport()
        {

            DataTable ds = new DataTable();
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBDDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetBDAllocationDetailsForReport()
        {

            DataTable ds = new DataTable();
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBDAllocationDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetBDRepairDetailsForRepair()
        {

            DataTable ds = new DataTable();
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBDRepairDetailsForRepair", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetBDRepairSpareDetailsForReport()
        {

            DataTable ds = new DataTable();
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBDRepairSpareDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetBDServiceDetailsForReport()
        {

            DataTable ds = new DataTable();
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBDServiceDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetBranchGroupDetails()
        {

            DataTable ds = new DataTable();
            try
            {


                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBranchGroupDetails");
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string intGroupIds;
        public DataTable GetBranchGroupDetailsForColorChange()
        {

            DataTable ds = new DataTable();
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupIds", intGroupIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBranchGroupDetailsForColorChange", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        // For DTM//
        //23-Nov-2018//
        public int intShiftId;
        public DataTable GetShiftTimeForDTM()
        {

            DataTable ds = new DataTable();
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intShiftId", intShiftId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetShiftTimeForDTM", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intEmployeeId;
        public string strStatus, strLocation, strLocationDetails, strInTime, strOutTime;
        public decimal dblTotalHrs, dblWorkHrs, dblIdleHrs, dblNonPerforHrs, dblShiftHrs, dblDailyInOutHrs;
        public string ManageDTMtblEmployeeTaskEntryHeader()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[15];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                parm[i++] = new SqlParameter("@strTaskDate", strTaskDate);
                parm[i++] = new SqlParameter("@intShiftId", intShiftId);
                parm[i++] = new SqlParameter("@strStatus", strStatus);
                parm[i++] = new SqlParameter("@strLocation", strLocation);
                parm[i++] = new SqlParameter("@strLocationDetails", strLocationDetails);
                parm[i++] = new SqlParameter("@strInTime", strInTime);
                parm[i++] = new SqlParameter("@strOutTime", strOutTime);
                parm[i++] = new SqlParameter("@dblTotalHrs", dblTotalHrs);
                parm[i++] = new SqlParameter("@dblWorkHrs", dblWorkHrs);
                parm[i++] = new SqlParameter("@dblIdleHrs", dblIdleHrs);
                parm[i++] = new SqlParameter("@dblNonPerforHrs", dblNonPerforHrs);
                parm[i++] = new SqlParameter("@dblShiftHrs", dblShiftHrs);
                parm[i++] = new SqlParameter("@dblDailyInOutHrs", dblDailyInOutHrs);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageDTMtblEmployeeTaskEntryHeader", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intEmployeeTaskId;
        public DataTable GettblEmployeeTaskEntryDetailsDetails()
        {

            DataTable ds = new DataTable();
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskId", intEmployeeTaskId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblEmployeeTaskEntryDetailsDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intSrNo, intProject;
        public string strType, strDetails, strTaskStatus;
        public decimal dblHours, dblTaskCompletion;
        public string ManagetblEmployeeTaskEntryDetailsForSave()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskId", intEmployeeTaskId);
                parm[i++] = new SqlParameter("@intSrNo", intSrNo);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intProject", intProject);
                parm[i++] = new SqlParameter("@strFromTime", strFromTime);
                parm[i++] = new SqlParameter("@strToTime", strToTime);
                parm[i++] = new SqlParameter("@dblHours", dblHours);
                parm[i++] = new SqlParameter("@strDetails", strDetails);
                parm[i++] = new SqlParameter("@strTaskStatus", strTaskStatus);
                parm[i++] = new SqlParameter("@dblTaskCompletion", dblTaskCompletion);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblEmployeeTaskEntryDetailsForSave", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public DataTable GetTaskTimeFromCalculate()
        {

            DataTable ds = new DataTable();
            try
            {

                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@FromActualTime", strFromTime);
                parm[i++] = new SqlParameter("@ToActualTime", strToTime);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTaskTimeFromCalculate", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        //--Aswani--//
        //24-Nov-2018//
        public DataTable GettblEmployeeTaskEntryHeaderIdWise()
        {
            DataTable ds = new DataTable();
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskId", intEmployeeTaskId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblEmployeeTaskEntryHeaderIdWise", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string ManagetblEmployeeTaskEntryDetailsForSaveForPendingTask()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskId", intEmployeeTaskId);
                parm[i++] = new SqlParameter("@intSrNo", intSrNo);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intProject", intProject);
                parm[i++] = new SqlParameter("@strFromTime", strFromTime);
                parm[i++] = new SqlParameter("@strToTime", strToTime);
                parm[i++] = new SqlParameter("@dblHours", dblHours);
                parm[i++] = new SqlParameter("@strDetails", strDetails);
                parm[i++] = new SqlParameter("@strTaskStatus", strTaskStatus);
                parm[i++] = new SqlParameter("@dblTaskCompletion", dblTaskCompletion);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblEmployeeTaskEntryDetailsForSaveForPendingTask", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public string ManagetblEmployeeTaskEntryDetailsForSaveForAllocatedTask()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskId", intEmployeeTaskId);
                parm[i++] = new SqlParameter("@intSrNo", intSrNo);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intProject", intProject);
                parm[i++] = new SqlParameter("@strFromTime", strFromTime);
                parm[i++] = new SqlParameter("@strToTime", strToTime);
                parm[i++] = new SqlParameter("@dblHours", dblHours);
                parm[i++] = new SqlParameter("@strDetails", strDetails);
                parm[i++] = new SqlParameter("@strTaskStatus", strTaskStatus);
                parm[i++] = new SqlParameter("@dblTaskCompletion", dblTaskCompletion);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblEmployeeTaskEntryDetailsForSaveForAllocatedTask", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public string ManageDTMtblEmployeeTaskEntryHeader_New()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[15];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                parm[i++] = new SqlParameter("@strTaskDate", strTaskDate);
                parm[i++] = new SqlParameter("@intShiftId", intShiftId);
                parm[i++] = new SqlParameter("@strStatus", strStatus);
                parm[i++] = new SqlParameter("@strLocation", strLocation);
                parm[i++] = new SqlParameter("@strLocationDetails", strLocationDetails);
                parm[i++] = new SqlParameter("@strInTime", strInTime);
                parm[i++] = new SqlParameter("@strOutTime", strOutTime);
                parm[i++] = new SqlParameter("@dblTotalHrs", dblTotalHrs);
                parm[i++] = new SqlParameter("@dblWorkHrs", dblWorkHrs);
                parm[i++] = new SqlParameter("@dblIdleHrs", dblIdleHrs);
                parm[i++] = new SqlParameter("@dblNonPerforHrs", dblNonPerforHrs);
                parm[i++] = new SqlParameter("@dblShiftHrs", dblShiftHrs);
                parm[i++] = new SqlParameter("@dblDailyInOutHrs", dblDailyInOutHrs);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageDTMtblEmployeeTaskEntryHeader_New", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public decimal dblTasktotalCompletion;
        public string ManagetblEmployeeTaskEntryDetailsForSave_New()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[11];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskId", intEmployeeTaskId);
                parm[i++] = new SqlParameter("@intSrNo", intSrNo);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intProject", intProject);
                parm[i++] = new SqlParameter("@strFromTime", strFromTime);
                parm[i++] = new SqlParameter("@strToTime", strToTime);
                parm[i++] = new SqlParameter("@dblHours", dblHours);
                parm[i++] = new SqlParameter("@strDetails", strDetails);
                parm[i++] = new SqlParameter("@strTaskStatus", strTaskStatus);
                parm[i++] = new SqlParameter("@dblTaskCompletion", dblTaskCompletion);
                parm[i++] = new SqlParameter("@dblTasktotalCompletion", dblTasktotalCompletion);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblEmployeeTaskEntryDetailsForSave_New", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public string ManagetblEmployeeTaskEntryDetailsForSaveForPendingTask_New()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskId", intEmployeeTaskId);
                parm[i++] = new SqlParameter("@intSrNo", intSrNo);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intProject", intProject);
                parm[i++] = new SqlParameter("@strFromTime", strFromTime);
                parm[i++] = new SqlParameter("@strToTime", strToTime);
                parm[i++] = new SqlParameter("@dblHours", dblHours);
                parm[i++] = new SqlParameter("@strDetails", strDetails);
                parm[i++] = new SqlParameter("@strTaskStatus", strTaskStatus);
                parm[i++] = new SqlParameter("@dblTaskCompletion", dblTaskCompletion);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblEmployeeTaskEntryDetailsForSaveForPendingTask_New", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public string ManagetblEmployeeTaskEntryDetailsForSaveForAllocatedTask_New()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskId", intEmployeeTaskId);
                parm[i++] = new SqlParameter("@intSrNo", intSrNo);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intProject", intProject);
                parm[i++] = new SqlParameter("@strFromTime", strFromTime);
                parm[i++] = new SqlParameter("@strToTime", strToTime);
                parm[i++] = new SqlParameter("@dblHours", dblHours);
                parm[i++] = new SqlParameter("@strDetails", strDetails);
                parm[i++] = new SqlParameter("@strTaskStatus", strTaskStatus);
                parm[i++] = new SqlParameter("@dblTaskCompletion", dblTaskCompletion);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblEmployeeTaskEntryDetailsForSaveForAllocatedTask_New", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public decimal totalhours;
        public DataTable GetTimeFromHrs()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@FromTime", FromTime);
                parm[i++] = new SqlParameter("@totalhours", totalhours);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTimeFromHrs", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        //Count Total Number Of Task Date Wise And All Task
        public DataTable GetTaskTotalDetailsDateWise_New()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                parm[i++] = new SqlParameter("@FromDate", strFormDate);
                parm[i++] = new SqlParameter("@ToDate", strToDate);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTaskTotalDetailsDateWise_New", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GettblEmployeeTaskEntryHeaderIdWise_New()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskId", intEmployeeTaskId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblEmployeeTaskEntryHeaderIdWise_New", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetShiftTimeForDTM_New()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intShiftId", intShiftId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetShiftTimeForDTM_New", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GettblEmployeeTaskEntryDetailsDetails_New()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskId", intEmployeeTaskId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblEmployeeTaskEntryDetailsDetails_New", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intEmployeeTaskDetails;
        public string ManageUpdatetblEmployeeTaskEntryDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskDetails", intEmployeeTaskDetails);
                parm[i++] = new SqlParameter("@strDetails", strDetails);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageUpdatetblEmployeeTaskEntryDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string currentDate;
        public DataTable GetEmployeeTaskHrsDetails_New()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchID);
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                parm[i++] = new SqlParameter("@ToDate", currentDate);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetEmployeeTaskHrsDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetEmployeeTaskHrsDetailsFix()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchID);
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetEmployeeTaskHrsDetailsFix", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetEmployeeTaskEntryDateDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchID);
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetEmployeeTaskEntryDateDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intProjectID;
        public DataTable GetProjectTaskDetailsView()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intProjectID", intprojectId);
                parm[i++] = new SqlParameter("@strProjectName", strProjectName);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetProjectTaskDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }
        public string ManagetblEmployeeTaskEntryDetailsForSaveForPendingTaskallocated_New()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeTaskId", intEmployeeTaskId);
                parm[i++] = new SqlParameter("@intSrNo", intSrNo);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intProject", intProject);
                parm[i++] = new SqlParameter("@strFromTime", strFromTime);
                parm[i++] = new SqlParameter("@strToTime", strToTime);
                parm[i++] = new SqlParameter("@dblHours", dblHours);
                parm[i++] = new SqlParameter("@strDetails", strDetails);
                parm[i++] = new SqlParameter("@strTaskStatus", strTaskStatus);
                parm[i++] = new SqlParameter("@dblTaskCompletion", dblTaskCompletion);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblEmployeeTaskEntryDetailsForSaveForPendingTaskallocated_New", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }
        public string CheckSequenceNoIsExist11()
        {
            string aReturnValue = string.Empty;

            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@Tasks", "CheckExist");
                parm[i++] = new SqlParameter("@intSquence", intSquence);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCSRGroupMaster", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }
        public DataTable getMaxSequence()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@Tasks", "CheckExist");
                parm[i++] = new SqlParameter("@intSquence", intSquence);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblCSRGroupMaster", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string CheckSequenceNoIsExist111()
        {
            string aReturnValue = string.Empty;

            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupDetailsId", intGroupDetailsId);
                parm[i++] = new SqlParameter("@Tasks", "CheckExist");
                parm[i++] = new SqlParameter("@intSquence", intSquence);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCSRGroupDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }
        public DataTable getMaxSequence1()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@Tasks", "getmaxNumber");
                //parm[i++] = new SqlParameter("@intSequence", intSequence);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblCSRGroupDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string checkSequenceNoIsExistssubsubgroup()
        {
            string aReturnValue = string.Empty;

            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intSubgroupDetailsId", intSubgroupDetailsId);
                parm[i++] = new SqlParameter("@Tasks", "CheckExist");
                parm[i++] = new SqlParameter("@intsequence", intSquence);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("MANAGEtblCSRSubGroupDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }
        public DataTable getMaxSequencesubsubgroup()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@intGroupDetailsId", intGroupDetailsId);
                parm[i++] = new SqlParameter("@Tasks", "getmaxNumber");
                //parm[i++] = new SqlParameter("@intSequence", intSequence);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("MANAGEtblCSRSubGroupDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intDTMIntimeMasterId;
        public DataTable GetDefaultShift()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intDTMIntimeMasterId", intDTMIntimeMasterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetDefaultShift", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable aGetProjectType()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intProjectTypeMaster", intProjectTypeMaster);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblDTMProjectTypeMaster", parm);
            }
            catch (Exception EXP)
            {
                dt = null;
                ExceptionHandler.LogError(EXP);
            }
            return dt;
        }

        public DataTable aGetCSRGroup()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("GetDeletetblCSRGroupMaster", parm);
            }
            catch (Exception EXP)
            {
                dt = null;
                ExceptionHandler.LogError(EXP);
            }
            return dt;
        }

        public DataTable aGetCSRGroupDetl()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@intGroupDetailsId", intGroupDetailsId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblCSRGroupDetails", parm);
            }
            catch (Exception EXP)
            {
                dt = null;
                ExceptionHandler.LogError(EXP);
            }
            return dt;
        }

        public DataTable aGetCSRSubGroupDetl()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intSubgroupDetailsId", intSubgroupDetailsId);
                parm[i++] = new SqlParameter("@intGroupDetailsId", intGroupDetailsId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("MANAGEtblCSRSubGroupDetails", parm);
            }
            catch (Exception EXP)
            {
                dt = null;
                ExceptionHandler.LogError(EXP);
            }
            return dt;
        }

        public DataTable aGetManageTaskDetails()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                parm[i++] = new SqlParameter("@intPTDId", intPTDId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("MANAGEtblDTMProjecttaskDetails", parm);
            }
            catch (Exception EXP)
            {
                dt = null;
                ExceptionHandler.LogError(EXP);
            }
            return dt;
        }

        public DataTable aGetManageTaskSubDetails()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intPTSID", intPTSID);
                parm[i++] = new SqlParameter("@intPTDId", intPTDId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblDTMProjectSubTaskDetails", parm);
            }
            catch (Exception EXP)
            {
                dt = null;
                ExceptionHandler.LogError(EXP);
            }
            return dt;
        }

        public DataTable aGetManageDTMDeptDetails()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intprojectId", intprojectId);
                parm[i++] = new SqlParameter("@intPdId", intPdId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblDTMProjectDepartmentDetails", parm);
            }
            catch (Exception EXP)
            {
                dt = null;
                ExceptionHandler.LogError(EXP);
            }
            return dt;
        }

        public DataTable aGetDTMProjectTaskEntry()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@intDepartmentId", intDepartmentId);
                parm[i++] = new SqlParameter("@Status", strStatus);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("GetDTMProjectDetailsForEntry", parm);
            }
            catch (Exception EXP)
            {
                dt = null;
                ExceptionHandler.LogError(EXP);
            }
            return dt;
        }

        public DataTable aGetManageDTMDeptWiseProjectDetails()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intProjectId", intProjectId);
                parm[i++] = new SqlParameter("@intDepartmentId", intDepartmentId);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("GetDTMTaskDetailsDepartmentwise", parm);
            }
            catch (Exception EXP)
            {
                dt = null;
                ExceptionHandler.LogError(EXP);
            }
            return dt;
        }

        public DataTable aGetManageTaskDateWise()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@ToDate", strToDate);
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("GetEmployeeTaskDetails", parm);
            }
            catch (Exception EXP)
            {
                dt = null;
                ExceptionHandler.LogError(EXP);
            }
            return dt;
        }

        public DataTable aGetManageTaskWise()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@ToDate", strToDate);
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("GetEmployeeTaskDetails_TaskWise", parm);
            }
            catch (Exception EXP)
            {
                dt = null;
                ExceptionHandler.LogError(EXP);
            }
            return dt;
        }
    }
}
