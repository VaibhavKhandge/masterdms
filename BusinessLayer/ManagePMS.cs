﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using DataLayer;
using System.Web;

namespace BusinessLayer
{
    public class ManagePMS
    {
        public int intCalibrationParameterId, ccid, intFyId, intMaterialinwardId, intFYId, DepartmentId, EmployeeId, WorkStationId;
        public string FromDate, Todate, strStatus, EquipmentCategory, strBDType;
        public DataTable GetTblCalibrationParameterMasterDetails()
        {
            DataTable ds = new DataTable();
            try
            {    
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intCalibrationParameterId", intCalibrationParameterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTblCalibrationParameterMasterDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strCalibrationParameterName, strCalibrationParameterDesc, strCalibrationParameterShortName;
        public string ManageTblCalibrationParameterMaster()
        {
            string aReturnValue = string.Empty;
              string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
            try
            {
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intCompanyId",intCompanyId);
                parm[i++] = new SqlParameter("@intCalibrationParameterId",intCalibrationParameterId);
                parm[i++] = new SqlParameter("@strCalibrationParameterName",strCalibrationParameterName);
                parm[i++] = new SqlParameter("@strCalibrationParameterDesc",strCalibrationParameterDesc);
                parm[i++] = new SqlParameter("@strCalibrationParameterShortName", strCalibrationParameterShortName);

                if (strCalibrationParameterShortName == null || strCalibrationParameterShortName == "")
                {
                    parm[i++] = new SqlParameter("@Operation", "ExeNull");
                    aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageTblCalibrationParameterMaster", parm);
                }
                else
                {
                    parm[i++] = new SqlParameter("@Operation", "NotExeNull");
                    aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageTblCalibrationParameterMaster", parm);
                }
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string DeleteTblCalibrationParameterMaster()
        {
            string aReturnValue = string.Empty;            
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intCalibrationParameterId", intCalibrationParameterId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ssp_DeleteTblCalibrationParameterMasterflag", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string DeleteFeulConsumption()
        {
            string astatus = string.Empty;
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                int i = 0;
                param[i++] = new SqlParameter("@intFuelConsumtionId",ccid);
                astatus = DataLayer.DataLinker.ExecuteSPScalar("ssp_DeleteTblFeulConsumption", param);
            }
            catch(Exception exp)
            {
                astatus = "";
                ExceptionHandler.LogError(exp);
            }
            return astatus;
        }
        /////////////////////////////

        //10-Mar-2019
        public int intEnergyMeterId, intOpeningReading, intBranchId, intInchargeId, intSubDepartmentId;
        public string strMeterNo, strDescription, strConsumerNo, strMeterType, strConsumerName, strDateofInstallation, strUsedFor, strAllocationDetails;
        public decimal dblMultiplyingFactor, dblRate;
        public string ManageTblEnergyMeterMaster()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[16];
                int i = 0;
                parm[i++] = new SqlParameter("@intEnergyMeterId",intEnergyMeterId);
                parm[i++] = new SqlParameter("@strMeterNo",strMeterNo);
                parm[i++] = new SqlParameter("@strDescription",strDescription);
                parm[i++] = new SqlParameter("@strConsumerNo",strConsumerNo);
                parm[i++] = new SqlParameter("@strMeterType",strMeterType);
                parm[i++] = new SqlParameter("@strConsumerName",strConsumerName);
                parm[i++] = new SqlParameter("@strDateofInstallation",strDateofInstallation);
                parm[i++] = new SqlParameter("@intOpeningReading",intOpeningReading);
                parm[i++] = new SqlParameter("@dblMultiplyingFactor",dblMultiplyingFactor);
                parm[i++] = new SqlParameter("@dblRate",dblRate);
                parm[i++] = new SqlParameter("@strUsedFor",strUsedFor);
                parm[i++] = new SqlParameter("@intBranchId",intBranchId);
                parm[i++] = new SqlParameter("@strAllocationDetails",strAllocationDetails);
                parm[i++] = new SqlParameter("@intInchargeId",intInchargeId);
                parm[i++] = new SqlParameter("@intSubDepartmentId", intSubDepartmentId);
                parm[i++] = new SqlParameter("@intCreatedByID",intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageTblEnergyMeterMaster", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetAllTblEnergyMeterMaster()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intEnergyMeterId", intEnergyMeterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetAllTblEnergyMeterMaster", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intItemId;
        public DataTable GetFuelRateandUOM()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intItemId", intItemId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetFuelRateandUOM", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intFuelConsumtionId, intFuelId, intEquipmentId, intUOMId;
        public string strDate, strFuelName, strVoucherNumber, strVoucherDate, strDetails, strRemark;
        public decimal dblQty, dblAmount;
        public string ManagetblFuelConsumptionDetail()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[16];
                int i = 0;
                parm[i++] = new SqlParameter("@intFuelConsumtionId", intFuelConsumtionId);
                parm[i++] = new SqlParameter("@intFyId",intFyId );
                parm[i++] = new SqlParameter("@intBranchId",intBranchId );
                parm[i++] = new SqlParameter("@strDate",strDate );
                parm[i++] = new SqlParameter("@intFuelId", intFuelId);
                parm[i++] = new SqlParameter("@strFuelName",strFuelName );
                parm[i++] = new SqlParameter("@intEquipmentId", intEquipmentId);
                parm[i++] = new SqlParameter("@dblQty", dblQty);
                parm[i++] = new SqlParameter("@intUOMId", intUOMId);
                parm[i++] = new SqlParameter("@dblRate", dblRate);
                parm[i++] = new SqlParameter("@dblAmount", dblAmount);
                parm[i++] = new SqlParameter("@strVoucherNumber", strVoucherNumber);
                parm[i++] = new SqlParameter("@strVoucherDate",strVoucherDate );
                parm[i++] = new SqlParameter("@strDetails",strDetails );
                parm[i++] = new SqlParameter("@strRemark",strRemark );
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);

                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblFuelConsumptionDetail", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GettblFuelConsumptionDetail()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intFuelConsumtionId", intFuelConsumtionId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblFuelConsumptionDetail", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable ManageGetEnergyMeterDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intEnergyMeterId", intEnergyMeterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManageGetEnergyMeterDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intEnergyMeterReadingId;
        public decimal intOpeningReadings, intReading, intUnits, dblMultiplyingFactors, dblCalculatedUnit, dblAdditionalCharges, dblTotalAmount;
        public string ManageTblEnergyMeterReadingDetail()
        {
            string aReturnValue = string.Empty;
            try
            {
               string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);                
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[17];
                int i = 0;
                parm[i++] = new SqlParameter("@intEnergyMeterReadingId", intEnergyMeterReadingId);
                parm[i++] = new SqlParameter("@intCompanyId",intCompanyId ); 
                parm[i++] = new SqlParameter("@intBranchId",intBranchId ); 
                parm[i++] = new SqlParameter("@intFyid", intFyId); 
                parm[i++] = new SqlParameter("@intEnergyMeterId",intEnergyMeterId ); 
                parm[i++] = new SqlParameter("@strDate",strDate ); 
                parm[i++] = new SqlParameter("@intOpeningReading",intOpeningReadings ); 
                parm[i++] = new SqlParameter("@intReading",intReading ); 
                parm[i++] = new SqlParameter("@intUnits",intUnits ); 
                parm[i++] = new SqlParameter("@dblMultiplyingFactor",dblMultiplyingFactors ); 
                parm[i++] = new SqlParameter("@dblCalculatedUnit",dblCalculatedUnit ); 
                parm[i++] = new SqlParameter("@dblRate", dblRate); 
                parm[i++] = new SqlParameter("@dblAmount",dblAmount ); 
                parm[i++] = new SqlParameter("@dblAdditionalCharges",dblAdditionalCharges ); 
                parm[i++] = new SqlParameter("@dblTotalAmount", dblTotalAmount);
                parm[i++] = new SqlParameter("@strRemark", strRemark); 
                parm[i++] = new SqlParameter("@intPreparedById", intCreatedId); 
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageTblEnergyMeterReadingDetail", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetDetailsTblEnergyMeterReadingDetail()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intEnergyMeterReadingId", intEnergyMeterReadingId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetDetailsTblEnergyMeterReadingDetail", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intMachineId;
        public DataTable GetAllTblMachineDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intMachineId", intMachineId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetAllTblMachineDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetBDAllocationDetailsReport_MHC()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intMachineId", intMachineId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBDAllocationDetailsReport_MHC", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetSpareListDetailsReport_MHC()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intMachineId", intMachineId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetSpareListDetailsReport_MHC", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string checkduplicate()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@strCalibrationParameterName", strCalibrationParameterName);
                parm[i++] = new SqlParameter("@strCalibrationParameterShortName", strCalibrationParameterShortName);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("cheakduplicatecheckpointname", parm);
            }
            catch (Exception ex)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(ex);
            }
            return aReturnValue;
        }

        public DataTable GetEnergyMeterMaster()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intEnergyMeterId", intEnergyMeterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetAllTblEnergyMeterMaster", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable FuelConsumptionDetailsView()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intFuelConsumtionId", intFuelConsumtionId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblFuelConsumptionDetail", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable MaterialInwardView()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@intMaterialinwardId", intMaterialinwardId);                
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblMaterialInward", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable BreakDownGeneral_Equipment()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFYId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@EmployeeId", EmployeeId);
                parm[i++] = new SqlParameter("@DepartmentId", DepartmentId);
                parm[i++] = new SqlParameter("@WorkStationId", WorkStationId);
                parm[i++] = new SqlParameter("@EquipmentCategory", EquipmentCategory);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetEquipmentDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable BreakDownGeneral_History()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFYId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@EmployeeId", EmployeeId);
                parm[i++] = new SqlParameter("@DepartmentId", DepartmentId);
                parm[i++] = new SqlParameter("@WorkStationId", WorkStationId);
                parm[i++] = new SqlParameter("@Status", strStatus);
                parm[i++] = new SqlParameter("@strBDType", strBDType);
                parm[i++] = new SqlParameter("@EquipmentCategory", EquipmentCategory);                
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBreakDownDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable BreakDownGeneral_PMHistory()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFYId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@EmployeeId", EmployeeId);
                parm[i++] = new SqlParameter("@DepartmentId", DepartmentId);
                parm[i++] = new SqlParameter("@WorkStationId", WorkStationId);
                parm[i++] = new SqlParameter("@Status", strStatus);
                parm[i++] = new SqlParameter("@strBDType", strBDType);
                parm[i++] = new SqlParameter("@EquipmentCategory", EquipmentCategory);  
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBreakDownDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable CheckPointView()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intCalibrationParameterId", intCalibrationParameterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTblCalibrationParameterMasterDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }
    }
}
