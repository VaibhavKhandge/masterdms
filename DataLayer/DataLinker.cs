﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;
using System.Data.OleDb;
using Microsoft.VisualBasic;

namespace DataLayer
{
    public class DataLinker
    {
        public static string sqlConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"].ToString();
        public static string regApplicationName = "";
        public static string regSuperAdminPassword = "";
        public static string regNoOfUser = "";
        public static string regValidity = "";
        public static string regNoOfBranches = "";
        public static string regCompanyName = "";
        public static string regCompanyShortName = "";
        public static string regNoOfModule = "";
        public static string regLicenceNo = "";
        public static string regUseFlag = "";

        static int indx = 0;
        public string tempscript;

        #region [GETDS()]
        public DataSet GetDs(string SP_Name)
        {
            DataSet ds;
            try
            {
                ds = SqlHelper.ExecuteDataset(sqlConnectionString, CommandType.StoredProcedure, SP_Name);
            }
            catch (Exception)
            {
                throw;
            }
            return ds;
        }
        #endregion

        #region [GETDS()]
        public DataSet GetDs(string SP_Name, object[] param)
        {
            DataSet ds;
            try
            {
                ds = SqlHelper.ExecuteDataset(sqlConnectionString, SP_Name, param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataSet GetDs(string SP_Name, string formname, string shortname)
        {
            DataSet ds;
            try
            {
                ds = SqlHelper.ExecuteDataset(sqlConnectionString, SP_Name, formname, shortname);
            }
            catch (Exception ex)
            {
                throw;
            }
            return ds;
        }


        public void GetDs1(string SP_Name, object[] param)
        {
            DataSet ds;
            try
            {
                ds = FillDS("ssp_GetDisplayFINISHEDGOODS", "0"); ds.Clear();
                OleDbConnection oconn = new OleDbConnection
            ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + SP_Name + "; Extended Properties=\"Excel 8.0;HDR=YES\";");//OledbConnection and 
                // connectionstring to connect to the Excel Sheet
                //After connecting to the Excel sheet here we are selecting the data 
                //using select statement from the Excel sheet
                OleDbCommand ocmd = new OleDbCommand("select * from [Sheet1$]", oconn);
                oconn.Open();
                //Here [Sheet1$] is the name of the sheet 
                //in the Excel file where the data is present
                OleDbDataReader odr = ocmd.ExecuteReader();
                while (odr.Read())
                {
                    //Here using this method we are inserting the data into the database
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["ItemCode"] = odr[0].ToString();
                    ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["strDrawingname"] = odr[1].ToString();
                    ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["Flag"] = odr[2].ToString();
                }
                oconn.Close();
                ds.Tables[0].TableName = "ssp_UpdateFINISHEDGOODS";
                SaveForBranch(ds);

            }
            catch (Exception ex)
            {
                throw;
            }
            //return ds;
        }
        #endregion

        # region [FillDS()]
        public DataSet FillDS(string SP_Name, string SearchField)
        {
            try
            {
                string strCommand = SP_Name + " '" + SearchField + "'";
                return SqlHelper.ExecuteDataset(sqlConnectionString, CommandType.Text, strCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [DELETE()]
        public int DeleteData(DataView ds, string SPName)
        {
            int errorCode = 0;
            SqlParameter[] Param = new SqlParameter[4];
            try
            {
                DataTable dt = ds.ToTable();

                Param[0] = new SqlParameter("@" + dt.Columns[0].ColumnName, dt.Columns[0].DataType);

                Param[0].Value = dt.Rows[0][0].ToString();
                Param[1] = new SqlParameter("@ttmstamp", SqlDbType.Timestamp);
                Param[1].Value = dt.Rows[0]["ttmStamp"];
                Param[2] = new SqlParameter("@errorCode", SqlDbType.Int);
                Param[2].Value = 0;
                Param[2].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(sqlConnectionString, CommandType.StoredProcedure, SPName, Param);
                errorCode = int.Parse(Param[2].Value.ToString());
            }
            catch (Exception)
            {
                throw;
            }
            return errorCode;
        }
        #endregion

        #region [NEWDELETE()]

        public int Delete(string ID_Field, int ID, string SP_DeleteDetails)
        {
            int errorCode = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();//sql connection needs to be open before we can add any transction to it.
                    using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                    {
                        try
                        {
                            SqlParameter[] DeleteParam = new SqlParameter[3];
                            DeleteParam[0] = new SqlParameter("@" + ID_Field, SqlDbType.Int);
                            DeleteParam[0].Value = ID;
                            DeleteParam[1] = new SqlParameter("@HeaderFlag", SqlDbType.Bit);
                            DeleteParam[1].Value = 1;
                            DeleteParam[2] = new SqlParameter("@errorCode", SqlDbType.Int);
                            DeleteParam[2].Value = 0;
                            DeleteParam[2].Direction = ParameterDirection.Output;
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                            errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 1].Value));
                        }
                        catch (Exception)
                        {
                            sqlTrans.Rollback();
                            return 0;
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return errorCode;
        }

        public int Delete(string ID_Field, string ID, string SP_DeleteDetails)
        {
            int errorCode = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();//sql connection needs to be open before we can add any transction to it.
                    using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                    {
                        try
                        {
                            SqlParameter[] DeleteParam = new SqlParameter[3];
                            DeleteParam[0] = new SqlParameter("@" + ID_Field, SqlDbType.VarChar, ID.ToString().Length);
                            DeleteParam[0].Value = ID;
                            DeleteParam[1] = new SqlParameter("@HeaderFlag", SqlDbType.Bit);
                            DeleteParam[1].Value = 1;
                            DeleteParam[2] = new SqlParameter("@errorCode", SqlDbType.Int);
                            DeleteParam[2].Value = 0;
                            DeleteParam[2].Direction = ParameterDirection.Output;
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                            errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 1].Value));
                        }
                        catch (Exception)
                        {
                            sqlTrans.Rollback();
                            return 0;
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return errorCode;
        }

        public int DeleteCA(string[] ID_Field, int[] ID, string SP_DeleteDetails)
        {
            int errorCode = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();//sql connection needs to be open before we can add any transction to it.
                    using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                    {
                        try
                        {
                            SqlParameter[] DeleteParam = new SqlParameter[4];
                            DeleteParam[0] = new SqlParameter("@" + ID_Field[0], SqlDbType.Int);
                            DeleteParam[0].Value = ID[0];
                            DeleteParam[1] = new SqlParameter("@" + ID_Field[1], SqlDbType.Int);
                            DeleteParam[1].Value = ID[1];
                            DeleteParam[2] = new SqlParameter("@HeaderFlag", SqlDbType.Bit);
                            DeleteParam[2].Value = 1;
                            DeleteParam[3] = new SqlParameter("@errorCode", SqlDbType.Int);
                            DeleteParam[3].Value = 0;
                            DeleteParam[3].Direction = ParameterDirection.Output;
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                            errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 1].Value));
                        }
                        catch (Exception)
                        {
                            sqlTrans.Rollback();
                            return 0;
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return errorCode;
        }

        public int Delete(string[] ID_Field, int[] ID, string SP_DeleteDetails)
        {
            int errorCode = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();//sql connection needs to be open before we can add any transction to it.
                    using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                    {
                        try
                        {
                            SqlParameter[] DeleteParam = new SqlParameter[3];
                            DeleteParam[0] = new SqlParameter("@" + ID_Field[0], SqlDbType.Int);
                            DeleteParam[0].Value = ID[0];
                            DeleteParam[1] = new SqlParameter("@" + ID_Field[1], SqlDbType.Int);
                            DeleteParam[1].Value = ID[1];
                            DeleteParam[2] = new SqlParameter("@errorCode", SqlDbType.Int);
                            DeleteParam[2].Value = 0;
                            DeleteParam[2].Direction = ParameterDirection.Output;
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                            errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 1].Value));
                        }
                        catch (Exception)
                        {
                            sqlTrans.Rollback();
                            return 0;
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return errorCode;
        }

        public int Delete(string ID_Field, int ID, string SP_DeleteDetails, bool blRevision)
        {
            int errorCode = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();//sql connection needs to be open before we can add any transction to it.
                    using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                    {
                        try
                        {
                            SqlParameter[] DeleteParam = new SqlParameter[4];
                            DeleteParam[0] = new SqlParameter("@" + ID_Field, SqlDbType.Int);
                            DeleteParam[0].Value = ID;
                            DeleteParam[1] = new SqlParameter("@HeaderFlag", SqlDbType.Bit);
                            DeleteParam[1].Value = 1;
                            DeleteParam[2] = new SqlParameter("@errorCode", SqlDbType.Int);
                            DeleteParam[2].Value = 0;
                            DeleteParam[2].Direction = ParameterDirection.Output;
                            DeleteParam[3] = new SqlParameter("@Revision", SqlDbType.VarChar);
                            DeleteParam[3].Value = false;
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                            errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 2].Value));
                        }
                        catch (Exception)
                        {
                            sqlTrans.Rollback();
                            return 0;
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return errorCode;
        }
        #endregion

        #region [SAVE]
        public string[] SaveData(DataSet ds, String SP_Name)
        {
            string[] errorCode = new string[3];
            try
            {
                SqlParameter[] Param = new SqlParameter[ds.Tables[0].Columns.Count + 3];
                Param = CreateSQLParameters(ds);
                SqlHelper.ExecuteNonQuery(sqlConnectionString, CommandType.StoredProcedure, SP_Name, Param);
                errorCode[0] = Param[indx].Value.ToString();
                errorCode[1] = Param[indx + 1].Value.ToString();
                errorCode[2] = Param[indx + 2].Value.ToString();
            }
            catch (Exception)
            {
                errorCode[0] = "10";
                throw;
            }
            return errorCode;
        }
        #endregion

        #region [ SAVEAllData ]

        public int SaveAllData(DataSet ds, SqlParameter[] DeleteParam, String[] SPName)
        {
            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                con.Open();//sql connection needs to be open before we can add any transction to it.
                using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                {
                    int errorCode = 0;
                    try
                    {
                        if (DeleteParam != null)
                        {
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, Convert.ToString(SPName[0]), DeleteParam);
                            errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 1].Value));
                            if (errorCode != 1)
                            {
                                sqlTrans.Rollback();
                                return errorCode;
                            }
                        }
                        for (int tblIndx = 0; tblIndx < ds.Tables.Count; tblIndx++)
                        {
                            for (int i = 0; i < ds.Tables[tblIndx].Rows.Count; i++)
                            {
                                SqlParameter[] Param = new SqlParameter[ds.Tables[tblIndx].Columns.Count + 1];
                                Param = CreateAllSQLParameters(ds.Tables[tblIndx], i, i);
                                SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SPName[tblIndx + 1].ToString(), Param);
                                errorCode = int.Parse(Param[indx].Value.ToString());
                                if (errorCode != 1 && errorCode != 2)
                                {
                                    sqlTrans.Rollback();
                                    return errorCode;
                                }
                            }
                        }
                        sqlTrans.Commit();
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                    return errorCode;
                }
            }
        }
        #endregion

        #region Create All SQL Parameter
        public SqlParameter[] CreateAllSQLParameters(DataTable dt, int tblIndex, int i)
        {
            try
            {
                // int errorCode = 0;
                SqlParameter[] Param;
                int j = i;
                if (tblIndex == 0)
                {
                    Param = new SqlParameter[dt.Columns.Count + 3];
                }
                else
                {
                    Param = new SqlParameter[dt.Columns.Count + 1];
                }
                try
                {
                    for (indx = 0; indx <= dt.Columns.Count - 1; indx++)
                    {
                        if (String.Compare(dt.Columns[indx].ColumnName.ToUpper(), "ttmStamp".ToUpper()) == 0)
                        {
                            Param[indx] = new SqlParameter("@" + dt.Columns[indx].ColumnName, SqlDbType.Timestamp);
                        }
                        else
                            Param[indx] = new SqlParameter("@" + dt.Columns[indx].ColumnName, dt.Columns[indx].DataType);
                        if (String.Compare(dt.Columns[indx].ColumnName.ToUpper(), "ttmStamp".ToUpper()) == 0)
                        {
                            if (dt.Rows[j]["ttmStamp"].ToString() == "")
                                Param[indx].Value = null;
                            else
                                Param[indx].Value = dt.Rows[j][indx];
                        }
                        else
                            Param[indx].Value = dt.Rows[j][indx];

                    }
                    Param[indx] = new SqlParameter("@errorCode", SqlDbType.Int);
                    Param[indx].Value = 0;
                    Param[indx].Direction = ParameterDirection.Output;
                    if (tblIndex == 0)
                    {
                        Param[indx + 1] = new SqlParameter("@OutPutID", SqlDbType.VarChar, 10);
                        Param[indx + 1].Value = "0";
                        Param[indx + 1].Direction = ParameterDirection.Output;
                        Param[indx + 2] = new SqlParameter("@OutPutCode", SqlDbType.VarChar, 25);
                        Param[indx + 2].Value = "0";
                        Param[indx + 2].Direction = ParameterDirection.Output;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                return Param;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SqlParameter[] CreateSQLParameters(DataSet ds)
        {
            try
            {
                int errorCode = 0;
                SqlParameter[] Param = new SqlParameter[ds.Tables[0].Columns.Count + 3];
                try
                {
                    for (indx = 0; indx <= ds.Tables[0].Columns.Count - 1; indx++)
                    {
                        if (String.Compare(ds.Tables[0].Columns[indx].ColumnName, "ttmStamp") == 0)
                        {
                            Param[indx] = new SqlParameter("@" + ds.Tables[0].Columns[indx].ColumnName, SqlDbType.Timestamp);
                        }
                        else
                            Param[indx] = new SqlParameter("@" + ds.Tables[0].Columns[indx].ColumnName, ds.Tables[0].Columns[indx].DataType);
                        if (String.Compare(ds.Tables[0].Columns[indx].ColumnName, "ttmStamp") == 0)
                        {
                            if (ds.Tables[0].Rows[0]["ttmStamp"].ToString() == "")
                                Param[indx].Value = null;
                            else
                                Param[indx].Value = ds.Tables[0].Rows[0][indx];
                        }
                        else
                            Param[indx].Value = ds.Tables[0].Rows[0][indx];

                    }
                    Param[indx] = new SqlParameter("@errorCode", SqlDbType.VarChar, 10);
                    Param[indx].Value = "0";
                    Param[indx].Direction = ParameterDirection.Output;
                    Param[indx + 1] = new SqlParameter("@OutPutID", SqlDbType.VarChar, 10);
                    Param[indx + 1].Value = "0";
                    Param[indx + 1].Direction = ParameterDirection.Output;
                    Param[indx + 2] = new SqlParameter("@OutPutCode", SqlDbType.VarChar, 25);
                    Param[indx + 2].Value = "0";
                    Param[indx + 2].Direction = ParameterDirection.Output;
                }
                catch (Exception)
                {
                    throw;
                }
                return Param;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [GETCODE()]
        public string[] GetCode(string Form_Name, string Form_Type, string Getcode_SP_Name, string Branch_Code, int FY_ID)
        {
            try
            {
                string[] Getcode = new string[2];

                SqlParameter[] Param = new SqlParameter[4];
                Param[0] = new SqlParameter("@FormName", SqlDbType.VarChar, 20);
                Param[0].Value = Form_Name;
                Param[1] = new SqlParameter("@FormType", SqlDbType.VarChar, 20);
                Param[1].Value = Form_Type;
                Param[2] = new SqlParameter("@BranchCode", SqlDbType.VarChar, 20);
                Param[2].Value = Form_Type;
                Param[3] = new SqlParameter("@FYID", SqlDbType.Int);
                Param[3].Value = Form_Type;
                Param[4] = new SqlParameter("@Getcode", SqlDbType.VarChar, 20);
                Param[4].Value = "0";
                Param[5].Direction = ParameterDirection.Output;
                Param[5] = new SqlParameter("@Getcode1", SqlDbType.VarChar, 20);
                Param[6].Value = "0";
                Param[6].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(sqlConnectionString, CommandType.StoredProcedure, Getcode_SP_Name, Param);
                Getcode[0] = Param[5].Value.ToString();
                Getcode[1] = Param[6].Value.ToString();
                return Getcode;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [DataSet Table Matching()]
        public DataSet DSTableMatching(string TblName)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(sqlConnectionString, CommandType.StoredProcedure, "ssp_GetDocDataBase_From_TblCompanyMaster");

                if (TblName == "iBizSolnDOC..TblEnclosure")
                {
                    //return SqlHelper.ExecuteDataset(sqlConnectionString, CommandType.Text, "Exec iBizSolnDOC..sp_Columns TblEnclosure");
                    return SqlHelper.ExecuteDataset(sqlConnectionString, CommandType.Text, "Exec " + ds.Tables[0].Rows[0]["strDocDataBase"].ToString() + "..sp_Columns TblEnclosure");
                }
                else
                    return SqlHelper.ExecuteDataset(sqlConnectionString, CommandType.Text, "Exec sp_Columns '" + TblName + "'");
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [ SaveDetail() ]
        public int SaveDetail(ref DataSet ds)
        {
            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                con.Open();//sql connection needs to be open before we can add any transction to it.
                using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                {
                    int errorCode = 0;
                    try
                    {
                        for (int tblIndx = 0; tblIndx < ds.Tables.Count; tblIndx++)
                        {
                            for (int i = 0; i < ds.Tables[tblIndx].Rows.Count; i++)
                            {
                                SqlParameter[] Param = new SqlParameter[ds.Tables[tblIndx].Columns.Count + 1];
                                Param = CreateAllSQLParameters(ds.Tables[tblIndx], i, i);
                                SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, ds.Tables[tblIndx].TableName.ToString().Trim(), Param);
                                errorCode = int.Parse(Param[indx].Value.ToString());
                                if (errorCode != 1 && errorCode != 2)
                                {
                                    sqlTrans.Rollback();
                                    return errorCode;
                                }
                            }
                        }
                        sqlTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        sqlTrans.Rollback();
                        //LogException(ex);
                    }
                    finally
                    {
                        con.Close();
                    }
                    return errorCode;
                }
            }
        }
        #endregion

        #region [Update()]
        public int SavePlanning(DataSet dsHeader, int Id, string ID_Field, string SP_DeleteDetails)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();//sql connection needs to be open before we can add any transction to it.
                    using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                    {
                        int errorCode = 0;
                        for (int i = 0; i < dsHeader.Tables[0].Rows.Count; i++)
                        {
                            SqlParameter[] DeleteParam = new SqlParameter[3];
                            DeleteParam[0] = new SqlParameter("@intOperationTypeID1", SqlDbType.Int);
                            DeleteParam[0].Value = dsHeader.Tables[0].Rows[i]["intOperationTypeID"].ToString();
                            DeleteParam[1] = new SqlParameter("@strStartDate1", SqlDbType.VarChar);
                            DeleteParam[1].Value = dsHeader.Tables[0].Rows[i]["strStartDate"].ToString();
                            DeleteParam[2] = new SqlParameter("@intItemID1", SqlDbType.Int);
                            DeleteParam[2].Value = dsHeader.Tables[0].Rows[i]["intItemID"].ToString();
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                        }
                        for (int i = 0; i < dsHeader.Tables[0].Rows.Count; i++)
                        {
                            SqlParameter[] Param;

                            Param = new SqlParameter[dsHeader.Tables[0].Columns.Count + 1];
                            Param = CreateAllSQLParameters(dsHeader.Tables[0], 1, i);
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[0].TableName.ToString(), Param);
                            errorCode = int.Parse(Param[indx].Value.ToString());
                            if (errorCode != 1 && errorCode != 2)
                            {
                                sqlTrans.Rollback();
                                return errorCode;
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Save(DataSet dsHeader, int Id, string ID_Field, string SP_DeleteDetails)
        {

            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                con.Open();//sql connection needs to be open before we can add any transction to it.
                using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                {
                    #region TRY BLOCK
                    try
                    {
                        int errorCode = 0;
                        SqlParameter[] DeleteParam = new SqlParameter[3];
                        DeleteParam[0] = new SqlParameter("@" + ID_Field, SqlDbType.Int);
                        DeleteParam[0].Value = Id;
                        DeleteParam[1] = new SqlParameter("@HeaderFlag", SqlDbType.Bit);
                        DeleteParam[1].Value = 0;
                        DeleteParam[2] = new SqlParameter("@errorCode", SqlDbType.Int);
                        DeleteParam[2].Value = 0;
                        DeleteParam[2].Direction = ParameterDirection.Output;
                        SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                        errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 1].Value));
                        if (errorCode != 1)
                        {
                            sqlTrans.Rollback();
                            return errorCode;
                        }

                        for (int tblIndx = 0; tblIndx < dsHeader.Tables.Count; tblIndx++)
                        {
                            for (int i = 0; i < dsHeader.Tables[tblIndx].Rows.Count; i++)
                            {
                                SqlParameter[] Param;

                                if (tblIndx == 0)
                                {
                                    string[] error = new string[3];

                                    Param = new SqlParameter[dsHeader.Tables[tblIndx].Columns.Count + 3];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[tblIndx], i, i);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[tblIndx].TableName.ToString(), Param);
                                    error[0] = Param[indx].Value.ToString();
                                    error[1] = Param[indx + 1].Value.ToString();
                                    error[2] = Param[indx + 2].Value.ToString();

                                    if (Convert.ToInt32(error[0].ToString()) != 1 && Convert.ToInt32(error[0].ToString()) != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return Convert.ToInt32(error[0].ToString());
                                    }
                                }
                                else
                                {
                                    Param = new SqlParameter[dsHeader.Tables[tblIndx].Columns.Count + 1];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[tblIndx], tblIndx, i);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[tblIndx].TableName.ToString(), Param);
                                    errorCode = int.Parse(Param[indx].Value.ToString());
                                    if (errorCode != 1 && errorCode != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return errorCode;
                                    }
                                }
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                    #endregion
                }
            }
        }



        public int Save(DataSet dsHeader, string Id, string ID_Field, string SP_DeleteDetails)
        {

            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                con.Open();//sql connection needs to be open before we can add any transction to it.
                using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                {
                    try
                    {
                        int errorCode = 0;
                        SqlParameter[] DeleteParam = new SqlParameter[3];
                        DeleteParam[0] = new SqlParameter("@" + ID_Field, SqlDbType.Text);
                        DeleteParam[0].Value = Id;
                        DeleteParam[1] = new SqlParameter("@HeaderFlag", SqlDbType.Bit);
                        DeleteParam[1].Value = 0;
                        DeleteParam[2] = new SqlParameter("@errorCode", SqlDbType.Int);
                        DeleteParam[2].Value = 0;
                        DeleteParam[2].Direction = ParameterDirection.Output;
                        SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                        errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 1].Value));
                        if (errorCode != 1)
                        {
                            sqlTrans.Rollback();
                            return errorCode;
                        }

                        for (int tblIndx = 0; tblIndx < dsHeader.Tables.Count; tblIndx++)
                        {
                            for (int i = 0; i < dsHeader.Tables[tblIndx].Rows.Count; i++)
                            {
                                SqlParameter[] Param;

                                if (tblIndx == 0)
                                {
                                    string[] error = new string[3];

                                    Param = new SqlParameter[dsHeader.Tables[tblIndx].Columns.Count + 3];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[tblIndx], i, i);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[tblIndx].TableName.ToString(), Param);
                                    error[0] = Param[indx].Value.ToString();
                                    error[1] = Param[indx + 1].Value.ToString();
                                    error[2] = Param[indx + 2].Value.ToString();

                                    if (Convert.ToInt32(error[0].ToString()) != 1 && Convert.ToInt32(error[0].ToString()) != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return Convert.ToInt32(error[0].ToString());
                                    }
                                }
                                else
                                {
                                    Param = new SqlParameter[dsHeader.Tables[tblIndx].Columns.Count + 1];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[tblIndx], tblIndx, i);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[tblIndx].TableName.ToString(), Param);
                                    errorCode = int.Parse(Param[indx].Value.ToString());
                                    if (errorCode != 1 && errorCode != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return errorCode;
                                    }
                                }
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;

                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }









        public int Save(DataSet dsHeader, int Id, string ID_Field, string SP_DeleteDetails, bool blRevision)
        {

            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                con.Open();//sql connection needs to be open before we can add any transction to it.
                using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                {
                    try
                    {
                        int errorCode = 0;
                        SqlParameter[] DeleteParam = new SqlParameter[4];
                        DeleteParam[0] = new SqlParameter("@" + ID_Field, SqlDbType.Int);
                        DeleteParam[0].Value = Id;
                        DeleteParam[1] = new SqlParameter("@HeaderFlag", SqlDbType.Bit);
                        DeleteParam[1].Value = 0;
                        DeleteParam[2] = new SqlParameter("@errorCode", SqlDbType.Int);
                        DeleteParam[2].Value = 0;
                        DeleteParam[2].Direction = ParameterDirection.Output;
                        DeleteParam[3] = new SqlParameter("@Revision", SqlDbType.VarChar);
                        DeleteParam[3].Value = blRevision;
                        SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                        errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 2].Value));
                        if (errorCode != 1)
                        {
                            sqlTrans.Rollback();
                            return errorCode;
                        }

                        for (int tblIndx = 0; tblIndx < dsHeader.Tables.Count; tblIndx++)
                        {
                            for (int i = 0; i < dsHeader.Tables[tblIndx].Rows.Count; i++)
                            {
                                SqlParameter[] Param;

                                if (tblIndx == 0)
                                {
                                    string[] error = new string[3];

                                    Param = new SqlParameter[dsHeader.Tables[tblIndx].Columns.Count + 3];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[tblIndx], i, i);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[tblIndx].TableName.ToString(), Param);
                                    error[0] = Param[indx].Value.ToString();
                                    error[1] = Param[indx + 1].Value.ToString();
                                    error[2] = Param[indx + 2].Value.ToString();

                                    if (Convert.ToInt32(error[0].ToString()) != 1 && Convert.ToInt32(error[0].ToString()) != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return Convert.ToInt32(error[0].ToString());
                                    }
                                    dsHeader.Tables[tblIndx].Rows[i].Delete();
                                    dsHeader.Tables[tblIndx].AcceptChanges(); i--;
                                }
                                else
                                {
                                    Param = new SqlParameter[dsHeader.Tables[tblIndx].Columns.Count + 1];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[tblIndx], tblIndx, i);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[tblIndx].TableName.ToString(), Param);
                                    errorCode = int.Parse(Param[indx].Value.ToString());
                                    if (errorCode != 1 && errorCode != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return errorCode;
                                    }
                                    dsHeader.Tables[tblIndx].Rows[i].Delete();
                                    dsHeader.Tables[tblIndx].AcceptChanges(); i--;
                                }
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        #endregion

        #region [Save()]
        public string[] Save(DataSet dsHeader, string ID_Field)
        {

            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                con.Open();
                using (SqlTransaction sqlTrans = con.BeginTransaction())
                {
                    try
                    {
                        string[] errorCode = new string[3];
                        for (int tblIndx = 0; tblIndx < dsHeader.Tables.Count; tblIndx++)
                        {
                            for (int i = 0; i < dsHeader.Tables[tblIndx].Rows.Count; i++)
                            {
                                SqlParameter[] Param;

                                if (tblIndx == 0)
                                {
                                    Param = new SqlParameter[dsHeader.Tables[tblIndx].Columns.Count + 3];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[tblIndx], i, i);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[tblIndx].TableName.ToString(), Param);
                                    errorCode[0] = Param[indx].Value.ToString();
                                    errorCode[1] = Param[indx + 1].Value.ToString();
                                    errorCode[2] = Param[indx + 2].Value.ToString();

                                    if (Convert.ToInt32(errorCode[0].ToString()) != 1 && Convert.ToInt32(errorCode[0].ToString()) != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return errorCode;
                                    }
                                }
                                else
                                {
                                    //do not delete it is being used for material Issue and Material Planning
                                    importantMethod(dsHeader, tblIndx);

                                    Param = new SqlParameter[dsHeader.Tables[tblIndx].Columns.Count + 1];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[tblIndx], tblIndx, i);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[tblIndx].TableName.ToString(), Param);
                                    errorCode[0] = Param[indx].Value.ToString();

                                    if (Convert.ToInt32(errorCode[0].ToString()) != 1 && Convert.ToInt32(errorCode[0].ToString()) != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return errorCode;
                                    }

                                    dsHeader.Tables[tblIndx].Rows[i].Delete();
                                    dsHeader.Tables[tblIndx].AcceptChanges(); i--;
                                }
                                if (tblIndx == 0 && ID_Field != "NO")
                                {
                                    InsertID(ref dsHeader, ID_Field, Convert.ToInt32(errorCode[1].ToString()), errorCode[2].ToString());
                                    dsHeader.Tables[tblIndx].Rows[i].Delete();
                                    dsHeader.Tables[tblIndx].AcceptChanges(); i--;
                                }
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }

                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        private static void importantMethod(DataSet dsHeader, int tblIndx)
        {

            if (dsHeader.Tables.Contains("ausp_SaveTblMaterialPlanningHeader"))
                if (dsHeader.Tables[tblIndx].TableName == "ausp_SaveTblWorkOrderForMP")
                    if (dsHeader.Tables[tblIndx].Columns.Contains("intMaterialPlanningID"))
                    {
                        for (int i = 0; i < dsHeader.Tables[tblIndx].Rows.Count; i++)
                            dsHeader.Tables[tblIndx].Rows[i]["intMRId"] =
                                dsHeader.Tables[tblIndx].Rows[i]["intMaterialPlanningID"].ToString();
                        dsHeader.Tables[tblIndx].Columns.Remove("intMaterialPlanningID");
                    }
            if (dsHeader.Tables.Contains("ausp_SaveTblMaterialIssueHeader"))
                if (dsHeader.Tables[tblIndx].TableName == "ausp_SaveTblWorkOrderForMI")
                    if (dsHeader.Tables[tblIndx].Columns.Contains("intMIId"))
                    {
                        for (int i = 0; i < dsHeader.Tables[tblIndx].Rows.Count; i++)
                            dsHeader.Tables[tblIndx].Rows[i]["intMRId"] =
                                dsHeader.Tables[tblIndx].Rows[i]["intMIId"].ToString();
                        dsHeader.Tables[tblIndx].Columns.Remove("intMIId");
                    }
            if (dsHeader.Tables.Contains("ausp_SaveTblMaterialPlanningPLDetails"))
                if (dsHeader.Tables[tblIndx].TableName == "ausp_SaveTblMaterialPlanningPLDetails")
                    if (dsHeader.Tables[tblIndx].Columns.Contains("intMIId"))
                        dsHeader.Tables[tblIndx].Columns.Remove("intMIId");

            if (dsHeader.Tables.Contains("ausp_SaveTblMaterialPlanningSundryPLDetails"))
                if (dsHeader.Tables[tblIndx].TableName == "ausp_SaveTblMaterialPlanningSundryPLDetails")
                    if (dsHeader.Tables[tblIndx].Columns.Contains("intMIId"))
                        dsHeader.Tables[tblIndx].Columns.Remove("intMIId");

            if (dsHeader.Tables.Contains("ausp_SaveTblMaterialPlanningJWDetails"))
                if (dsHeader.Tables[tblIndx].TableName == "ausp_SaveTblMaterialPlanningJWDetails")
                    if (dsHeader.Tables[tblIndx].Columns.Contains("intMIId"))
                        dsHeader.Tables[tblIndx].Columns.Remove("intMIId");

            if (dsHeader.Tables.Contains("ausp_SaveTblMaterialPlanningWKDetails"))
                if (dsHeader.Tables[tblIndx].TableName == "ausp_SaveTblMaterialPlanningWKDetails")
                    if (dsHeader.Tables[tblIndx].Columns.Contains("intMIId"))
                        dsHeader.Tables[tblIndx].Columns.Remove("intMIId");
        }

        public string[] SaveAssetData(DataSet dsHeader, string ID, bool AddMode, string SP_DeleteDetails)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();
                    using (SqlTransaction sqlTrans = con.BeginTransaction())
                    {

                        if (AddMode == false)
                        {
                            string[] CodeString = new string[1];
                            SqlParameter[] DeleteParam = new SqlParameter[2];
                            DeleteParam[0] = new SqlParameter("@intAssetGroupID", SqlDbType.VarChar);
                            DeleteParam[0].Value = ID;
                            DeleteParam[1] = new SqlParameter("@errorCode", SqlDbType.Int);
                            DeleteParam[1].Value = 0;
                            DeleteParam[1].Direction = ParameterDirection.Output;
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                            CodeString[0] = Convert.ToString(DeleteParam[DeleteParam.Length - 1].Value);
                            if (CodeString[0] != "1")
                            {
                                sqlTrans.Rollback();
                                return CodeString;
                            }
                        }



                        SqlParameter[] Param;
                        string[] errorCode = new string[3];

                        for (int i = 0; i < dsHeader.Tables[0].Rows.Count; i++)
                        {

                            Param = new SqlParameter[dsHeader.Tables[0].Columns.Count + 3];
                            Param = CreateAllSQLParameters(dsHeader.Tables[0], 0, i);
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[0].TableName.ToString(), Param);
                            errorCode[0] = Param[indx].Value.ToString();
                            errorCode[1] = Param[indx + 1].Value.ToString();
                            errorCode[2] = Param[indx + 2].Value.ToString();

                            if (Convert.ToInt32(errorCode[0].ToString()) != 1 && Convert.ToInt32(errorCode[0].ToString()) != 2)
                            {
                                sqlTrans.Rollback();
                                return errorCode;
                            }

                            #region When Header' table Count is greater than 4
                            for (int s = 0; s < dsHeader.Tables[2].Rows.Count; s++)
                            {
                                if ((dsHeader.Tables[0].Rows[i]["strCodeString"].ToString() + " | " + dsHeader.Tables[0].Rows[i]["strAssetCode"].ToString()).ToString() == dsHeader.Tables[2].Rows[s]["strAssetCode"].ToString())
                                {
                                    dsHeader.Tables[2].Rows[s]["intAssetId"] = errorCode[1].ToString();
                                    Param = new SqlParameter[dsHeader.Tables[2].Columns.Count + 1];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[2], 1, s);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[2].TableName.ToString(), Param);
                                    errorCode[0] = Param[indx].Value.ToString();
                                    if (Convert.ToInt32(errorCode[0].ToString()) != 1 && Convert.ToInt32(errorCode[0].ToString()) != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return errorCode;
                                    }
                                }
                            }

                            for (int s = 0; s < dsHeader.Tables[3].Rows.Count; s++)
                            {
                                dsHeader.Tables[3].Rows[s]["intAssetId"] = errorCode[1].ToString();
                                dsHeader.Tables[3].Rows[s]["strAcqisitionDate"] = dsHeader.Tables[0].Rows[i]["strAcqisitionDate"].ToString();
                                Param = new SqlParameter[dsHeader.Tables[3].Columns.Count + 1];
                                Param = CreateAllSQLParameters(dsHeader.Tables[3], 1, s);
                                SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[3].TableName.ToString(), Param);
                                errorCode[0] = Param[indx].Value.ToString();
                                if (Convert.ToInt32(errorCode[0].ToString()) != 1 && Convert.ToInt32(errorCode[0].ToString()) != 2)
                                {
                                    sqlTrans.Rollback();
                                    return errorCode;
                                }
                            }

                            for (int s = 0; s < dsHeader.Tables[4].Rows.Count; s++)
                            {
                                if ((dsHeader.Tables[0].Rows[i]["strCodeString"].ToString() + " | " + dsHeader.Tables[0].Rows[i]["strAssetCode"].ToString()) ==
                                    dsHeader.Tables[4].Rows[s]["strAssetCode"].ToString())
                                {
                                    dsHeader.Tables[4].Rows[s]["intAssetId"] = errorCode[1].ToString();
                                    Param = new SqlParameter[dsHeader.Tables[4].Columns.Count + 1];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[4], 1, s);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[4].TableName.ToString(), Param);
                                    errorCode[0] = Param[indx].Value.ToString();
                                    if (Convert.ToInt32(errorCode[0].ToString()) != 1 && Convert.ToInt32(errorCode[0].ToString()) != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return errorCode;
                                    }
                                }
                            }
                            #endregion

                        }

                        if (dsHeader.Tables.Count > 1 && dsHeader.Tables[1].TableName == "ausp_SaveTblAssetPurchaseDetails")
                        {
                            for (int j = 0; j < dsHeader.Tables[1].Rows.Count; j++)
                            {

                                dsHeader.Tables[1].Rows[j]["intAssetId"] = errorCode[1].ToString();
                                //dsHeader.Tables[1].Rows[j]["strAssetCode"] = dsHeader.Tables[0].Rows[i]["strAssetCode"].ToString();
                                dsHeader.Tables[1].Rows[j]["strAcqisitionDate"] = dsHeader.Tables[0].Rows[0]["strAcqisitionDate"].ToString();
                                Param = new SqlParameter[dsHeader.Tables[1].Columns.Count + 1];
                                Param = CreateAllSQLParameters(dsHeader.Tables[1], 1, j);
                                SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[1].TableName.ToString(), Param);
                                errorCode[0] = Param[indx].Value.ToString();
                                if (Convert.ToInt32(errorCode[0].ToString()) != 1 && Convert.ToInt32(errorCode[0].ToString()) != 2)
                                {
                                    sqlTrans.Rollback();
                                    return errorCode;
                                }
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [SaveEnclosure()]
        public int SaveEnclosureCA(DataSet dsHeader, int[] Id, string[] ID_Field, string SP_DeleteDetails)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();//sql connection needs to be open before we can add any transction to it.
                    using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                    {
                        int errorCode = 0;
                        SqlParameter[] DeleteParam = new SqlParameter[4];
                        DeleteParam[0] = new SqlParameter("@" + ID_Field[0], SqlDbType.Int);
                        DeleteParam[0].Value = Id[0];
                        DeleteParam[1] = new SqlParameter("@" + ID_Field[1], SqlDbType.Int);
                        DeleteParam[1].Value = Id[1];
                        DeleteParam[2] = new SqlParameter("@HeaderFlag", SqlDbType.Bit);
                        DeleteParam[2].Value = 0;
                        DeleteParam[3] = new SqlParameter("@errorCode", SqlDbType.Int);
                        DeleteParam[3].Value = 0;
                        DeleteParam[3].Direction = ParameterDirection.Output;
                        SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                        errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 1].Value));
                        if (errorCode != 1)
                        {
                            sqlTrans.Rollback();
                            return errorCode;
                        }

                        for (int tblIndx = 0; tblIndx < dsHeader.Tables.Count; tblIndx++)
                        {
                            for (int i = 0; i < dsHeader.Tables[tblIndx].Rows.Count; i++)
                            {
                                SqlParameter[] Param;

                                if (tblIndx == 0)
                                {
                                    string[] error = new string[3];

                                    Param = new SqlParameter[dsHeader.Tables[tblIndx].Columns.Count + 3];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[tblIndx], i, i);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[tblIndx].TableName.ToString(), Param);
                                    error[0] = Param[indx].Value.ToString();
                                    error[1] = Param[indx + 1].Value.ToString();
                                    error[2] = Param[indx + 2].Value.ToString();

                                    if (Convert.ToInt32(error[0].ToString()) != 1 && Convert.ToInt32(error[0].ToString()) != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return Convert.ToInt32(error[0].ToString());
                                    }
                                }
                                else
                                {
                                    Param = new SqlParameter[dsHeader.Tables[tblIndx].Columns.Count + 1];
                                    Param = CreateAllSQLParameters(dsHeader.Tables[tblIndx], tblIndx, i);
                                    SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[tblIndx].TableName.ToString(), Param);
                                    errorCode = int.Parse(Param[indx].Value.ToString());
                                    if (errorCode != 1 && errorCode != 2)
                                    {
                                        sqlTrans.Rollback();
                                        return errorCode;
                                    }
                                }
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int SaveEnclosure(DataSet dsHeader, int[] Id, string[] ID_Field, string SP_DeleteDetails)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();//sql connection needs to be open before we can add any transction to it.
                    using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                    {
                        int errorCode = 0;
                        SqlParameter[] DeleteParam = new SqlParameter[3];
                        DeleteParam[0] = new SqlParameter("@" + ID_Field[0], SqlDbType.Int);
                        DeleteParam[0].Value = Id[0];
                        DeleteParam[1] = new SqlParameter("@" + ID_Field[1], SqlDbType.Int);
                        DeleteParam[1].Value = Id[1];
                        DeleteParam[2] = new SqlParameter("@errorCode", SqlDbType.Int);
                        DeleteParam[2].Value = 0;
                        DeleteParam[2].Direction = ParameterDirection.Output;
                        SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                        errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 1].Value));
                        if (errorCode != 1)
                        {
                            sqlTrans.Rollback();
                            return errorCode;
                        }

                        for (int i = 0; i < dsHeader.Tables[0].Rows.Count; i++)
                        {
                            SqlParameter[] Param;

                            Param = new SqlParameter[dsHeader.Tables[0].Columns.Count + 3];
                            Param = CreateAllSQLParameters(dsHeader.Tables[0], 0, i);
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[0].TableName.ToString(), Param);
                            errorCode = int.Parse(Param[indx].Value.ToString());
                            if (errorCode != 1 && errorCode != 2)
                            {
                                sqlTrans.Rollback();
                                return errorCode;
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int SaveEnclosure(DataSet dsHeader, string[] Id, string[] ID_Field, string SP_DeleteDetails)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();//sql connection needs to be open before we can add any transction to it.
                    using (SqlTransaction sqlTrans = con.BeginTransaction())// Begin transection for open sql connection 
                    {
                        int errorCode = 0;
                        SqlParameter[] DeleteParam = new SqlParameter[3];
                        DeleteParam[0] = new SqlParameter("@" + ID_Field[0], SqlDbType.VarChar);
                        DeleteParam[0].Value = Id[0];
                        DeleteParam[1] = new SqlParameter("@" + ID_Field[1], SqlDbType.VarChar);
                        DeleteParam[1].Value = Id[1];
                        DeleteParam[2] = new SqlParameter("@errorCode", SqlDbType.Int);
                        DeleteParam[2].Value = 0;
                        DeleteParam[2].Direction = ParameterDirection.Output;
                        SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, SP_DeleteDetails, DeleteParam);
                        errorCode = int.Parse(Convert.ToString(DeleteParam[DeleteParam.Length - 1].Value));
                        if (errorCode != 1)
                        {
                            sqlTrans.Rollback();
                            return errorCode;
                        }

                        for (int i = 0; i < dsHeader.Tables[0].Rows.Count; i++)
                        {
                            SqlParameter[] Param;

                            Param = new SqlParameter[dsHeader.Tables[0].Columns.Count + 3];
                            Param = CreateAllSQLParameters(dsHeader.Tables[0], 0, i);
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[0].TableName.ToString(), Param);
                            errorCode = int.Parse(Param[indx].Value.ToString());
                            if (errorCode != 1 && errorCode != 2)
                            {
                                sqlTrans.Rollback();
                                return errorCode;
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region SaveForSelectedBranch
        public int SaveForBranch(DataSet dsHeader)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();
                    using (SqlTransaction sqlTrans = con.BeginTransaction())
                    {
                        //SqlParameter[] sqlParam = new SqlParameter[4];
                        //sqlParam[0] = new SqlParameter("@intNewBranchId", SqlDbType.VarChar);
                        //sqlParam[0].Value = Param[0];
                        //sqlParam[1] = new SqlParameter("@intOldBranchId", SqlDbType.VarChar);
                        //sqlParam[1].Value = Param[1];
                        //sqlParam[2] = new SqlParameter("@strChargeFlag", SqlDbType.VarChar);
                        //sqlParam[2].Value = Param[2];
                        //sqlParam[3] = new SqlParameter("@errorCode", SqlDbType.VarChar);
                        //sqlParam[3].Value = "0";
                        //sqlParam[3].Direction = ParameterDirection.Output;

                        int errorCode = 0;
                        //SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, spName.ToString(), sqlParam);
                        //errorCode = int.Parse(sqlParam[3].Value.ToString());
                        //if (errorCode != 1 && errorCode != 2)
                        //{
                        //    sqlTrans.Rollback();
                        //    return errorCode;
                        //}
                        //sqlTrans.Commit();
                        //return errorCode;

                        for (int i = 0; i < dsHeader.Tables[0].Rows.Count; i++)
                        {
                            SqlParameter[] Param;

                            Param = new SqlParameter[dsHeader.Tables[0].Columns.Count + 3];
                            Param = CreateAllSQLParameters(dsHeader.Tables[0], 0, i);
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[0].TableName.ToString(), Param);
                            errorCode = int.Parse(Param[indx].Value.ToString());
                            if (errorCode != 1 && errorCode != 2)
                            {
                                sqlTrans.Rollback();
                                return errorCode;
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int SaveForBranchFinal(DataSet dsHeader)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();
                    using (SqlTransaction sqlTrans = con.BeginTransaction())
                    {
                        //SqlParameter[] sqlParam = new SqlParameter[4];
                        //sqlParam[0] = new SqlParameter("@intNewBranchId", SqlDbType.VarChar);
                        //sqlParam[0].Value = Param[0];
                        //sqlParam[1] = new SqlParameter("@intOldBranchId", SqlDbType.VarChar);
                        //sqlParam[1].Value = Param[1];
                        //sqlParam[2] = new SqlParameter("@strChargeFlag", SqlDbType.VarChar);
                        //sqlParam[2].Value = Param[2];
                        //sqlParam[3] = new SqlParameter("@errorCode", SqlDbType.VarChar);
                        //sqlParam[3].Value = "0";
                        //sqlParam[3].Direction = ParameterDirection.Output;

                        int errorCode = 0;
                        //SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, spName.ToString(), sqlParam);
                        //errorCode = int.Parse(sqlParam[3].Value.ToString());
                        //if (errorCode != 1 && errorCode != 2)
                        //{
                        //    sqlTrans.Rollback();
                        //    return errorCode;
                        //}
                        //sqlTrans.Commit();
                        //return errorCode;

                        for (int i = 0; i < dsHeader.Tables[0].Rows.Count; i++)
                        {
                            SqlParameter[] Param;

                            Param = new SqlParameter[dsHeader.Tables[0].Columns.Count + 1];
                            Param = CreateAllSQLParameters(dsHeader.Tables[0], 0, i);
                            SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[0].TableName.ToString(), Param);
                            errorCode = int.Parse(Param[indx].Value.ToString());
                            if (errorCode != 1 && errorCode != 2)
                            {
                                sqlTrans.Rollback();
                                return errorCode;
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int SaveDepreciation(DataSet dsHeader, bool AddMode)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    con.Open();
                    using (SqlTransaction sqlTrans = con.BeginTransaction())
                    {
                        int errorCode = 0;
                        if (AddMode == false)
                        {
                            //SqlParameter[] sqlParam = new SqlParameter[3];
                            //sqlParam[0] = new SqlParameter("@intFYID", SqlDbType.VarChar);
                            //sqlParam[0].Value = dsHeader.Tables[0].Rows[0]["intFYId"].ToString();
                            //sqlParam[1] = new SqlParameter("@intBranchId", SqlDbType.VarChar);
                            //sqlParam[1].Value = dsHeader.Tables[0].Rows[0]["intBranchId"].ToString();
                            //sqlParam[2] = new SqlParameter("@errorCode", SqlDbType.VarChar);
                            //sqlParam[2].Value = "0";
                            //sqlParam[2].Direction = ParameterDirection.Output;

                            //SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, "dsp_DeleteTblAssetDepreciationCalculation", sqlParam);
                            //errorCode = int.Parse(sqlParam[3].Value.ToString());
                            //if (errorCode != 1 && errorCode != 2)
                            //{
                            //    sqlTrans.Rollback();
                            //}
                        }

                        for (int j = 0; j < dsHeader.Tables.Count; j++)
                        {
                            for (int i = 0; i < dsHeader.Tables[j].Rows.Count; i++)
                            {
                                SqlParameter[] Param;
                                if (j == 0)
                                    Param = new SqlParameter[dsHeader.Tables[j].Columns.Count + 3];
                                else
                                    Param = new SqlParameter[dsHeader.Tables[j].Columns.Count + 1];

                                Param = CreateAllSQLParameters(dsHeader.Tables[j], j, i);
                                SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, dsHeader.Tables[j].TableName.ToString(), Param);
                                errorCode = int.Parse(Param[indx].Value.ToString());
                                if (errorCode != 1 && errorCode != 2)
                                {
                                    sqlTrans.Rollback();
                                    return errorCode;
                                }
                            }
                        }
                        sqlTrans.Commit();
                        return errorCode;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [InsertID()]
        public void InsertID(ref DataSet dataSet, string ID_Field, int ID, string IDs)
        {
            try
            {
                for (int i = 0; i < dataSet.Tables.Count; i++)
                {
                    if (!dataSet.Tables[i].Columns.Contains("intAuditHistoryTranId"))
                        if (!dataSet.Tables[i].Columns.Contains(ID_Field))
                            dataSet.Tables[i].Columns.Add(ID_Field);

                    for (int f = 0; f < dataSet.Tables[i].Rows.Count; f++)
                    {
                        if (dataSet.Tables[i].Columns.Contains("intAuditHistoryTranId"))
                        {
                            dataSet.Tables[i].Rows[f]["intAuditHistoryTranId"] = ID;
                            dataSet.Tables[i].Rows[f]["strDocumentNo"] = IDs;
                        }
                        else
                            dataSet.Tables[i].Rows[f][ID_Field] = ID;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [chkDuplication()]
        public DataSet chkDuplication(string TableName, string Condition, string SP_Name)
        {
            DataSet ds;
            try
            {
                SqlParameter[] Param = new SqlParameter[2];
                Param[0] = new SqlParameter("@TableName", SqlDbType.VarChar, 50);
                Param[0].Value = TableName;
                Param[1] = new SqlParameter("@Condition", SqlDbType.VarChar, 2000);
                Param[1].Value = Condition;
                ds = SqlHelper.ExecuteDataset(sqlConnectionString, CommandType.StoredProcedure, SP_Name, Param);
            }
            catch (Exception ex)
            {
                ds = null;
                throw;
            }
            return ds;
        }
        #endregion

        #region [SaveQuery()]
        public int SaveQuery(string SP_Name, object[] param)
        {
            int Count;
            try
            {
                Count = SqlHelper.ExecuteNonQuery(sqlConnectionString, SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
            return Count;
        }
        #endregion

        #region [SAVE Reports]
        public int SaveData(string Query)
        {
            int errorCode = 0;
            try
            {
                errorCode = SqlHelper.ExecuteNonQuery(sqlConnectionString, CommandType.Text, Query);
            }
            catch (Exception ex)
            {

                throw;
            }
            return errorCode;
        }

        #endregion

        #region [SAVE Reports DS]
        public int SaveDataReport(DataSet ds, String SPName)
        {
            int errorCode = 0;
            try
            {
                SqlParameter[] Param = new SqlParameter[ds.Tables[0].Columns.Count + 1];
                Param = CreateAllSQLParameters(ds.Tables[0], 1, 0);
                SqlHelper.ExecuteNonQuery(sqlConnectionString, CommandType.StoredProcedure, SPName, Param);
                errorCode = int.Parse(Param[indx].Value.ToString());
            }
            catch (Exception ex)
            {
                errorCode = 10;
                throw;
            }
            return errorCode;
        }
        #endregion

        #region [BuildReport()]
        public void BuildReport(string WhereClause, string ViewName, string SP_Name)
        {
            int j;
            try
            {
                SqlParameter[] Param = new SqlParameter[2];
                Param[0] = new SqlParameter("@WhereClause", SqlDbType.VarChar, 5000);
                Param[0].Value = WhereClause;
                Param[1] = new SqlParameter("@ViewName", SqlDbType.VarChar, 100);
                Param[1].Value = ViewName;
                j = SqlHelper.ExecuteNonQuery(sqlConnectionString, CommandType.StoredProcedure, SP_Name, Param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void BuildReport(int FId, int BranchId, string WhereClause, string ViewName, string SP_Name, string FormDate, string ToDate)
        {
            int j;
            try
            {
                SqlParameter[] Param = new SqlParameter[6];
                Param[0] = new SqlParameter("@WhereClause", SqlDbType.VarChar, 5000);
                Param[0].Value = WhereClause;
                Param[1] = new SqlParameter("@ViewName", SqlDbType.VarChar, 100);
                Param[1].Value = ViewName;
                Param[2] = new SqlParameter("@FId", SqlDbType.VarChar, 5000);
                Param[2].Value = FId;
                Param[3] = new SqlParameter("@BranchId", SqlDbType.VarChar, 100);
                Param[3].Value = BranchId;
                Param[4] = new SqlParameter("@FormDate", SqlDbType.VarChar, 5000);
                Param[4].Value = FormDate;
                Param[5] = new SqlParameter("@ToDate", SqlDbType.VarChar, 100);
                Param[5].Value = ToDate;
                j = SqlHelper.ExecuteNonQuery(sqlConnectionString, CommandType.StoredProcedure, SP_Name, Param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void BuildReport(int FId, int BranchId, string ViewName, string SP_Name)
        {
            int j;
            try
            {
                SqlParameter[] Param = new SqlParameter[3];

                Param[0] = new SqlParameter("@ViewName", SqlDbType.VarChar, 100);
                Param[0].Value = ViewName;
                Param[1] = new SqlParameter("@FId", SqlDbType.VarChar, 5000);
                Param[1].Value = FId;
                Param[2] = new SqlParameter("@BranchId", SqlDbType.VarChar, 100);
                Param[2].Value = BranchId;

                j = SqlHelper.ExecuteNonQuery(sqlConnectionString, CommandType.StoredProcedure, SP_Name, Param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region [Posting()]
        public void Posting(string SP_Name, string strTblName, string strDate, string strEmpId, string strClause)
        {
            int j;
            try
            {
                SqlParameter[] Param = new SqlParameter[4];
                Param[0] = new SqlParameter("@TblName", SqlDbType.VarChar, 5000);
                Param[0].Value = strTblName;
                Param[1] = new SqlParameter("@strDate", SqlDbType.VarChar, 100);
                Param[1].Value = strDate;
                Param[2] = new SqlParameter("@intPostingBy", SqlDbType.VarChar, 100);
                Param[2].Value = strEmpId;
                Param[3] = new SqlParameter("@clause", SqlDbType.VarChar, 5000);
                Param[3].Value = strClause;

                j = SqlHelper.ExecuteNonQuery(sqlConnectionString, CommandType.StoredProcedure, SP_Name, Param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region [ChkUseFlag()]
        public Boolean ChkUseFlag(string SP_Name, string strTblName, string strFldName, Int32 intID)
        {
            int j;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] Param = new SqlParameter[3];
                Param[0] = new SqlParameter("@strTblName", SqlDbType.VarChar, 500);
                Param[0].Value = strTblName;
                Param[1] = new SqlParameter("@strFldName", SqlDbType.VarChar, 500);
                Param[1].Value = strFldName;
                Param[2] = new SqlParameter("@intID", SqlDbType.Int);
                Param[2].Value = intID;
                ds = SqlHelper.ExecuteDataset(sqlConnectionString, CommandType.StoredProcedure, SP_Name, Param);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[0]["strUseFlag"].ToString()).Trim() == "Y")
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region [ChkUseFlagBranchwise()]
        public Boolean ChkUseFlagBranchwise(string SP_Name, string strTblName, string strFldName, Int32 intID, Int32 intBranchID)
        {
            int j;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] Param = new SqlParameter[4];
                Param[0] = new SqlParameter("@strTblName", SqlDbType.VarChar, 500);
                Param[0].Value = strTblName;
                Param[1] = new SqlParameter("@strFldName", SqlDbType.VarChar, 500);
                Param[1].Value = strFldName;
                Param[2] = new SqlParameter("@intID", SqlDbType.Int);
                Param[2].Value = intID;
                Param[3] = new SqlParameter("@intBranchID", SqlDbType.Int);
                Param[3].Value = intBranchID;
                ds = SqlHelper.ExecuteDataset(sqlConnectionString, CommandType.StoredProcedure, SP_Name, Param);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[0]["strUseFlag"].ToString()).Trim() == "Y")
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region GetScript Change Code
        public string GetScript(string SP_Script)
        {            
            string tempscript;
            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                con.Open();
                using (SqlTransaction sqlTrans = con.BeginTransaction())
                {
                    object scripttemp;
                    scripttemp = SqlHelper.ExecuteScalar(sqlTrans, CommandType.Text, "exec ssp_GetBindfrmScriptForItemTree " + SP_Script + "");
                    tempscript = Convert.ToString(scripttemp);
                }
            }

            int cnt = tempscript.Trim().Length;
            tempscript = tempscript.Replace("END", "");
            tempscript = tempscript.Substring(tempscript.IndexOf("BEGIN") + 6);

            return tempscript;
        }
        #endregion

        #region GetServerParameter
        public string[] GetServerParameter()
        {
            string[] str1 = sqlConnectionString.Split('=');
            string[] str2 = str1[1].Split(';');
            string[] str3 = str1[2].Split(';');
            string[] str4 = str1[3].Split(';');
            string[] str5 = str1[4].Split(';');

            str1[0] = str2[0].ToString();
            str1[1] = str3[0].ToString();
            str1[2] = str4[0].ToString();
            str1[3] = str5[0].ToString();

            return str1;
        }
        #endregion

        #region GetFromMailId
        public string GetFromMailId(string SP_Name, string strCompanyID, string BranchID, string UserID)
        {
            DataSet DsTmp = new DataSet();
            object[] param = new object[3];
            param[0] = strCompanyID;
            param[1] = BranchID;
            param[2] = UserID;
            DsTmp = GetDs(SP_Name, param);
            if (DsTmp.Tables[0].Rows[0][0].ToString().Trim().Length > 0 && DsTmp.Tables[0].Rows.Count > 0 && DsTmp != null)
                return DsTmp.Tables[0].Rows[0]["strEmailid"].ToString();
            else
                return System.Configuration.ConfigurationSettings.AppSettings["FromMailAddress"].ToString();
        }
        #endregion

        #region GetFilterchar
        public string GetFilterchar(string SP_Name, string TableName, string Field, string FromChar, string ToChar)
        {
            string Filter = "";
            if (FromChar.Trim().Length == 1 && ToChar.Trim().Length == 1)
            {
                string charStart = "N";
                DataSet dsFilter = new DataSet();
                object[] param = new object[4];
                param[0] = TableName;
                param[1] = Field;
                param[2] = FromChar;
                param[3] = ToChar;
                dsFilter = GetDs(SP_Name, param);
                if (dsFilter != null && dsFilter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsFilter.Tables[0].Rows.Count; i++)
                    {
                        if (dsFilter.Tables[0].Rows[i]["strChar"].ToString().ToUpper() == FromChar.Trim().ToString().ToUpper())
                        {
                            charStart = "Y";
                            Filter = Filter + dsFilter.Tables[0].Rows[i]["TbFilter"].ToString() + " like '" + dsFilter.Tables[0].Rows[i]["strChar"].ToString() + "%'";
                        }
                        else if (FromChar.Trim().ToString().ToUpper() != ToChar.Trim().ToString().ToUpper() && charStart == "Y" && dsFilter.Tables[0].Rows[i]["strChar"].ToString().ToUpper() != ToChar.Trim().ToString().ToUpper() && dsFilter.Tables[0].Rows[i]["strChar"].ToString().ToUpper() != ToChar.Trim().ToString().ToUpper())
                        {
                            Filter = Filter + " or " + dsFilter.Tables[0].Rows[i]["TbFilter"].ToString() + " like '" + dsFilter.Tables[0].Rows[i]["strChar"].ToString() + "%'";
                        }
                        else if (FromChar.Trim().ToString().ToUpper() != ToChar.Trim().ToString().ToUpper() && charStart == "Y" && dsFilter.Tables[0].Rows[i]["strChar"].ToString().ToUpper() == ToChar.Trim().ToString().ToUpper())
                        {
                            Filter = Filter + " or " + dsFilter.Tables[0].Rows[i]["TbFilter"].ToString() + " like '" + dsFilter.Tables[0].Rows[i]["strChar"].ToString() + "%'";
                            i = dsFilter.Tables[0].Rows.Count;
                        }
                    }
                }
            }
            else
                Filter = TableName.ToString() + "." + Field.ToString() + " like '" + FromChar.Trim().ToString() + "%'";

            if (Filter != "") Filter = "( " + Filter + " )";

            return Filter;
        }
        #endregion

        #region [SAVE BULKY DETAIL DATA]

        /// <summary>
        /// Created By Vaibhav To save bulky data to avoid request time out exception
        /// 
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="strTableName"></param>
        public void SaveBulkyDetailData(DataSet ds, String strTableName)
        {
            string errorCode = string.Empty;
            string cs = sqlConnectionString;
            SqlConnection dest = new SqlConnection(cs);
            DataTable dataTable = ds.Tables[0];
            SqlBulkCopy bulkCopy = new SqlBulkCopy(dest);
            dest.Open();
            bulkCopy.DestinationTableName = strTableName;
            bulkCopy.ColumnMappings.Clear();
            foreach (DataColumn myCol in dataTable.Columns)
                bulkCopy.ColumnMappings.Add(myCol.ColumnName, myCol.ColumnName);

            bulkCopy.WriteToServer(dataTable);
        }
        #endregion   

        #region GetDataBaseName
        public DataSet GetDBName()
        {
            DataSet ds;
            try
            {
                ds = SqlHelper.ExecuteDataset(sqlConnectionString, CommandType.StoredProcedure, "sp_databases");
            }
            catch (Exception ex)
            {
                throw;
            }
            return ds;
        }
        #endregion

        #region SetConnectionString
        public void SetConnectionString(string KeyName)
        {
            try
            {
                string myDataSource = "";
                string myInitialCatalog = "";
                string myUserID = "";
                string myPassword = "";
                string myApplicationName = "";
                string mySuperAdminPassword = "";
                string myNoOfUser = "";
                string myValidity = "";
                string myNoOfBranches = "";
                string myCompanyName = "";
                string myCompanyShortName = "";
                string myNoOfModule = "";
                string myLicenceNo = "";
                string myUseFlag = "";

                string[] strTemp;
                string ServerPath = KeyName;
                ServerPath = ServerPath.Substring(0, (ServerPath.Length - 1));
                strTemp = ServerPath.Split('\\');
                ServerPath = "";
                for (int i = 0; i < strTemp.Count(); i++)
                {
                    if (i > 0)
                        ServerPath = ServerPath + "\\" + EncryptFieldName(strTemp[i].ToString());
                }

                //"VaibhavConn" change registry name for new registry
                string ConS = "VaibhavConn" + ServerPath + "\\";
                Microsoft.Win32.RegistryKey rkey;
                rkey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(ConS);
                if (rkey == null)
                {
                    // the Key does not exist
                }
                else
                {
                    myDataSource = Desncrypt((string)rkey.GetValue(EncryptFieldName("DataSource")));
                    myInitialCatalog = Desncrypt((string)rkey.GetValue(EncryptFieldName("InitialCatalog")));
                    myUserID = Desncrypt((string)rkey.GetValue(EncryptFieldName("UserID")));
                    myPassword = Desncrypt((string)rkey.GetValue(EncryptFieldName("Password")));
                    myApplicationName = Desncrypt((string)rkey.GetValue(EncryptFieldName("ApplicationName")));
                    mySuperAdminPassword = Desncrypt((string)rkey.GetValue(EncryptFieldName("SuperAdminPassword")));
                    myNoOfUser = Desncrypt((string)rkey.GetValue(EncryptFieldName("NoOfUser")));
                    myValidity = Desncrypt((string)rkey.GetValue(EncryptFieldName("Validity")));
                    myNoOfBranches = Desncrypt((string)rkey.GetValue(EncryptFieldName("NoOfBranches")));
                    myCompanyName = Desncrypt((string)rkey.GetValue(EncryptFieldName("CompanyName")));
                    myCompanyShortName = Desncrypt((string)rkey.GetValue(EncryptFieldName("CompanyShortName")));
                    myNoOfModule = Desncrypt((string)rkey.GetValue(EncryptFieldName("NoOfModule")));
                    myLicenceNo = Desncrypt((string)rkey.GetValue(EncryptFieldName("LicenceNo")));
                    myUseFlag = Desncrypt((string)rkey.GetValue(EncryptFieldName("UseFlag")));
                }
                
                sqlConnectionString = "Data Source=" + myDataSource + ";" + "Initial Catalog=" + myInitialCatalog +
                      ";" + "User ID=" + myUserID + ";" + "Password=" + myPassword;

                regApplicationName = myApplicationName;
                regSuperAdminPassword = mySuperAdminPassword;
                regNoOfUser = myNoOfUser;
                regValidity = myValidity;
                regNoOfBranches = myNoOfBranches;
                regCompanyName = myCompanyName;
                regCompanyShortName = myCompanyShortName;
                regNoOfModule = myNoOfModule;
                regLicenceNo = myLicenceNo;
                regUseFlag = myUseFlag;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetConnectionString
        public static string GetConnectionString()
        {
            try
            {
                string myDataSource = "";
                string myInitialCatalog = "";
                string myUserID = "";
                string myPassword = "";
                string myApplicationName = "";
                string mySuperAdminPassword = "";
                string myNoOfUser = "";
                string myValidity = "";
                string myNoOfBranches = "";
                string myCompanyName = "";
                string myNoOfModule = "";
                string myLicenceNo = "";
                string ConnectionString = "";

                Microsoft.Win32.RegistryKey rkey;
                rkey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("ConneBill");
                if (rkey == null)
                {
                    // the Key does not exist
                }
                else
                {
                    myDataSource = Desncrypt((string)rkey.GetValue(EncryptFieldName("DataSource")));
                    myInitialCatalog = Desncrypt((string)rkey.GetValue(EncryptFieldName("InitialCatalog")));
                    myUserID = Desncrypt((string)rkey.GetValue(EncryptFieldName("UserID")));
                    myPassword = Desncrypt((string)rkey.GetValue(EncryptFieldName("Password")));
                    myApplicationName = Desncrypt((string)rkey.GetValue(EncryptFieldName("ApplicationName")));
                    mySuperAdminPassword = Desncrypt((string)rkey.GetValue(EncryptFieldName("SuperAdminPassword")));
                    myNoOfUser = Desncrypt((string)rkey.GetValue(EncryptFieldName("NoOfUser")));
                    myValidity = Desncrypt((string)rkey.GetValue(EncryptFieldName("Validity")));
                    myNoOfBranches = Desncrypt((string)rkey.GetValue(EncryptFieldName("NoOfBranches")));
                    myCompanyName = Desncrypt((string)rkey.GetValue(EncryptFieldName("CompanyName")));
                    myNoOfModule = Desncrypt((string)rkey.GetValue(EncryptFieldName("NoOfModule")));
                    myLicenceNo = Desncrypt((string)rkey.GetValue(EncryptFieldName("LicenceNo")));
                }

                // "Data Source=ESS005-PC;Initial Catalog=Pritesh;User ID=sa;Password=sa1234#"
                return ConnectionString = "Data Source=" + myDataSource + ";" + "Initial Catalog=" + myInitialCatalog +
                      ";" + "User ID=" + myUserID + ";" + "Password=" + myPassword;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [Desncrypt]

        public static string Desncrypt(string strval)
        {
            string str = "";
            try
            {
                for (int i = 1; i <= strval.Length; i++)
                {
                    switch (i % 5)
                    {
                        case 0:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) - 3);
                            break;
                        case 1:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) + 6);
                            break;
                        case 2:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) - 5);
                            break;
                        case 3:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) + 10);
                            break;
                        case 4:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) - 15);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return str;
        }
        #endregion

        #region Enum Letters
        public enum Letters
        {
            a = 25, b = 24, c = 23, d = 22, e = 21, f = 20, g = 19, h = 18, i = 17, j = 16, k = 15, l = 14, m = 13,
            n = 12, o = 11, p = 10, q = 09, r = 08, s = 07, t = 06, u = 05, v = 04, w = 03, x = 02, y = 01, z = 00,

            A = 26, B = 27, C = 28, D = 29, E = 30, F = 31, G = 32, H = 33, I = 34, J = 35, K = 36, L = 37, M = 38,
            N = 39, O = 40, P = 41, Q = 42, R = 43, S = 44, T = 45, U = 46, V = 47, W = 48, X = 49, Y = 50, Z = 51
        }
        #endregion

        #region [EncryptFieldName]

        public static string EncryptFieldName(string strval)
        {
            string str = "";
            try
            {
                for (int i = 0; i < strval.Length; i++)
                {
                    str = str + EncryptFieldNameLogic(strval, i);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return str;
        }

        public static string EncryptFieldNameLogic(string strval, int i)
        {
            string str = "";
            try
            {
                string sChar = strval.Substring(i, (strval.Length - (strval.Length - 1)));

                if (sChar == " ")
                {
                    str = str + "52".ToString();
                }
                else
                {
                    var ActualValue = (Int32)((Letters)Enum.Parse(typeof(Letters), sChar));

                    if (ActualValue.ToString() == sChar)
                    {
                        str = str + Enum.GetName(typeof(Letters), Convert.ToInt32(sChar)).ToString();
                    }
                    else
                    {
                        if (Convert.ToInt32(ActualValue) < 10)
                            str = str + "0" + ActualValue.ToString();
                        else
                            str = str + ActualValue.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return str;
        }
        #endregion

        # region [FillRegistryDATA()]
        public DataSet FillRegistryDATA(string KeyName)
        {
            try
            {
                SetConnectionString(KeyName);
                DataSet ds = new DataSet();
                ds.Tables.Add("RegistryDATA");
                if (!ds.Tables[0].Columns.Contains("regApplicationName"))
                {
                    DataColumn regApplicationName = new DataColumn("regApplicationName");
                    regApplicationName.DataType = System.Type.GetType("System.String");
                    regApplicationName.DefaultValue = "";
                    ds.Tables[0].Columns.Add(regApplicationName);
                }
                if (!ds.Tables[0].Columns.Contains("regSuperAdminPassword"))
                {
                    DataColumn regSuperAdminPassword = new DataColumn("regSuperAdminPassword");
                    regSuperAdminPassword.DataType = System.Type.GetType("System.String");
                    regSuperAdminPassword.DefaultValue = "";
                    ds.Tables[0].Columns.Add(regSuperAdminPassword);
                }
                if (!ds.Tables[0].Columns.Contains("regNoOfUser"))
                {
                    DataColumn regNoOfUser = new DataColumn("regNoOfUser");
                    regNoOfUser.DataType = System.Type.GetType("System.String");
                    regNoOfUser.DefaultValue = "";
                    ds.Tables[0].Columns.Add(regNoOfUser);
                }
                if (!ds.Tables[0].Columns.Contains("regValidity"))
                {
                    DataColumn regValidity = new DataColumn("regValidity");
                    regValidity.DataType = System.Type.GetType("System.String");
                    regValidity.DefaultValue = "";
                    ds.Tables[0].Columns.Add(regValidity);
                }
                if (!ds.Tables[0].Columns.Contains("regNoOfBranches"))
                {
                    DataColumn regNoOfBranches = new DataColumn("regNoOfBranches");
                    regNoOfBranches.DataType = System.Type.GetType("System.String");
                    regNoOfBranches.DefaultValue = "";
                    ds.Tables[0].Columns.Add(regNoOfBranches);
                }
                if (!ds.Tables[0].Columns.Contains("regCompanyName"))
                {
                    DataColumn regCompanyName = new DataColumn("regCompanyName");
                    regCompanyName.DataType = System.Type.GetType("System.String");
                    regCompanyName.DefaultValue = "";
                    ds.Tables[0].Columns.Add(regCompanyName);
                }
                if (!ds.Tables[0].Columns.Contains("regCompanyShortName"))
                {
                    DataColumn regCompanyShortName = new DataColumn("regCompanyShortName");
                    regCompanyShortName.DataType = System.Type.GetType("System.String");
                    regCompanyShortName.DefaultValue = "";
                    ds.Tables[0].Columns.Add(regCompanyShortName);
                }
                if (!ds.Tables[0].Columns.Contains("regNoOfModule"))
                {
                    DataColumn regNoOfModule = new DataColumn("regNoOfModule");
                    regNoOfModule.DataType = System.Type.GetType("System.String");
                    regNoOfModule.DefaultValue = "";
                    ds.Tables[0].Columns.Add(regNoOfModule);
                }
                if (!ds.Tables[0].Columns.Contains("regLicenceNo"))
                {
                    DataColumn regLicenceNo = new DataColumn("regLicenceNo");
                    regLicenceNo.DataType = System.Type.GetType("System.String");
                    regLicenceNo.DefaultValue = "";
                    ds.Tables[0].Columns.Add(regLicenceNo);
                }
                if (!ds.Tables[0].Columns.Contains("regUseFlag"))
                {
                    DataColumn regUseFlag = new DataColumn("regUseFlag");
                    regUseFlag.DataType = System.Type.GetType("System.String");
                    regUseFlag.DefaultValue = "";
                    ds.Tables[0].Columns.Add(regUseFlag);
                }
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                ds.Tables[0].Rows[0]["regApplicationName"] = DataLayer.DataLinker.regApplicationName;
                ds.Tables[0].Rows[0]["regSuperAdminPassword"] = DataLayer.DataLinker.regSuperAdminPassword;
                ds.Tables[0].Rows[0]["regNoOfUser"] = DataLayer.DataLinker.regNoOfUser;
                ds.Tables[0].Rows[0]["regValidity"] = DataLayer.DataLinker.regValidity;
                ds.Tables[0].Rows[0]["regNoOfBranches"] = DataLayer.DataLinker.regNoOfBranches;
                ds.Tables[0].Rows[0]["regCompanyName"] = DataLayer.DataLinker.regCompanyName;
                ds.Tables[0].Rows[0]["regNoOfModule"] = DataLayer.DataLinker.regNoOfModule;
                ds.Tables[0].Rows[0]["regLicenceNo"] = DataLayer.DataLinker.regLicenceNo;
                ds.Tables[0].Rows[0]["regUseFlag"] = DataLayer.DataLinker.regUseFlag;
                ds.Tables[0].Rows[0]["regCompanyShortName"] = DataLayer.DataLinker.regCompanyShortName;

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        # region [Registry Data]
        public string DataLinkerRegApplicationName()
        {
            try
            {
                return regApplicationName;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DataLinkerRegSuperAdminPassword()
        {
            try
            {
                return regSuperAdminPassword;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DataLinkerRegNoOfUser()
        {
            try
            {
                return regNoOfUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DataLinkerRegValidity()
        {
            try
            {
                return regValidity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DataLinkerRegNoOfBranches()
        {
            try
            {
                return regNoOfBranches;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DataLinkerRegCompanyName()
        {
            try
            {
                return regCompanyName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DataLinkerRegCompanyShortName()
        {
            try
            {
                return regCompanyShortName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DataLinkerRegNoOfModule()
        {
            try
            {
                return regNoOfModule;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DataLinkerRegLicenceNo()
        {
            try
            {
                return regLicenceNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DataLinkerRegUseFlag()
        {
            try
            {
                return regUseFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DataLinkersqlConnectionString()
        {
            try
            {
                return sqlConnectionString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public static string ExecuteSPScalar(string sp_name, params SqlParameter[] cmdParams)
        {
            string sRetVal = "";
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sp_name;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = cmd;
                if (cmdParams != null)
                {
                    foreach (SqlParameter param in cmdParams)
                        cmd.Parameters.Add(param);
                }
                System.Data.DataSet ds = new DataSet();
                adapter.Fill(ds);
                cmd.Dispose();
                conn.Close();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                        sRetVal = Convert.ToString(ds.Tables[0].Rows[0][0]);
                    else
                        sRetVal = "";

                }
                else
                    sRetVal = "";
                ds.Dispose();
                return sRetVal;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }

        public static System.Data.DataTable ExecuteSPDataTable(string sp_name, params SqlParameter[] cmdParams)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sp_name;
                cmd.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = cmd;

                if (cmdParams != null)
                {
                    foreach (SqlParameter param in cmdParams)
                        cmd.Parameters.Add(param);
                }

                System.Data.DataTable ds = new DataTable();
                adapter.Fill(ds);
                cmd.Dispose();
                conn.Close();
                return ds;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }

        public static System.Data.DataTable ExecuteSPDataTable(string sp_name)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sp_name;
                cmd.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = cmd;

                System.Data.DataTable ds = new DataTable();
                adapter.Fill(ds);
                cmd.Dispose();
                conn.Close();
                return ds;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }

        public static System.Data.DataSet ExecuteSPDataSet(string sp_name, params SqlParameter[] cmdParams)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sp_name;//"sp_addr_book_users_get";
                cmd.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = cmd;

                if (cmdParams != null)
                {
                    foreach (SqlParameter param in cmdParams)
                        cmd.Parameters.Add(param);
                }

                System.Data.DataSet ds = new DataSet();
                adapter.Fill(ds);
                cmd.Dispose();
                conn.Close();
                return ds;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }

        /// //////////////Added On 15 APR 2019  ////////////////
        #region  [Get TableNames,ColumnNames,SpNames]
        public static System.Data.DataTable GetDatabaseTableNames()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {

                conn.Open();
                var dbName = conn.Database;
                cmd.Connection = conn;
                cmd.CommandText = "select object_id,name from " + dbName + ".sys.tables";
                cmd.CommandType = CommandType.Text;
                adapter.SelectCommand = cmd;

                System.Data.DataTable ds = new DataTable();
                //ds = conn.GetSchema("Tables");   Also Usable
                adapter.Fill(ds);
                conn.Close();
                return ds;
            }
            catch
            {
                conn.Close();
                throw;
            }

        }

        public static System.Data.DataTable GetDatabaseSPNames()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {

                conn.Open();
                var dbName = conn.Database;
                cmd.Connection = conn;
                cmd.CommandText = "select object_id,name from " + dbName + ".sys.procedures Where (name like '%Get%') or (name like '%get%') or (name like '%Display%') or (name like '%Display%')";
                cmd.CommandType = CommandType.Text;
                adapter.SelectCommand = cmd;

                System.Data.DataTable ds = new DataTable();

                adapter.Fill(ds);
                conn.Close();
                return ds;
            }
            catch
            {
                conn.Close();
                throw;
            }

        }

        public static System.Data.DataTable GetTableFieldNames(string tableName)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {

                conn.Open();
                var dbName = conn.Database;
                cmd.Connection = conn;
                cmd.CommandText = "select ORDINAL_POSITION,COLUMN_NAME from " + dbName + ".INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tableName + "'";
                cmd.CommandType = CommandType.Text;
                adapter.SelectCommand = cmd;

                System.Data.DataTable ds = new DataTable();

                adapter.Fill(ds);
                conn.Close();
                return ds;
            }
            catch
            {
                conn.Close();
                throw;
            }

        }

        public static System.Data.DataTable GetSPColumnName(string SPName)
        {
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(SPName, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = cmd.ExecuteReader();
                    return reader.GetSchemaTable();
                }
            }
            catch
            {
                conn.Close();
                throw;
            }
        }
        #endregion
    }
}
