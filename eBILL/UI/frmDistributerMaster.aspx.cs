﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.Data;

namespace eBILL.UI
{
    public partial class frmDistributerMaster : System.Web.UI.Page
    {
        # region [Company_ID Property]
        public int Company_ID
        {
            get { return ViewState["Company_ID"] != null ? (int)ViewState["Company_ID"] : 0; }
            set { ViewState["Company_ID"] = value; }
        }
        # endregion

        # region [Branch_ID Property]
        public int Branch_ID
        {
            get { return ViewState["Branch_ID"] != null ? (int)ViewState["Branch_ID"] : 0; }
            set { ViewState["Branch_ID"] = value; }
        }
        # endregion

        # region [FY_ID Property]
        public int FY_ID
        {
            get { return ViewState["FY_ID"] != null ? (int)ViewState["FY_ID"] : 0; }
            set { ViewState["FY_ID"] = value; }
        }
        # endregion

        # region [Role_ID Property]
        public int Role_ID
        {
            get { return ViewState["Role_ID"] != null ? (int)ViewState["Role_ID"] : 0; }
            set { ViewState["Role_ID"] = value; }
        }
        # endregion

        # region [dsFillCustomer Property]
        public DataSet dsFillCustomer
        {
            get { return ViewState["dsFillCustomer"] != null ? (DataSet)ViewState["dsFillCustomer"] : null; }
            set { ViewState["dsFillCustomer"] = value; }
        }
        # endregion

        ManageDistributor OBJDistributor = new ManageDistributor();
        DataTable aGetCompany = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Branch_ID = Convert.ToInt32(Session["BranchID"].ToString());
                    Company_ID = Convert.ToInt32(Session["CompanyID"].ToString());
                    FY_ID = Convert.ToInt32(Session["FinancialYearId"].ToString());
                    Role_ID = Convert.ToInt32(Session["RoleId"].ToString());
                    txtCloseDate.Text = System.DateTime.Now.ToString("dd/MMM/yyyy");

                    //OBJDistributor.intDistributerID = Company_ID;
                    //OBJDistributor.intDistributerID = Branch_ID;
                    //OBJDistributor.intDistributerID = FY_ID;
                    //aGetCompany = OBJDistributor.aGetCompanyDetails();
                    //cmdCompany.DataSource = aGetCompany.Columns[0];
                    //cmdCompany.DataTextField = "strCompanyName";
                    //cmdCompany.DataValueField = "Company_ID";

                    if (Request.QueryString["intDistributerID"] != null)
                    {
                        ControlEnableDisable(false);
                        btnSave.Visible = false;
                        btnUpdate.Visible = true;

                        HIDDistId.Value = Request.QueryString["intDistributerID"].ToString();
                        DataTable aGetDistrbuterDetails = new DataTable();
                        OBJDistributor.intDistributerID = Convert.ToInt32(Request.QueryString["intDistributerID"].ToString());
                        aGetDistrbuterDetails = OBJDistributor.GettblDistributerMaster();

                        if (aGetDistrbuterDetails.Rows.Count > 0)
                        {
                            HIDDistId.Value = aGetDistrbuterDetails.Rows[0]["intDistributerID"].ToString();
                            HIDDistCompany.Value = aGetDistrbuterDetails.Rows[0]["intCompanyID"].ToString();
                            txtDistCompany.Text = aGetDistrbuterDetails.Rows[0]["strCompanyName"].ToString();
                            txtDistName.Text = aGetDistrbuterDetails.Rows[0]["strDistributerName"].ToString();
                            txtDistAddress.Text = aGetDistrbuterDetails.Rows[0]["strDistributerAddress"].ToString();
                            txtDistWarehouse.Text = aGetDistrbuterDetails.Rows[0]["strDistributerWarehouse"].ToString();
                            txtDistPhone.Text = aGetDistrbuterDetails.Rows[0]["strDistributerPhoneNo"].ToString();
                            txtMobile.Text = aGetDistrbuterDetails.Rows[0]["strDistributerMobileNo"].ToString();
                            txtEmail.Text = aGetDistrbuterDetails.Rows[0]["strDistributerEmail"].ToString();
                            txtFAX.Text = aGetDistrbuterDetails.Rows[0]["strDistributerFAX"].ToString();
                            chkClose.Checked = aGetDistrbuterDetails.Rows[0]["strCloseFlag"].ToString() == "Y" ? true : false;
                            if (chkClose.Checked == true)
                            {
                                chkClose_CheckedChanged(null, null);
                                txtCloseDate.Text = aGetDistrbuterDetails.Rows[0]["strCloseDate"].ToString();
                                txtCloseRemark.Text = aGetDistrbuterDetails.Rows[0]["strCloseRemark"].ToString();
                            }
                            else
                            {
                                txtCloseDate.Text = System.DateTime.Now.ToString("dd/MMM/yyyy");
                                txtCloseRemark.Text = "";
                            }
                            txtPINCode.Text = aGetDistrbuterDetails.Rows[0]["intPINCode"].ToString();
                            txtDistCode.Text = aGetDistrbuterDetails.Rows[0]["strDistributerCode"].ToString();
                            ddlState.SelectedItem.Text = aGetDistrbuterDetails.Rows[0]["strDistributorState"].ToString();
                            txtCity.Text = aGetDistrbuterDetails.Rows[0]["strDistributorCity"].ToString();
                        }
                    }
                }
                setSearchButton();
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void ControlEnableDisable(bool bVal)
        {
            txtDistName.Enabled = bVal;
            btnDistCompany.Visible = bVal;
            txtDistCompany.Enabled = bVal;
            //txtDistAddress.Enabled = bVal;
            //txtDistWarehouse.Enabled = bVal;
            //txtDistPhone.Enabled = bVal;
            //txtMobile.Enabled = bVal;
            //txtEmail.Enabled = bVal;
            //txtFAX.Enabled = bVal;
            //chkClose.Enabled = bVal;
            //txtCloseRemark.Enabled = bVal;
        }

        private void setSearchButton()
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "setsearchButton('frmDistributerMaster','" + HIDDistCompany.ClientID + "','" + txtDistCompany.ClientID + "','strCompanyName','SelectDistibutorCompany');", true);
        }

        protected void chkClose_CheckedChanged(object sender, EventArgs e)
        {
            if (chkClose.Checked == true)
                divClose.Visible = true;
            else
                divClose.Visible = false;
        }

        protected void Page_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string aStatus = string.Empty;
                if (e.CommandName == "btnSave")
                {
                    OBJDistributor.intDistributerID = HIDDistId.Value == "" ? 0 : Convert.ToInt32(HIDDistId.Value);
                    OBJDistributor.intCompanyID = HIDDistCompany.Value == "" ? 0 : Convert.ToInt32(HIDDistCompany.Value);
                    OBJDistributor.strDistributerName = txtDistName.Text != "" ? txtDistName.Text.Trim() : "";
                    OBJDistributor.strDistributerAddress = txtDistAddress.Text != "" ? txtDistAddress.Text.Trim() : "";
                    OBJDistributor.strDistributerWarehouse = txtDistWarehouse.Text != "" ? txtDistWarehouse.Text.Trim() : "";
                    OBJDistributor.strDistributerPhoneNo = txtDistPhone.Text != "" ? txtDistPhone.Text.Trim() : "";
                    OBJDistributor.strDistributerMobileNo = txtMobile.Text != "" ? txtMobile.Text.Trim() : "";
                    OBJDistributor.strDistributerEmail = txtEmail.Text != "" ? txtEmail.Text.Trim() : "";
                    OBJDistributor.strDistributerFAX = txtFAX.Text != "" ? txtFAX.Text.Trim() : "";
                    OBJDistributor.strCloseFlag = chkClose.Checked ? "Y" : "N";
                    if (chkClose.Checked == true)
                    {
                        OBJDistributor.intCloseByID = Convert.ToInt32(Role_ID);
                        OBJDistributor.strCloseDate = "";
                        OBJDistributor.strCloseRemark = txtCloseRemark.Text != "" ? txtCloseRemark.Text.Trim() : "";
                    }
                    else
                    {
                        OBJDistributor.intCloseByID = 0;
                        OBJDistributor.strCloseDate = "";
                        OBJDistributor.strCloseRemark = "";
                    }
                    OBJDistributor.intPINCode = txtPINCode.Text != "" ? Convert.ToInt32(txtPINCode.Text.Trim()) : 0;
                    OBJDistributor.strDistributorState = ddlState.SelectedItem.Text != "" ? ddlState.SelectedItem.Text : "";
                    OBJDistributor.strDistributorCity = txtCity.Text != "" ? txtCity.Text.Trim() : "";
                    aStatus = OBJDistributor.ManagetblDistributerMaster();
                    if (aStatus == "Duplicate")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(3);", true);
                        return;
                    }
                    else if (aStatus != "")
                    {
                        if (aStatus == HIDDistId.Value)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(1);", true);
                        }
                        else if (aStatus != "")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(2);", true);
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "CloseDiv();", true);
                    }
                }
                else if (e.CommandName == "btnClear")
                {
                    txtDistCode.Text = string.Empty;
                    txtDistName.Text = string.Empty;
                    txtDistCompany.Text = string.Empty;
                    HIDDistCompany.Value = "0";
                    txtDistAddress.Text = string.Empty;
                    txtDistWarehouse.Text = string.Empty;

                    txtDistPhone.Text = string.Empty;
                    txtMobile.Text = string.Empty;
                    txtEmail.Text = string.Empty;
                    txtFAX.Text = string.Empty;
                    txtPINCode.Text = string.Empty;

                    chkClose.Checked = false;
                    txtCloseDate.Text = string.Empty;
                    txtCloseRemark.Text = string.Empty;
                }
                else if (e.CommandName == "btnUpdate")
                {
                    OBJDistributor.intDistributerID = HIDDistId.Value == "" ? 0 : Convert.ToInt32(HIDDistId.Value);
                    OBJDistributor.intCompanyID = HIDDistCompany.Value == "" ? 0 : Convert.ToInt32(HIDDistCompany.Value);
                    OBJDistributor.strDistributerName = txtDistName.Text != "" ? txtDistName.Text.Trim() : "";
                    OBJDistributor.strDistributerAddress = txtDistAddress.Text != "" ? txtDistAddress.Text.Trim() : "";
                    OBJDistributor.strDistributerWarehouse = txtDistWarehouse.Text != "" ? txtDistWarehouse.Text.Trim() : "";
                    OBJDistributor.strDistributerPhoneNo = txtDistPhone.Text != "" ? txtDistPhone.Text.Trim() : "";
                    OBJDistributor.strDistributerMobileNo = txtMobile.Text != "" ? txtMobile.Text.Trim() : "";
                    OBJDistributor.strDistributerEmail = txtEmail.Text != "" ? txtEmail.Text.Trim() : "";
                    OBJDistributor.strDistributerFAX = txtFAX.Text != "" ? txtFAX.Text.Trim() : "";
                    OBJDistributor.strCloseFlag = chkClose.Checked ? "Y" : "N";
                    if (chkClose.Checked == true)
                    {
                        OBJDistributor.intCloseByID = Convert.ToInt32(Role_ID);
                        OBJDistributor.strCloseDate = "";
                        OBJDistributor.strCloseRemark = txtCloseRemark.Text != "" ? txtCloseRemark.Text.Trim() : "";
                    }
                    else
                    {
                        OBJDistributor.intCloseByID = 0;
                        OBJDistributor.strCloseDate = "";
                        OBJDistributor.strCloseRemark = "";
                    }
                    OBJDistributor.intPINCode = txtPINCode.Text != "" ? Convert.ToInt32(txtPINCode.Text.Trim()) : 0;
                    OBJDistributor.strDistributorState = ddlState.SelectedItem.Text != "" ? ddlState.SelectedItem.Text : "";
                    OBJDistributor.strDistributorCity = txtCity.Text != "" ? txtCity.Text.Trim() : "";

                    aStatus = OBJDistributor.ManagetblDistributerMaster();
                    if (aStatus == "Duplicate")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(3);", true);
                        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert1", "alert('Record can not be duplicated,enter another Item...');", true);
                        return;
                    }
                    else if (aStatus != "")
                    {
                        if (aStatus == HIDDistId.Value)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(1);", true);
                        }
                        else if (aStatus != "")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(2);", true);
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "CloseDiv();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }
    }
}