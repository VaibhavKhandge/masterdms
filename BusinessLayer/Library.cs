﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataLayer;
using System.Web.UI.WebControls;
using System.IO;
//using ISNet.WebUI.WebGrid;
//using ISNet.WebUI;
//using ISNet.WebUI.Design;
using Microsoft.VisualBasic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Web.UI;

namespace BusinessLayer
{
    public class Library
    {
        public static string sqlConnectionSMS = System.Configuration.ConfigurationSettings.AppSettings["strsms"].ToString();
        public static string ServerPath = "";
        private DataLayer.DataServices ObjDataServices = new DataServices();

        WebProxy objProxy1 = null;

        # region [Constructor()]
        public Library()
        {

        }
        # endregion
        # region [Getcode_SP_Name Property]
        public string Getcode_SP_Name
        {
            get
            {
                return "ssp_GetCode";
            }
        }
        # endregion
        # region [SP_AutoFieldsReference Property]
        public string SP_AutoFieldsReference
        {
            get
            {
                return "ssp_GetReferenceAutoFillFields";
            }
        }
        # endregion
        # region [SP_AutoFillReference Property]
        public string SP_AutoFillReference
        {
            get
            {
                return "ssp_GetRecordReferenceAutoFillFields";
            }
        }
        # endregion
        # region [ChkUseFlag_SP_Name Property]
        public string ChkUseFlag_SP_Name
        {
            get
            {
                return "ssp_ChkUseFlag";
            }
        }
        # endregion
        # region [DataSelection_SP_Name Property]
        public string DataSelection_SP_Name
        {
            get
            {
                return "ssp_DataSelection";
            }
        }
        # endregion
        # region [CHKDuplication_SP_Name Property]
        public string CHKDuplication_SP_Name
        {
            get
            {
                return "ssp_CheckDuplication";
            }
        }
        # endregion
        # region [CheckReport_SP_Name Property]
        public string CheckReport_SP_Name
        {
            get
            {
                return "ssp_CheckReport";
            }
        }
        # endregion
        # region [SaveAuditHistory]
        public string SaveAuditHistory
        {
            get
            {
                return "ausp_SaveTblAuditHistory";
            }
        }
        # endregion
        # region [DisplayAuditHistory]
        public string DisplayAuditHistory
        {
            get
            {
                return "TblAuditHistory";
            }
        }
        # endregion
        # region [SaveCloseTransaction]
        public string SaveCloseTransaction
        {
            get
            {
                return "ausp_SaveTblCloseTransaction";
            }
        }
        # endregion
        # region [DisplayCloseTransaction]
        public string DisplayCloseTransaction
        {
            get
            {
                return "TblCloseTransactionSaveDetail";
            }
        }
        # endregion
        public DataSet GetDataSet(string SP_Name)
        {
            try
            {
                return (ObjDataServices.GetDataSet(SP_Name));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        # region [GetDataReportDrafter_SP_Name Property]
        public string GetDataReportDrafter_SP_Name
        {
            get
            {
                return "ssp_GetDataForReport";
            }
        }
        # endregion
        # region [SP_ErrorLog Property]
        public string SP_ErrorLog
        {
            get
            {
                return "ausp_savetblErrorLog";
            }
        }
        # endregion
        # region [SP_CreateXMLFile Property]
        public string SP_CreateXMLFile
        {
            get
            {
                return "ssp_GetFormControl";//
            }
        }
        # endregion

        # region [SP_GetBindRole Property]
        public string SP_GetBindRole
        {
            get
            {
                return "ssp_GetBindRole";
            }
        }
        # endregion
        # region [SP_GetBindRolePost Property]
        public string SP_GetBindRolePost
        {
            get
            {
                return "ssp_GetBindRolePost";
            }
        }
        # endregion

        # region [SP_Report_ssp_Report_GetTblBranchMaster Property]
        public string SP_Report_ssp_Report_GetTblBranchMaster
        {
            get
            {
                return "ssp_Report_GetTblBranchMaster";//
            }
        }
        # endregion

        # region [SP_Report_ssp_Report_GetRelatedDoc Property]
        public string SP_Report_ssp_Report_GetRelatedDoc
        {
            get
            {
                return "ssp_Report_GetRelatedDoc";//
            }
        }
        # endregion

        # region [SP_FillDDLSearch Property]
        public string SP_FillDDLSearch
        {
            get
            {
                return "ssp_GetQueryDataSet";//
            }
        }
        # endregion

        # region [SP_FillDDLSearchTab Property]
        public string SP_FillDDLSearchTab
        {
            get
            {
                return "ssp_getTabquerydataset";//
            }
        }
        # endregion

        # region [SP_UOMHelp Property]
        public string SP_UOMHelp
        {
            get
            {
                return "ssp_GetBindDetailUOM";
            }
        }
        # endregion

        # region [SP_AccountHelp Property]
        public string SP_AccountHelp
        {
            get
            {
                return "ssp_GetBindDetailAccountName";
            }
        }
        # endregion

        # region [SP_TariffHeadingNumberHelp Property]
        public string SP_TariffHeadingNumberHelp
        {
            get
            {
                return "ssp_GetBindDetailTariffHeadingNumber";
            }
        }
        # endregion

        # region [SP_GetConversionRate Property]
        public string SP_GetConversionRate
        {
            get
            {
                return "ssp_GetDisplayUOMConversionRate";
            }
        }
        # endregion

        # region [SP_FillConversionRate Property]
        public string SP_FillConversionRate
        {
            get
            {
                return "ssp_GetBindCurrencyConversionValues";//
            }
        }
        # endregion

        # region [SP_FillVendorInfo Property]
        public string SP_FillVendorInfo
        {
            get
            {
                return "ssp_GetVendorInfoForPL";//
            }
        }
        # endregion
        # region [SP_GetBranchMasterDetail Property]
        public string SP_GetBranchMasterDetail
        {
            get
            {
                return "ssp_GetDisplayTblBranchMaster";
            }
        }
        # endregion

        # region [BindGrid()]
        public DataTable BindGrid(string SP_Name, string Company_ID)
        {
            try
            {
                return ObjDataServices.BindGrid(SP_Name, Company_ID);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        # endregion

        # region [BindGrid()]
        public DataTable BindGrid(string SP_Name, string Company_ID, string Branch_ID)
        {
            try
            {
                object[] param = new object[2];
                param[0] = Company_ID;
                param[1] = Branch_ID;

                return ObjDataServices.GetDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void BindGrid1(string SP_Name, string Company_ID, string Branch_ID)
        {
            try
            {
                object[] param = new object[2];
                param[0] = Company_ID;
                param[1] = Branch_ID;
                ObjDataServices.GetDataSet1(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        # region [FillDataSet()]
        public DataSet FillDataSet(string SP_Name, string ID1, string ID2)
        {
            try
            {
                object[] param = new object[2];
                param[0] = ID1;
                param[1] = ID2;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        public DataSet FillDataSetMIS(string SP_Name, string ID1, string ID2, string ID3, string ID4)
        {
            try
            {
                object[] param = new object[4];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillDataSetMISNew(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5)
        {
            try
            {
                object[] param = new object[5];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #region [FillDataSet()]
        public DataSet FillDataSet(string SP_Name, int Id, string ID1)
        {

            try
            {
                object[] param = new object[2];
                param[0] = Id;
                param[1] = ID1;

                return ObjDataServices.FillDataSet(SP_Name, param);

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        # region [FillDataSet() Added by Satish]
        public int SaveQuery(string SP_Name, string ID1, string ID2)
        {
            try
            {
                object[] param = new object[2];
                param[0] = ID1;
                param[1] = ID2;

                return ObjDataServices.SaveQuery(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public int SaveQuery(string SP_Name, object[] param)
        {
            try
            {
                //object[] param = new object[2];
                //param[0] = ID1;
                //param[1] = ID2;

                return ObjDataServices.SaveQuery(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int SaveQuery(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5)
        {
            try
            {
                object[] param = new object[5];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;

                return ObjDataServices.SaveQuery(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        # region [FillDataSet()]
        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3)
        {
            try
            {
                object[] param = new object[3];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        public DataTable BindGrid(string SP_Name, object[] param)
        {
            try
            {
                return ObjDataServices.GetDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # region [FillDataSet()]
        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4)
        {
            try
            {
                object[] param = new object[4];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5)
        {
            try
            {
                object[] param = new object[5];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;
                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6)
        {
            try
            {
                object[] param = new object[6];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;
                param[5] = ID6;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7)
        {
            try
            {
                object[] param = new object[7];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;
                param[5] = ID6;
                param[6] = ID7;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8)
        {
            try
            {
                object[] param = new object[8];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;
                param[5] = ID6;
                param[6] = ID7;
                param[7] = ID8;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9)
        {
            try
            {
                object[] param = new object[9];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;
                param[5] = ID6;
                param[6] = ID7;
                param[7] = ID8;
                param[8] = ID9;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillDataSet(string SP_Name, object[] param)
        {
            try
            {
                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        # region [BindGrid()]
        public DataTable BindGrid(string SP_Name, string Company_ID, string Branch_ID, string FY_ID)
        {
            try
            {
                object[] param = new object[3];
                param[0] = Company_ID;
                param[1] = Branch_ID;
                param[2] = FY_ID;

                return ObjDataServices.GetDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        # region [BindGrid()]
        public DataTable BindGrid(string SP_Name, string Company_ID, string Branch_ID, string FY_ID, string fromDate, string toDate)
        {
            try
            {
                object[] param = new object[5];
                param[0] = Company_ID;
                param[1] = Branch_ID;
                param[2] = FY_ID;
                param[3] = fromDate;
                param[4] = toDate;

                return ObjDataServices.GetDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataTable BindGrid(string SP_Name, string Company_ID, string Branch_ID, string FY_ID, string fromDate, string toDate, string Role_ID, bool ALLFlag)
        {
            try
            {
                object[] param = new object[7];
                param[0] = Company_ID;
                param[1] = Branch_ID;
                param[2] = FY_ID;
                param[3] = fromDate;
                param[4] = toDate;
                param[5] = Role_ID;
                param[6] = ALLFlag.ToString();
                return ObjDataServices.GetDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        # region [BindGrid()]
        public DataTable BindGrid(string SP_Name, string Company_ID, string Branch_ID, string FY_ID, string Role_ID, bool ALLFlag)
        {
            try
            {
                object[] param = new object[5];
                param[0] = Company_ID;
                param[1] = Branch_ID;
                param[2] = FY_ID;
                param[3] = Role_ID;
                param[4] = ALLFlag.ToString();
                return ObjDataServices.GetDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public DataTable BindGrid(string SP_Name, string Company_ID, string Branch_ID, string FY_ID, string Role_ID, bool ALLFlag, string Where)
        {
            try
            {
                object[] param = new object[6];
                param[0] = Company_ID;
                param[1] = Branch_ID;
                param[2] = FY_ID;
                param[3] = Role_ID;
                param[4] = ALLFlag.ToString();
                param[5] = Where.ToString();
                return ObjDataServices.GetDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void EnableDisableEnclouserImage(DataTable dt, ref GridView Grid, string Encl, string Encl1)
        {
            try
            {
                for (int j = 0, i = 0; j < Grid.Rows.Count; j++, i++)
                    if (dt.Rows[j + (Grid.PageIndex * Grid.PageSize)]["intRecordId"].ToString() != "0")
                    {
                        ((ImageButton)Grid.Rows[i].FindControl(Encl)).Visible = true;
                        ((ImageButton)Grid.Rows[i].FindControl(Encl1)).Visible = false;
                    }
                    else
                    {
                        ((ImageButton)Grid.Rows[i].FindControl(Encl)).Visible = false;
                        ((ImageButton)Grid.Rows[i].FindControl(Encl1)).Visible = true;
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void EnableDisableCorrespondenceImage(DataTable dt, ref GridView Grid, string Correspondence, string Correspondence1)
        {
            try
            {
                for (int j = 0, i = 0; j < Grid.Rows.Count; j++, i++)
                    if (dt.Rows[j + (Grid.PageIndex * Grid.PageSize)]["intCorrespondenceFormId"].ToString() != "0")
                    {
                        ((ImageButton)Grid.Rows[i].FindControl(Correspondence)).Visible = true;
                        ((ImageButton)Grid.Rows[i].FindControl(Correspondence1)).Visible = false;
                    }
                    else
                    {
                        ((ImageButton)Grid.Rows[i].FindControl(Correspondence)).Visible = false;
                        ((ImageButton)Grid.Rows[i].FindControl(Correspondence1)).Visible = true;
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        # endregion

        # region [BindGrid()]
        public DataTable BindGrid(string SP_Name, string Company_ID, string Branch_ID, string Role_ID, bool ALLFlag)
        {
            try
            {
                object[] param = new object[4];
                param[0] = Company_ID;
                param[1] = Branch_ID;
                param[2] = Role_ID;
                param[3] = ALLFlag.ToString();
                return ObjDataServices.GetDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        # region [BindGrid()]
        //01-Aug-2009
        public DataTable BindGrid(string SP_Name, string Organization_ID, string Branch_ID, string fromDate, string toDate)
        {
            try
            {
                object[] param = new object[4];
                param[0] = Organization_ID;
                param[1] = Branch_ID;
                param[2] = fromDate;
                param[3] = toDate;

                return ObjDataServices.GetDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion
        # region [BindGrid()]
        public DataTable BindGrid(string SP_Name, string Company_ID, string Branch_ID, string FY_ID, string Role_ID, string Emp_Id, bool ALLFlag)
        {
            try
            {
                object[] param = new object[6];
                param[0] = Company_ID;
                param[1] = Branch_ID;
                param[2] = FY_ID;
                param[3] = Role_ID;
                param[4] = Emp_Id;
                param[5] = ALLFlag.ToString();
                return ObjDataServices.GetDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        #region [GetDates()]
        public void GetDates(ref RadioButton RBFYear, ref TextBox txtFromDate, ref TextBox txtToDate)
        {
            if (RBFYear.Checked)
            {
                string Apr = "01-Apr-" + System.DateTime.Today.Year.ToString(), Dec = "31-Dec-" + System.DateTime.Now.Year.ToString();
                if (Convert.ToDateTime(Apr) <= Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) && Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) <= Convert.ToDateTime(Dec))
                {
                    txtFromDate.Text = "01-Apr-" + System.DateTime.Today.Year;
                    txtToDate.Text = System.DateTime.Today.ToString("dd-MMM-yyyy");
                }
                else if (Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) <= Convert.ToDateTime("31-Mar-" + System.DateTime.Now.Year.ToString()))
                {
                    txtFromDate.Text = "01-Apr-" + Convert.ToString(Convert.ToInt32(System.DateTime.Today.Year) - 1);
                    txtToDate.Text = System.DateTime.Today.ToString("dd-MMM-yyyy");
                }
            }
            else
            {
                string Jul = "01-Jul-" + System.DateTime.Today.Year.ToString(), Dec = "31-Dec-" + System.DateTime.Now.Year.ToString();
                if (Convert.ToDateTime(Jul) <= Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) && Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) <= Convert.ToDateTime(Dec))
                {
                    txtFromDate.Text = "01-Jul-" + System.DateTime.Today.Year;
                    txtToDate.Text = System.DateTime.Today.ToString("dd-MMM-yyyy");
                }
                else if (Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) <= Convert.ToDateTime("30-Jun-" + System.DateTime.Now.Year.ToString()))
                {
                    txtFromDate.Text = "01-Jul-" + Convert.ToString(Convert.ToInt32(System.DateTime.Today.Year) - 1);
                    txtToDate.Text = System.DateTime.Today.ToString("dd-MMM-yyyy");
                }
            }
        }
        public void GetDates(ref TextBox txtFromDate, ref TextBox txtToDate)
        {
            string Apr = "01-Apr-" + System.DateTime.Today.Year.ToString(), Dec = "31-Dec-" + System.DateTime.Now.Year.ToString();
            if (Convert.ToDateTime(Apr) <= Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) && Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) <= Convert.ToDateTime(Dec))
            {
                txtFromDate.Text = "01-Apr-" + System.DateTime.Today.Year;
                txtToDate.Text = System.DateTime.Today.ToString("dd-MMM-yyyy");
            }
            else if (Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) <= Convert.ToDateTime("31-Mar-" + System.DateTime.Now.Year.ToString()))
            {
                txtFromDate.Text = "01-Apr-" + Convert.ToString(Convert.ToInt32(System.DateTime.Today.Year) - 1);
                txtToDate.Text = System.DateTime.Today.ToString("dd-MMM-yyyy");
            }
        }
        public void GetFinancialYear(ref TextBox txtFromDate, ref TextBox txtToDate)
        {
            string Apr = "01-Apr-" + System.DateTime.Today.Year.ToString(), Dec = "31-Dec-" + System.DateTime.Now.Year.ToString();
            if (Convert.ToDateTime(Apr) <= Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) && Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) <= Convert.ToDateTime(Dec))
            {
                txtFromDate.Text = "01-Apr-" + System.DateTime.Today.Year;
                txtToDate.Text = "31-Mar-" + Convert.ToString(Convert.ToInt32(System.DateTime.Today.Year) + 1);
            }
            else if (Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")) <= Convert.ToDateTime("31-Mar-" + System.DateTime.Now.Year.ToString()))
            {
                txtFromDate.Text = "01-Apr-" + Convert.ToString(Convert.ToInt32(System.DateTime.Today.Year) - 1);
                txtToDate.Text = "31-Mar-" + System.DateTime.Now.Year.ToString();
            }
        }

        //Added By Rupesh on Date 16 Apr 2011
        public void GetDates(ref TextBox txtFromDate, ref TextBox txtToDate, string strFinancialYear)
        {
            string[] str = strFinancialYear.Split('-');

            string Apr = "01-Apr-" + str[0], Dec = "31-Dec-" + str[0];
            string strMarch = "31-Mar-" + str[1];
            txtFromDate.Text = "01-Apr-" + str[0];
            if (Convert.ToDateTime(strMarch) <= Convert.ToDateTime(DateTime.Today.ToString("dd-MMM-yyyy")))
            {
                txtToDate.Text = strMarch;
            }
            else
            {
                txtToDate.Text = System.DateTime.Today.ToString("dd-MMM-yyyy");
            }



        }
        #endregion

        # region [FillDT()]
        public DataTable FillDT(string SP_Name)
        {
            try
            {
                return ObjDataServices.FillDT(SP_Name);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataTable FillDT(string SP_Name, string FormName, string ControlName)
        {
            try
            {
                return FillDataSet(SP_Name, FormName, ControlName).Tables[0];
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        # region [FillDS()]
        //public void FillDS(DataSet[] ds, string[] SP_Name, string SearchField1, string SearchField2)
        //{
        //    try
        //    {
        //        for (int i = 0; i < SP_Name.Length; i++)
        //        {
        //            ds[i] = ObjDataServices.FillDS(SP_Name[i], SearchField);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
        public void FillDS(DataSet[] ds, string[] SP_Name, string SearchField)
        {
            try
            {
                for (int i = 0; i < SP_Name.Length; i++)
                {
                    ds[i] = ObjDataServices.FillDS(SP_Name[i], SearchField);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        # region [FillDS()]
        public DataSet FillDS(string SP_Name, string SearchField)
        {
            try
            {
                return ObjDataServices.FillDS(SP_Name, SearchField);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        # region [FillDDLSearch()]
        public void FillDDLSearch(DropDownList DDLSearch, string Form_Name)
        {
            try
            {
                DataSet dsFillDDLSearch = ObjDataServices.FillDS(SP_FillDDLSearch, Form_Name);
                DDLSearch.AppendDataBoundItems = true;
                DDLSearch.Items.Add("...Select One...");
                DDLSearch.DataSource = dsFillDDLSearch.Tables[0];
                DDLSearch.DataTextField = "nvDisplayName";
                DDLSearch.DataValueField = "Common";
                DDLSearch.DataBind();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        //Added for Filter Tab
        # region [FillDDLSearchTab()]
        public void FillDDLSearchTab(DropDownList DDLSearch, string Form_Name)
        {
            try
            {
                DataSet dsFillDDLSearchTab = ObjDataServices.FillDS(SP_FillDDLSearchTab, Form_Name);
                DDLSearch.AppendDataBoundItems = true;
                DDLSearch.Items.Add("...Select One...");
                DDLSearch.DataSource = dsFillDDLSearchTab.Tables[0];
                DDLSearch.DataTextField = "strTabName";
                DDLSearch.DataValueField = "strTabName";
                DDLSearch.DataBind();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        public void FillDDLSearchNew(DropDownList DDLSearch, string Form_Name, string strQueryFieldName, string QueryFieldValue)
        {
            try
            {
                DataSet dsFillDDLSearch = FillDataSet("ssp_getFormControlTrasactionForSearchFeild", Form_Name, strQueryFieldName, QueryFieldValue);
                DDLSearch.AppendDataBoundItems = true;
                DDLSearch.Items.Add("...Select One...");
                DDLSearch.DataSource = dsFillDDLSearch.Tables[0];
                DDLSearch.DataTextField = "nvDisplayName";
                DDLSearch.DataValueField = "Common";
                DDLSearch.DataBind();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        #region [DataSet Table Matching()]
        public DataSet DSTableMatching(DataSet Ds, string TblName)
        {
            try
            {
                DataSet MatchDataSet = ObjDataServices.DSTableMatching(TblName);

                for (int i = 0; i < Ds.Tables[0].Columns.Count; i++)
                {
                    string flag = "Y";
                    for (int j = 0; j < MatchDataSet.Tables[0].Rows.Count; j++)
                    {

                        if (Ds.Tables[0].Columns[i].ToString().ToUpper().CompareTo(MatchDataSet.Tables[0].Rows[j]["Column_Name"].ToString().ToUpper()) == 0)
                        {
                            flag = "Y";
                            break;
                        }
                        else
                            flag = "N";
                    }
                    if (flag == "N")
                    {
                        Ds.Tables[0].Columns.Remove(Ds.Tables[0].Columns[i].ToString());
                        Ds.AcceptChanges();
                        i = i - 1;
                    }
                }
                return Ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable DSTableMatching(DataTable Ds, string TblName)
        {
            try
            {
                DataSet MatchDataSet = ObjDataServices.DSTableMatching(TblName);

                for (int i = 0; i < Ds.Columns.Count; i++)
                {
                    string flag = "Y";
                    for (int j = 0; j < MatchDataSet.Tables[0].Rows.Count; j++)
                    {
                        if (Ds.Columns[i].ToString().ToUpper().CompareTo(MatchDataSet.Tables[0].Rows[j]["Column_Name"].ToString().ToUpper()) == 0)
                        {
                            flag = "Y";
                            break;
                        }
                        else
                            flag = "N";
                    }
                    if (flag == "N")
                    {
                        Ds.Columns.Remove(Ds.Columns[i].ToString());
                        Ds.AcceptChanges();
                        i = i - 1;
                    }
                }
                return Ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion Get Columns Save

        #region [SaveData()]
        public string[] SaveData(DataSet ds, string SP_Name)
        {
            try
            {
                return ObjDataServices.SaveData(ds, SP_Name);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [InsertID()]
        public void InsertID(ref DataSet dataSet, string ID_Field, int ID)
        {
            try
            {
                for (int i = 0; i < dataSet.Tables.Count; i++)
                {
                    if (dataSet.Tables[i].Columns.Contains(ID_Field))
                    {
                        dataSet.Tables[i].Columns.Add(ID_Field);
                    }
                    for (int f = 0; f < dataSet.Tables[i].Rows.Count; i++)
                    {
                        dataSet.Tables[i].Rows[f][ID_Field] = ID;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [Update()]
        public int SavePlanning(DataSet dsHeader, int Id, string ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return ObjDataServices.SavePlanning(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Save(DataSet dsHeader, int Id, string ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return ObjDataServices.Save(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Save(DataSet dsHeader, int Id, string ID_Field, string SP_DeleteDetails, bool blRevision)
        {
            try
            {
                return ObjDataServices.Save(dsHeader, Id, ID_Field, SP_DeleteDetails, blRevision);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [Save()]
        public string[] Save(DataSet dsHeader, string ID_Field)
        {
            try
            {
                return ObjDataServices.Save(dsHeader, ID_Field);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] SaveAssetData(DataSet dsHeader, string ID, bool AddMode, string SP_DeleteDetails)
        {
            try
            {
                return ObjDataServices.SaveAssetData(dsHeader, ID, AddMode, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public int Save(DataSet dsHeader, string Id, string ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return ObjDataServices.Save(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region [ SaveDetail() ]
        public int SaveDetail(ref DataSet ds)
        {
            try
            {
                return ObjDataServices.SaveDetail(ref ds);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [SaveEnclosure()]
        public int SaveEnclosure(DataSet dsHeader, int[] Id, string[] ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return ObjDataServices.SaveEnclosure(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int SaveEnclosureCA(DataSet dsHeader, int[] Id, string[] ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return ObjDataServices.SaveEnclosureCA(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int SaveEnclosure(DataSet dsHeader, string[] Id, string[] ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return ObjDataServices.SaveEnclosure(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region FillDSHugeData
        public DataSet FillDSHugeData(string SP_Name, int TimeOut, string ID1)
        {
            SqlConnection objConn = new SqlConnection(sqlConnectionString());
            objConn.Open();

            SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.CommandTimeout = TimeOut;

            objCmd.Parameters.Add("@id", SqlDbType.Text);
            objCmd.Parameters["@id"].Value = ID1.ToString();

            SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
            DataSet objDS = new DataSet("pp");

            objDA.Fill(objDS);

            objConn.Close();

            return objDS;
        }
        #endregion

        #region SaveForSelectedBranch
        public int SaveForBranch(DataSet dsHeader)
        {
            try
            {
                return ObjDataServices.SaveForBranch(dsHeader);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int SaveForBranchFinal(DataSet dsHeader)
        {
            try
            {
                return ObjDataServices.SaveForBranchFinal(dsHeader);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int SaveDepreciation(DataSet dsHeader, bool AddMode)
        {
            try
            {
                return ObjDataServices.SaveDepreciation(dsHeader, AddMode);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [chkPendingApprovalRecord()]
        public Boolean CheckPendingApprovalStatus(string TableName, string Condition)
        {
            try
            {
                DataSet ds = ObjDataServices.chkDuplication(TableName, Condition, "ssp_CheckPendingApprovalStatus");
                return ds.Tables[0].Rows.Count > 0 ? false : true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [chkDuplication()]
        public Boolean chkDuplication(string TableName, string Condition)
        {
            try
            {
                DataSet ds = ObjDataServices.chkDuplication(TableName, Condition, CHKDuplication_SP_Name);
                return ds.Tables[0].Rows.Count > 0 ? false : true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void chkDuplication(string TableName, ref string TaxName, string Condition, string Column)
        {
            try
            {
                DataSet ds = ObjDataServices.chkDuplication(TableName, Condition, CHKDuplication_SP_Name);
                TaxName = ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0][Column].ToString() : "";
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [GridSearch()]
        public DataTable GridSearch(string TableName, string Condition)
        {
            try
            {
                DataSet ds = ObjDataServices.chkDuplication(TableName, Condition, CHKDuplication_SP_Name);
                return ds.Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [Delete()]
        public int Delete(string ID_Field, int Id, string SP_DeleteDetails)
        {
            try
            {
                return ObjDataServices.Delete(ID_Field, Id, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
                return 0;
            }
        }

        public int Delete(string ID_Field, string Id, string SP_DeleteDetails)
        {
            try
            {
                return ObjDataServices.Delete(ID_Field, Id, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
                return 0;
            }
        }

        public int Delete(string[] ID_Field, int[] Id, string SP_DeleteDetails)
        {
            try
            {
                return ObjDataServices.Delete(ID_Field, Id, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
                return 0;
            }
        }

        public int DeleteCA(string[] ID_Field, int[] Id, string SP_DeleteDetails)
        {
            try
            {
                return ObjDataServices.DeleteCA(ID_Field, Id, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
                return 0;
            }
        }

        public int Delete(string ID_Field, int Id, string SP_DeleteDetails, bool blRevision)
        {
            try
            {
                return ObjDataServices.Delete(ID_Field, Id, SP_DeleteDetails, blRevision);
            }
            catch (Exception)
            {
                throw;
                return 0;
            }
        }
        #endregion

        #region [ErrorLog()]
        public string ErrorLog(string Error_Message, string Form_Name, int User_Id, int Branch_Id, string Company_ID)
        {
            try
            {
                return ObjDataServices.ErrorLog(Error_Message, Form_Name, User_Id, Branch_Id, Company_ID, SP_ErrorLog);
            }
            catch (Exception)
            {
                throw;
                return "";
            }
        }
        #endregion

        #region [FincialAccountReport()]
        public DataSet FincialAccountReport(string SP_ReportName, int intOrganizationId, int intBranchId, int intFyId, string strDate)
        {
            try
            {
                return ObjDataServices.FincialAccountReport(SP_ReportName, intOrganizationId, intBranchId, intFyId, strDate);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [FincialAccountReport()]
        public DataSet FincialAccountReport(string SP_ReportName, int intOrganizationId, int intBranchId, int intFyId, string strDate, string strPrintFlag)
        {
            try
            {
                return ObjDataServices.FincialAccountReport(SP_ReportName, intOrganizationId, intBranchId, intFyId, strDate, strPrintFlag);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [FincialAccountReport()]
        public DataSet FincialAccountReport(string SP_ReportName, object[] param)
        {
            try
            {
                return ObjDataServices.FincialAccountReport(SP_ReportName, param);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [DesignControl()]
        #region [DesignControl()]
        public void DesignControl(WebControl[] Cntr, string XMLFile, string Form_Name)
        {
            try
            {
                DataSet Ds = new DataSet();
            ReadXML: if (File.Exists(XMLFile))
                {
                    Ds.ReadXml(XMLFile);
                    for (int i = 0; i < Cntr.Length; i++)
                    {
                        DataRow[] DrXML = new DataRow[Ds.Tables[0].Columns.Count];
                        DrXML = Ds.Tables[0].Select("ControlName='" + Cntr[i].ID + "' or LableName = '" + Cntr[i].ID + "'");
                        if (DrXML.Length != 0)
                        {
                            if (Cntr[i].ID.Substring(0, 3).ToString() == "LBL")
                            {
                                Label Lbl = (Label)Cntr[i];
                                Lbl.Text = DrXML[0]["NewCaption"].ToString();
                            }
                            if (Cntr[i].ID.Substring(0, 3).ToString() != "LBL")
                            {
                                Cntr[i].Enabled = Convert.ToBoolean(DrXML[0]["Enabale"].ToString());
                                Cntr[i].ToolTip = DrXML[0]["HelpText"].ToString();
                            }
                            Cntr[i].Visible = Convert.ToBoolean(DrXML[0]["Visibility"].ToString());
                            //Cntr[i].Width = Unit.Parse(DrXML[0]["CntrSize"].ToString());
                        }
                    }
                }
                else
                {
                    DataSet DsXML = ObjDataServices.FillDS(SP_CreateXMLFile, Form_Name);
                    DsXML.WriteXml(XMLFile); goto ReadXML;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public void DesignControlNew(WebControl[] Cntr, string XMLFile, string Form_Name)
        {
            try
            {
                DataSet Ds = new DataSet();
            ReadXML: if (File.Exists(XMLFile))
                {
                    Ds.ReadXml(XMLFile);
                    for (int i = 0; i < Cntr.Length; i++)
                    {
                        DataRow[] DrXML = new DataRow[Ds.Tables[0].Columns.Count];
                        if (Cntr[i] != null)
                        {
                            DrXML = Ds.Tables[0].Select("ControlName='" + Cntr[i].ID + "' or LableName = '" + Cntr[i].ID + "'");
                            if (DrXML != null)
                            {
                                if (Cntr[i].ID.Substring(0, 3).ToString().ToUpper() != "LBL" && Cntr[i].ID.Substring(0, 3).ToString().ToUpper() != "LNK" && !Cntr[i].ID.ToString().ToUpper().Contains("LABEL"))
                                {
                                    Cntr[i].Enabled = Convert.ToBoolean(DrXML[0]["Enabale"].ToString());
                                    Cntr[i].ToolTip = DrXML[0]["HelpText"].ToString();
                                    if (DrXML[0].Table.Columns.Contains("AccessKey"))
                                        Cntr[i].AccessKey = DrXML[0]["AccessKey"].ToString() != null ? DrXML[0]["AccessKey"].ToString() : GetFirstCharCaption(DrXML[0]["NewCaption"].ToString(), ((DrXML[0].Table.Columns.Contains("AccessKey") && DrXML[0]["AccessKey"].ToString() != null) ? DrXML[0]["AccessKey"].ToString() : ""));
                                }
                                else
                                {
                                    if (DrXML[0]["strLabelType"].ToString() == "LNK")
                                    {
                                        LinkButton Lnk = (LinkButton)Cntr[i];
                                        //Lnk.Text = DrXML[0]["NewCaption"].ToString();
                                        Lnk.Text = GetAccessKeyCaption(DrXML[0]["NewCaption"].ToString(), ((DrXML[0].Table.Columns.Contains("AccessKey") && DrXML[0]["AccessKey"].ToString() != null) ? DrXML[0]["AccessKey"].ToString() : ""));
                                        Lnk.Font.Underline = false;
                                    }
                                    else
                                    {
                                        Label Lbl = (Label)Cntr[i];
                                        Lbl.Text = GetAccessKeyCaption(DrXML[0]["NewCaption"].ToString(), ((DrXML[0].Table.Columns.Contains("AccessKey") && DrXML[0]["AccessKey"].ToString() != null) ? DrXML[0]["AccessKey"].ToString() : ""));
                                        if (DrXML[0].Table.Columns.Contains("AccessKey"))
                                            Lbl.AccessKey = DrXML[0]["AccessKey"].ToString() != null ? DrXML[0]["AccessKey"].ToString() : GetFirstCharCaption(DrXML[0]["NewCaption"].ToString(), ((DrXML[0].Table.Columns.Contains("AccessKey") && DrXML[0]["AccessKey"].ToString() != null) ? DrXML[0]["AccessKey"].ToString() : ""));
                                        if (DrXML[0].Table.Columns.Contains("ControlName"))
                                            Lbl.AssociatedControlID = DrXML[0]["ControlName"].ToString() != null ? DrXML[0]["ControlName"].ToString() : GetFirstCharCaption(DrXML[0]["NewCaption"].ToString(), ((DrXML[0].Table.Columns.Contains("AccessKey") && DrXML[0]["AccessKey"].ToString() != null) ? DrXML[0]["AccessKey"].ToString() : ""));
                                    }
                                }
                                if (DrXML[0].Table.Columns.Contains("intTabIndex"))
                                    Cntr[i].TabIndex = Convert.ToInt16(DrXML[0]["intTabIndex"].ToString());
                                else
                                    Cntr[i].TabIndex = 1;

                                Cntr[i].Visible = Convert.ToBoolean(DrXML[0]["Visibility"].ToString());
                            }
                        }
                    }
                }
                else
                {
                    DataSet DsXML = ObjDataServices.FillDS(SP_CreateXMLFile, Form_Name);
                    DsXML.WriteXml(XMLFile); goto ReadXML;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        protected string GetFirstCharCaption(string Caption, string AccessKey)
        {
            try
            {
                string CharWord = "";
                if (Caption != "")
                {
                    if (AccessKey == "")
                    {
                        if (Caption.Substring(0, 1).Contains("*") == true)
                        {
                            CharWord = Caption.Substring(1, 1);
                        }
                        else
                        {
                            CharWord = Caption.Substring(0, 1);
                        }
                    }
                    else
                    {
                        if (Caption.Contains(AccessKey))
                        {
                            CharWord = AccessKey;
                        }
                        else
                        {
                            if (Caption.Substring(0, 1).Contains("*") == true)
                            {
                                CharWord = Caption.Substring(1, 1);
                            }
                            else
                            {
                                CharWord = Caption.Substring(0, 1);
                            }
                        }
                    }
                }
                return CharWord;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected string GetAccessKeyCaption(string Caption, string AccessKey)
        {
            try
            {
                string NewCaption = "", InitialWord = "", RestWord = "", CharWord = "", NewWord = "";
                if (Caption != "")
                {
                    if (Caption.Substring(0, 1).Contains("*") == true)
                    {
                        if (AccessKey != "")
                            CharWord = Caption.Substring(1, 1);
                        else
                            CharWord = "<u>" + Caption.Substring(1, 1) + "</u>";

                        InitialWord = Caption.Substring(0, 1) + CharWord;
                        RestWord = Caption.Substring(2, Caption.Length - 2);
                    }
                    else
                    {
                        if (AccessKey != "")
                            InitialWord = Caption.Substring(0, 1);
                        else
                            InitialWord = "<u>" + Caption.Substring(0, 1) + "</u>";

                        RestWord = Caption.Substring(1, Caption.Length - 1);
                    }
                    NewCaption = InitialWord + RestWord;
                    if (AccessKey != "")
                    {
                        if (NewCaption.Contains(AccessKey))
                        {
                            //RestWord.IndexOf(AccessKey)
                            if (NewCaption.IndexOf(AccessKey) == 0)
                                NewWord = ("<u>" + NewCaption.Substring(NewCaption.IndexOf(AccessKey), NewCaption.IndexOf(AccessKey) + 1) + "</u>") +
                                   NewCaption.Substring(NewCaption.IndexOf(AccessKey) + 1, NewCaption.Length - (1 + NewCaption.IndexOf(AccessKey)));
                            else
                                NewWord = NewCaption.Substring(0, NewCaption.IndexOf(AccessKey)) +
                             ("<u>" + NewCaption.Substring(NewCaption.IndexOf(AccessKey), 1) + "</u>") +
                             NewCaption.Substring(NewCaption.IndexOf(AccessKey) + 1, NewCaption.Length - (1 + NewCaption.IndexOf(AccessKey)));
                            NewCaption = "";
                            NewCaption = NewWord;
                        }
                        else
                        {
                            if (InitialWord.Contains("*"))
                            {
                                NewCaption = InitialWord.Substring(0, 1) +
                                    ("<u>" + InitialWord.Substring(1, 1) + "</u>") +
                                    RestWord;
                            }
                            else
                                NewCaption = ("<u>" + InitialWord.Substring(0, 1) + "</u>") + RestWord;
                        }
                    }
                }
                return NewCaption;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        # region [FillReportListBox()]
        public void FillReportListBox(string Form_Name, CheckBoxList ObjListBox)
        {
            try
            {
                DataSet ds = ObjDataServices.FillDS(SP_Report_ssp_Report_GetRelatedDoc, Form_Name);
                ObjListBox.Items.Clear();
                if (ds != null && ds.Tables.Count > 0)
                {
                    ObjListBox.DataSource = ds.Tables[0];
                    ObjListBox.DataTextField = "DocName";
                    ObjListBox.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void FillReportListBox(string Form_Name, RadioButtonList ObjListBox, string RoleID)
        {
            try
            {
                DataSet ds = ObjDataServices.FillDataSet("ssp_Report_GetRelatedDocNew", Form_Name, RoleID);
                ObjListBox.Items.Clear();
                if (ds != null && ds.Tables.Count > 0)
                {
                    ObjListBox.DataSource = ds.Tables[0];
                    ObjListBox.DataTextField = "DocName";
                    ObjListBox.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void FillReportListBox(string Form_Name, RadioButtonList ObjListBox)
        {
            try
            {
                DataSet ds = ObjDataServices.FillDS(SP_Report_ssp_Report_GetRelatedDoc, Form_Name);
                ObjListBox.Items.Clear();
                if (ds != null && ds.Tables.Count > 0)
                {
                    ObjListBox.DataSource = ds.Tables[0];
                    ObjListBox.DataTextField = "DocName";
                    ObjListBox.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        # region [FillReportListBoxReport()]
        public void FillReportListBoxReport(string Form_Name, string ShortName, RadioButtonList ObjListBox)
        {
            try
            {
                DataSet ds = ObjDataServices.FillDataSet("ssp_GetDisplayTblRelatedDoc", ShortName, Form_Name);

                DataSet dsTemp = new DataSet();
                DataView dv = new DataView();
                dv = GetTempDS(ds, " strVisibleFlag ='Y'");
                dsTemp.Tables.Add(dv.ToTable().Copy());
                ds = dsTemp.Copy();

                ObjListBox.Items.Clear();
                if (ds != null && ds.Tables.Count > 0)
                {
                    ObjListBox.DataSource = ds.Tables[0];
                    ObjListBox.DataTextField = "DocName";
                    ObjListBox.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        #region GridClear()
        public DataSet GridClear(ref DataGrid ObjGrid, DataSet ObjDataSet)
        {
            try
            {
                if (ObjDataSet != null)
                {
                    ObjDataSet.Clear();
                    ObjGrid.DataSource = ObjDataSet;
                    ObjGrid.DataBind();
                }
                return ObjDataSet;
            }
            catch (Exception ex)
            {
                ObjDataSet.Clear();
                return ObjDataSet;
                throw;
            }
        }
        public DataSet GridClear(ref GridView ObjGrid, DataSet ObjDataSet)
        {
            try
            {
                if (ObjDataSet != null)
                {
                    ObjDataSet.Clear();
                    ObjGrid.DataSource = ObjDataSet;
                    ObjGrid.DataBind();
                }
                return ObjDataSet;
            }
            catch (Exception ex)
            {
                ObjDataSet.Clear();
                return ObjDataSet;
                throw;
            }
        }
        #endregion

        //#region WebGridClear()
        //public DataSet WebGridClear(ref ISNet.WebUI.WebGrid.WebGrid ObjGrid, DataSet ObjDataSet)
        //{
        //    try
        //    {
        //        if (ObjDataSet != null)
        //        {
        //            ObjDataSet.Clear();
        //            ObjGrid.DataSource = ObjDataSet;
        //            ObjGrid.DataBind();
        //        }
        //        return ObjDataSet;
        //    }
        //    catch (Exception ex)
        //    {
        //        ObjDataSet.Clear();
        //        return ObjDataSet;
        //        throw;
        //    }
        //}
        //#endregion

        #region SetRole()
        public DataTable SetRole(string Role_ID, string Form_Name, string Company_ID)
        {
            try
            {
                object[] param = new object[3];
                param[0] = Role_ID;
                param[1] = Form_Name;
                param[2] = Company_ID;
                return ObjDataServices.GetDataSet(SP_GetBindRole, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region SetRolePost()
        public DataTable SetRolePost(string Role_ID, string Form_Name, string Company_ID)
        {
            try
            {
                object[] param = new object[3];
                param[0] = Role_ID;
                param[1] = Form_Name;
                param[2] = Company_ID;
                return ObjDataServices.GetDataSet(SP_GetBindRolePost, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region ChkGridRecordDuplication()
        public Boolean ChkGridRecordDuplication(int index, string SearchField, string SearchData, DataSet dsSearch)
        {
            try
            {
                if (index != (dsSearch.Tables[0].Rows.Count - 1))
                {
                    for (int i = 0; i < dsSearch.Tables[0].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[0].Rows[i][SearchField].ToString() != "")
                        {
                            if (dsSearch.Tables[0].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }
                else if (index != 0 && dsSearch.Tables[0].Rows.Count != 1)
                {
                    for (int i = 0; i < dsSearch.Tables[0].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[0].Rows[i][SearchField].ToString() != "")
                        {
                            if (dsSearch.Tables[0].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                //Common.LogException(ex);
                return false;
            }
        }
        public Boolean ChkGridRecordDuplication(int index, string SearchField, string SearchData, DataSet dsSearch, int TableIndex)
        {
            try
            {
                if (index != (dsSearch.Tables[TableIndex].Rows.Count - 1))
                {
                    for (int i = 0; i < dsSearch.Tables[TableIndex].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[TableIndex].Rows[i][SearchField].ToString() != "")
                        {
                            if (dsSearch.Tables[TableIndex].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }
                else if (index != 0 && dsSearch.Tables[TableIndex].Rows.Count != 1)
                {
                    for (int i = 0; i < dsSearch.Tables[TableIndex].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[TableIndex].Rows[i][SearchField].ToString() != "")
                        {
                            if (dsSearch.Tables[TableIndex].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                //Common.LogException(ex);
                return false;
            }
        }
        public Boolean ChkGridRecordDuplication(int index, string SearchField, string SearchData, string SearchField1, string SearchData1, DataSet dsSearch)
        {
            try
            {
                if (index != (dsSearch.Tables[0].Rows.Count - 1))
                {
                    for (int i = 0; i < dsSearch.Tables[0].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[0].Rows[i][SearchField].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField1].ToString() != "")
                        {
                            if (dsSearch.Tables[0].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField1].ToString().ToLower() == SearchData1.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }
                else if (index != 0 && dsSearch.Tables[0].Rows.Count != 1)
                {
                    for (int i = 0; i < dsSearch.Tables[0].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[0].Rows[i][SearchField].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField1].ToString() != "")
                        {
                            if (dsSearch.Tables[0].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField1].ToString().ToLower() == SearchData1.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                //Common.LogException(ex);
                return false;
            }
        }

        public Boolean ChkGridRecordDuplication(int index, string SearchField, string SearchData, string SearchField1, string SearchData1, DataSet dsSearch, int TableIndex)
        {
            try
            {
                if (index != (dsSearch.Tables[TableIndex].Rows.Count - 1))
                {
                    for (int i = 0; i < dsSearch.Tables[TableIndex].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[TableIndex].Rows[i][SearchField].ToString() != "" && dsSearch.Tables[TableIndex].Rows[i][SearchField1].ToString() != "")
                        {
                            if (dsSearch.Tables[TableIndex].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim() && dsSearch.Tables[TableIndex].Rows[i][SearchField1].ToString().ToLower() == SearchData1.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }
                else if (index != 0 && dsSearch.Tables[TableIndex].Rows.Count != 1)
                {
                    for (int i = 0; i < dsSearch.Tables[TableIndex].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[TableIndex].Rows[i][SearchField].ToString() != "" && dsSearch.Tables[TableIndex].Rows[i][SearchField1].ToString() != "")
                        {
                            if (dsSearch.Tables[TableIndex].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim() && dsSearch.Tables[TableIndex].Rows[i][SearchField1].ToString().ToLower() == SearchData1.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                //Common.LogException(ex);
                return false;
            }
        }

        public Boolean ChkGridRecordDuplication(int index, string SearchField, string SearchData, string SearchField1, string SearchData1, string SearchField2, string SearchData2, DataSet dsSearch)
        {
            try
            {
                if (index != (dsSearch.Tables[0].Rows.Count - 1))
                {
                    for (int i = 0; i < dsSearch.Tables[0].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[0].Rows[i][SearchField].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField1].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField2].ToString() != "")
                        {
                            if (dsSearch.Tables[0].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField1].ToString().ToLower() == SearchData1.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField2].ToString().ToLower() == SearchData2.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }
                else if (index != 0 && dsSearch.Tables[0].Rows.Count != 1)
                {
                    for (int i = 0; i < dsSearch.Tables[0].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[0].Rows[i][SearchField].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField1].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField2].ToString() != "")
                        {
                            if (dsSearch.Tables[0].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField1].ToString().ToLower() == SearchData1.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField2].ToString().ToLower() == SearchData2.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                //Common.LogException(ex);
                return false;
            }
        }

        public Boolean ChkGridRecordDuplication(int index, string SearchField, string SearchData, string SearchField1, string SearchData1, string SearchField2, string SearchData2, string SearchField3, string SearchData3, DataSet dsSearch)
        {
            try
            {
                if (index != (dsSearch.Tables[0].Rows.Count - 1))
                {
                    for (int i = 0; i < dsSearch.Tables[0].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[0].Rows[i][SearchField].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField1].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField2].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField3].ToString() != "")
                        {
                            if (dsSearch.Tables[0].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField1].ToString().ToLower() == SearchData1.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField2].ToString().ToLower() == SearchData2.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField3].ToString().ToLower() == SearchData3.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }
                else if (index != 0 && dsSearch.Tables[0].Rows.Count != 1)
                {
                    for (int i = 0; i < dsSearch.Tables[0].Rows.Count; i++)
                    {
                        if (i != index && dsSearch.Tables[0].Rows[i][SearchField].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField1].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField2].ToString() != "" && dsSearch.Tables[0].Rows[i][SearchField3].ToString() != "")
                        {
                            if (dsSearch.Tables[0].Rows[i][SearchField].ToString().ToLower() == SearchData.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField1].ToString().ToLower() == SearchData1.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField2].ToString().ToLower() == SearchData2.ToLower().Trim() && dsSearch.Tables[0].Rows[i][SearchField3].ToString().ToLower() == SearchData3.ToLower().Trim())
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                //Common.LogException(ex);
                return false;
            }
        }
        #endregion

        #region [BuildReport()]
        public void BuildReport(string WhereClause, string ViewName, string SP_Name)
        {
            try
            {
                ObjDataServices.BuildReport(WhereClause, ViewName, SP_Name);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region [Posting()]
        public void Posting(string SP_Name, string strTblName, string strDate, string strEmpId, string strClause)
        {
            try
            {
                ObjDataServices.Posting(SP_Name, strTblName, strDate, strEmpId, strClause);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region TaxCalculation Function
        public DataSet GenerateTaxItemds()
        {
            DataSet Ds = new DataSet();
            try
            {
                Ds = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationGenerateTaxItemDS", "0");
                DataRow row;
                for (int I = 0; I < 25; I++)
                {
                    row = Ds.Tables[0].NewRow();
                    Ds.Tables[0].Rows.Add(row);
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["ItemId"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["Qty"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["Rate"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["AccessRate"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["AmortRate"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["ItemDiscount"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["GrossDiscount"] = 0;
                }
                return Ds;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GenerateTaxItemds(int cnt)
        {
            DataSet Ds = new DataSet();
            try
            {
                Ds = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationGenerateTaxItemDS", "0");
                DataRow row;
                for (int I = 0; I < cnt; I++)
                {
                    row = Ds.Tables[0].NewRow();
                    Ds.Tables[0].Rows.Add(row);
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["ItemId"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["Qty"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["Rate"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["AccessRate"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["AmortRate"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["ItemDiscount"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["GrossDiscount"] = 0;
                }
                return Ds;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GenerateTaxIdds()
        {
            DataSet Ds = new DataSet();
            try
            {
                Ds = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationGenerateTaxDS", "0");
                DataRow row;
                for (int I = 0; I < 25; I++)
                {
                    row = Ds.Tables[0].NewRow();
                    Ds.Tables[0].Rows.Add(row);
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxID"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxChargeValue"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxCharge"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["SurchargePercentage"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["Surcharge"] = 0;
                }
                return Ds;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GenerateTaxIdds(int cnt)
        {
            DataSet Ds = new DataSet();
            try
            {
                Ds = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationGenerateTaxDS", "0");
                DataRow row;
                for (int I = 0; I < cnt; I++)
                {
                    row = Ds.Tables[0].NewRow();
                    Ds.Tables[0].Rows.Add(row);
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxID"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxChargeValue"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxCharge"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["SurchargePercentage"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["Surcharge"] = 0;
                }
                return Ds;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GenerateTaxIddsWithOtherTax()
        {
            DataSet Ds = new DataSet();
            try
            {
                Ds = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationGenerateTaxDSForOtherTaxes", "0");
                DataRow row;
                for (int I = 0; I < 25; I++)
                {
                    row = Ds.Tables[0].NewRow();
                    Ds.Tables[0].Rows.Add(row);
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxID"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxChargeValue"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxCharge"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["SurchargePercentage"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["Surcharge"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["strTaxType"] = "";
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["strGroup"] = "";
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["dblOnTAmount"] = 0;
                }
                return Ds;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GenerateTaxIddsWithOtherTax(int cnt)
        {
            DataSet Ds = new DataSet();
            try
            {
                Ds = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationGenerateTaxDSForOtherTaxes", "0");
                DataRow row;
                for (int I = 0; I < cnt; I++)
                {
                    row = Ds.Tables[0].NewRow();
                    Ds.Tables[0].Rows.Add(row);
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxID"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxChargeValue"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["TaxCharge"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["SurchargePercentage"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["Surcharge"] = 0;
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["strTaxType"] = "";
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["strGroup"] = "";
                    Ds.Tables[0].Rows[Ds.Tables[0].Rows.Count - 1]["dblOnTAmount"] = 0;
                }
                return Ds;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void TaxCalulation(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges)//, Page p)
        {
            dblGrossAmt = 0.00;
            double dblAccessAmt = 0.00;
            double dblpercent = 0.00;
            double dblGrossAmtCal = 0.00;
            string strpaytype = "";
            string strGrossApp = "";
            string strAssessApp = "";
            Boolean ChkFalg;

            for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
            {
                if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                {
                    dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                {
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                {
                    double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
            }
            dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt;
            dblDiscountAmt = 0.00;
            if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
            {
                if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                {
                    dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
                else
                {
                    dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
            }
            dblNetAmt = dblGrossAmt - dblDiscountAmt;
            DataSet DsTmp = new DataSet();
            DataTable DtTmp = new DataTable();

            DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
            if (DtTmp.Rows.Count > 0)
            {
                for (int i = 0; i < DtTmp.Rows.Count; i++)
                {
                    if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                        dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                    else
                        dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                }
            }

            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                {
                    dblTaxAmt = 0;
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                            strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                        }
                    }
                    else
                    {
                        dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                        strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                        strGrossApp = "Y";
                        strAssessApp = "N";
                    }

                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                            {
                                //if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                {
                                    ChkFalg = true;
                                    for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                    {
                                        if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                        {
                                            ChkFalg = false;
                                            if (strpaytype == "A")
                                            {
                                                dblTaxAmt = dblTaxAmt;
                                            }
                                            else
                                            {
                                                dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                            }
                                        }
                                    }
                                    if (ChkFalg == true)
                                    {
                                        //Response.Write("<script>alert(' One of the Taxes Is Not Added...</script>");
                                        //ScriptManager.RegisterStartupScript(p, p.GetType(), Guid.NewGuid().ToString(), "alert('One of the Taxes Is Not Added...')", true);
                                    }
                                }
                                else
                                {
                                    dblTaxAmt = 0;
                                    if (strGrossApp == "Y")
                                    {
                                        if (strpaytype == "A")
                                            dblTaxAmt = dblpercent;
                                        else
                                            dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                                    }
                                    else if (strAssessApp == "Y")
                                    {
                                        if (strpaytype == "A")
                                            dblTaxAmt = dblpercent;
                                        else
                                            dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                                    }
                                }
                            }
                        }
                        dblSurChargeAmt = 0;
                        if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                        {
                            dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                        }
                    }
                    else
                    {
                        dblTaxAmt = 0;
                        if (strGrossApp == "Y")
                        {
                            if (strpaytype == "A")
                                dblTaxAmt = dblpercent;
                            else
                                dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                        }
                        else if (strAssessApp == "Y")
                        {
                            if (strpaytype == "A")
                                dblTaxAmt = dblpercent;
                            else
                                dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                        }
                    }
                    dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(Math.Round(dblTaxAmt, 0).ToString("######0.00"));
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(Math.Round(dblSurChargeAmt, 0).ToString("######0.00"));
                }
            }
            dblTaxTotal = 0;
            dblCharges = 0;
            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                else
                    dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
            }
            dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
        }

        public void TaxCalulationOld(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, string ID, string CustomerVendorFlag)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.00;
                double dblAccessAmt = 0.00;
                double dblpercent = 0.00;
                double dblGrossAmtCal = 0.00;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.00;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        dblTaxAmt = 0;
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            }
                        }
                        else
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = "Y";
                            strAssessApp = "N";
                        }

                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            if (ID.ToString() == "")
                                ID = "0";
                            ////////////// Getting tax detail from CustomerTaxFormula/VendorTaxFormula detail , If no records from their it will come from taxdetail in tax master
                            if (CustomerVendorFlag == "C")
                            {
                                DsTmp = FillDataSet("ssp_GetTaxChargeCalculationTblCustomerTaxFomulaDetail", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]), ID.ToString());
                            }
                            else if (CustomerVendorFlag == "V")
                                DsTmp = FillDataSet("ssp_GetTaxChargeCalculationTblVendorTaxFomulaDetail", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]), ID.ToString());

                            if (DsTmp.Tables[0].Rows.Count == 0)
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    //if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        ChkFalg = true;
                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                            {
                                                ChkFalg = false;
                                                if (strpaytype == "A")
                                                {
                                                    dblTaxAmt = dblTaxAmt;
                                                }
                                                else
                                                {
                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                                }
                                            }
                                        }
                                        if (ChkFalg == true)
                                        {
                                            //Response.Write("<script>alert(' One of the Taxes Is Not Added...</script>");
                                            //ScriptManager.RegisterStartupScript(p, p.GetType(), Guid.NewGuid().ToString(), "alert('One of the Taxes Is Not Added...')", true);
                                        }
                                    }
                                    else
                                    {
                                        dblTaxAmt = 0;
                                        if (strGrossApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblpercent;
                                            else
                                                dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblpercent;
                                            else
                                                dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                                        }
                                    }
                                }
                            }
                            dblSurChargeAmt = 0;
                            if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                            {
                                dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                            }
                        }
                        else
                        {
                            dblTaxAmt = 0;
                            if (strGrossApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                            }
                            else if (strAssessApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(Math.Round(dblTaxAmt, 0).ToString("######0.00"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(Math.Round(dblSurChargeAmt, 0).ToString("######0.00"));
                    }
                }
                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    else
                        dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulation(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, string ID, string CustomerVendorFlag)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.00;
                double dblAccessAmt = 0.00;
                double dblpercent = 0.00;
                double dblGrossAmtCal = 0.00;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strNone = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.00;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        dblTaxAmt = 0;
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                                //  strWtType = DsTmp.Tables[0].Rows[0]["strWeightType"].ToString();
                            }
                        }
                        //Calculation 
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                            if (strpaytype == "P")
                        {
                            #region Tax Calculation For Percent Type
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            // dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    //   dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            // dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            //  dblCalOnVal = dblCalOnVal + Amt;

                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(Math.Round(dblTaxAmt, 0).ToString("######0.00"));
                        //dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    else
                        dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion TaxCalculation Function

        #region TaxCalculationWithoutRoundUp
        public void TaxCalculationWithoutRoundUp(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges)//, Page p)
        {
            dblGrossAmt = 0.000;
            double dblAccessAmt = 0.000;
            double dblpercent = 0.00;
            double dblGrossAmtCal = 0.000;
            string strpaytype = "";
            string strGrossApp = "";
            string strAssessApp = "";
            Boolean ChkFalg;

            for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
            {
                if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                {
                    dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                {
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                {
                    double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
            }
            dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt;
            dblDiscountAmt = 0.00;
            if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
            {
                if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                {
                    dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
                else
                {
                    dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
            }
            dblNetAmt = dblGrossAmt - dblDiscountAmt;
            DataSet DsTmp = new DataSet();
            DataTable DtTmp = new DataTable();

            DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
            if (DtTmp.Rows.Count > 0)
            {
                for (int i = 0; i < DtTmp.Rows.Count; i++)
                {
                    if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                        dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                    else
                        dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                }
            }

            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                {
                    dblTaxAmt = 0;
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                            strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                        }
                    }
                    else
                    {
                        dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                        strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                        strGrossApp = "Y";
                        strAssessApp = "N";
                    }

                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                            {
                                //if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                {
                                    ChkFalg = true;
                                    for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                    {
                                        if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                        {
                                            ChkFalg = false;
                                            if (strpaytype == "A")
                                            {
                                                dblTaxAmt = dblTaxAmt;
                                            }
                                            else
                                            {
                                                dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                            }
                                        }
                                    }
                                    if (ChkFalg == true)
                                    {
                                        //Response.Write("<script>alert(' One of the Taxes Is Not Added...</script>");
                                        //ScriptManager.RegisterStartupScript(p, p.GetType(), Guid.NewGuid().ToString(), "alert('One of the Taxes Is Not Added...')", true);
                                    }
                                }
                                else
                                {
                                    dblTaxAmt = 0;
                                    if (strGrossApp == "Y")
                                    {
                                        if (strpaytype == "A")
                                            dblTaxAmt = dblpercent;
                                        else
                                            dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                                    }
                                    else if (strAssessApp == "Y")
                                    {
                                        if (strpaytype == "A")
                                            dblTaxAmt = dblpercent;
                                        else
                                            dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                                    }
                                }
                            }
                        }
                        dblSurChargeAmt = 0;
                        if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                        {
                            dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                        }
                    }
                    else
                    {
                        dblTaxAmt = 0;
                        if (strGrossApp == "Y")
                        {
                            if (strpaytype == "A")
                                dblTaxAmt = dblpercent;
                            else
                                dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                        }
                        else if (strAssessApp == "Y")
                        {
                            if (strpaytype == "A")
                                dblTaxAmt = dblpercent;
                            else
                                dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                        }
                    }
                    dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.###"));
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.###"));

                }
            }
            dblTaxTotal = 0;
            dblCharges = 0;
            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                //if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                //    dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                //else
                //    dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                {
                    double a = 0.000;
                    a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    double b = 0.000;
                    b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                }
                else
                {
                    double c = 0.000;
                    c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    dblCharges = dblCharges + Math.Round(c, 0);
                }
            }
            dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
        }

        //New With Additional Taxes And Charges
        public void TaxCalculationWithoutRoundUpNewForPI(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges, ref double dblAssAmt, double dblCftWt, double dblTotWt)//, Page p)
        {
            dblGrossAmt = 0.000;
            double dblAccessAmt = 0.000;
            double dblpercent = 0.00;
            double dblGrossAmtCal = 0.000;
            string strpaytype = "";
            string strGrossApp = "";
            string strAssessApp = "";
            string strNone = "";
            string strWtType = "";
            Boolean ChkFalg;
            double dblQty = 0.000;

            for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
            {
                if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                {
                    dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                {
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                {
                    double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                dblQty = dblQty + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString());
            }
            dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt;
            dblDiscountAmt = 0.00;
            if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
            {
                if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                {
                    dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
                else
                {
                    dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
            }
            dblNetAmt = dblGrossAmt - dblDiscountAmt;
            DataSet DsTmp = new DataSet();
            DataTable DtTmp = new DataTable();

            DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
            if (DtTmp.Rows.Count > 0)
            {
                for (int i = 0; i < DtTmp.Rows.Count; i++)
                {
                    if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                        dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                    else
                        dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                }
            }

            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {

                //Master Details
                if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                {

                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                            strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                            dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                            //Add Group And Tax Type
                            dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                            dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                        }
                    }
                    else
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                        dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                        strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                            strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                            dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                            dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            strWtType = DsTmp.Tables[0].Rows[0]["strWeightType"].ToString();
                        }
                    }
                    //Calculation 

                    double dblCalOnVal = 0;
                    dblTaxAmt = 0;

                    if (strpaytype == "A")
                    {
                        dblTaxAmt = dblpercent;
                    }
                    else
                        if (strpaytype == "P")
                    {
                        #region Tax Calculation For Percentage Case
                        //Check Tax Type C/T
                        DsTmp = new DataSet();
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        }
                        else
                        {
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        }

                        //DS Temp 
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                            {
                                if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                    {
                                        Double Amt = 0;

                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                            {
                                                Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                            }
                                            if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                            {
                                                Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                            }
                                        }
                                        dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                        //Calculated On value
                                        dblCalOnVal = dblCalOnVal + Amt;
                                    }
                                    else
                                    {
                                        Double Amt = 0;
                                        ChkFalg = true;
                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                            {
                                                Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                dblCalOnVal = dblCalOnVal + Amt;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (strGrossApp == "Y")
                                    {
                                        Double Amt = 0;
                                        Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                        dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                        dblCalOnVal = dblCalOnVal + Amt;
                                    }
                                    else if (strAssessApp == "Y")
                                    {
                                        Double Amt = 0;
                                        Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                        dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                        dblCalOnVal = dblCalOnVal + Amt;
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    else
                            if (strpaytype == "Q")
                    {
                        dblTaxAmt = dblpercent * dblQty;
                        dblCalOnVal = dblQty;
                    }
                    else
                    {
                        if (strWtType == "C")
                        {
                            dblTaxAmt = dblCftWt * dblpercent;
                            dblCalOnVal = dblCftWt;
                        }
                        else
                        {
                            dblTaxAmt = dblTotWt * dblpercent;
                            dblCalOnVal = dblTotWt;
                        }

                    }
                    dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                    dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                }
            }

            // End Of Calculation
            dblTaxTotal = 0;
            dblCharges = 0;
            dblAdditionalTaxes = 0;
            dblAdditionalCharges = 0;
            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);
                    }
                    else
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                    }
                }
                else
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        // dblAdditionalCharges = dblAdditionalCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblCharges = dblCharges + Math.Round(c, 3);
                    }
                }
            }
            dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
            dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
            dblAssAmt = dblAccessAmt;
        }

        public void TaxCalculationWithoutRoundUpForMultipleItemNewForPI(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges, double dblHeaderGrossAmt, double TotItemQty, double dblHeaderAssAmt, double dblCFTWT, double dblTotWt)//, Page p)
        {
            dblGrossAmt = 0.000;
            double dblAccessAmt = 0.000;
            double dblpercent = 0.00;
            double dblGrossAmtCal = 0.000;
            string strpaytype = "";
            string strGrossApp = "";
            string strAssessApp = "";
            string strNone = "";
            string strWtType = "";
            Boolean ChkFalg;
            double dblAssesAmtCal = 0.000;
            double dblQty = 0.000;

            for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
            {
                if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                {
                    dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                {
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                {
                    double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                dblQty = dblQty + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString());
            }
            dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt;
            dblDiscountAmt = 0.00;
            if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
            {
                if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                {
                    dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
                else
                {
                    dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
            }
            dblNetAmt = dblGrossAmt - dblDiscountAmt;
            DataSet DsTmp = new DataSet();
            DataTable DtTmp = new DataTable();

            DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
            if (DtTmp.Rows.Count > 0)
            {
                for (int i = 0; i < DtTmp.Rows.Count; i++)
                {
                    if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                        dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                    else
                        dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                }
            }
            //Tax Calculation
            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                //Master Details
                if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                {

                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                            strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                            dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                            //Add Group And Tax Type
                            dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                            dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                        }
                    }
                    else
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                        dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                        strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                            strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                            dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                            dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            strWtType = DsTmp.Tables[0].Rows[0]["strWeightType"].ToString();
                        }
                    }
                    //Calculation 

                    double dblCalOnVal = 0;
                    dblTaxAmt = 0;

                    if (strpaytype == "A")
                    {
                        if (strGrossApp == "Y")
                        {
                            if (dblGrossAmtCal > 0)
                                dblTaxAmt = (dblGrossAmtCal * (dblpercent / dblHeaderGrossAmt));
                            else
                                dblTaxAmt = (dblpercent / TotItemQty);
                        }
                        else if (strAssessApp == "Y")
                        {
                            if (dblAccessAmt > 0)
                                dblTaxAmt = (dblAccessAmt * (dblpercent / dblHeaderAssAmt));
                            else
                                dblTaxAmt = (dblpercent / TotItemQty);
                        }
                        else
                        {
                            //For None Case Gross Take for By fergation of amount
                            dblTaxAmt = (dblpercent / TotItemQty);
                        }
                    }
                    else

                        if (strpaytype == "P")
                    {
                        #region TaxCalculation For Percent Case
                        DsTmp = new DataSet();
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        }
                        else
                        {
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        }
                        //DS Temp 
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                            {
                                if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                    {
                                        Double Amt = 0;

                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                            {
                                                Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                            }
                                            if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                            {
                                                Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                            }
                                        }
                                        dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                        //Calculated On value
                                        dblCalOnVal = dblCalOnVal + Amt;
                                    }
                                    else
                                    {
                                        Double Amt = 0;
                                        ChkFalg = true;
                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                            {
                                                Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                dblCalOnVal = dblCalOnVal + Amt;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (strGrossApp == "Y")
                                    {
                                        Double Amt = 0;
                                        Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                        dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                        dblCalOnVal = dblCalOnVal + Amt;
                                    }
                                    else if (strAssessApp == "Y")
                                    {
                                        Double Amt = 0;
                                        Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                        dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                        dblCalOnVal = dblCalOnVal + Amt;
                                    }
                                }
                            }
                        }
                        #endregion
                        //Check Tax Type C/T                          
                    }
                    else

                            if (strpaytype == "Q")
                    {
                        dblTaxAmt = dblpercent * dblQty;
                        dblCalOnVal = dblQty;
                    }
                    else
                    {
                        if (strWtType == "C")
                        {
                            dblTaxAmt = dblCFTWT * dblpercent;
                            dblCalOnVal = dblCFTWT;
                        }
                        else
                        {
                            dblTaxAmt = dblTotWt * dblpercent;
                            dblCalOnVal = dblTotWt;
                        }
                    }
                    dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                    dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                }
            }
            // End Of Calculation

            dblTaxTotal = 0;
            dblCharges = 0;
            dblAdditionalTaxes = 0;
            dblAdditionalCharges = 0;
            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);
                    }
                    else
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                    }
                }
                else
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        // dblAdditionalCharges = dblAdditionalCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblCharges = dblCharges + Math.Round(c, 3);
                    }
                }
            }
            dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
            dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
        }

        public void TaxCalculationWithoutRoundUp(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges)//, Page p)
        {
            dblGrossAmt = 0.000;
            double dblAccessAmt = 0.000;
            double dblpercent = 0.00;
            double dblGrossAmtCal = 0.000;
            string strpaytype = "";
            string strGrossApp = "";
            string strAssessApp = "";
            Boolean ChkFalg;

            for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
            {
                if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                {
                    dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                {
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                {
                    double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
            }
            dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt;
            dblDiscountAmt = 0.00;
            if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
            {
                if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                {
                    dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
                else
                {
                    dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
            }
            dblNetAmt = dblGrossAmt - dblDiscountAmt;
            DataSet DsTmp = new DataSet();
            DataTable DtTmp = new DataTable();

            DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
            if (DtTmp.Rows.Count > 0)
            {
                for (int i = 0; i < DtTmp.Rows.Count; i++)
                {
                    if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                        dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                    else
                        dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                }
            }

            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                {
                    dblTaxAmt = 0;
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                            strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                        }
                    }
                    else
                    {
                        dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                        strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                        strGrossApp = "Y";
                        strAssessApp = "N";
                    }

                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                            {
                                //if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                    {
                                        Double Amt = 0;

                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                            {
                                                Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                            }
                                            if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                            {
                                                Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                            }
                                        }

                                        if (strpaytype == "A")
                                        {
                                            dblTaxAmt = dblTaxAmt;
                                        }
                                        else
                                        {
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                        }
                                    }
                                    else
                                    {
                                        ChkFalg = true;
                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                            {
                                                ChkFalg = false;
                                                if (strpaytype == "A")
                                                {
                                                    dblTaxAmt = dblTaxAmt;
                                                }
                                                else
                                                {
                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100) / 100));
                                                }
                                            }
                                        }
                                        if (ChkFalg == true)
                                        {
                                            //Response.Write("<script>alert(' One of the Taxes Is Not Added...</script>");
                                            //ScriptManager.RegisterStartupScript(p, p.GetType(), Guid.NewGuid().ToString(), "alert('One of the Taxes Is Not Added...')", true);
                                        }
                                    }
                                }
                                else
                                {
                                    //dblTaxAmt = 0;
                                    if (strGrossApp == "Y")
                                    {
                                        if (strpaytype == "A")
                                            dblTaxAmt = dblTaxAmt + dblpercent;
                                        else
                                            dblTaxAmt = dblTaxAmt + ((dblGrossAmtCal * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100) * dblpercent) / 100;
                                    }
                                    else if (strAssessApp == "Y")
                                    {
                                        if (strpaytype == "A")
                                            dblTaxAmt = dblTaxAmt + dblpercent;
                                        else
                                            dblTaxAmt = dblTaxAmt + (((dblAccessAmt * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100) * dblpercent) / 100;
                                    }
                                }
                            }
                        }
                        dblSurChargeAmt = 0;
                        if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                        {
                            dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                        }
                    }
                    else
                    {
                        dblTaxAmt = 0;
                        if (strGrossApp == "Y")
                        {
                            if (strpaytype == "A")
                                dblTaxAmt = dblpercent;
                            else
                                dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                        }
                        else if (strAssessApp == "Y")
                        {
                            if (strpaytype == "A")
                                dblTaxAmt = dblpercent;
                            else
                                dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                        }
                    }
                    dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.###"));
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.###"));
                }
            }
            dblTaxTotal = 0;
            dblCharges = 0;
            dblAdditionalTaxes = 0;
            dblAdditionalCharges = 0;
            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);
                    }
                    else
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                    }
                }
                else
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        // dblAdditionalCharges = dblAdditionalCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblCharges = dblCharges + Math.Round(c, 3);
                    }
                }
            }
            dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
            dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
        }

        // For calculating tax For taxchargeType Amt(A) and for multiple Item
        public void TaxCalculationWithoutRoundUpForMultipleItem(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges, double dblHeaderGrossAmt)//, Page p)
        {
            dblGrossAmt = 0.000;
            double dblAccessAmt = 0.000;
            double dblpercent = 0.00;
            double dblGrossAmtCal = 0.000;
            string strpaytype = "";
            string strGrossApp = "";
            string strAssessApp = "";
            Boolean ChkFalg;
            double dblAssesAmtCal = 0.000;

            for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
            {
                if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                {
                    dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                {
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                {
                    double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
            }
            dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt;
            dblDiscountAmt = 0.00;
            if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
            {
                if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                {
                    dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
                else
                {
                    dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
            }
            dblNetAmt = dblGrossAmt - dblDiscountAmt;
            DataSet DsTmp = new DataSet();
            DataTable DtTmp = new DataTable();

            DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
            if (DtTmp.Rows.Count > 0)
            {
                for (int i = 0; i < DtTmp.Rows.Count; i++)
                {
                    if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                        dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                    else
                        dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                }
            }

            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                {
                    dblTaxAmt = 0;
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                            strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                        }
                    }
                    else
                    {
                        dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                        strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                        strGrossApp = "Y";
                        strAssessApp = "N";
                    }

                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                            {
                                //if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                {

                                    if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                    {
                                        Double Amt = 0;

                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                            {
                                                Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                            }
                                            if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                            {
                                                Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                            }
                                        }

                                        if (strpaytype == "A")
                                        {
                                            dblTaxAmt = dblTaxAmt;
                                        }
                                        else
                                        {
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                        }
                                    }
                                    else
                                    {
                                        ChkFalg = true;
                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                            {
                                                ChkFalg = false;
                                                if (strpaytype == "A")
                                                {
                                                    dblTaxAmt = dblTaxAmt;
                                                }
                                                else
                                                {
                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100) / 100));
                                                }
                                            }
                                        }
                                        if (ChkFalg == true)
                                        {
                                            //Response.Write("<script>alert(' One of the Taxes Is Not Added...</script>");
                                            //ScriptManager.RegisterStartupScript(p, p.GetType(), Guid.NewGuid().ToString(), "alert('One of the Taxes Is Not Added...')", true);
                                        }
                                    }
                                }
                                else
                                {
                                    dblTaxAmt = 0;
                                    if (strGrossApp == "Y")
                                    {
                                        if (strpaytype == "A")
                                            dblTaxAmt = (dblGrossAmtCal * (dblpercent / dblHeaderGrossAmt));
                                        else
                                            dblTaxAmt = ((dblGrossAmtCal * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100) * dblpercent) / 100;
                                    }
                                    else if (strAssessApp == "Y")
                                    {
                                        if (strpaytype == "A")
                                            dblTaxAmt = (dblGrossAmtCal * (dblpercent / dblHeaderGrossAmt));
                                        else
                                            dblTaxAmt = ((dblAccessAmt * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100) * dblpercent) / 100;
                                    }
                                }
                            }
                        }
                        dblSurChargeAmt = 0;
                        if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                        {
                            dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                        }
                    }
                    else
                    {
                        dblTaxAmt = 0;
                        if (strGrossApp == "Y")
                        {
                            if (strpaytype == "A")
                                dblTaxAmt = (dblGrossAmtCal * (dblpercent / dblHeaderGrossAmt));
                            //dblTaxAmt = dblpercent;
                            else
                                dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                        }
                        else if (strAssessApp == "Y")
                        {
                            if (strpaytype == "A")
                                dblTaxAmt = (dblGrossAmtCal * (dblpercent / dblHeaderGrossAmt));
                            // dblTaxAmt = dblpercent;
                            else
                                dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                        }
                    }
                    dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.###"));
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.###"));

                }
            }
            dblTaxTotal = 0;
            dblCharges = 0;
            dblAdditionalTaxes = 0;
            dblAdditionalCharges = 0;
            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {

                if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);
                    }
                    else
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                    }
                }
                else
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        // dblAdditionalCharges = dblAdditionalCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblCharges = dblCharges + Math.Round(c, 3);
                    }
                }
            }
            dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
            dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
        }

        public void TaxCalulationWithOutRoundUpWithCustomerVendorFlag(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, string ID, string CustomerVendorFlag)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.000;
                double dblAccessAmt = 0.000;
                double dblpercent = 0.00;
                double dblGrossAmtCal = 0.000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.00;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        dblTaxAmt = 0;
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            }
                        }
                        else
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = "Y";
                            strAssessApp = "N";
                        }

                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            if (ID.ToString() == "")
                                ID = "0";
                            ////////////// Getting tax detail from CustomerTaxFormula/VendorTaxFormula detail , If no records from their it will come from taxdetail in tax master
                            if (CustomerVendorFlag == "C")
                            {
                                DsTmp = FillDataSet("ssp_GetTaxChargeCalculationTblCustomerTaxFomulaDetail", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]), ID.ToString());
                            }
                            else if (CustomerVendorFlag == "V")
                                DsTmp = FillDataSet("ssp_GetTaxChargeCalculationTblVendorTaxFomulaDetail", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]), ID.ToString());

                            if (DsTmp.Tables[0].Rows.Count == 0)
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    //if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        ChkFalg = true;
                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                            {
                                                ChkFalg = false;
                                                if (strpaytype == "A")
                                                {
                                                    dblTaxAmt = dblTaxAmt;
                                                }
                                                else
                                                {
                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                                }
                                            }
                                        }
                                        if (ChkFalg == true)
                                        {
                                            //Response.Write("<script>alert(' One of the Taxes Is Not Added...</script>");
                                            //ScriptManager.RegisterStartupScript(p, p.GetType(), Guid.NewGuid().ToString(), "alert('One of the Taxes Is Not Added...')", true);
                                        }
                                    }
                                    else
                                    {
                                        dblTaxAmt = 0;
                                        if (strGrossApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblpercent;
                                            else
                                                dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblpercent;
                                            else
                                                dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                                        }
                                    }
                                }
                            }
                            dblSurChargeAmt = 0;
                            if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                            {
                                dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                            }
                        }
                        else
                        {
                            dblTaxAmt = 0;
                            if (strGrossApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                            }
                            else if (strAssessApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.###"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.###"));

                        //dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(Math.Round(dblTaxAmt, 0).ToString("######0.00"));
                        //if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        //    dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(Math.Round(dblSurChargeAmt, 0).ToString("######0.00"));
                    }
                }
                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                        //dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblCharges = dblCharges + Math.Round(c, 0);
                        //dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    }
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Delete SubGrid Records
        public DataSet DeleteSubDetailRecords(int ID, DataSet ds, string IDCol)
        {
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
            {
                if (Convert.ToInt32(ds.Tables[0].Rows[j][IDCol]) == ID)
                {
                    ds.Tables[0].Rows[j].Delete();
                    ds.AcceptChanges();
                    j--;
                }
            }
            return ds;
        }

        public DataSet DeleteSubDetailRecordsString(string ID1, DataSet ds, string IDCol)
        {
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
            {
                if (Convert.ToString(ds.Tables[0].Rows[j][IDCol].ToString()) == ID1)
                {
                    ds.Tables[0].Rows[j].Delete();
                    ds.AcceptChanges();
                    j--;
                }
            }
            return ds;
        }

        //protected DataSet DeleteSubDetailRecords(string Filter, DataSet ds)
        //{
        //    DataSet dsCopy = new DataSet();
        //    DataSet dsDelete = new DataSet();
        //    if (ds.Tables[0].Rows.Count != 0)
        //    {
        //        dsDelete.Tables.Add(GetTempDS(ds, Filter.ToString()).ToTable().Copy());
        //        dsCopy.Merge(dsDelete);
        //        ds.Tables.Remove(ds.Tables[0]);
        //        ds = dsCopy.Copy();
        //    }
        //    return ds;
        //}
        #endregion

        #region GerTempDS For Grid
        public DataView GetTempDS(DataSet DS, int ID, string ColID)
        {
            DataView dv = new DataView(DS.Tables[0]);
            string strCriteriaId = "" + ColID + "" + ID;
            dv.RowFilter = strCriteriaId;
            return dv;
        }

        public DataView GetTempDS(DataSet DS, string ID, string ColID)
        {
            DataView dv = new DataView(DS.Tables[0]);
            string strCriteriaId = "" + ColID + "" + ID + "";
            dv.RowFilter = strCriteriaId;
            return dv;
        }

        public DataView GetTempDS(DataSet DS, string FilterCriteria)
        {
            DataView dv = new DataView(DS.Tables[0]);
            dv.RowFilter = FilterCriteria;
            return dv;
        }

        public DataSet GetTempDSRet(DataSet DS, string FilterCriteria)
        {
            DataSet ds = new DataSet();
            DataView dv = new DataView(DS.Tables[0]);
            dv.RowFilter = FilterCriteria;
            ds.Tables.Add(dv.ToTable());
            return ds;
        }

        public DataTable GetTempTable(DataTable DT, string FilterCriteria)
        {
            DataTable dt = DT.Clone();
            DataView dv = new DataView(DT);
            dv.RowFilter = FilterCriteria;
            dt.Merge(dv.ToTable());
            return dt;
        }
        #endregion

        #region [ChkUseFlag()]
        public Boolean ChkUseFlag(string strTblName, string strFldName, Int32 intID)
        {
            try
            {
                return ObjDataServices.ChkUseFlag(ChkUseFlag_SP_Name, strTblName, strFldName, intID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region [ChkUseFlagBranchwise()]
        public Boolean ChkUseFlagBranchwise(string strTblName, string strFldName, Int32 intID, Int32 intBranchID)
        {
            try
            {
                return ObjDataServices.ChkUseFlagBranchwise("ssp_ChkUseFlagForBranch", strTblName, strFldName, intID, intBranchID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        # region [GetConversionRate]
        public string GetConversionRate(string SP_Name, string ID1, string ID2, string ID3, string Wt)
        {
            try
            {
                if (ID1 != ID2)
                {
                    String FromUomId = ID1;
                    String ToUomId = ID2;
                    DataSet dsRate = FillDataSet(SP_Name, FromUomId, ToUomId);

                    if (dsRate.Tables[0].Rows.Count > 0)
                    {
                        double dblConvRate = Convert.ToDouble(dsRate.Tables[0].Rows[0][2]);
                        ID3 = (Convert.ToDouble(Wt) * dblConvRate).ToString();
                    }

                    if (dsRate.Tables[0].Rows.Count <= 0)
                    {
                        FromUomId = ID2;
                        ToUomId = ID1;
                        dsRate = FillDataSet(SP_Name, FromUomId, ToUomId);

                        if (dsRate.Tables[0].Rows.Count > 0)
                        {
                            double dblConvRate = Convert.ToDouble(dsRate.Tables[0].Rows[0][2]);
                            ID3 = (Convert.ToDouble(Wt) / dblConvRate).ToString();
                        }
                    }
                }
                return ID3;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        # endregion

        //#region Export Report To PDF Format
        //public void GetReportFile(int Id, int Branch_ID, string path, string NewPath, string[] SPs)
        //{
        //    CrystalDecisions.CrystalReports.Engine.ReportDocument report = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
        //    DataSet dsReport = new DataSet();
        //    GetReportDataSet(Id, Branch_ID, SPs, ref  dsReport);
        //    GetReportObject(dsReport, path, SPs, ref  report);
        //    report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, NewPath);
        //}

        //public void GetReportFileforMultile(int Id, int Branch_ID, string path, string NewPath, string[] SPs, string where)
        //{
        //    CrystalDecisions.CrystalReports.Engine.ReportDocument report = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
        //    DataSet dsReport = new DataSet();
        //    GetReportDataSetManagement(Branch_ID, SPs, ref dsReport, where.ToString(), "", "1-Apr-2012", "1-Apr-2015");
        //    // GetReportDataSet(Id, Branch_ID, SPs, ref  dsReport);
        //    GetReportObject(dsReport, path, SPs, ref  report);
        //    report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, NewPath);
        //}
        //#endregion

        #region Get dsReport and Report Object
        public void GetReportDataSet(int Id, int Branch_ID, string[] SPs, ref DataSet dsReport)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                tempds = FillDS(SPs[i], Id.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDS(SP_Report_ssp_Report_GetTblBranchMaster, Branch_ID.ToString());
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        #region for ChequePrintingHTML
        public void GetReportDataSet(string Id, int Branch_ID, string[] SPs, ref DataSet dsReport)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                tempds = FillDS(SPs[i], Id.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDS(SP_Report_ssp_Report_GetTblBranchMaster, Branch_ID.ToString());
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }
        #endregion

        public void GetReportDataSetOtherReport(int Id, int Branch_ID, string[] SPs, ref DataSet dsReport, string fyid, string CompanyId)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                tempds = FillDataSet(SPs[i], Id.ToString(), fyid.ToString(), CompanyId.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDS(SP_Report_ssp_Report_GetTblBranchMaster, Branch_ID.ToString());
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        public void GetReportDataSetManagement(int Branch_ID, string[] SPs, ref DataSet dsReport, string where, string reportwhere, string FromDate, string ToDate)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                tempds = FillDataSet(SPs[i], where.ToString(), FromDate.ToString(), ToDate.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterForReport", Branch_ID.ToString(), reportwhere.ToString());
            // tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);

            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        //ForImage
        public void GetReportDataSetForImage(int Id, int Branch_ID, string[] SPs, ref DataSet dsReport)
        {
            DataSet tempds = new DataSet();
            tempds = FillDS(SP_Report_ssp_Report_GetTblBranchMaster, Branch_ID.ToString());
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
            for (int i = 0; i < SPs.Length; i++)
            {

                tempds = FillDS(SPs[i], Id.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
            }
        }

        // for multiple cheque printing
        public void GetReportDataSet(string Id, int Branch_ID, string[] SPs, ref DataSet dsReport, string UserEmployeeId, string ReportTypeID)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                tempds = FillDS(SPs[i], Id.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        public void GetReportDataSet(int Id, int Branch_ID, string[] SPs, ref DataSet dsReport, string UserEmployeeId, string ReportTypeID)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                tempds = FillDS(SPs[i], Id.ToString());
                //tempds = FillDataSet(SPs[i], Id.ToString(), Branch_ID.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        public void GetReportDataSetEmployeeMaster(int Id, int Branch_ID, string[] SPs, ref DataSet dsReport, string UserEmployeeId, string ReportTypeID)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                //tempds = FillDS(SPs[i], Id.ToString());
                tempds = FillDataSet(SPs[i], Id.ToString(), Branch_ID.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        // For Balance Sheet General Finance MIS CrystalReport.
        // ObjLibrary.GetReportDataSet(Convert.ToInt32(Session["ID"].ToString()), Company_ID.ToString(), Convert.ToInt32(Session["BranchID"].ToString()), FY_ID.ToString(),Session["GroupLevelID"].ToString(),SPs,ref dsReport,Session["UserEmployeeId"].ToString(),Session["ReportTypeID"].ToString()) ;
        public void GetReportDataSet(int Id, string Company, int Branch_ID, string Fy_id, string GroupLevelID, string[] SPs, ref DataSet dsReport, string UserEmployeeId, string ReportTypeID)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                tempds = FillDataSet(SPs[i], Id.ToString(), GroupLevelID.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        // For Balance Sheet General Finance MIS CrystalReport with Month.
        // ObjLibrary.GetReportDataSet(Convert.ToInt32(Session["ID"].ToString()), Company_ID.ToString(), Convert.ToInt32(Session["BranchID"].ToString()), FY_ID.ToString(),Session["GroupLevelID"].ToString(),SPs,ref dsReport,Session["UserEmployeeId"].ToString(),Session["ReportTypeID"].ToString()) ;
        public void GetReportDataSet(int Id, string Company, int Branch_ID, string Fy_id, string GroupLevelID, string Month, string[] SPs, ref DataSet dsReport, string UserEmployeeId, string ReportTypeID)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                //tempds = FillDataSet(SPs[i], Id.ToString(), GroupLevelID.ToString());
                tempds = FillDataSetMIS(SPs[i], Id.ToString(), GroupLevelID.ToString(), Month.ToString(), Fy_id.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        // New for Trial Balance AccountGroup Only Ledger(FinanceMIS)
        public void GetReportDataSet(int Id, string Company, int Branch_ID, string Fy_id, string GroupLevelID, string Month, string AllPandLBS, string[] SPs, ref DataSet dsReport, string UserEmployeeId, string ReportTypeID)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                //tempds = FillDataSet(SPs[i], Id.ToString(), GroupLevelID.ToString());
                tempds = FillDataSetMISNew(SPs[i], Id.ToString(), GroupLevelID.ToString(), Month.ToString(), Fy_id.ToString(), AllPandLBS.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        public void GetReportDataSetOtherReport(int Id, int Branch_ID, string[] SPs, ref DataSet dsReport, string fyid, string CompanyId, string UserEmployeeId, string ReportTypeID)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                tempds = FillDataSet(SPs[i], Id.ToString(), fyid.ToString(), CompanyId.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        public void GetReportDataSetManagement(int Branch_ID, string[] SPs, ref DataSet dsReport, string where, string reportwhere, string FromDate, string ToDate, string UserEmployeeId, string ReportTypeID)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //if (!SPs[i].Contains("ISO|"))
                //{
                tempds = FillDataSet(SPs[i], where.ToString(), FromDate.ToString(), ToDate.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
                //}
                //else
                //{
                //    SPs[i] = SPs[i].Replace("ISO|", "");
                //    tempds = FillDS("ssp_Report_GetISONumber", SPs[i].ToString());
                //    tempds.Tables[0].TableName = SPs[i].ToString();
                //    dsReport.Tables.Add(tempds.Tables[0].Copy());
                //    tempds.Clear();
                //}
            }
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        //ForImage
        public void GetReportDataSetForImage(int Id, int Branch_ID, string[] SPs, ref DataSet dsReport, string UserEmployeeId, string ReportTypeID)
        {
            DataSet tempds = new DataSet();
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
            for (int i = 0; i < SPs.Length; i++)
            {
                tempds = FillDS(SPs[i], Id.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
            }
        }

        //public void GetReportObject(DataSet dsReport, string path, string[] SPs, ref CrystalDecisions.CrystalReports.Engine.ReportDocument report)
        //{
        //    report.FileName = path;
        //    string[] str = ObjDataServices.GetServerParameter();
        //    report.SetDatabaseLogon(str[2], str[3], str[0], str[1]);
        //    report.Database.Tables[0].SetDataSource(dsReport.Tables["Branch Detail"]);
        //    for (int i = 1; i <= SPs.Length; i++)
        //        report.Database.Tables[i].SetDataSource(dsReport.Tables[SPs[i - 1].ToString()]);
        //}

        //public void SetDatabaseLogon(ref CrystalDecisions.CrystalReports.Engine.ReportDocument report)
        //{
        //    string[] str = ObjDataServices.GetServerParameter();
        //    report.SetDatabaseLogon(str[2], str[3], str[0], str[1]);
        //}

        // this method Added for Fetching Data Against Multiple ID's(String)
        public void GetReportDataSet(string Id, string Id1, int Branch_ID, string[] SPs, ref DataSet dsReport, string UserEmployeeId, string ReportTypeID)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //tempds = FillDS(SPs[i], Id.ToString());
                tempds = FillDataSet(SPs[i], Id.ToString(), Id1.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
            }
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }

        // this method Added for Fetching Data Against Multiple ID's(Integer)
        public void GetReportDataSet(int Id, string Id1, int Branch_ID, string[] SPs, ref DataSet dsReport, string UserEmployeeId, string ReportTypeID)
        {
            DataSet tempds = new DataSet();
            for (int i = 0; i < SPs.Length; i++)
            {
                //tempds = FillDS(SPs[i], Id.ToString());
                tempds = FillDataSet(SPs[i], Id.ToString(), Id1.ToString());
                tempds.Tables[0].TableName = SPs[i].ToString();
                dsReport.Tables.Add(tempds.Tables[0].Copy());
                tempds.Clear();
            }
            tempds = FillDataSet("ssp_Report_GetTblBranchMasterAndUserDetails", Branch_ID.ToString(), UserEmployeeId, ReportTypeID);
            tempds.Tables[0].TableName = "Branch Detail";
            dsReport.Tables.Add(tempds.Tables[0].Copy());
            tempds.Clear();
        }
        #endregion

        #region Write File
        public string WriteFile(string fileName, byte[] FileByte, string TargetFolder, string Targetfile)
        {
            if (fileName.ToString() != "")
            {
                string ext = fileName.ToString();
                byte[] reader = FileByte;
                //string[] se = ext.Split('.');
                //ext = se[se.Length - 1];
                //Targetfile = Targetfile + "." + ext;
                FileStream stream;
                BinaryWriter writer;
                if (!Directory.Exists(TargetFolder))
                    Directory.CreateDirectory(TargetFolder);
                //if (!File.Exists(Targetfile))
                //{
                stream = new FileStream(Targetfile, FileMode.Create, FileAccess.ReadWrite);
                writer = new BinaryWriter(stream);
                writer.Write(reader);
                writer.Flush();
                writer.Close();
                stream.Close();
                //}
            }
            return Targetfile;
        }
        #endregion

        #region TabButtonEnableFunction
        public void TabButtonEnableFunction(Button AddBtn, Button SaveBtn, ref DataGrid GRD, bool blADDMode, bool blEDITMode)
        {
            if (blADDMode == true || blEDITMode == true)
            {
                AddBtn.Enabled = true;
                SaveBtn.Enabled = true;
                GRD.EditItemIndex = -1;
            }
        }
        #endregion

        #region WebGridDataRelation
        public DataSet WebGridDataRelation(DataSet dsItemTemp)
        {
            try
            {
                if (dsItemTemp != null)
                {
                    int[] tblcnt = new int[dsItemTemp.Tables.Count];
                    int rel = 0, rw = 0, r = 0, tblNo = 0;
                    tblcnt[1] = rw;
                    for (int i = 1; i < dsItemTemp.Tables.Count; i++)
                    {
                        rw = 0;
                        r = 0;
                        int[] Colm = new int[dsItemTemp.Tables[i].Columns.Count];

                        DataRelation relation;
                        for (int j = 0; j < dsItemTemp.Tables[i].Columns.Count; j++)
                        {
                            if (dsItemTemp.Tables[i].Columns.Contains("Level" + j))
                            {
                                Colm[rw++] = j;
                            }
                        }
                        for (int k = 0; k < rw; k++)
                        {
                            if (dsItemTemp.Tables[tblNo].Columns.Contains("Level" + Colm[k]))
                            {
                                r++;
                            }
                        }

                        if (rw > 2)
                        {
                            int p = Colm[0];
                            int c = Colm[1];
                            DataColumn[] parentCols = new DataColumn[r];
                            DataColumn[] childCols = new DataColumn[r];
                            for (int k = 0; k < rw; k++)
                            {
                                if (dsItemTemp.Tables[tblcnt[p]].Columns.Contains("Level" + Colm[k]) && tblNo == 0)
                                {
                                    relation = new DataRelation("HeaderRelation1" + i, dsItemTemp.Tables[tblcnt[p]].Columns["Level" + Colm[k]], dsItemTemp.Tables[i].Columns["Level" + Colm[k]]);
                                    dsItemTemp.Relations.Add(relation);
                                }
                                else if (dsItemTemp.Tables[tblNo].Columns.Contains("Level" + Colm[k]) && tblNo > 0)
                                {
                                    parentCols[k] = dsItemTemp.Tables[tblNo].Columns["Level" + Colm[k]];
                                    childCols[k] = dsItemTemp.Tables[i].Columns["Level" + Colm[k]];
                                }
                            }
                            if (tblNo > 0)
                            {
                                relation = new DataRelation("Relation" + rel++, parentCols, childCols);
                                dsItemTemp.Relations.Add(relation);
                            }
                            tblNo = i;
                        }
                        else if (rw == 2)
                        {
                            int p = Colm[0];
                            int c = Colm[1];
                            relation = new DataRelation("HeaderRelation1" + i, dsItemTemp.Tables[tblcnt[p]].Columns["Level" + p], dsItemTemp.Tables[i].Columns["Level" + p]);
                            dsItemTemp.Relations.Add(relation);
                            tblcnt[c] = i;
                        }
                        else if (rw == 1)
                        {
                            int p = Colm[0];
                            relation = new DataRelation("HeaderRelation1" + i, dsItemTemp.Tables[tblcnt[p]].Columns["Level" + p], dsItemTemp.Tables[i].Columns["Level" + p]);
                            dsItemTemp.Relations.Add(relation);
                        }
                    }
                    dsItemTemp.AcceptChanges();
                }
                return dsItemTemp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        //#region WebGridInvisibleColoumns()
        //public void WebGridInvisibleColoumns(ref WebGrid GridRootTbl)
        //{
        //    try
        //    {
        //        if (GridRootTbl.RootTable != null)
        //        {
        //            for (int j = 0; j < GridRootTbl.RootTable.Columns.Count; j++)
        //            {
        //                for (int k = 0; k < 10; k++)
        //                {
        //                    if (GridRootTbl.RootTable.Columns[j].Name == "Level" + k)
        //                    {
        //                        GridRootTbl.RootTable.Columns[j].Visible = false;
        //                        if (GridRootTbl.RootTable.HasChildTable)
        //                        {
        //                            WebGridChildInvisibleColoumns(GridRootTbl.RootTable);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //protected void WebGridChildInvisibleColoumns(ISNet.WebUI.WebGrid.WebGridTable GridChildTbl)
        //{
        //    try
        //    {
        //        for (int i = 0; i < GridChildTbl.ChildTables.Count; i++)
        //        {
        //            if (GridChildTbl.ChildTables[i] != null)
        //            {
        //                for (int j = 0; j < GridChildTbl.ChildTables[i].Columns.Count; j++)
        //                {
        //                    for (int k = 0; k < 10; k++)
        //                    {
        //                        if (GridChildTbl.ChildTables[i].Columns[j].Name == "Level" + k)
        //                        {
        //                            GridChildTbl.ChildTables[i].Columns[j].Visible = false;
        //                            if (GridChildTbl.ChildTables[i].HasChildTable)
        //                            {
        //                                WebGridChildInvisibleColoumns(GridChildTbl.ChildTables[i]);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //#endregion

        #region WebGridEnableDisableControls()
        public void WebGridEnableDisableControls(ref Button btnNew, ref Button btnInsert, ref Button btnDelete, string Case, bool blAddMode, bool blEditMode)
        {
            try
            {
                if (blAddMode == false && blEditMode == false)
                {
                    btnNew.Enabled = false;
                    btnInsert.Enabled = false;
                    btnDelete.Enabled = false;
                }
                else
                {
                    if (Case == "New")
                    {
                        btnNew.Enabled = false;
                        btnInsert.Enabled = true;
                        btnDelete.Enabled = false;
                    }
                    else if (Case == "Insert")
                    {
                        btnNew.Enabled = true;
                        btnInsert.Enabled = false;
                        btnDelete.Enabled = false;
                    }
                    else if (Case == "Update")
                    {
                        btnNew.Enabled = false;
                        btnInsert.Enabled = true;
                        btnDelete.Enabled = true;
                    }
                    else if (Case == "Delete")
                    {
                        btnNew.Enabled = true;
                        btnInsert.Enabled = false;
                        btnDelete.Enabled = false;
                    }
                    else if (Case == "Cancel")
                    {
                        btnNew.Enabled = true;
                        btnInsert.Enabled = false;
                        btnDelete.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void WebGridEnableDisableControls(ref Button btnNew, ref Button btnInsert, ref Button btnDelete, ref Button btnCancel, string Case, bool blAddMode, bool blEditMode)
        {
            try
            {
                if (blAddMode == false && blEditMode == false)
                {
                    btnNew.Enabled = false;
                    btnInsert.Enabled = false;
                    btnDelete.Enabled = false;
                    btnCancel.Enabled = false;
                }
                else
                {
                    if (Case == "New")
                    {
                        btnNew.Enabled = false;
                        btnInsert.Enabled = true;
                        btnDelete.Enabled = false;
                        btnCancel.Enabled = true;
                    }
                    else if (Case == "Insert")
                    {
                        btnNew.Enabled = true;
                        btnInsert.Enabled = false;
                        btnDelete.Enabled = false;
                        btnCancel.Enabled = false;
                    }
                    else if (Case == "Update")
                    {
                        btnNew.Enabled = false;
                        btnInsert.Enabled = true;
                        btnDelete.Enabled = true;
                        btnCancel.Enabled = true;
                    }
                    else if (Case == "Delete")
                    {
                        btnNew.Enabled = true;
                        btnInsert.Enabled = false;
                        btnDelete.Enabled = false;
                        btnCancel.Enabled = false;
                    }
                    else if (Case == "Cancel")
                    {
                        btnNew.Enabled = true;
                        btnInsert.Enabled = false;
                        btnDelete.Enabled = false;
                        btnCancel.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void WebGridEnableDisableControls(ref Button btnNew, ref Button btnInsert, ref Button btnDelete, ref Button btnSave, ref Button btnCancel, string Case, bool blAddMode, bool blEditMode)
        {
            try
            {
                if (blAddMode == false && blEditMode == false)
                {
                    btnNew.Enabled = false;
                    btnInsert.Enabled = false;
                    btnDelete.Enabled = false;
                    btnCancel.Enabled = true;
                    btnSave.Enabled = false;
                }
                else
                {
                    if (Case == "New")
                    {
                        btnNew.Enabled = false;
                        btnInsert.Enabled = true;
                        btnDelete.Enabled = false;
                        btnSave.Enabled = false;
                        btnCancel.Enabled = true;
                    }
                    else if (Case == "Insert")
                    {
                        btnNew.Enabled = true;
                        btnInsert.Enabled = false;
                        btnDelete.Enabled = false;
                        btnSave.Enabled = true;
                        btnCancel.Enabled = true;
                    }
                    else if (Case == "Update")
                    {
                        btnNew.Enabled = false;
                        btnInsert.Enabled = true;
                        btnDelete.Enabled = true;
                        btnSave.Enabled = false;
                        btnCancel.Enabled = true;
                    }
                    else if (Case == "Delete")
                    {
                        btnNew.Enabled = true;
                        btnInsert.Enabled = false;
                        btnDelete.Enabled = false;
                        btnSave.Enabled = true;
                        btnCancel.Enabled = true;
                    }
                    else if (Case == "Cancel")
                    {
                        btnNew.Enabled = true;
                        btnInsert.Enabled = false;
                        btnDelete.Enabled = false;
                        btnSave.Enabled = false;
                        btnCancel.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetScript
        public string GetScript(string SP_Script)
        {
            return ObjDataServices.GetScript(SP_Script);
        }
        #endregion

        #region Get Sub Tab Match Record To Parent DS Parameter FOUR
        public DataSet GetSubTabMatchRecordToParentDS(DataSet DSParent, DataSet DSSub, string DSParentColn1, string DSSubColn1, string DSParentColn2, string DSSubColn2, string DSParentColn3, string DSSubColn3, string DSParentColn4, string DSSubColn4)
        {
            DataSet dsTempMerge = new DataSet();
            for (int i = 0; i < DSParent.Tables[0].Rows.Count; i++)
            {
                string Filter = "" + DSSubColn1 + "" + DSParent.Tables[0].Rows[i][DSParentColn1].ToString() + " and " + DSSubColn2 + "" + DSParent.Tables[0].Rows[i][DSParentColn2].ToString() + " and " + DSSubColn3 + "" + DSParent.Tables[0].Rows[i][DSParentColn3].ToString() + " and " + DSSubColn4 + "" + DSParent.Tables[0].Rows[i][DSParentColn4].ToString();
                if (DSSub.Tables[0].Rows.Count > 0)
                {
                    DataSet dsTemp = new DataSet();
                    dsTemp.Tables.Add(GetTempDS(DSSub, Filter).ToTable().Copy());
                    dsTempMerge.Merge(dsTemp);
                }
            }
            if (DSSub.Tables[0].Rows.Count > 0)
            {
                DSSub.Tables.Remove(DSSub.Tables[0]);
                DSSub = dsTempMerge.Copy();
            }
            return DSSub;
        }
        #endregion

        #region Get Sub Tab Match Record To Parent DS Parameter THREE
        public DataSet GetSubTabMatchRecordToParentDS(DataSet DSParent, DataSet DSSub, string DSParentColn1, string DSSubColn1, string DSParentColn2, string DSSubColn2, string DSParentColn3, string DSSubColn3)
        {
            DataSet dsTempMerge = new DataSet();
            for (int i = 0; i < DSParent.Tables[0].Rows.Count; i++)
            {
                string Filter = "" + DSSubColn1 + "" + DSParent.Tables[0].Rows[i][DSParentColn1].ToString() + " and " + DSSubColn2 + "" + DSParent.Tables[0].Rows[i][DSParentColn2].ToString() + " and " + DSSubColn3 + "" + DSParent.Tables[0].Rows[i][DSParentColn3].ToString();
                if (DSSub.Tables[0].Rows.Count > 0)
                {
                    DataSet dsTemp = new DataSet();
                    dsTemp.Tables.Add(GetTempDS(DSSub, Filter).ToTable().Copy());
                    dsTempMerge.Merge(dsTemp);
                }
            }
            if (DSSub.Tables[0].Rows.Count > 0)
            {
                DSSub.Tables.Remove(DSSub.Tables[0]);
                DSSub = dsTempMerge.Copy();
            }
            return DSSub;
        }
        #endregion

        #region Get Sub Tab Match Record To Parent DS Parameter TWO
        public DataSet GetSubTabMatchRecordToParentDS(DataSet DSParent, DataSet DSSub, string DSParentColn1, string DSSubColn1, string DSParentColn2, string DSSubColn2)
        {
            DataSet dsTempMerge = new DataSet();
            for (int i = 0; i < DSParent.Tables[0].Rows.Count; i++)
            {
                string Filter = "" + DSSubColn1 + "" + DSParent.Tables[0].Rows[i][DSParentColn1].ToString() + " and " + DSSubColn2 + "" + DSParent.Tables[0].Rows[i][DSParentColn2].ToString();
                if (DSSub.Tables[0].Rows.Count > 0)
                {
                    DataSet dsTemp = new DataSet();
                    dsTemp.Tables.Add(GetTempDS(DSSub, Filter).ToTable().Copy());
                    dsTempMerge.Merge(dsTemp);
                }
            }
            if (DSSub.Tables[0].Rows.Count > 0)
            {
                DSSub.Tables.Remove(DSSub.Tables[0]);
                DSSub = dsTempMerge.Copy();
            }
            return DSSub;
        }
        #endregion

        #region Get Sub Tab Match Record To Parent DS Parameter ONE
        public DataSet GetSubTabMatchRecordToParentDS(DataSet DSParent, DataSet DSSub, string DSParentColn1, string DSSubColn1)
        {
            DataSet dsTempMerge = new DataSet();
            for (int i = 0; i < DSParent.Tables[0].Rows.Count; i++)
            {
                string Filter = "" + DSSubColn1 + "" + DSParent.Tables[0].Rows[i][DSParentColn1].ToString();
                if (DSSub.Tables[0].Rows.Count > 0)
                {
                    DataSet dsTemp = new DataSet();
                    dsTemp.Tables.Add(GetTempDS(DSSub, Filter).ToTable().Copy());
                    dsTempMerge.Merge(dsTemp);
                }
            }
            if (DSSub.Tables[0].Rows.Count > 0)
            {
                DSSub.Tables.Remove(DSSub.Tables[0]);
                DSSub = dsTempMerge.Copy();
            }
            return DSSub;
        }
        #endregion

        # region [ssp_GetUserMailIdFormMail Property]
        public string ssp_GetUserMailId
        {
            get
            {
                return "ssp_GetUserMailId";
            }
        }
        # endregion

        #region GetFilterchar
        public string GetFilterchar(string TableName, string Field, string FromChar, string ToChar)
        {
            return ObjDataServices.GetFilterchar("ssp_GetFilterRecordCharcterWise", TableName, Field, FromChar, ToChar);
        }
        #endregion

        #region GetFromMailId
        public string GetFromMailId(string strCompanyID, string BranchID, string UserID)
        {
            return ObjDataServices.GetFromMailId(ssp_GetUserMailId, strCompanyID, BranchID, UserID);
        }
        #endregion

        #region GetFromBranchMailId
        public string GetFromBranchMailId(string strCompanyID, string BranchID, string UserID)
        {
            return ObjDataServices.GetFromMailId("ssp_GetfrmContentMasterAutoFillFromMailId", strCompanyID, BranchID, "0");
        }
        #endregion

        #region FillConversionRate
        public DataSet FillConversionRate(string intCountryID)
        {
            DataSet ds;
            return ds = FillDS(SP_FillConversionRate, intCountryID);
        }
        #endregion

        #region FillConversionRateForEstimationAndCosting
        public DataSet FillConversionRateForEstimationAndCosting(string intCurrencyID)
        {
            DataSet ds;
            return ds = FillDS("ssp_GetBindCurrencyConversionValuesForEstimationAndCosting", intCurrencyID);
        }
        #endregion

        #region FillConversionRate
        public DataSet FillVendorInfo(string intVendorID)
        {
            DataSet ds;
            return ds = FillDS(SP_FillVendorInfo, intVendorID);
        }
        #endregion

        #region [Encrypt]
        public string Encrypt(string strval)
        {
            string str = "";
            try
            {
                for (int i = 1; i <= strval.Length; i++)
                {
                    switch (i % 5)
                    {
                        case 0:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) + 3);
                            break;
                        case 1:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) - 6);
                            break;
                        case 2:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) + 5);
                            break;
                        case 3:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) - 10);
                            break;
                        case 4:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) + 15);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return str;
        }
        #endregion [Encrypt]

        #region [Desncrypt]
        public string Desncrypt(string strval)
        {
            string str = "";
            try
            {
                for (int i = 1; i <= strval.Length; i++)
                {
                    switch (i % 5)
                    {
                        case 0:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) - 3);
                            break;
                        case 1:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) + 6);
                            break;
                        case 2:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) - 5);
                            break;
                        case 3:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) + 10);
                            break;
                        case 4:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) - 15);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return str;
        }
        #endregion [Encrypt]

        #region [LoadImageOnReport]

        public void LoadImageOnReport(int intBranchID)
        {
            DataSet ds = new DataSet();
            byte[] file;
            string FileName;

            ds = ObjDataServices.FillDS(SP_GetBranchMasterDetail, intBranchID.ToString());

            if (!Convert.IsDBNull(ds.Tables[0].Rows[0]["imgBranchLogo"]))
                file = (byte[])ds.Tables[0].Rows[0]["imgBranchLogo"];
            else
                file = null;
            FileName = "BranchLogo.BMP";

            DispFile(FileName, file);

            if (!Convert.IsDBNull(ds.Tables[0].Rows[0]["imgCertificationLogo"]))
                file = (byte[])ds.Tables[0].Rows[0]["imgCertificationLogo"];
            else
                file = null;
            FileName = "CertificationLogo.BMP";

            DispFile(FileName, file);
        }

        private void DispFile(string fileName, byte[] File)
        {
            try
            {
                if (fileName.ToString() != "")
                {
                    string ext = fileName.ToString();
                    string strPath = "C:\\Images";
                    byte[] reader = File;

                    FileStream stream;
                    BinaryWriter writer;
                    if (!Directory.Exists(strPath))
                    {
                        Directory.CreateDirectory(strPath);
                    }
                    string s;
                    s = strPath + "\\" + fileName;
                    stream = new FileStream(s, FileMode.Create, FileAccess.ReadWrite);
                    writer = new BinaryWriter(stream);
                    writer.Write(reader);
                    writer.Flush();
                    writer.Close();
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetEMailUserId
        public string GetEMailUserId(string strCompanyID, string BranchID, string UserID)
        {
            return ObjDataServices.GetFromMailId("ssp_GeteMailUser", strCompanyID, BranchID, UserID);
        }
        #endregion

        #region [DeEncrypt]
        public string DeEncrypt(string strval)
        {
            string str = "";
            try
            {
                for (int i = 1; i <= strval.Length; i++)
                {
                    switch (i % 5)
                    {
                        case 0:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) + 3);
                            break;
                        case 1:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) - 6);
                            break;
                        case 2:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) + 5);
                            break;
                        case 3:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) - 10);
                            break;
                        case 4:
                            str = str + Strings.Chr(Strings.Asc(Strings.Mid(strval, i, 1)) + 15);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return str;
        }
        #endregion [DeEncrypt]

        public string GetMonth(int month)
        {
            switch (month)
            {
                case 1:
                    return "Jan";
                    break;
                case 2:
                    return "Feb";
                    break;
                case 3:
                    return "Mar";
                    break;
                case 4:
                    return "Apr";
                    break;
                case 5:
                    return "May";
                    break;
                case 6:
                    return "Jun";
                    break;
                case 7:
                    return "Jul";
                    break;
                case 8:
                    return "Aug";
                    break;
                case 9:
                    return "Sep";
                    break;
                case 10:
                    return "Oct";
                    break;
                case 11:
                    return "Nov";
                    break;
                default:
                    return "Dec";
            }
        }

        public int GetMonth(string month)
        {
            switch (month)
            {
                case "Jan":
                    return 1;
                    break;
                case "Feb":
                    return 2;
                    break;
                case "Mar":
                    return 3;
                    break;
                case "Apr":
                    return 4;
                    break;
                case "May":
                    return 5;
                    break;
                case "Jun":
                    return 6;
                    break;
                case "Jul":
                    return 7;
                    break;
                case "Aug":
                    return 8;
                    break;
                case "Sep":
                    return 9;
                    break;
                case "Oct":
                    return 10;
                    break;
                case "Nov":
                    return 11;
                    break;
                default:
                    return 12;
            }
        }

        public DataSet GetTempDSSort(DataSet DS, string FilterCriteria)
        {
            DataSet ds = new DataSet();
            DataView dv = new DataView(DS.Tables[0]);
            dv.Sort = FilterCriteria;
            ds.Tables.Add(dv.ToTable());
            return ds;
        }

        public DataSet GetAutoFillDS(string FormName)
        {
            DataTable dt = new DataTable();
            DataTable dtRecord = new DataTable();

            DataSet ds = new DataSet();
            string tblName, FieldId, FieldName;
            dt = BindGrid(SP_AutoFieldsReference, FormName);

            FieldName = dt.Rows[0]["strFieldName"].ToString();
            FieldId = dt.Rows[0]["strFieldID"].ToString();
            tblName = dt.Rows[0]["TblName"].ToString();

            if (FieldName != "" && FieldId != "" && tblName != "")
            {
                dtRecord = BindGrid(SP_AutoFillReference, FieldName, FieldId, tblName);
                if (dtRecord.Rows.Count == 0)
                {
                    dtRecord.Rows.Add(dtRecord.NewRow());
                    dtRecord.Rows[0][0] = "";
                    dtRecord.Rows[0][1] = 0;
                }
                ds.Tables.Add(dtRecord.Copy());
            }
            if (ds.Tables.Count == 0)
            {
                DataTable dttemp = new DataTable();
                dttemp.Columns.Add(FieldId);
                dttemp.Columns.Add(FieldName);
                dttemp.Rows.Add(dttemp.NewRow());
                dttemp.Rows[0][0] = "";
                dttemp.Rows[0][1] = 0;
                ds.Tables.Add(dttemp.Copy());
            }
            return ds;
        }

        #region Check UnCheck CheckBoxes
        #region UnCheck ALL
        public void UnCheck(bool Flag, ref DataGrid GRDCreditType, ref CheckBox CHKAllCriteria, string ColName)
        {
            for (int i = 0; i < GRDCreditType.Items.Count; i++)
            {
                CheckBox chk = (CheckBox)GRDCreditType.Items[i].FindControl(ColName);
                if (!chk.Checked)
                    Flag = true;
            }
            if (!Flag) CHKAllCriteria.Checked = true; else CHKAllCriteria.Checked = false;
        }
        public void UnCheck(bool Flag, ref GridView GRDCreditType, ref CheckBox CHKAllCriteria, string ColName)
        {
            for (int i = 0; i < GRDCreditType.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)GRDCreditType.Rows[i].FindControl(ColName);
                if (!chk.Checked)
                    Flag = true;
            }
            if (!Flag) CHKAllCriteria.Checked = true; else CHKAllCriteria.Checked = false;
        }
        #endregion

        #region Check ALL
        public void Check(ref DataGrid GRDCreditType, ref CheckBox CHKAllCriteria, string ColName)
        {
            if (CHKAllCriteria.Checked)
            {
                for (int i = 0; i < GRDCreditType.Items.Count; i++)
                {
                    CheckBox chk = (CheckBox)GRDCreditType.Items[i].FindControl(ColName);
                    chk.Checked = true;
                }
            }
            else
            {
                for (int i = 0; i < GRDCreditType.Items.Count; i++)
                {
                    CheckBox chk = (CheckBox)GRDCreditType.Items[i].FindControl(ColName);
                    chk.Checked = false;
                }
            }
        }
        public void Check(ref GridView GRDCreditType, ref CheckBox CHKAllCriteria, string ColName)
        {
            if (CHKAllCriteria.Checked)
            {
                for (int i = 0; i < GRDCreditType.Rows.Count; i++)
                {
                    CheckBox chk = (CheckBox)GRDCreditType.Rows[i].FindControl(ColName);
                    chk.Checked = true;
                }
            }
            else
            {
                for (int i = 0; i < GRDCreditType.Rows.Count; i++)
                {
                    CheckBox chk = (CheckBox)GRDCreditType.Rows[i].FindControl(ColName);
                    chk.Checked = false;
                }
            }
        }
        #endregion
        #endregion

        #region GetItemDetail()
        public DataTable GetItemDetail(int Company_ID, int Branch_ID, string intID, string Sp_Name)
        {
            try
            {
                object[] param = new object[3];
                param[0] = Company_ID;
                param[1] = Branch_ID;
                param[2] = intID;
                DataTable dt = ObjDataServices.GetDataSet(Sp_Name, param);
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region DesignAttendanceXML
        /// <summary>
        /// Added 
        /// To Create XML file using dataset
        /// </summary>
        /// <param name="XMLFile"></param>
        /// <param name="DsXML"></param>
        public void DesignAttendanceXML(string XMLFile, DataSet DsXML)
        {
            try
            {
                DataSet Ds = new DataSet();
                DsXML.WriteXml(XMLFile);

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [SAVE BULKY DETAIL DATA]
        /// <summary>
        /// Added To save bulky data to avoid request time out exception
        /// 
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="strTableName"></param>
        public void SaveBulkyDetailData(DataSet ds, String strTableName)
        {
            ObjDataServices.SaveBulkyDetailData(ds, strTableName);
        }
        #endregion

        #region [Setting Filter Criteria For Controls]
        /// <summary>
        /// Added for
        /// Setting the values of textboxes and Filter extender properties according to the uomMaster
        /// </summary>
        /// <param name="filteredCntr"></param>
        /// <param name="wbCntr"></param>
        /// <param name="intUOMId"></param>
        /// <param name="intCompanyId"></param>
        /// <param name="strFormatProvider"></param>
        public void SetFilteredTextBoxExtender(AjaxControlToolkit.FilteredTextBoxExtender[] filteredCntr, WebControl[] wbCntr, int intUOMId, int intCompanyId, ref string strFormatProvider)
        {
            try
            {
                if (intUOMId != 0)
                {
                    DataSet dsUOMDet = new DataSet();
                    dsUOMDet = FillDataSet("ssp_SelectUOMDetails", intCompanyId.ToString(), intUOMId.ToString());
                    if (dsUOMDet.Tables[0].Rows.Count > 0)
                    {
                        if (dsUOMDet.Tables[0].Rows[0]["strWholeNoFlag"].ToString() == "Y")
                        {
                            for (int i = 0; i < wbCntr.Length; i++)
                            {
                                TextBox txt = (TextBox)wbCntr[i];

                                if (txt.Text.Trim().Contains('.'))
                                {
                                    string[] arrSplit = txt.Text.Trim().Split('.');
                                    if (Convert.ToInt32(arrSplit[1]) > 0)
                                    {
                                        txt.Text = (Convert.ToInt32(arrSplit[0]) + 1).ToString();
                                    }
                                    else
                                        txt.Text = arrSplit[0];
                                }
                            }

                            for (int i = 0; i < filteredCntr.Length; i++)
                            {
                                AjaxControlToolkit.FilteredTextBoxExtender filterTextExt = (AjaxControlToolkit.FilteredTextBoxExtender)filteredCntr[i];
                                filterTextExt.FilterMode = AjaxControlToolkit.FilterModes.ValidChars;
                                filterTextExt.ValidChars = ".0123456789";
                            }
                            strFormatProvider = "########0";
                        }
                        else
                        {
                            for (int i = 0; i < filteredCntr.Length; i++)
                            {
                                AjaxControlToolkit.FilteredTextBoxExtender filterTextExt = (AjaxControlToolkit.FilteredTextBoxExtender)filteredCntr[i];
                                filterTextExt.FilterMode = AjaxControlToolkit.FilterModes.ValidChars;
                                filterTextExt.ValidChars = ".0123456789";
                            }
                            switch (dsUOMDet.Tables[0].Rows[0]["intlDecimalCount"].ToString())
                            {
                                case "1":
                                    strFormatProvider = "########0.0";
                                    break;
                                case "2":
                                    strFormatProvider = "########0.00";
                                    break;
                                case "3":
                                    strFormatProvider = "########0.000";
                                    break;
                                default:
                                    strFormatProvider = "########0.0000";
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < filteredCntr.Length; i++)
                    {
                        AjaxControlToolkit.FilteredTextBoxExtender filterTextExt = (AjaxControlToolkit.FilteredTextBoxExtender)filteredCntr[i];
                        filterTextExt.FilterMode = AjaxControlToolkit.FilterModes.ValidChars;
                        filterTextExt.ValidChars = ".0123456789";
                    }
                    strFormatProvider = "########0.0000";
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [Unit Conversion]
        /// <summary>
        /// Added To save bulky data to avoid request time out exception
        /// 
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="strTableName"></param>
        /// 
        public double conv_UOM(int _FormUnit, int _ToUnit, int _UOMTypeId, double _Value)
        {
            double _result = 0.00;
            double _halfresult = 0.00;

            DataSet dstemp = new DataSet();
            DataSet STD = new DataSet();
            ///////
            dstemp = FillDataSet("SSP_Check_UOM_Std", _UOMTypeId.ToString(), _FormUnit.ToString());
            string formula = _Value.ToString() + " * " + dstemp.Tables[0].Rows[0]["DblConvFactor"].ToString();

            STD = FillDataSet("SSP_Calculate_ItemWeight", formula, "0");
            _halfresult = Convert.ToDouble(STD.Tables[0].Rows[0][0].ToString());

            ///////

            dstemp = FillDataSet("SSP_Check_UOM_Std", _UOMTypeId.ToString(), _ToUnit.ToString());
            formula = _halfresult.ToString() + " / " + dstemp.Tables[0].Rows[0]["DblConvFactor"].ToString();
            STD = FillDataSet("SSP_Calculate_ItemWeight", formula, "0");
            _result = Convert.ToDouble(STD.Tables[0].Rows[0][0].ToString());
            //LBLUnit.Text = dstemp.Tables[0].Rows[0]["strUOM"].ToString();

            return _result;
        }
        #endregion

        public string SendSMS(string User, string password, string Mobile_Number, string Message)
        {
            string stringpost = null;
            this.GetSMSParameter();
            User = param[2];
            password = param[3];

            stringpost = "User=" + User + "&passwd=" + password + "&mobilenumber=" + Mobile_Number + "&message=" + Message;

            HttpWebRequest objWebRequest = null;
            HttpWebResponse objWebResponse = default(HttpWebResponse);
            StreamWriter objStreamWriter = null;
            StreamReader objStreamReader = null;

            try
            {
                string stringResult = null;

                objWebRequest = (HttpWebRequest)WebRequest.Create("http://www.smscountry.com/Compose.aspx");
                objWebRequest.Method = "POST";

                if ((objProxy1 != null))
                {
                    objWebRequest.Proxy = objProxy1;
                }

                // Use below code if you want to SETUP PROXY.
                //Parameters to pass: 1. ProxyAddress 2. Port
                //You can find both the parameters in Connection settings of your internet explorer.

                //WebProxy myProxy = new WebProxy("YOUR PROXY", PROXPORT);
                //myProxy.BypassProxyOnLocal = true;
                //wrGETURL.Proxy = myProxy;

                objWebRequest.ContentType = "application/x-www-form-urlencoded";

                objStreamWriter = new StreamWriter(objWebRequest.GetRequestStream());
                objStreamWriter.Write(stringpost);
                objStreamWriter.Flush();
                objStreamWriter.Close();

                objWebResponse = (HttpWebResponse)objWebRequest.GetResponse();
                objStreamReader = new StreamReader(objWebResponse.GetResponseStream());
                stringResult = objStreamReader.ReadToEnd();

                objStreamReader.Close();
                return stringResult;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                if ((objStreamWriter != null))
                {
                    objStreamWriter.Close();
                }
                if ((objStreamReader != null))
                {
                    objStreamReader.Close();
                }
                objWebRequest = null;
                objWebResponse = null;
                objProxy1 = null;
            }
        }

        private void StreamReader(Stream stream)
        {
            throw new NotImplementedException();
        }

        string[] param = new string[4];
        public string[] GetSMSParameter()
        {
            string[] str1 = sqlConnectionSMS.Split('=');
            string[] str2 = str1[1].Split(';');
            string[] str3 = str1[2].Split(';');
            string[] str4 = str1[3].Split(';');
            string[] str5 = str1[4].Split(';');

            str1[0] = str2[0].ToString();
            str1[1] = str3[0].ToString();
            str1[2] = str4[0].ToString();
            str1[3] = str5[0].ToString();

            param[0] = str1[0].ToString();
            param[1] = str1[1].ToString();
            param[2] = str1[2].ToString();
            param[3] = str1[3].ToString();

            return param;
        }

        /// <summary>
        ///  Supplementory Invoice
        /// </summary>
        /// <param name="dsItemDetail"></param>
        /// <param name="dsTaxDetail"></param>
        /// <param name="RecNo"></param>
        /// <param name="dblTaxAmt"></param>
        /// <param name="dblSurChargeAmt"></param>
        /// <param name="dblGrossAmt"></param>
        /// <param name="dblDiscountAmt"></param>
        /// <param name="dblNetAmt"></param>
        /// <param name="dblTaxTotal"></param>
        /// <param name="dblCharges"></param>
        public void TaxCalulationSupplemetory(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges)//, Page p)
        {
            dblGrossAmt = 0.00;
            double dblAccessAmt = 0.00;
            double dblpercent = 0.00;
            double dblGrossAmtCal = 0.00;
            string strpaytype = "";
            string strGrossApp = "";
            string strAssessApp = "";
            Boolean ChkFalg;

            for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
            {
                if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                {
                    dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                {
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
                else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                {
                    double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                    dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                }
            }
            dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt;
            dblDiscountAmt = 0.00;
            if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
            {
                if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                {
                    dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
                else
                {
                    dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                    dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                }
            }
            dblNetAmt = dblGrossAmt - dblDiscountAmt;
            DataSet DsTmp = new DataSet();
            DataTable DtTmp = new DataTable();

            DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
            if (DtTmp.Rows.Count > 0)
            {
                for (int i = 0; i < DtTmp.Rows.Count; i++)
                {
                    if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                        dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                    else
                        dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                }
            }

            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                {
                    dblTaxAmt = 0;
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                            strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                        }
                    }
                    else
                    {
                        dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                        strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                        strGrossApp = "Y";
                        strAssessApp = "N";
                    }

                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                            {
                                //if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                {
                                    ChkFalg = true;
                                    for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                    {
                                        if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                        {
                                            ChkFalg = false;
                                            if (strpaytype == "A")
                                            {
                                                dblTaxAmt = dblTaxAmt;
                                            }
                                            else
                                            {
                                                dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                            }
                                        }
                                    }
                                    if (ChkFalg == true)
                                    {
                                        //Response.Write("<script>alert(' One of the Taxes Is Not Added...</script>");
                                        //ScriptManager.RegisterStartupScript(p, p.GetType(), Guid.NewGuid().ToString(), "alert('One of the Taxes Is Not Added...')", true);
                                    }
                                }
                                else
                                {
                                    dblTaxAmt = 0;
                                    if (strGrossApp == "Y")
                                    {
                                        if (strpaytype == "A")
                                            dblTaxAmt = dblpercent;
                                        else
                                            dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                                    }
                                    else if (strAssessApp == "Y")
                                    {
                                        if (strpaytype == "A")
                                            dblTaxAmt = dblpercent;
                                        else
                                            dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                                    }
                                }
                            }
                        }
                        dblSurChargeAmt = 0;
                        if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                        {
                            dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                        }
                    }
                    else
                    {
                        dblTaxAmt = 0;
                        if (strGrossApp == "Y")
                        {
                            if (strpaytype == "A")
                                dblTaxAmt = dblpercent;
                            else
                                dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                        }
                        else if (strAssessApp == "Y")
                        {
                            if (strpaytype == "A")
                                dblTaxAmt = dblpercent;
                            else
                                dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                        }
                    }
                    dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.###"));
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.###"));
                }
            }
            dblTaxTotal = 0;
            dblCharges = 0;
            for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
            {
                if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                {
                    double a = 0.000;
                    a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    double b = 0.000;
                    b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                }
                else
                {
                    double c = 0.000;
                    c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    dblCharges = dblCharges + Math.Round(c, 0);
                }
                //if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                //    dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                //else
                //    dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
            }
            dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
        }

        public void TaxCalulationSupplemetory(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, string ID, string CustomerVendorFlag)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.00;
                double dblAccessAmt = 0.00;
                double dblpercent = 0.00;
                double dblGrossAmtCal = 0.00;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.00;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        dblTaxAmt = 0;
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            }
                        }
                        else
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = "Y";
                            strAssessApp = "N";
                        }

                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            if (ID.ToString() == "")
                                ID = "0";
                            ////////////// Getting tax detail from CustomerTaxFormula/VendorTaxFormula detail , If no records from their it will come from taxdetail in tax master
                            if (CustomerVendorFlag == "C")
                            {
                                DsTmp = FillDataSet("ssp_GetTaxChargeCalculationTblCustomerTaxFomulaDetail", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]), ID.ToString());
                            }
                            else if (CustomerVendorFlag == "V")
                                DsTmp = FillDataSet("ssp_GetTaxChargeCalculationTblVendorTaxFomulaDetail", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]), ID.ToString());

                            if (DsTmp.Tables[0].Rows.Count == 0)
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    //if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        ChkFalg = true;
                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                            {
                                                ChkFalg = false;
                                                if (strpaytype == "A")
                                                {
                                                    dblTaxAmt = dblTaxAmt;
                                                }
                                                else
                                                {
                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                                }
                                            }
                                        }
                                        if (ChkFalg == true)
                                        {
                                            //Response.Write("<script>alert(' One of the Taxes Is Not Added...</script>");
                                            //ScriptManager.RegisterStartupScript(p, p.GetType(), Guid.NewGuid().ToString(), "alert('One of the Taxes Is Not Added...')", true);
                                        }
                                    }
                                    else
                                    {
                                        dblTaxAmt = 0;
                                        if (strGrossApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblpercent;
                                            else
                                                dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblpercent;
                                            else
                                                dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                                        }
                                    }
                                }
                            }
                            dblSurChargeAmt = 0;
                            if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                            {
                                dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                            }
                        }
                        else
                        {
                            dblTaxAmt = 0;
                            if (strGrossApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                            }
                            else if (strAssessApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.###"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.###"));
                    }
                }
                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                    }
                    else
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblCharges = dblCharges + Math.Round(c, 0);
                    }
                    //if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    //    dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    //else
                    //    dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Previous Method 
        public void TaxCalulationWithOutCustomerVendorID(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.000;
                double dblAccessAmt = 0.000;
                double dblpercent = 0.00;
                double dblGrossAmtCal = 0.000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.00;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        dblTaxAmt = 0;
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            }
                        }
                        else
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = "Y";
                            strAssessApp = "N";
                        }
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            //if (ID.ToString() == "")
                            //    ID = "0";
                            //////////////// Getting tax detail from CustomerTaxFormula/VendorTaxFormula detail , If no records from their it will come from taxdetail in tax master
                            //if (CustomerVendorFlag == "C")
                            //{
                            //    DsTmp = FillDataSet("ssp_GetTaxChargeCalculationTblCustomerTaxFomulaDetail", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]), ID.ToString());
                            //}
                            //else if (CustomerVendorFlag == "V")
                            //    DsTmp = FillDataSet("ssp_GetTaxChargeCalculationTblVendorTaxFomulaDetail", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]), ID.ToString());

                            //if (DsTmp.Tables[0].Rows.Count == 0)
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    //if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        ChkFalg = true;
                                        for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                        {
                                            if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                            {
                                                ChkFalg = false;
                                                if (strpaytype == "A")
                                                {
                                                    dblTaxAmt = dblTaxAmt;
                                                }
                                                else
                                                {
                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                                }
                                            }
                                        }
                                        if (ChkFalg == true)
                                        {
                                            //Response.Write("<script>alert(' One of the Taxes Is Not Added...</script>");
                                            //ScriptManager.RegisterStartupScript(p, p.GetType(), Guid.NewGuid().ToString(), "alert('One of the Taxes Is Not Added...')", true);
                                        }
                                    }
                                    else
                                    {
                                        dblTaxAmt = 0;
                                        if (strGrossApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblpercent;
                                            else
                                                dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblpercent;
                                            else
                                                dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                                        }
                                    }
                                }
                            }
                            dblSurChargeAmt = 0;
                            if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                            {
                                dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                            }
                        }
                        else
                        {
                            dblTaxAmt = 0;
                            if (strGrossApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                            }
                            else if (strAssessApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.###"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.###"));

                        //dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(Math.Round(dblTaxAmt, 0).ToString("######0.00"));
                        //if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        //    dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(Math.Round(dblSurChargeAmt, 0).ToString("######0.00"));

                    }
                }
                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                        //dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblCharges = dblCharges + Math.Round(c, 0);
                        //dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    }
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Rejection Delivery Challan Tax Calculation Methods
        public void TaxCalulationForRejectionDCNew(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges, ref double dblASSAmt, double dblCFTWt, double dblTOTWt)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strWtType = "";
                string strNone = "";
                double dblQty = 0.00;
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0.0000" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - dblDiscount;
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    dblQty = dblQty + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString());
                }
                dblGrossAmtCal = dblDiscountAmt != 0.0000 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.0000;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {

                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                                strWtType = DsTmp.Tables[0].Rows[0]["strWeightType"].ToString();
                            }
                        }

                        double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                            if (strpaytype == "P")
                        {
                            #region Tax Calculation For Percent Type
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                                if (strpaytype == "Q")
                        {
                            dblTaxAmt = dblpercent * dblQty;
                            dblCalOnVal = dblQty;
                        }
                        else
                        {
                            if (strWtType == "C")
                            {
                                dblTaxAmt = dblCFTWt * dblpercent;
                                dblCalOnVal = dblCFTWt;
                            }
                            else
                            {
                                dblTaxAmt = dblTOTWt * dblpercent;
                                dblCalOnVal = dblTOTWt;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation

                dblTaxTotal = 0;
                dblCharges = 0;
                dblAdditionalTaxes = 0;
                dblAdditionalCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);

                            //   dblAdditionalTaxes = dblAdditionalTaxes + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        }
                        else
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                        }
                    }
                    else
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        }
                        else
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblCharges = dblCharges + Math.Round(c, 3);
                        }
                    }
                }
                dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblASSAmt = dblASSAmt + dblAccessAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationForRejectionDeliveryChallanNewForWOItem(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblAssAmt, double dblHeaderGrossAmt, double dblHeaderAssAmt)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strNone = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0.0000" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - dblDiscount;
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.0000 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.0000;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }

                        //Calculation 
                        double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                        {
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                    }
                                }
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation
                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        //double a = 0.000;
                        //a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //double b = 0.000;
                        //b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        //dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                        dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        //double c = 0.000;
                        //c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //dblCharges = dblCharges + Math.Round(c, 0);
                        dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    }
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblAssAmt = dblAssAmt + dblAccessAmt;
                //dblGrossAmt = dblGrossAmt + dblAmortTotal;
                //dblGrossAmt = dblGrossAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationForRejectionDCNewItem(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblAssAmt, double dblHeaderGrossAmt, double dblHeaderAssAmt, double TotItemQty, double dblCftWt, double dblTotWt)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strNone = "";
                string strWtType = "";
                double dblQty = 0.000;
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0.0000" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - dblDiscount;
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    dblQty = dblQty + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString());
                }
                dblGrossAmtCal = dblDiscountAmt != 0.0000 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.0000;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                                strWtType = DsTmp.Tables[0].Rows[0]["strWeightType"].ToString();
                            }
                        }

                        //Calculation 
                        double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            if (strGrossApp == "Y")
                            {
                                if (dblGrossAmtCal > 0)
                                    dblTaxAmt = (dblGrossAmtCal * (dblpercent / dblHeaderGrossAmt));
                                else
                                    dblTaxAmt = (dblpercent / TotItemQty);
                            }
                            else if (strAssessApp == "Y")
                            {
                                if (dblAccessAmt > 0)
                                    dblTaxAmt = (dblAccessAmt * (dblpercent / dblHeaderAssAmt));
                                else
                                    dblTaxAmt = (dblpercent / TotItemQty);
                            }
                            else
                            {
                                //For None Case Gross Take for By fergation of amount
                                dblTaxAmt = (dblpercent / TotItemQty);
                            }
                        }
                        else
                            if (strpaytype == "P")
                        {
                            #region Tax Calculation For Percent Type
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                                if (strpaytype == "Q")
                        {
                            dblTaxAmt = dblpercent * dblQty;
                            dblCalOnVal = dblQty;
                        }
                        else
                        {
                            if (strWtType == "C")
                            {
                                dblTaxAmt = dblCftWt * dblpercent;
                                dblCalOnVal = dblCftWt;
                            }
                            else
                            {
                                dblTaxAmt = dblTotWt * dblpercent;
                                dblCalOnVal = dblTotWt;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation  

                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        //double a = 0.000;
                        //a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //double b = 0.000;
                        //b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        //dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                        dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        //double c = 0.000;
                        //c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //dblCharges = dblCharges + Math.Round(c, 0);
                        dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    }
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblAssAmt = dblAssAmt + dblAccessAmt;
                //dblGrossAmt = dblGrossAmt + dblAmortTotal;
                //dblGrossAmt = dblGrossAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Delivery Challan Tax Calculation 
        public void TaxCalulationForDeliveryChallanNew(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges, ref double dblASSAmt, double dblCFTWt, double dblTOTWt)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblQty = 0.000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strNone = "";
                string strWtType = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0.0000" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - dblDiscount;
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }

                    dblQty = dblQty + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString());
                }
                dblGrossAmtCal = dblDiscountAmt != 0.0000 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.0000;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                                strWtType = DsTmp.Tables[0].Rows[0]["strWeightType"].ToString();
                            }
                        }
                        //Calculation 
                        double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                            if (strpaytype == "P")
                        {
                            #region Tax Calculation For Percent Type
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                                if (strpaytype == "Q")
                        {
                            dblTaxAmt = dblpercent * dblQty;
                            dblCalOnVal = dblQty;
                        }
                        else
                        {
                            if (strWtType == "C")
                            {
                                dblTaxAmt = dblCFTWt * dblpercent;
                                dblCalOnVal = dblCFTWt;
                            }
                            else
                            {
                                dblTaxAmt = dblTOTWt * dblpercent;
                                dblCalOnVal = dblTOTWt;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation

                dblTaxTotal = 0;
                dblCharges = 0;
                dblAdditionalTaxes = 0;
                dblAdditionalCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);

                            //   dblAdditionalTaxes = dblAdditionalTaxes + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        }
                        else
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                        }
                    }
                    else
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        }
                        else
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblCharges = dblCharges + Math.Round(c, 3);
                        }
                    }
                }
                dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblASSAmt = dblASSAmt + dblAccessAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationForDeliveryChallanNewForWOItem(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblAssAmt, double dblHeaderGrossAmt, double dblHeaderAssAmt)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strNone = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0.0000" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - dblDiscount;
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.0000 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.0000;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                //Tax Calculation
                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }

                        //Calculation 
                        double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                        {
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                    }
                                }
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation
                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        //double a = 0.000;
                        //a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //double b = 0.000;
                        //b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        //dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                        dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        //double c = 0.000;
                        //c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //dblCharges = dblCharges + Math.Round(c, 0);
                        dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    }
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblAssAmt = dblAssAmt + dblAccessAmt;
                //dblGrossAmt = dblGrossAmt + dblAmortTotal;
                //dblGrossAmt = dblGrossAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationForDeliveryChallanNewItem(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblAssAmt, double dblHeaderGrossAmt, double dblHeaderAssAmt, double TotItemQty, double dblCftWt, double dblTotWt)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                double dblQty = 0.000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strNone = "";
                string strWtType = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0.0000" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - dblDiscount;
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }

                    dblQty = dblQty + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString());
                }
                dblGrossAmtCal = dblDiscountAmt != 0.0000 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.0000;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                //Tax Calculation
                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                                strWtType = DsTmp.Tables[0].Rows[0]["strWeightType"].ToString();
                            }
                        }

                        //Calculation 

                        double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            if (strGrossApp == "Y")
                            {
                                if (dblGrossAmtCal > 0)
                                    dblTaxAmt = (dblGrossAmtCal * (dblpercent / dblHeaderGrossAmt));
                                else
                                    dblTaxAmt = (dblpercent / TotItemQty);
                            }
                            else if (strAssessApp == "Y")
                            {
                                if (dblAccessAmt > 0)
                                    dblTaxAmt = (dblAccessAmt * (dblpercent / dblHeaderAssAmt));
                                else
                                    dblTaxAmt = (dblpercent / TotItemQty);
                            }
                            else
                            {
                                //For None Case Gross Take for By fergation of amount
                                dblTaxAmt = (dblpercent / TotItemQty);
                            }

                        }
                        else
                            if (strpaytype == "P")
                        {
                            #region Tax Calculation For Percent Type
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;

                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                                if (strpaytype == "Q")
                        {
                            dblTaxAmt = dblpercent * dblQty;
                            dblCalOnVal = dblQty;
                        }
                        else
                        {
                            if (strWtType == "C")
                            {
                                dblTaxAmt = dblCftWt * dblpercent;
                                dblCalOnVal = dblCftWt;
                            }
                            else
                            {
                                dblTaxAmt = dblTotWt * dblpercent;
                                dblCalOnVal = dblTotWt;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation  

                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        //double a = 0.000;
                        //a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //double b = 0.000;
                        //b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        //dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                        dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        //double c = 0.000;
                        //c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //dblCharges = dblCharges + Math.Round(c, 0);
                        dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    }
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblAssAmt = dblAssAmt + dblAccessAmt;
                //dblGrossAmt = dblGrossAmt + dblAmortTotal;
                //dblGrossAmt = dblGrossAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //New Tax Calculation Method For Other Taxes
        //For Delivery Challan
        public void TaxCalulationWithOutCustomerVendorID(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges, ref double dblASSAmt)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0.0000" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - dblDiscount;
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.0000 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.0000;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    double dblCalOnVal = 0;

                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        dblTaxAmt = 0;
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            }
                        }
                        else
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = "Y";
                            strAssessApp = "N";
                        }

                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }

                                            if (strpaytype == "A")
                                            {
                                                dblTaxAmt = dblTaxAmt;
                                            }
                                            else
                                            {
                                                dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                                //Calculated On value
                                                dblCalOnVal = dblCalOnVal + Amt;
                                            }
                                        }
                                        else
                                        {
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    ChkFalg = false;
                                                    if (strpaytype == "A")
                                                    {
                                                        dblTaxAmt = dblTaxAmt;
                                                    }
                                                    else
                                                    {
                                                        dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblTaxAmt + dblpercent;
                                            else
                                                dblTaxAmt = dblTaxAmt + (dblGrossAmtCal * dblpercent) / 100;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblTaxAmt + dblpercent;
                                            else
                                                dblTaxAmt = dblTaxAmt + (dblAccessAmt * dblpercent) / 100;
                                        }
                                    }
                                }
                            }
                            dblSurChargeAmt = 0;
                            if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                                dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                        }
                        else
                        {
                            dblTaxAmt = 0;
                            if (strGrossApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                            }
                            else if (strAssessApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                dblTaxTotal = 0;
                dblCharges = 0;
                dblAdditionalTaxes = 0;
                dblAdditionalCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);
                            //   dblAdditionalTaxes = dblAdditionalTaxes + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        }
                        else
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                        }
                    }
                    else
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        }
                        else
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblCharges = dblCharges + Math.Round(c, 3);
                        }
                    }
                }
                // dblASSAmt = dblAccessAmt;
                //    dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblASSAmt = dblASSAmt + dblAccessAmt;
                //dblGrossAmt = dblGrossAmt + dblAmortTotal;
                //dblGrossAmt = dblGrossAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationForOA(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strWtType = "";
                string strNone = "";
                double dblQty = 0.00;
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                    dblAccessAmt = dblAccessAmt + dblAmortAmt;
                    dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    dblQty = dblQty + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString());
                }

                dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.00;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }

                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();

                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        DsTmp = new DataSet();
                        DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                        if (DsTmp.Tables[0].Rows.Count > 0)
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                            strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                            //Add Group And Tax Type
                            dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                            dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            strWtType = DsTmp.Tables[0].Rows[0]["strWeightType"].ToString();
                        }
                        //Calculation 
                        //    double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                            if (strpaytype == "P")
                        {
                            #region Tax Calculation For Percent Type
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            //if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            //{
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            //}
                            //else
                            //{
                            //    DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            //}
                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                                if (strpaytype == "Q")
                        {
                            dblTaxAmt = dblpercent * 0;
                            //dblCalOnVal = dblQty;
                        }
                        else
                        {
                            if (strWtType == "C")
                            {
                                dblTaxAmt = 0 * dblpercent;
                                // dblCalOnVal = dblCFTWt;
                            }
                            else
                            {
                                dblTaxAmt = 0 * dblpercent;
                                // dblCalOnVal = dblTOTWt;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        //dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }

                dblTaxTotal = 0;
                dblCharges = 0;
                dblAdditionalTaxes = 0;
                dblAdditionalCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double a = 0.000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);

                            //   dblAdditionalTaxes = dblAdditionalTaxes + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        }
                        else
                        {
                            double a = 0.000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                        }
                    }
                    else
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double c = 0.000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        }
                        else
                        {
                            double c = 0.000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblCharges = dblCharges + Math.Round(c, 3);
                        }
                    }
                }
                // dblASSAmt = dblAccessAmt;
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationForPO(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strWtType = "";
                string strNone = "";
                double dblQty = 0.00;
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                    dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                    dblAccessAmt = dblAccessAmt + dblAmortAmt;
                    dblAmortTotal = dblAmortTotal + dblAmortAmt;

                    dblQty = dblQty + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString());
                }

                dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.00;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }

                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                                //  strWtType = DsTmp.Tables[0].Rows[0]["strWeightType"].ToString();
                            }
                        }

                        //Calculation 
                        //    double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                            if (strpaytype == "P")
                        {
                            #region Tax Calculation For Percent Type
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }

                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            // dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    //   dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            // dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            //  dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        //dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation
                //for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                //{
                //    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                //    {
                //        dblTaxAmt = 0;
                //        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                //        {
                //            DsTmp = new DataSet();
                //            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                //            if (DsTmp.Tables[0].Rows.Count > 0)
                //            {
                //                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                //                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                //                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                //                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                //                strTaxType = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                //            }
                //        }
                //        else
                //        {
                //            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                //            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                //            strGrossApp = "Y";
                //            strAssessApp = "N";
                //            strTaxType = dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString();
                //        }
                //        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                //        {
                //            DsTmp = new DataSet();
                //            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                //            if (DsTmp.Tables[0].Rows.Count > 0)
                //            {
                //                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                //                {
                //                    //if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                //                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                //                    {
                //                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                //                        {
                //                            Double Amt = 0;
                //                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                //                            {
                //                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                //                                {
                //                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                //                                }
                //                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                //                                {
                //                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                //                                }
                //                            }
                //                            if (strpaytype == "A")
                //                            {
                //                                dblTaxAmt = dblTaxAmt;
                //                            }
                //                            else
                //                            {
                //                                dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                //                            }
                //                        }
                //                        else
                //                        {
                //                            ChkFalg = true;
                //                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                //                            {
                //                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                //                                {
                //                                    ChkFalg = false;
                //                                    if (strpaytype == "A")
                //                                    {
                //                                        dblTaxAmt = dblTaxAmt;
                //                                    }
                //                                    else
                //                                    {
                //                                        dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100) / 100));
                //                                    }
                //                                }
                //                            }
                //                            if (ChkFalg == true)
                //                            {
                //                                //Response.Write("<script>alert(' One of the Taxes Is Not Added...</script>");
                //                                //ScriptManager.RegisterStartupScript(p, p.GetType(), Guid.NewGuid().ToString(), "alert('One of the Taxes Is Not Added...')", true);
                //                            }
                //                        }
                //                    }
                //                    else
                //                    {
                //                        //   dblTaxAmt = 0;
                //                        if (strGrossApp == "Y")
                //                        {
                //                            if (strpaytype == "A")
                //                                dblTaxAmt = dblTaxAmt + dblpercent;
                //                            else
                //                                dblTaxAmt = dblTaxAmt + (((dblGrossAmtCal * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100) * dblpercent) / 100;
                //                        }
                //                        else if (strAssessApp == "Y")
                //                        {
                //                            if (strpaytype == "A")
                //                                dblTaxAmt = dblTaxAmt + dblpercent;
                //                            else
                //                                dblTaxAmt = dblTaxAmt + (((dblAccessAmt * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100) * dblpercent) / 100;
                //                        }
                //                    }
                //                }
                //            }
                //            dblSurChargeAmt = 0;
                //            if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                //            {
                //                dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                //            }
                //        }
                //        else
                //        {
                //            dblTaxAmt = 0;
                //            if (strGrossApp == "Y")
                //            {
                //                if (strpaytype == "A")
                //                    dblTaxAmt = dblpercent;
                //                else
                //                    dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                //            }
                //            else if (strAssessApp == "Y")
                //            {
                //                if (strpaytype == "A")
                //                    dblTaxAmt = dblpercent;
                //                else
                //                    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                //            }
                //        }
                //        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(Math.Round(dblTaxAmt, 3).ToString("0#########.###"));
                //        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                //            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(Math.Round(dblSurChargeAmt, 3).ToString("0#########.###"));
                //        //dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(Math.Round(dblTaxAmt, 0).ToString("######0.00"));
                //        //if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                //        //    dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(Math.Round(dblSurChargeAmt, 0).ToString("######0.00"));
                //    }
                //}
                dblTaxTotal = 0;
                dblCharges = 0;
                dblAdditionalTaxes = 0;
                dblAdditionalCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double a = 0.000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);
                            //   dblAdditionalTaxes = dblAdditionalTaxes + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        }
                        else
                        {
                            double a = 0.000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                        }
                    }
                    else
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double c = 0.000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        }
                        else
                        {
                            double c = 0.000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblCharges = dblCharges + Math.Round(c, 3);
                        }
                    }
                }
                // dblASSAmt = dblAccessAmt;
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationForQuote(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.000;
                double dblAccessAmt = 0.000;
                double dblpercent = 0.00;
                double dblGrossAmtCal = 0.000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strTaxType = "";
                string strNone = "";

                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                }

                dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.00;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {

                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                                //  strWtType = DsTmp.Tables[0].Rows[0]["strWeightType"].ToString();
                            }
                        }

                        //Calculation 
                        //    double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                            if (strpaytype == "P")
                        {
                            #region Tax Calculation For Percent Type
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            // dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    //   dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            // dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            //  dblCalOnVal = dblCalOnVal + Amt;

                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        //dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation

                dblTaxTotal = 0;
                dblCharges = 0;

                //for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                //{
                //    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                //    {
                //        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                //        {
                //            double a = 0.000;
                //            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                //            double b = 0.000;
                //            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                //            dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);
                //            //   dblAdditionalTaxes = dblAdditionalTaxes + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                //        }
                //        else
                //        {
                //            double a = 0.000;
                //            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                //            double b = 0.000;
                //            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                //            dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                //        }
                //    }
                //    else
                //    {
                //        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                //        {
                //            double c = 0.000;
                //            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                //            dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                //        }
                //        else
                //        {
                //            double c = 0.000;
                //            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                //            dblCharges = dblCharges + Math.Round(c, 3);
                //        }
                //    }
                //}
                // dblASSAmt = dblAccessAmt;

                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                        //dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblCharges = dblCharges + Math.Round(c, 0);
                        //dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    }
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                //dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                //dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationForJFS(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.000;
                double dblAccessAmt = 0.000;
                double dblpercent = 0.00;
                double dblGrossAmtCal = 0.000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strTaxType = "";
                string strNone = "";

                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                }

                dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.00;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }

                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                                //  strWtType = DsTmp.Tables[0].Rows[0]["strWeightType"].ToString();
                            }
                        }
                        //Calculation 

                        //    double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                            if (strpaytype == "P")
                        {
                            #region Tax Calculation For Percent Type
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            // dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                                    //   dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            // dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            //  dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(Math.Round(dblTaxAmt).ToString("0#########.####"));
                        //dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation

                dblTaxTotal = 0;
                dblCharges = 0;

                //for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                //{
                //    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                //    {
                //        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                //        {
                //            double a = 0.000;
                //            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                //            double b = 0.000;
                //            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                //            dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);
                //            //   dblAdditionalTaxes = dblAdditionalTaxes + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                //        }
                //        else
                //        {
                //            double a = 0.000;
                //            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                //            double b = 0.000;
                //            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                //            dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                //        }
                //    }
                //    else
                //    {
                //        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                //        {
                //            double c = 0.000;
                //            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                //            dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                //        }
                //        else
                //        {
                //            double c = 0.000;
                //            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                //            dblCharges = dblCharges + Math.Round(c, 3);
                //        }
                //    }
                //}
                // dblASSAmt = dblAccessAmt;

                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        double a = 0.000;
                        a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        double b = 0.000;
                        b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                        //dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        double c = 0.000;
                        c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        dblCharges = dblCharges + Math.Round(c, 0);
                        //dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    }
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                //dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                //dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationWithOutCustomerVendorID(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.000;
                double dblAccessAmt = 0.000;
                double dblpercent = 0.00;
                double dblGrossAmtCal = 0.000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strTaxType = "";

                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                }

                dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.00;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }

                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        dblTaxAmt = 0;
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strTaxType = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                            }
                        }
                        else
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = "Y";
                            strAssessApp = "N";
                            strTaxType = dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString();
                        }

                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();

                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    //if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }

                                            if (strpaytype == "A")
                                            {
                                                dblTaxAmt = dblTaxAmt;
                                            }
                                            else
                                            {
                                                dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            }
                                        }
                                        else
                                        {
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    ChkFalg = false;
                                                    if (strpaytype == "A")
                                                    {
                                                        dblTaxAmt = dblTaxAmt;
                                                    }
                                                    else
                                                    {
                                                        dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100) / 100));
                                                    }
                                                }
                                            }
                                            if (ChkFalg == true)
                                            {
                                                //Response.Write("<script>alert(' One of the Taxes Is Not Added...</script>");
                                                //ScriptManager.RegisterStartupScript(p, p.GetType(), Guid.NewGuid().ToString(), "alert('One of the Taxes Is Not Added...')", true);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //   dblTaxAmt = 0;
                                        if (strGrossApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblTaxAmt + dblpercent;
                                            else
                                                dblTaxAmt = dblTaxAmt + (((dblGrossAmtCal * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100) * dblpercent) / 100;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblTaxAmt + dblpercent;
                                            else
                                                dblTaxAmt = dblTaxAmt + (((dblAccessAmt * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100) * dblpercent) / 100;
                                        }
                                    }
                                }
                            }
                            dblSurChargeAmt = 0;
                            if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                            {
                                dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                            }
                        }
                        else
                        {
                            dblTaxAmt = 0;
                            if (strGrossApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                            }
                            else if (strAssessApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(Math.Round(dblTaxAmt, 3).ToString("0#########.###"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(Math.Round(dblSurChargeAmt, 3).ToString("0#########.###"));

                        //dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(Math.Round(dblTaxAmt, 0).ToString("######0.00"));
                        //if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                        //    dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(Math.Round(dblSurChargeAmt, 0).ToString("######0.00"));
                    }
                }
                dblTaxTotal = 0;
                dblCharges = 0;
                dblAdditionalTaxes = 0;
                dblAdditionalCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double a = 0.000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);
                            //   dblAdditionalTaxes = dblAdditionalTaxes + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        }
                        else
                        {
                            double a = 0.000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                        }
                    }
                    else
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double c = 0.000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        }
                        else
                        {
                            double c = 0.000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblCharges = dblCharges + Math.Round(c, 3);
                        }
                    }
                }
                // dblASSAmt = dblAccessAmt;
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationWithOutCustomerVendorIDDeliveryChallan(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblAssAmt, double dblHeaderGrossAmt, double dblHeaderAssAmt)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0.0000" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - dblDiscount;
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.0000 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.0000;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        dblTaxAmt = 0;
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            }
                        }
                        else
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = "Y";
                            strAssessApp = "N";
                        }

                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            if (strpaytype == "A")
                                            {
                                                dblTaxAmt = dblTaxAmt;
                                            }
                                            else
                                            {
                                                dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            }
                                        }
                                        else
                                        {
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    ChkFalg = false;
                                                    if (strpaytype == "A")
                                                    {
                                                        dblTaxAmt = dblTaxAmt;
                                                    }
                                                    else
                                                    {
                                                        dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // dblTaxAmt = 0;
                                        if (strGrossApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblTaxAmt + (dblGrossAmtCal * (dblpercent / dblHeaderGrossAmt));
                                            else
                                                dblTaxAmt = dblTaxAmt + ((dblGrossAmtCal * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100) * dblpercent) / 100;

                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = dblTaxAmt + (dblAccessAmt * (dblpercent / dblHeaderAssAmt));
                                            else
                                                dblTaxAmt = dblTaxAmt + ((dblAccessAmt * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100) * dblpercent) / 100;

                                            //if (strpaytype == "A")
                                            //    dblTaxAmt = dblpercent;
                                            //else
                                            //    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                                        }
                                    }
                                }
                            }
                            dblSurChargeAmt = 0;
                            if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                                dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                        }
                        else
                        {
                            dblTaxAmt = 0;
                            if (strGrossApp == "Y")
                            {
                                //if (strpaytype == "A")
                                //    dblTaxAmt = dblpercent;
                                //else
                                //    dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                                if (strpaytype == "A")
                                    dblTaxAmt = (dblGrossAmtCal * (dblpercent / dblHeaderGrossAmt));
                                else
                                    dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                            }
                            else if (strAssessApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = (dblAccessAmt * (dblpercent / dblHeaderAssAmt));
                                else
                                    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;

                                //if (strpaytype == "A")
                                //    dblTaxAmt = dblpercent;
                                //else
                                //    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        //double a = 0.000;
                        //a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //double b = 0.000;
                        //b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        //dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                        dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        //double c = 0.000;
                        //c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //dblCharges = dblCharges + Math.Round(c, 0);
                        dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    }
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblAssAmt = dblAssAmt + dblAccessAmt;
                //dblGrossAmt = dblGrossAmt + dblAmortTotal;
                //dblGrossAmt = dblGrossAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationWithOutCustomerVendorIDDeliveryChallan(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblAssAmt)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0.0000" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - dblDiscount;
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.0000 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.0000;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        dblTaxAmt = 0;
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                            }
                        }
                        else
                        {
                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                            strGrossApp = "Y";
                            strAssessApp = "N";
                        }

                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            if (strpaytype == "A")
                                            {
                                                dblTaxAmt = dblTaxAmt;
                                            }
                                            else
                                            {
                                                dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            }
                                        }
                                        else
                                        {
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    ChkFalg = false;
                                                    if (strpaytype == "A")
                                                    {
                                                        dblTaxAmt = dblTaxAmt;
                                                    }
                                                    else
                                                    {
                                                        dblTaxAmt = dblTaxAmt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dblTaxAmt = 0;
                                        if (strGrossApp == "Y")
                                        {
                                            if (strpaytype == "A")
                                                dblTaxAmt = (dblGrossAmtCal);
                                            else
                                                dblTaxAmt = ((dblGrossAmtCal * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100) * dblpercent) / 100;

                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            //if (strpaytype == "A")
                                            //    dblTaxAmt = (dblAccessAmt * (dblpercent / dblHeaderAssAmt));
                                            //else
                                            //    dblTaxAmt = ((dblAccessAmt * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100) * dblpercent) / 100;

                                            if (strpaytype == "A")
                                                dblTaxAmt = dblpercent;
                                            else
                                                dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                                        }
                                    }
                                }
                            }
                            dblSurChargeAmt = 0;
                            if (Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString()) > 0)
                                dblSurChargeAmt = (dblTaxAmt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["SurchargePercentage"].ToString())) / 100;
                        }
                        else
                        {
                            dblTaxAmt = 0;
                            if (strGrossApp == "Y")
                            {
                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                                //if (strpaytype == "A")
                                //    dblTaxAmt = (dblGrossAmtCal * (dblpercent / dblHeaderGrossAmt));
                                //else
                                //    dblTaxAmt = (dblGrossAmtCal * dblpercent) / 100;
                            }
                            else if (strAssessApp == "Y")
                            {
                                //if (strpaytype == "A")
                                //    dblTaxAmt = (dblAccessAmt * (dblpercent / dblHeaderAssAmt));
                                //else
                                //    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;

                                if (strpaytype == "A")
                                    dblTaxAmt = dblpercent;
                                else
                                    dblTaxAmt = (dblAccessAmt * dblpercent) / 100;
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                dblTaxTotal = 0;
                dblCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        //double a = 0.000;
                        //a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //double b = 0.000;
                        //b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        //dblTaxTotal = dblTaxTotal + Math.Round(a, 0) + Math.Round(b, 0);
                        dblTaxTotal = dblTaxTotal + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                    }
                    else
                    {
                        //double c = 0.000;
                        //c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                        //dblCharges = dblCharges + Math.Round(c, 0);
                        dblCharges = dblCharges + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                    }
                }
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblAssAmt = dblAssAmt + dblAccessAmt;
                //dblGrossAmt = dblGrossAmt + dblAmortTotal;
                //dblGrossAmt = dblGrossAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //==================Tax Calculation for Supplementory
        public void TaxCalulationForSupplementoryDCMain(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.000;
                double dblAccessAmt = 0.000;
                double dblpercent = 0.00;
                double dblGrossAmtCal = 0.000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strTaxType = "";
                string strNone = "";

                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.00 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.00;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }

                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        //Calculation 
                        double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                        {
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                    }
                                }
                            }
                        }

                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation
                dblTaxTotal = 0;
                dblCharges = 0;
                dblAdditionalTaxes = 0;
                dblAdditionalCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);

                            //   dblAdditionalTaxes = dblAdditionalTaxes + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        }
                        else
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                        }
                    }
                    else
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        }
                        else
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblCharges = dblCharges + Math.Round(c, 3);
                        }
                    }
                }
                dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
                // dblASSAmt = dblAccessAmt;
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TaxCalulationForSupplementoryDeliveryChallanNew(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblAssAmt, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strNone = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0.0000" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - dblDiscount;
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                }

                dblGrossAmtCal = dblDiscountAmt != 0.0000 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.0000;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }

                //Tax Calculation
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        //Calculation 
                        double dblCalOnVal = 0;
                        dblTaxAmt = 0;

                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                        {
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }

                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                    }
                                }
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation

                dblTaxTotal = 0;
                dblCharges = 0;
                dblAdditionalTaxes = 0;
                dblAdditionalCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);

                            //   dblAdditionalTaxes = dblAdditionalTaxes + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        }
                        else
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                        }
                    }
                    else
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        }
                        else
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblCharges = dblCharges + Math.Round(c, 3);
                        }
                    }
                }
                dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblAssAmt = dblAssAmt + dblAccessAmt;
                //dblGrossAmt = dblGrossAmt + dblAmortTotal;
                //dblGrossAmt = dblGrossAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Tax Cal for Tax Diff
        public void TaxCalulationForSupplementoryDeliveryChallanNewForTaxDiffCase(DataSet dsItemDetail, DataSet dsTaxDetail, int RecNo, ref double dblTaxAmt, ref double dblSurChargeAmt, ref double dblGrossAmt, ref double dblDiscountAmt, ref double dblNetAmt, ref double dblTaxTotal, ref double dblCharges, ref double dblAssAmt, ref double dblCTC, ref double dblAdditionalTaxes, ref double dblAdditionalCharges)//, Page p)
        {
            try
            {
                dblGrossAmt = 0.0000;
                double dblAccessAmt = 0.0000;
                double dblAmortAmt = 0.0000;
                double dblAmortTotal = 0.0000;
                double dblpercent = 0.0000;
                double dblGrossAmtCal = 0.00000;
                string strpaytype = "";
                string strGrossApp = "";
                string strAssessApp = "";
                string strNone = "";
                Boolean ChkFalg;

                for (int i = 0; i < dsItemDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0.0000" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "" || dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString() == "0")
                    {
                        dblGrossAmt = dblGrossAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString());
                        dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "A")
                    {
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString());
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                    else if (dsItemDetail.Tables[0].Rows[i]["ItemDiscountPAFlag"].ToString() == "P")
                    {
                        double dblDiscount = ((Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["ItemDiscount"].ToString())) / 100;
                        dblGrossAmt = dblGrossAmt + (Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Rate"].ToString())) - dblDiscount;
                        if (dsItemDetail.Tables[0].Rows[i]["strDiscAssFlag"].ToString() == "Y")
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString()) - dblDiscount;
                        else
                            dblAccessAmt = dblAccessAmt + Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AccessRate"].ToString());
                        dblAmortAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["Qty"].ToString()) * Convert.ToDouble(dsItemDetail.Tables[0].Rows[i]["AmortRate"].ToString());
                        dblAccessAmt = dblAccessAmt + dblAmortAmt;
                        dblAmortTotal = dblAmortTotal + dblAmortAmt;
                    }
                }
                dblGrossAmtCal = dblDiscountAmt != 0.0000 ? dblGrossAmt - dblDiscountAmt : dblGrossAmt; dblDiscountAmt = 0.0000;
                if (Convert.ToDecimal(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString()) > 0)
                {
                    if (dsItemDetail.Tables[0].Rows[0]["GrossDiscountFlag"].ToString() == "P")
                    {
                        dblDiscountAmt = ((dblGrossAmt * Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString())) / 100);
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                    else
                    {
                        dblDiscountAmt = Convert.ToDouble(dsItemDetail.Tables[0].Rows[0]["GrossDiscount"].ToString());
                        dblGrossAmtCal = dblGrossAmt - dblDiscountAmt;
                    }
                }
                dblNetAmt = dblGrossAmt - dblDiscountAmt;
                DataSet DsTmp = new DataSet();
                DataTable DtTmp = new DataTable();

                DtTmp = ObjDataServices.FillDT("ssp_GetTaxChargeCalculationTblChargeMasterDetail");
                if (DtTmp.Rows.Count > 0)
                {
                    for (int i = 0; i < DtTmp.Rows.Count; i++)
                    {
                        if (DtTmp.Rows[i]["strChargeCode"].ToString().Trim() == "GOO1")
                            dblGrossAmtCal = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblGrossAmtCal)) / 100);
                        else
                            dblAccessAmt = Convert.ToDouble((Convert.ToDecimal(DtTmp.Rows[i]["dblChargeValue"].ToString()) * Convert.ToDecimal(dblAccessAmt)) / 100);
                    }
                }
                //Tax Calculation

                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    //Master Details
                    if (Convert.ToInt32(dsTaxDetail.Tables[0].Rows[i]["TaxId"].ToString()) > 0)
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                                strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();

                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                //Add Group And Tax Type
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strTaxType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        else
                        {
                            DsTmp = new DataSet();
                            DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));

                            dblpercent = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString());
                            strpaytype = dsTaxDetail.Tables[0].Rows[i]["TaxChargePaFlag"].ToString();

                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                strGrossApp = DsTmp.Tables[0].Rows[0]["strGrossAppFlag"].ToString();
                                strAssessApp = DsTmp.Tables[0].Rows[0]["strAccessAppFlag"].ToString();
                                strNone = DsTmp.Tables[0].Rows[0]["strNoneAppFlag"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strTaxType"] = DsTmp.Tables[0].Rows[0]["strChargeType"].ToString();
                                dsTaxDetail.Tables[0].Rows[i]["strGroup"] = DsTmp.Tables[0].Rows[0]["strGroup"].ToString();
                            }
                        }
                        //Calculation 
                        double dblCalOnVal = 0;
                        dblTaxAmt = 0;
                        if (strpaytype == "A")
                        {
                            dblTaxAmt = dblpercent;
                        }
                        else
                        {
                            //Check Tax Type C/T
                            DsTmp = new DataSet();
                            if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString().Trim() == "T")
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblTaxDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }
                            else
                            {
                                DsTmp = ObjDataServices.FillDS("ssp_GetTaxChargeCalculationTblChargeDetailMaster", Convert.ToString(dsTaxDetail.Tables[0].Rows[i]["TaxID"]));
                            }

                            //DS Temp 
                            if (DsTmp.Tables[0].Rows.Count > 0)
                            {
                                for (int J = 0; J < DsTmp.Tables[0].Rows.Count; J++)
                                {
                                    if (DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "G001" && DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim() != "A001") // && DsTmp.Tables[0].Rows[J]["strCTFlag"].ToString().Trim() == "T")
                                    {
                                        if (DsTmp.Tables[0].Rows[J]["strGroupFlag"].ToString().Trim() == "Y")
                                        {
                                            Double Amt = 0;

                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "T" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                                if (dsTaxDetail.Tables[0].Rows[b]["TaxChargeType"].ToString() == "C" && dsTaxDetail.Tables[0].Rows[b]["strGroup"].ToString() == DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString().Trim())
                                                {
                                                    Amt = Amt + Convert.ToDouble((Convert.ToDecimal(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDecimal(DsTmp.Tables[0].Rows[J]["dblValue"].ToString())) / 100);
                                                }
                                            }
                                            dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100);
                                            //Calculated On value
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else
                                        {
                                            Double Amt = 0;
                                            ChkFalg = true;
                                            for (int b = 0; b < dsTaxDetail.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(DsTmp.Tables[0].Rows[J]["strCalculatedOn"].ToString()) == Convert.ToInt32(dsTaxDetail.Tables[0].Rows[b]["TaxID"]))
                                                {
                                                    Amt = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[b]["TaxCharge"].ToString()) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;

                                                    dblTaxAmt = dblTaxAmt + Convert.ToDouble((Amt * (Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxChargeValue"].ToString())) / 100));

                                                    dblCalOnVal = dblCalOnVal + Amt;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (strGrossApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblGrossAmtCal) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                        else if (strAssessApp == "Y")
                                        {
                                            Double Amt = 0;
                                            Amt = Convert.ToDouble(dblAccessAmt) * Convert.ToDouble(DsTmp.Tables[0].Rows[J]["dblValue"].ToString()) / 100;
                                            dblTaxAmt = dblTaxAmt + (Amt * dblpercent) / 100;
                                            dblCalOnVal = dblCalOnVal + Amt;
                                        }
                                    }
                                }
                            }
                        }
                        dsTaxDetail.Tables[0].Rows[i]["TaxCharge"] = Convert.ToDecimal(dblTaxAmt.ToString("0#########.####"));
                        dsTaxDetail.Tables[0].Rows[i]["dblOnTAmount"] = Convert.ToDecimal(dblCalOnVal.ToString("0#########.####"));
                        if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                            dsTaxDetail.Tables[0].Rows[i]["Surcharge"] = Convert.ToDecimal(dblSurChargeAmt.ToString("0#########.####"));
                    }
                }
                // End Of Calculation

                dblTaxTotal = 0;
                dblCharges = 0;
                dblAdditionalTaxes = 0;
                dblAdditionalCharges = 0;
                for (int i = 0; i < dsTaxDetail.Tables[0].Rows.Count; i++)
                {
                    if (dsTaxDetail.Tables[0].Rows[i]["TaxChargeType"].ToString() == "T")
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblAdditionalTaxes = dblAdditionalTaxes + Math.Round(a, 3) + Math.Round(b, 3);

                            //   dblAdditionalTaxes = dblAdditionalTaxes + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString()) + Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                        }
                        else
                        {
                            double a = 0.0000;
                            a = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            double b = 0.0000;
                            b = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["Surcharge"].ToString());
                            dblTaxTotal = dblTaxTotal + Math.Round(a, 3) + Math.Round(b, 3);
                        }
                    }
                    else
                    {
                        if (dsTaxDetail.Tables[0].Rows[i]["strTaxType"].ToString() == "C")
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblAdditionalCharges = dblAdditionalCharges + Math.Round(c, 3);
                        }
                        else
                        {
                            double c = 0.0000;
                            c = Convert.ToDouble(dsTaxDetail.Tables[0].Rows[i]["TaxCharge"].ToString());
                            dblCharges = dblCharges + Math.Round(c, 3);
                        }
                    }
                }
                dblCTC = dblNetAmt + dblAdditionalCharges + dblAdditionalTaxes;
                dblNetAmt = dblNetAmt + dblTaxTotal + dblCharges;
                dblAssAmt = dblAssAmt + dblAccessAmt;
                //dblGrossAmt = dblGrossAmt + dblAmortTotal;
                //dblGrossAmt = dblGrossAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //======================= IP Address =================================
        public string IPAddress()
        {
            try
            {
                string strHostName = "";
                strHostName = System.Net.Dns.GetHostName();
                IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
                IPAddress[] addr = ipEntry.AddressList;

                return addr[addr.Length - 1].ToString();
            }
            catch (Exception)
            {
                throw;
            }
        }

        //#region  ///[Inserting Pending Request Before Posting]
        //public void InsertRecordInToAuthorityApprovalPendingTable(string strDocType, string strDocNo, string strDocDate, string strPartyName, int companyId, int BranchId, int FyId, string frmFormName)
        //{
        //    try
        //    {
        //        //Check if that form or document has authority level
        //        DataSet ds = new DataSet();
        //        object[] param = new object[1];
        //        param[0] = frmFormName;
        //        ds = FillDataSet("ssp_CheckAuthorityLevel", param);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            int Count = 0;
        //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //            {
        //                object[] param1 = new object[14];
        //                param1[0] = strDocType;
        //                param1[1] = strDocNo;
        //                param1[2] = strDocDate;
        //                param1[3] = strPartyName;
        //                param1[4] = ds.Tables[0].Rows[i]["intSequence"];
        //                param1[5] = ds.Tables[0].Rows[i]["intUserId"];
        //                param1[6] = ds.Tables[0].Rows[i]["strEmpId"];
        //                param1[7] = ds.Tables[0].Rows[i]["intDocumentID"];
        //                param1[8] = companyId;
        //                param1[9] = BranchId;
        //                param1[10] = FyId;
        //                param1[11] = "Pending";
        //                param1[12] = ds.Tables[0].Rows[i]["strOptionalFlag"];
        //                param1[13] = ds.Tables[0].Rows[i]["strSuperUserFlag"];

        //                Count += ObjDataServices.SaveQuery("ssp_InsertTblAuthorisationApproval", param1);
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        //public bool CheckAuthorityForApproval(string frmFormName)
        //{
        //    //Check if that form or document has authority level
        //    DataSet ds = new DataSet();
        //    object[] param = new object[1];
        //    param[0] = frmFormName;
        //    ds = FillDataSet("ssp_CheckAuthorityLevel", param);
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        return true;
        //    }
        //    return false;
        //}
        //#endregion

        #region  ///[Inserting Pending Request Before Posting]
        public void InsertRecordInToAuthorityApprovalPendingTableForPurchaseOrder(string strDocType, string strDocNo, int intDocId, string strDocDate, string strPartyName, int companyId, int BranchId, int FyId, string frmFormName, string DocCategory, string RevisionFlag, int DeptId, int SubDeptId, int UserId, int ReportingToId)
        {
            try
            {
                DataSet dsMaxApprovalsetNo = new DataSet();  // For Max ApprovalsetNo
                //Check if that form or document has authority level
                DataSet ds = new DataSet();
                object[] param = new object[3];
                param[0] = frmFormName;
                param[1] = RevisionFlag;
                param[2] = BranchId;

                ds = FillDataSet("ssp_CheckAuthorityLevel", param);

                //Check if that form or document and document category has authority level
                DataSet ds2 = new DataSet();
                object[] param2 = new object[4];
                param2[0] = frmFormName;
                param2[1] = DocCategory;
                param2[2] = RevisionFlag;
                param2[3] = BranchId;
                ds2 = FillDataSet("ssp_CheckAuthorityLevelForDocumentCategory", param2);

                //Auto Fill Department/SubDepartment Approval User for form or document
                for (int p = 0; p < ds.Tables[0].Rows.Count; p++)
                {
                    if (ds.Tables[0].Rows[p]["strType"].ToString() != "Employee")
                    {
                        DataSet DSdocUser = new DataSet();
                        int TypeID = 0;

                        if (Convert.ToInt32(ds.Tables[0].Rows[p]["intTypeId"]) == 0)
                        {
                            if (ds.Tables[0].Rows[p]["strType"].ToString() == "Department")
                                TypeID = DeptId;
                            else if (ds.Tables[0].Rows[p]["strType"].ToString() == "SubDepartment")
                                TypeID = SubDeptId;
                            else if (ds.Tables[0].Rows[p]["strType"].ToString() == "ReportingOfficer")
                                TypeID = ReportingToId;
                        }
                        else
                            TypeID = Convert.ToInt32(ds.Tables[0].Rows[p]["intTypeId"]);

                        object[] param4 = new object[4];
                        param4[0] = ds.Tables[0].Rows[p]["strType"].ToString();
                        param4[1] = TypeID.ToString();
                        param4[2] = ds.Tables[0].Rows[p]["intDesignationId"].ToString();
                        param4[3] = BranchId.ToString();
                        DSdocUser = FillDataSet("ssp_GetDeptSubDeptUsersForAuthorisationApproval", param4);

                        if (DSdocUser != null && DSdocUser.Tables[0].Rows.Count > 0)
                        {
                            ds.Tables[0].Rows[p]["intUserId"] = DSdocUser.Tables[0].Rows[0]["user1"].ToString();
                            ds.Tables[0].Rows[p]["intSecondaryUserId"] = DSdocUser.Tables[0].Rows[0]["user2"].ToString();
                            ds.Tables[0].Rows[p]["intTertiaryUserId"] = DSdocUser.Tables[0].Rows[0]["user3"].ToString();
                            ds.Tables[0].Rows[p]["strEmpId"] = DSdocUser.Tables[0].Rows[0]["intEmployeeId"].ToString();
                        }
                    }
                }

                //Auto Fill Department/SubDepartment Approval User for form or document and document category
                for (int q = 0; q < ds2.Tables[0].Rows.Count; q++)
                {
                    if (ds2.Tables[0].Rows[q]["strType"].ToString() != "Employee")
                    {
                        DataSet DSdocCategoryUser = new DataSet();
                        int TypeID = 0;

                        if (Convert.ToInt32(ds2.Tables[0].Rows[q]["intTypeId"]) == 0)
                        {
                            if (ds2.Tables[0].Rows[q]["strType"].ToString() == "Department")
                                TypeID = DeptId;
                            else if (ds2.Tables[0].Rows[q]["strType"].ToString() == "SubDepartment")
                                TypeID = SubDeptId;
                            else if (ds2.Tables[0].Rows[q]["strType"].ToString() == "ReportingOfficer")
                                TypeID = ReportingToId;
                        }
                        else
                            TypeID = Convert.ToInt32(ds2.Tables[0].Rows[q]["intTypeId"]);

                        object[] param5 = new object[4];
                        param5[0] = ds2.Tables[0].Rows[q]["strType"].ToString();
                        param5[1] = TypeID.ToString();
                        param5[2] = ds2.Tables[0].Rows[q]["intDesignationId"].ToString();
                        param5[3] = BranchId.ToString();
                        DSdocCategoryUser = FillDataSet("ssp_GetDeptSubDeptUsersForAuthorisationApproval", param5);

                        if (DSdocCategoryUser != null && DSdocCategoryUser.Tables[0].Rows.Count > 0)
                        {
                            ds2.Tables[0].Rows[q]["intUserId"] = DSdocCategoryUser.Tables[0].Rows[0]["user1"].ToString();
                            ds2.Tables[0].Rows[q]["intSecondaryUserId"] = DSdocCategoryUser.Tables[0].Rows[0]["user2"].ToString();
                            ds2.Tables[0].Rows[q]["intTertiaryUserId"] = DSdocCategoryUser.Tables[0].Rows[0]["user3"].ToString();
                            ds2.Tables[0].Rows[q]["strEmpId"] = DSdocCategoryUser.Tables[0].Rows[0]["intEmployeeId"].ToString();
                        }
                    }
                }
                DataSet ds3 = new DataSet();

                object[] param3 = new object[2];
                param3[0] = intDocId;
                param3[1] = frmFormName;
                ds3 = FillDataSet("ssp_GetRevisionNoOfDocument", param3);

                object[] param10 = new object[3];
                param10[0] = 0;
                param10[1] = 0;
                param10[2] = 0;
                dsMaxApprovalsetNo = FillDataSet("ssp_GettblAuthorisationApprovalMaxApprovalsetNo", param10);

                if (ds2.Tables[0].Rows.Count > 0)
                {
                    int Count = 0;
                    for (int i = 0; i < ds2.Tables[0].Rows.Count; i++)
                    {
                        object[] param1 = new object[35];
                        //object[] param1 = new object[17];
                        param1[0] = strDocType;
                        param1[1] = strDocNo;
                        param1[2] = intDocId;
                        param1[3] = strDocDate;
                        param1[4] = strPartyName;
                        param1[5] = ds2.Tables[0].Rows[i]["intSequence"];
                        param1[6] = ds2.Tables[0].Rows[i]["intUserId"];
                        param1[7] = ds2.Tables[0].Rows[i]["strEmpId"];
                        param1[8] = ds2.Tables[0].Rows[i]["intDocumentID"];
                        param1[9] = companyId;
                        param1[10] = BranchId;
                        param1[11] = FyId;
                        param1[12] = "Pending";
                        param1[13] = ds2.Tables[0].Rows[i]["strOptionalFlag"];
                        param1[14] = ds2.Tables[0].Rows[i]["strSuperUserFlag"];
                        param1[15] = ds2.Tables[0].Rows[i]["intDocumentCategoryId"];
                        param1[16] = ds2.Tables[0].Rows[i]["strDocumentCategory"];
                        param1[17] = ds2.Tables[0].Rows[i]["intAuthorisationId"];
                        param1[18] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intTotalRevisionno"] : 0;
                        param1[19] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionDate"].ToString() : "";
                        param1[20] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionRemarks"].ToString() : "";
                        param1[21] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intLastRevisedId"] : 0;
                        param1[22] = ds2.Tables[0].Rows[i]["intSecondaryUserId"];
                        param1[23] = ds2.Tables[0].Rows[i]["intTertiaryUserId"];
                        param1[24] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intItemId"] : 0;
                        param1[25] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intPartyId"] : 0;
                        param1[26] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["dblRateDiff"] : 0;

                        param1[27] = ds2.Tables[0].Rows[i]["strType"];
                        param1[28] = ds2.Tables[0].Rows[i]["intTypeId"];
                        param1[29] = ds2.Tables[0].Rows[i]["intDesignationId"];

                        param1[30] = ds2.Tables[0].Rows[i]["intDueDays"];
                        param1[31] = ds2.Tables[0].Rows[i]["intTotaldueDays"];
                        param1[32] = ds2.Tables[0].Rows[i]["intEsclationDays"];
                        param1[33] = dsMaxApprovalsetNo.Tables[0].Rows[0]["ApprovalsetNo"];

                        Count += ObjDataServices.SaveQuery("ssp_InsertTblAuthorisationApproval", param1);
                    }
                }
                else if (ds.Tables[0].Rows.Count > 0)
                {
                    int Count = 0;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        object[] param1 = new object[35];
                        //object[] param1 = new object[17];
                        param1[0] = strDocType;
                        param1[1] = strDocNo;
                        param1[2] = intDocId;
                        param1[3] = strDocDate;
                        param1[4] = strPartyName;
                        param1[5] = ds.Tables[0].Rows[i]["intSequence"];
                        param1[6] = ds.Tables[0].Rows[i]["intUserId"];
                        param1[7] = ds.Tables[0].Rows[i]["strEmpId"];
                        param1[8] = ds.Tables[0].Rows[i]["intDocumentID"];
                        param1[9] = companyId;
                        param1[10] = BranchId;
                        param1[11] = FyId;
                        param1[12] = "Pending";
                        param1[13] = ds.Tables[0].Rows[i]["strOptionalFlag"];
                        param1[14] = ds.Tables[0].Rows[i]["strSuperUserFlag"];
                        param1[15] = 0;
                        param1[16] = "";
                        param1[17] = ds.Tables[0].Rows[i]["intAuthorisationId"];
                        param1[18] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intTotalRevisionno"] : 0;
                        param1[19] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionDate"].ToString() : "";
                        param1[20] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionRemarks"].ToString() : "";
                        param1[21] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intLastRevisedId"] : 0;
                        param1[22] = ds.Tables[0].Rows[i]["intSecondaryUserId"];
                        param1[23] = ds.Tables[0].Rows[i]["intTertiaryUserId"];
                        param1[24] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intItemId"] : 0;
                        param1[25] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intPartyId"] : 0;
                        param1[26] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["dblRateDiff"] : 0;

                        param1[27] = ds.Tables[0].Rows[i]["strType"];
                        param1[28] = ds.Tables[0].Rows[i]["intTypeId"];
                        param1[29] = ds.Tables[0].Rows[i]["intDesignationId"];

                        param1[30] = ds.Tables[0].Rows[i]["intDueDays"];
                        param1[31] = ds.Tables[0].Rows[i]["intTotaldueDays"];
                        param1[32] = ds.Tables[0].Rows[i]["intEsclationDays"];
                        param1[33] = dsMaxApprovalsetNo.Tables[0].Rows[0]["ApprovalsetNo"];
                        param1[34] = ds2.Tables[0].Rows[i]["intLevel"];

                        Count += ObjDataServices.SaveQuery("ssp_InsertTblAuthorisationApproval", param1);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateRecordInToAuthorityApprovalPendingTable_Revise(string strDocType, string strDocNo, int intDocId, string strDocDate, string strPartyName, int companyId, int BranchId, int FyId, string frmFormName, string DocCategory, string RevisionFlag)
        {
            try
            {
                //Check if that form or document has authority level
                DataSet ds = new DataSet();
                object[] param = new object[3];
                param[0] = frmFormName;
                param[1] = RevisionFlag;
                param[2] = BranchId;

                ds = FillDataSet("ssp_CheckAuthorityLevel", param);

                //Check if that form or document and document category has authority level
                DataSet ds2 = new DataSet();
                object[] param2 = new object[4];
                param2[0] = frmFormName;
                param2[1] = DocCategory;
                param2[2] = RevisionFlag;
                param2[3] = BranchId;
                ds2 = FillDataSet("ssp_CheckAuthorityLevelForDocumentCategory", param2);


                DataSet ds3 = new DataSet();

                object[] param3 = new object[2];
                param3[0] = intDocId;
                param3[1] = frmFormName;
                ds3 = FillDataSet("ssp_GetRevisionNoOfDocument", param3);

                if (ds2.Tables[0].Rows.Count > 0)
                {
                    int Count = 0;
                    for (int i = 0; i < ds2.Tables[0].Rows.Count; i++)
                    {
                        object[] param1 = new object[27];
                        //object[] param1 = new object[17];
                        param1[0] = strDocType;
                        param1[1] = strDocNo;
                        param1[2] = intDocId;
                        param1[3] = strDocDate;
                        param1[4] = strPartyName;
                        param1[5] = ds2.Tables[0].Rows[i]["intSequence"];
                        param1[6] = ds2.Tables[0].Rows[i]["intUserId"];
                        param1[7] = ds2.Tables[0].Rows[i]["strEmpId"];
                        param1[8] = ds2.Tables[0].Rows[i]["intDocumentID"];
                        param1[9] = companyId;
                        param1[10] = BranchId;
                        param1[11] = FyId;
                        param1[12] = "Pending";
                        param1[13] = ds2.Tables[0].Rows[i]["strOptionalFlag"];
                        param1[14] = ds2.Tables[0].Rows[i]["strSuperUserFlag"];
                        param1[15] = ds2.Tables[0].Rows[i]["intDocumentCategoryId"];
                        param1[16] = ds2.Tables[0].Rows[i]["strDocumentCategory"];
                        param1[17] = ds2.Tables[0].Rows[i]["intAuthorisationId"];
                        param1[18] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intTotalRevisionno"] : 0;
                        param1[19] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionDate"].ToString() : "";
                        param1[20] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionRemarks"].ToString() : "";
                        param1[21] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intLastRevisedId"] : 0;
                        param1[22] = ds2.Tables[0].Rows[i]["intSecondaryUserId"];
                        param1[23] = ds2.Tables[0].Rows[i]["intTertiaryUserId"];
                        param1[24] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intItemId"] : 0;
                        param1[25] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intPartyId"] : 0;
                        param1[26] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["dblRateDiff"] : 0;


                        Count += ObjDataServices.SaveQuery("ssp_UpdateTblAuthorisationApproval", param1);
                    }
                }
                else if (ds.Tables[0].Rows.Count > 0)
                {
                    int Count = 0;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        object[] param1 = new object[27];
                        //object[] param1 = new object[17];
                        param1[0] = strDocType;
                        param1[1] = strDocNo;
                        param1[2] = intDocId;
                        param1[3] = strDocDate;
                        param1[4] = strPartyName;
                        param1[5] = ds.Tables[0].Rows[i]["intSequence"];
                        param1[6] = ds.Tables[0].Rows[i]["intUserId"];
                        param1[7] = ds.Tables[0].Rows[i]["strEmpId"];
                        param1[8] = ds.Tables[0].Rows[i]["intDocumentID"];
                        param1[9] = companyId;
                        param1[10] = BranchId;
                        param1[11] = FyId;
                        param1[12] = "Pending";
                        param1[13] = ds.Tables[0].Rows[i]["strOptionalFlag"];
                        param1[14] = ds.Tables[0].Rows[i]["strSuperUserFlag"];
                        param1[15] = 0;
                        param1[16] = "";
                        param1[17] = ds.Tables[0].Rows[i]["intAuthorisationId"];
                        param1[18] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intTotalRevisionno"] : 0;
                        param1[19] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionDate"].ToString() : "";
                        param1[20] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionRemarks"].ToString() : "";
                        param1[21] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intLastRevisedId"] : 0;
                        param1[22] = ds.Tables[0].Rows[i]["intSecondaryUserId"];
                        param1[23] = ds.Tables[0].Rows[i]["intTertiaryUserId"];
                        param1[24] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intItemId"] : 0;
                        param1[25] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intPartyId"] : 0;
                        param1[26] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["dblRateDiff"] : 0;



                        Count += ObjDataServices.SaveQuery("ssp_UpdateTblAuthorisationApproval", param1);
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CheckAuthorityForApprovalEmployeeOfficial(string frmFormName, int ID, string RevisionFlag)
        {

            DataSet ds = new DataSet();
            object[] param = new object[3];
            param[0] = frmFormName;
            param[1] = ID;
            param[2] = RevisionFlag;


            ds = FillDataSet("ssp_CheckAuthorityForApprovalEmployeeOfficial", param);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }


        public bool CheckAuthorityForApproval(string frmFormName, string RevisionFlag, int BranchId)
        {
            //Check if that form or document has authority level
            DataSet ds = new DataSet();
            object[] param = new object[3];
            param[0] = frmFormName;
            param[1] = RevisionFlag;
            param[2] = BranchId;
            ds = FillDataSet("ssp_CheckAuthorityLevel", param);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }
        public bool CheckApprovalAuthorityForDocumentCategory(string frmFormName, string DocCategory, string RevisionFlag, int BranchId)
        {
            //Check if that form or document has authority level
            DataSet ds = new DataSet();
            object[] param = new object[4];
            param[0] = frmFormName;
            param[1] = DocCategory;
            param[2] = RevisionFlag;
            param[3] = BranchId;
            ds = FillDataSet("ssp_CheckAuthorityLevelForDocumentCategory", param);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }



        public void InsertRecordInToAuthorityApprovalPendingTableLeaveApplication(string strDocType, string strDocNo, int intDocId, string strDocDate, string strPartyName, int companyId,
                    int BranchId, int FyId, string frmFormName, string DocCategory, string RevisionFlag, int DeptId, int SubDeptId, int UserId,
            int ReportingToId, int AllocatedToId, int AllocatedToDesigId, int EmployeeId, int LeavePolicyId, string LeaveFromDate,
            string LeaveTodate, decimal TotalLeaveDays, string ApprFromAuthorisation, string StrDocumentRemark1, string StrDocumentRemark2, string strEntryType, string intTotalLevel, string intTotalRevision)
        {
            try
            {
                DataSet dsMaxApprovalsetNo = new DataSet();  // For Max ApprovalsetNo
                if (ApprFromAuthorisation.ToString() == "Y")
                {
                    //Check if that form or document has authority level
                    DataSet ds = new DataSet();
                    object[] param = new object[3];
                    param[0] = frmFormName;
                    param[1] = RevisionFlag;
                    param[2] = BranchId;

                    ds = FillDataSet("ssp_CheckAuthorityLevel", param);


                    if (AllocatedToId.ToString() != "0")
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                            ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["strType"] = "Allocated To";
                            ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["intTypeId"] = AllocatedToId.ToString();
                            ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["intDesignationId"] = AllocatedToDesigId.ToString();
                            ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["intSequence"] = 1;
                            ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["strOptionalFlag"] = "N";
                            ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["strSuperUserFlag"] = "N";
                            //ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["intDocumentCategoryId"] = ds.Tables[0].Rows[0]["intDocumentCategoryId"];
                            //ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["strDocumentCategory"] = ds.Tables[0].Rows[0]["strDocumentCategory"];
                            ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["intAuthorisationId"] = ds.Tables[0].Rows[0]["intAuthorisationId"];
                            ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["intDocumentID"] = ds.Tables[0].Rows[0]["intDocumentID"];

                        }
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count - 1; i++)
                            {
                                ds.Tables[0].Rows[i]["intSequence"] = Convert.ToInt32(ds.Tables[0].Rows[i]["intSequence"].ToString()) + 1;
                            }
                            ds = GetTempDSSort(ds, "intSequence");

                        }
                    }



                    //Check if that form or document and document category has authority level
                    DataSet ds2 = new DataSet();
                    object[] param2 = new object[4];
                    param2[0] = frmFormName;
                    param2[1] = DocCategory;
                    param2[2] = RevisionFlag;
                    param2[3] = BranchId;
                    ds2 = FillDataSet("ssp_CheckAuthorityLevelForDocumentCategory", param2);

                    if (AllocatedToId.ToString() != "0")
                    {
                        if (ds2.Tables[0].Rows.Count > 0)
                        {
                            ds2.Tables[0].Rows.Add(ds2.Tables[0].NewRow());
                            ds2.Tables[0].Rows[ds2.Tables[0].Rows.Count - 1]["strType"] = "Allocated To";
                            ds2.Tables[0].Rows[ds2.Tables[0].Rows.Count - 1]["intTypeId"] = AllocatedToId.ToString();
                            ds2.Tables[0].Rows[ds2.Tables[0].Rows.Count - 1]["intDesignationId"] = AllocatedToDesigId.ToString();
                            ds2.Tables[0].Rows[ds2.Tables[0].Rows.Count - 1]["intSequence"] = 1;
                            ds2.Tables[0].Rows[ds2.Tables[0].Rows.Count - 1]["strOptionalFlag"] = "N";
                            ds2.Tables[0].Rows[ds2.Tables[0].Rows.Count - 1]["strSuperUserFlag"] = "N";
                            ds2.Tables[0].Rows[ds2.Tables[0].Rows.Count - 1]["intDocumentCategoryId"] = ds2.Tables[0].Rows[0]["intDocumentCategoryId"];
                            ds2.Tables[0].Rows[ds2.Tables[0].Rows.Count - 1]["strDocumentCategory"] = ds2.Tables[0].Rows[0]["strDocumentCategory"];
                            ds2.Tables[0].Rows[ds2.Tables[0].Rows.Count - 1]["intAuthorisationId"] = ds2.Tables[0].Rows[0]["intAuthorisationId"];
                            ds2.Tables[0].Rows[ds2.Tables[0].Rows.Count - 1]["intDocumentID"] = ds2.Tables[0].Rows[0]["intDocumentID"];
                        }

                        for (int i = 0; i < ds2.Tables[0].Rows.Count - 1; i++)
                        {
                            ds2.Tables[0].Rows[i]["intSequence"] = Convert.ToInt32(ds2.Tables[0].Rows[i]["intSequence"].ToString()) + 1;
                        }
                        ds2 = GetTempDSSort(ds2, "intSequence");


                    }



                    //Auto Fill Department/SubDepartment Approval User for form or document
                    for (int p = 0; p < ds.Tables[0].Rows.Count; p++)
                    {
                        if (ds.Tables[0].Rows[p]["strType"].ToString() != "Employee")
                        {
                            DataSet DSdocUser = new DataSet();
                            int TypeID = 0;

                            if (Convert.ToInt32(ds.Tables[0].Rows[p]["intTypeId"]) == 0)
                            {
                                if (ds.Tables[0].Rows[p]["strType"].ToString() == "Department")
                                    TypeID = DeptId;
                                else if (ds.Tables[0].Rows[p]["strType"].ToString() == "SubDepartment")
                                    TypeID = SubDeptId;
                                else if (ds.Tables[0].Rows[p]["strType"].ToString() == "ReportingOfficer")
                                    TypeID = ReportingToId;
                            }
                            else
                                TypeID = Convert.ToInt32(ds.Tables[0].Rows[p]["intTypeId"]);


                            object[] param4 = new object[4];
                            param4[0] = ds.Tables[0].Rows[p]["strType"].ToString();
                            param4[1] = TypeID.ToString();
                            param4[2] = ds.Tables[0].Rows[p]["intDesignationId"].ToString();
                            param4[3] = BranchId.ToString();
                            DSdocUser = FillDataSet("ssp_GetDeptSubDeptUsersForAuthorisationApproval", param4);

                            if (DSdocUser != null && DSdocUser.Tables[0].Rows.Count > 0)
                            {
                                ds.Tables[0].Rows[p]["intUserId"] = DSdocUser.Tables[0].Rows[0]["user1"].ToString();
                                ds.Tables[0].Rows[p]["intSecondaryUserId"] = DSdocUser.Tables[0].Rows[0]["user2"].ToString();
                                ds.Tables[0].Rows[p]["intTertiaryUserId"] = DSdocUser.Tables[0].Rows[0]["user3"].ToString();
                                ds.Tables[0].Rows[p]["strEmpId"] = DSdocUser.Tables[0].Rows[0]["intEmployeeId"].ToString();
                            }
                            else
                            {
                                ds.Tables[0].Rows[p]["intUserId"] = "0";
                                ds.Tables[0].Rows[p]["intSecondaryUserId"] = "0";
                                ds.Tables[0].Rows[p]["intTertiaryUserId"] = "0";
                                ds.Tables[0].Rows[p]["strEmpId"] = TypeID.ToString();
                            }

                        }
                    }

                    //Auto Fill Department/SubDepartment Approval User for form or document and document category
                    for (int q = 0; q < ds2.Tables[0].Rows.Count; q++)
                    {
                        if (ds2.Tables[0].Rows[q]["strType"].ToString() != "Employee")
                        {
                            DataSet DSdocCategoryUser = new DataSet();
                            int TypeID = 0;

                            if (Convert.ToInt32(ds2.Tables[0].Rows[q]["intTypeId"]) == 0)
                            {
                                if (ds2.Tables[0].Rows[q]["strType"].ToString() == "Department")
                                    TypeID = DeptId;
                                else if (ds2.Tables[0].Rows[q]["strType"].ToString() == "SubDepartment")
                                    TypeID = SubDeptId;
                                else if (ds2.Tables[0].Rows[q]["strType"].ToString() == "ReportingOfficer")
                                    TypeID = ReportingToId;
                            }
                            else
                                TypeID = Convert.ToInt32(ds2.Tables[0].Rows[q]["intTypeId"]);

                            object[] param5 = new object[4];
                            param5[0] = ds2.Tables[0].Rows[q]["strType"].ToString();
                            param5[1] = TypeID.ToString();
                            param5[2] = ds2.Tables[0].Rows[q]["intDesignationId"].ToString();
                            param5[3] = BranchId.ToString();
                            DSdocCategoryUser = FillDataSet("ssp_GetDeptSubDeptUsersForAuthorisationApproval", param5);

                            if (DSdocCategoryUser != null && DSdocCategoryUser.Tables[0].Rows.Count > 0)
                            {
                                ds2.Tables[0].Rows[q]["intUserId"] = DSdocCategoryUser.Tables[0].Rows[0]["user1"].ToString();
                                ds2.Tables[0].Rows[q]["intSecondaryUserId"] = DSdocCategoryUser.Tables[0].Rows[0]["user2"].ToString();
                                ds2.Tables[0].Rows[q]["intTertiaryUserId"] = DSdocCategoryUser.Tables[0].Rows[0]["user3"].ToString();
                                ds2.Tables[0].Rows[q]["strEmpId"] = DSdocCategoryUser.Tables[0].Rows[0]["intEmployeeId"].ToString();
                            }
                            else
                            {
                                ds2.Tables[0].Rows[q]["intUserId"] = "0";
                                ds2.Tables[0].Rows[q]["intSecondaryUserId"] = "0";
                                ds2.Tables[0].Rows[q]["intTertiaryUserId"] = "0";
                                ds2.Tables[0].Rows[q]["strEmpId"] = TypeID.ToString();

                            }

                        }
                    }


                    DataSet ds3 = new DataSet();
                    // DataSet dsMaxApprovalsetNo = new DataSet();

                    object[] param3 = new object[2];
                    param3[0] = intDocId;
                    param3[1] = frmFormName;
                    ds3 = FillDataSet("ssp_GetRevisionNoOfDocument", param3);

                    // DataSet dsMaxApprovalsetNo = new DataSet();
                    object[] param10 = new object[3];
                    param10[0] = 0;
                    param10[1] = 0;
                    param10[2] = 0;
                    dsMaxApprovalsetNo = FillDataSet("ssp_GettblAuthorisationApprovalMaxApprovalsetNo", param10);

                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        int Count = 0;
                        for (int i = 0; i < ds2.Tables[0].Rows.Count; i++)
                        {
                            // Insert Into Pending Document Approval

                            object[] param1 = new object[47];

                            param1[0] = strDocType;
                            param1[1] = strDocNo;
                            param1[2] = intDocId;
                            param1[3] = strDocDate;
                            param1[4] = strPartyName;
                            param1[5] = ds2.Tables[0].Rows[i]["intSequence"];
                            param1[6] = ds2.Tables[0].Rows[i]["intUserId"].ToString() != "" ? ds2.Tables[0].Rows[i]["intUserId"].ToString() : "0";
                            param1[7] = ds2.Tables[0].Rows[i]["strEmpId"];
                            param1[8] = ds2.Tables[0].Rows[i]["intDocumentID"];
                            param1[9] = companyId;
                            param1[10] = BranchId;
                            param1[11] = FyId;
                            param1[12] = "Pending";
                            param1[13] = ds2.Tables[0].Rows[i]["strOptionalFlag"];
                            param1[14] = ds2.Tables[0].Rows[i]["strSuperUserFlag"];
                            param1[15] = ds2.Tables[0].Rows[i]["intDocumentCategoryId"];
                            param1[16] = ds2.Tables[0].Rows[i]["strDocumentCategory"];
                            param1[17] = ds2.Tables[0].Rows[i]["intAuthorisationId"];
                            param1[18] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intTotalRevisionno"] : 0;
                            param1[19] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionDate"].ToString() : "";
                            param1[20] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionRemarks"].ToString() : "";
                            param1[21] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intLastRevisedId"] : 0;
                            param1[22] = ds2.Tables[0].Rows[i]["intSecondaryUserId"];
                            param1[23] = ds2.Tables[0].Rows[i]["intTertiaryUserId"];
                            param1[24] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intItemId"] : 0;
                            param1[25] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intPartyId"] : 0;
                            param1[26] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["dblRateDiff"] : 0;


                            param1[27] = ds2.Tables[0].Rows[i]["strType"];
                            param1[28] = ds2.Tables[0].Rows[i]["intTypeId"];
                            param1[29] = ds2.Tables[0].Rows[i]["intDesignationId"];

                            //param1[27] = ds.Tables[0].Rows[i]["strType"];
                            //param1[28] = ds.Tables[0].Rows[i]["intTypeId"];
                            //param1[29] = ds.Tables[0].Rows[i]["intDesignationId"];

                            param1[30] = ds2.Tables[0].Rows[i]["intDueDays"];
                            param1[31] = ds2.Tables[0].Rows[i]["intTotaldueDays"];
                            param1[32] = ds2.Tables[0].Rows[i]["intEsclationDays"];
                            param1[33] = dsMaxApprovalsetNo.Tables[0].Rows[0]["ApprovalsetNo"];
                            param1[34] = StrDocumentRemark1;
                            param1[35] = StrDocumentRemark2;
                            param1[36] = strEntryType;
                            param1[37] = ds2.Tables[0].Rows[i]["strRevisionFlag"];
                            param1[38] = intTotalLevel;
                            param1[39] = intTotalRevision;
                            param1[40] = "Employee";// ds2.Tables[0].Rows[i]["strApprovalType"];
                            param1[41] = ds2.Tables[0].Rows[i]["strFormName"]; ;
                            param1[42] = "";//(ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["StrItemName"].ToString() : "";
                            param1[43] = ""; //(ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["StrPartNumber"].ToString() : "";
                            param1[44] = ds2.Tables[0].Rows[i]["strFormViewFlag"];
                            param1[45] = ds2.Tables[0].Rows[i]["strDetailViewFalg"];
                            param1[46] = ds2.Tables[0].Rows[i]["strFaEffectFlag"];

                            Count += ObjDataServices.SaveQuery("ssp_InsertTblAuthorisationApproval", param1);



                        }
                    }
                    else if (ds.Tables[0].Rows.Count > 0)
                    {
                        int Count = 0;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            object[] param1 = new object[47];

                            param1[0] = strDocType;
                            param1[1] = strDocNo;
                            param1[2] = intDocId;
                            param1[3] = strDocDate;
                            param1[4] = strPartyName;
                            param1[5] = ds.Tables[0].Rows[i]["intSequence"];
                            param1[6] = ds.Tables[0].Rows[i]["intUserId"].ToString() != "" ? ds.Tables[0].Rows[i]["intUserId"].ToString() : "0";
                            param1[7] = ds.Tables[0].Rows[i]["strEmpId"];
                            param1[8] = ds.Tables[0].Rows[i]["intDocumentID"];
                            param1[9] = companyId;
                            param1[10] = BranchId;
                            param1[11] = FyId;
                            param1[12] = "Pending";
                            param1[13] = ds.Tables[0].Rows[i]["strOptionalFlag"];
                            param1[14] = ds.Tables[0].Rows[i]["strSuperUserFlag"];
                            param1[15] = 0;
                            param1[16] = "";
                            param1[17] = ds.Tables[0].Rows[i]["intAuthorisationId"];
                            param1[18] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intTotalRevisionno"] : 0;
                            param1[19] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionDate"].ToString() : "";
                            param1[20] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionRemarks"].ToString() : "";
                            param1[21] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intLastRevisedId"] : 0;
                            param1[22] = ds.Tables[0].Rows[i]["intSecondaryUserId"];
                            param1[23] = ds.Tables[0].Rows[i]["intTertiaryUserId"];
                            param1[24] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intItemId"] : 0;
                            param1[25] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intPartyId"] : 0;
                            param1[26] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["dblRateDiff"] : 0;


                            param1[27] = ds.Tables[0].Rows[i]["strType"];
                            param1[28] = ds.Tables[0].Rows[i]["intTypeId"];
                            param1[29] = ds.Tables[0].Rows[i]["intDesignationId"];

                            param1[30] = 0;
                            param1[31] = 0;
                            param1[32] = 0;


                            //param1[30] = ds2.Tables[0].Rows[i]["intDueDays"];
                            //param1[31] = ds2.Tables[0].Rows[i]["intTotaldueDays"];
                            //param1[32] = ds2.Tables[0].Rows[i]["intEsclationDays"];
                            param1[33] = dsMaxApprovalsetNo.Tables[0].Rows[0]["ApprovalsetNo"];
                            param1[34] = StrDocumentRemark1;
                            param1[35] = StrDocumentRemark2;
                            param1[36] = strEntryType;
                            param1[37] = ds2.Tables[0].Rows[i]["strRevisionFlag"];
                            //param1[38] = ds2.Tables[0].Rows[i]["inttotLevel"];
                            //param1[39] = ds2.Tables[0].Rows[i]["intTotRevisionLevel"];
                            param1[38] = intTotalLevel;
                            param1[39] = intTotalRevision;

                            param1[40] = "Employee";//ds2.Tables[0].Rows[i]["strApprovalType"];
                            param1[41] = ds2.Tables[0].Rows[i]["strFormName"]; ;
                            param1[42] = "";//(ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["StrItemName"].ToString() : "";
                            param1[43] = ""; //(ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["StrPartNumber"].ToString() : "";
                            param1[44] = ds2.Tables[0].Rows[i]["strFormViewFlag"];
                            param1[45] = ds2.Tables[0].Rows[i]["strDetailViewFalg"];
                            param1[46] = ds2.Tables[0].Rows[i]["strFaEffectFlag"];

                            Count += ObjDataServices.SaveQuery("ssp_InsertTblAuthorisationApproval", param1);



                        }

                    }
                }
                else    // Fill Approvers from Employee Official Info
                {
                    DataSet ds4 = new DataSet();

                    object[] param2 = new object[4];
                    param2[0] = strDocType;
                    //param2[1] = EmployeeId;
                    param2[1] = EmployeeId;
                    param2[2] = DocCategory;
                    param2[3] = BranchId;

                    ds4 = FillDataSet("ssp_GetAutofillApproversFromEmployeeOfficialInfo", param2);

                    if (AllocatedToId.ToString() != "0")
                    {
                        if (ds4.Tables[0].Rows.Count > 0)
                        {
                            if (strDocType != "Gate Pass")
                            {

                                ds4.Tables[0].Rows.Add(ds4.Tables[0].NewRow());
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strType"] = "Allocated To";
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intTypeId"] = AllocatedToId.ToString();
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intDesignationId"] = ds4.Tables[0].Rows[0]["intDesignationId"];
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intSequence"] = 1;
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intEmployeeID"] = EmployeeId;
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strDocumentType"] = ds4.Tables[0].Rows[0]["strDocumentType"];
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strOptionalFlag"] = "N";
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strSuperUserFlag"] = "N";
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intDocumentCategoryId"] = ds4.Tables[0].Rows[0]["intDocumentCategoryId"];
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strDocumentCategory"] = ds4.Tables[0].Rows[0]["strDocumentCategory"];
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intAuthorisationId"] = ds4.Tables[0].Rows[0]["intAuthorisationId"];
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intDocumentID"] = ds4.Tables[0].Rows[0]["intDocumentID"];

                            }
                            else
                            {
                                ds4.Clear();
                                ds4.Tables[0].Rows.Add(ds4.Tables[0].NewRow());
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strType"] = "Allocated To";
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intTypeId"] = AllocatedToId.ToString();
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intDesignationId"] = ds4.Tables[0].Rows[0]["intDesignationId"];
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intSequence"] = 1;
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intEmployeeID"] = EmployeeId;
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strDocumentType"] = ds4.Tables[0].Rows[0]["strDocumentType"];
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strOptionalFlag"] = "N";
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strSuperUserFlag"] = "N";
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intDocumentCategoryId"] = ds4.Tables[0].Rows[0]["intDocumentCategoryId"];
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strDocumentCategory"] = ds4.Tables[0].Rows[0]["strDocumentCategory"];
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intAuthorisationId"] = ds4.Tables[0].Rows[0]["intAuthorisationId"];
                                ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intDocumentID"] = ds4.Tables[0].Rows[0]["intDocumentID"];
                            }
                        }
                        else
                        {

                            ds4.Tables[0].Rows.Add(ds4.Tables[0].NewRow());
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strType"] = "Allocated To";
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intTypeId"] = AllocatedToId.ToString();
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intDesignationId"] = ds4.Tables[0].Rows[0]["intDesignationId"];
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intSequence"] = 1;
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intEmployeeID"] = EmployeeId;
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strDocumentType"] = ds4.Tables[0].Rows[0]["strDocumentType"];
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strOptionalFlag"] = "N";
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strSuperUserFlag"] = "N";
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intDocumentCategoryId"] = ds4.Tables[0].Rows[0]["intDocumentCategoryId"];
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["strDocumentCategory"] = ds4.Tables[0].Rows[0]["strDocumentCategory"];
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intAuthorisationId"] = ds4.Tables[0].Rows[0]["intAuthorisationId"];
                            ds4.Tables[0].Rows[ds4.Tables[0].Rows.Count - 1]["intDocumentID"] = ds4.Tables[0].Rows[0]["intDocumentID"];
                        }


                        for (int i = 0; i < ds4.Tables[0].Rows.Count - 1; i++)
                        {
                            ds4.Tables[0].Rows[i]["intSequence"] = Convert.ToInt32(ds4.Tables[0].Rows[i]["intSequence"].ToString()) + 1;
                        }

                        ds4 = GetTempDSSort(ds4, "intSequence");


                        DataSet DSdocCategoryUserOfficial = new DataSet();
                        int TypeID1 = AllocatedToId;

                        object[] param7 = new object[4];
                        param7[0] = ds4.Tables[0].Rows[0]["strType"].ToString();
                        param7[1] = TypeID1.ToString();
                        if (AllocatedToDesigId == 0)
                            param7[2] = 0;
                        else
                            param7[2] = ds4.Tables[0].Rows[0]["intDesignationId"].ToString();
                        param7[3] = BranchId.ToString();
                        DSdocCategoryUserOfficial = FillDataSet("ssp_GetDeptSubDeptUsersForAuthorisationApproval", param7);

                        if (DSdocCategoryUserOfficial != null && DSdocCategoryUserOfficial.Tables[0].Rows.Count > 0)
                        {
                            ds4.Tables[0].Rows[0]["intUserId"] = DSdocCategoryUserOfficial.Tables[0].Rows[0]["user1"].ToString();
                            ds4.Tables[0].Rows[0]["intSecondaryUserId"] = DSdocCategoryUserOfficial.Tables[0].Rows[0]["user2"].ToString();
                            ds4.Tables[0].Rows[0]["intTertiaryUserId"] = DSdocCategoryUserOfficial.Tables[0].Rows[0]["user3"].ToString();
                            ds4.Tables[0].Rows[0]["strEmpId"] = DSdocCategoryUserOfficial.Tables[0].Rows[0]["intEmployeeId"].ToString();
                        }
                        else
                        {
                            ds4.Tables[0].Rows[0]["intUserId"] = "0";
                            ds4.Tables[0].Rows[0]["intSecondaryUserId"] = "0";
                            ds4.Tables[0].Rows[0]["intTertiaryUserId"] = "0";
                            ds4.Tables[0].Rows[0]["strEmpId"] = TypeID1.ToString();

                        }
                    }


                    DataSet ds5 = new DataSet();

                    object[] param3 = new object[2];
                    param3[0] = intDocId;
                    param3[1] = frmFormName;
                    ds5 = FillDataSet("ssp_GetRevisionNoOfDocument", param3);

                    //DataSet dsMaxApprovalsetNo = new DataSet();
                    object[] param13 = new object[3];
                    param13[0] = 0;
                    param13[1] = 0;
                    param13[2] = 0;
                    dsMaxApprovalsetNo = FillDataSet("ssp_GettblAuthorisationApprovalMaxApprovalsetNo", param13);

                    if (ds4.Tables[0].Rows.Count > 0)
                    {
                        int Count = 0;
                        for (int i = 0; i < ds4.Tables[0].Rows.Count; i++)
                        {
                            // Insert Into Pending Document Approval

                            object[] param1 = new object[47];

                            param1[0] = strDocType;
                            param1[1] = strDocNo;
                            param1[2] = intDocId;
                            param1[3] = strDocDate;
                            param1[4] = strPartyName;
                            param1[5] = ds4.Tables[0].Rows[i]["intSequence"];
                            param1[6] = ds4.Tables[0].Rows[i]["intUserId"].ToString() != "" ? ds4.Tables[0].Rows[i]["intUserId"].ToString() : "0";
                            param1[7] = ds4.Tables[0].Rows[i]["strEmpId"];
                            param1[8] = ds4.Tables[0].Rows[i]["intDocumentID"];
                            param1[9] = companyId;
                            param1[10] = BranchId;
                            param1[11] = FyId;
                            param1[12] = "Pending";
                            param1[13] = ds4.Tables[0].Rows[i]["strOptionalFlag"];
                            param1[14] = ds4.Tables[0].Rows[i]["strSuperUserFlag"];
                            param1[15] = ds4.Tables[0].Rows[i]["intDocumentCategoryId"];
                            param1[16] = DocCategory;
                            param1[17] = ds4.Tables[0].Rows[i]["intAuthorisationId"];
                            // param1[18] = (ds5 != null && ds5.Tables[0].Rows.Count > 0) ? ds5.Tables[0].Rows[0]["intTotalRevisionno"] : 0;
                            param1[18] = intTotalRevision;
                            param1[19] = (ds5 != null && ds5.Tables[0].Rows.Count > 0) ? ds5.Tables[0].Rows[0]["strRevisionDate"].ToString() : "";
                            param1[20] = (ds5 != null && ds5.Tables[0].Rows.Count > 0) ? ds5.Tables[0].Rows[0]["strRevisionRemarks"].ToString() : "";
                            param1[21] = (ds5 != null && ds5.Tables[0].Rows.Count > 0) ? ds5.Tables[0].Rows[0]["intLastRevisedId"] : 0;
                            param1[22] = ds4.Tables[0].Rows[i]["intSecondaryUserId"];
                            param1[23] = ds4.Tables[0].Rows[i]["intTertiaryUserId"];
                            param1[24] = (ds5 != null && ds5.Tables[0].Rows.Count > 0) ? ds5.Tables[0].Rows[0]["intItemId"] : 0;
                            param1[25] = (ds5 != null && ds5.Tables[0].Rows.Count > 0) ? ds5.Tables[0].Rows[0]["intPartyId"] : 0;
                            param1[26] = (ds5 != null && ds5.Tables[0].Rows.Count > 0) ? ds5.Tables[0].Rows[0]["dblRateDiff"] : 0;


                            param1[27] = ds4.Tables[0].Rows[i]["strType"];
                            param1[28] = ds4.Tables[0].Rows[i]["intTypeId"];
                            param1[29] = ds4.Tables[0].Rows[i]["intDesignationId"];


                            param1[30] = 0;
                            param1[31] = 0;
                            param1[32] = 0;
                            param1[33] = dsMaxApprovalsetNo.Tables[0].Rows[0]["ApprovalsetNo"];
                            param1[34] = "";
                            param1[35] = "";
                            param1[36] = strEntryType;
                            param1[37] = ds4.Tables[0].Rows[i]["strRevisionFlag"];
                            param1[38] = intTotalLevel;
                            param1[39] = intTotalRevision;
                            param1[40] = "Employee";//ds4.Tables[0].Rows[i]["strType"];
                            param1[41] = frmFormName;//ds2.Tables[0].Rows[i]["strFormName"]; ;
                            param1[42] = "";//(ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["StrItemName"].ToString() : "";
                            param1[43] = ""; //(ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["StrPartNumber"].ToString() : "";
                            param1[44] = "Y";
                            param1[45] = "Y";
                            param1[46] = "N";

                            Count += ObjDataServices.SaveQuery("ssp_InsertTblAuthorisationApproval", param1);

                        }
                    }

                }
            }

            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        public void InsertRecordInToAuthorityApprovalPendingTable(string strDocType, string strDocNo, int intDocId, string strDocDate, string strPartyName, int companyId, int BranchId, int FyId, string frmFormName, string DocCategory, string RevisionFlag, int DeptId, int SubDeptId, int UserId, int ReportingToId, string StrDocumentRemark1, string StrDocumentRemark2, string strEntryType)
        {
            try
            {
                DataSet dsMaxApprovalsetNo = new DataSet();  // For Max ApprovalsetNo // For Max ApprovalsetNo
                //Check if that form or document has authority level
                DataSet ds = new DataSet();
                object[] param = new object[3];
                param[0] = frmFormName;
                param[1] = RevisionFlag;
                param[2] = BranchId;

                ds = FillDataSet("ssp_CheckAuthorityLevel", param);

                //Check if that form or document and document category has authority level
                DataSet ds2 = new DataSet();
                object[] param2 = new object[4];
                param2[0] = frmFormName;
                param2[1] = DocCategory;
                param2[2] = RevisionFlag;
                param2[3] = BranchId;
                ds2 = FillDataSet("ssp_CheckAuthorityLevelForDocumentCategory", param2);

                //Auto Fill Department/SubDepartment Approval User for form or document
                for (int p = 0; p < ds.Tables[0].Rows.Count; p++)
                {
                    if (ds.Tables[0].Rows[p]["strType"].ToString() != "Employee")
                    {
                        DataSet DSdocUser = new DataSet();
                        int TypeID = 0;

                        if (Convert.ToInt32(ds.Tables[0].Rows[p]["intTypeId"]) == 0)
                        {
                            if (ds.Tables[0].Rows[p]["strType"].ToString() == "Department")
                                TypeID = DeptId;
                            else if (ds.Tables[0].Rows[p]["strType"].ToString() == "SubDepartment")
                                TypeID = SubDeptId;
                            else if (ds.Tables[0].Rows[p]["strType"].ToString() == "ReportingOfficer")
                                TypeID = ReportingToId;
                        }
                        else
                            TypeID = Convert.ToInt32(ds.Tables[0].Rows[p]["intTypeId"]);

                        object[] param4 = new object[4];
                        param4[0] = ds.Tables[0].Rows[p]["strType"].ToString();
                        param4[1] = TypeID.ToString();
                        param4[2] = ds.Tables[0].Rows[p]["intDesignationId"].ToString();
                        param4[3] = BranchId.ToString();
                        DSdocUser = FillDataSet("ssp_GetDeptSubDeptUsersForAuthorisationApproval", param4);

                        if (DSdocUser != null && DSdocUser.Tables[0].Rows.Count > 0)
                        {
                            ds.Tables[0].Rows[p]["intUserId"] = DSdocUser.Tables[0].Rows[0]["user1"].ToString();
                            ds.Tables[0].Rows[p]["intSecondaryUserId"] = DSdocUser.Tables[0].Rows[0]["user2"].ToString();
                            ds.Tables[0].Rows[p]["intTertiaryUserId"] = DSdocUser.Tables[0].Rows[0]["user3"].ToString();
                            ds.Tables[0].Rows[p]["strEmpId"] = DSdocUser.Tables[0].Rows[0]["intEmployeeId"].ToString();
                        }
                    }
                }

                //Auto Fill Department/SubDepartment Approval User for form or document and document category
                for (int q = 0; q < ds2.Tables[0].Rows.Count; q++)
                {
                    if (ds2.Tables[0].Rows[q]["strType"].ToString() != "Employee")
                    {
                        DataSet DSdocCategoryUser = new DataSet();
                        int TypeID = 0;

                        if (Convert.ToInt32(ds2.Tables[0].Rows[q]["intTypeId"]) == 0)
                        {
                            if (ds2.Tables[0].Rows[q]["strType"].ToString() == "Department")
                                TypeID = DeptId;
                            else if (ds2.Tables[0].Rows[q]["strType"].ToString() == "SubDepartment")
                                TypeID = SubDeptId;
                            else if (ds2.Tables[0].Rows[q]["strType"].ToString() == "ReportingOfficer")
                                TypeID = ReportingToId;
                        }
                        else
                            TypeID = Convert.ToInt32(ds2.Tables[0].Rows[q]["intTypeId"]);

                        object[] param5 = new object[4];
                        param5[0] = ds2.Tables[0].Rows[q]["strType"].ToString();
                        param5[1] = TypeID.ToString();
                        param5[2] = ds2.Tables[0].Rows[q]["intDesignationId"].ToString();
                        param5[3] = BranchId.ToString();
                        DSdocCategoryUser = FillDataSet("ssp_GetDeptSubDeptUsersForAuthorisationApproval", param5);

                        if (DSdocCategoryUser != null && DSdocCategoryUser.Tables[0].Rows.Count > 0)
                        {
                            ds2.Tables[0].Rows[q]["intUserId"] = DSdocCategoryUser.Tables[0].Rows[0]["user1"].ToString();
                            ds2.Tables[0].Rows[q]["intSecondaryUserId"] = DSdocCategoryUser.Tables[0].Rows[0]["user2"].ToString();
                            ds2.Tables[0].Rows[q]["intTertiaryUserId"] = DSdocCategoryUser.Tables[0].Rows[0]["user3"].ToString();
                            ds2.Tables[0].Rows[q]["strEmpId"] = DSdocCategoryUser.Tables[0].Rows[0]["intEmployeeId"].ToString();
                        }
                    }
                }
                DataSet ds3 = new DataSet();

                object[] param3 = new object[2];
                param3[0] = intDocId;
                param3[1] = frmFormName;
                ds3 = FillDataSet("ssp_GetRevisionNoOfDocument", param3);
                // DataSet dsMaxApprovalsetNo = new DataSet(); 
                object[] param10 = new object[3];
                param10[0] = 0;
                param10[1] = 0;
                param10[2] = 0;
                dsMaxApprovalsetNo = FillDataSet("ssp_GettblAuthorisationApprovalMaxApprovalsetNo", param10);

                if (ds2.Tables[0].Rows.Count > 0)
                {
                    int Count = 0;
                    for (int i = 0; i < ds2.Tables[0].Rows.Count; i++)
                    {
                        object[] param1 = new object[47];
                        //object[] param1 = new object[17];
                        param1[0] = strDocType;
                        param1[1] = strDocNo;
                        param1[2] = intDocId;
                        param1[3] = strDocDate;
                        param1[4] = strPartyName;
                        param1[5] = ds2.Tables[0].Rows[i]["intSequence"];
                        param1[6] = ds2.Tables[0].Rows[i]["intUserId"];
                        param1[7] = ds2.Tables[0].Rows[i]["strEmpId"];
                        param1[8] = ds2.Tables[0].Rows[i]["intDocumentID"];
                        param1[9] = companyId;
                        param1[10] = BranchId;
                        param1[11] = FyId;
                        param1[12] = "Pending";
                        param1[13] = ds2.Tables[0].Rows[i]["strOptionalFlag"];
                        param1[14] = ds2.Tables[0].Rows[i]["strSuperUserFlag"];
                        param1[15] = ds2.Tables[0].Rows[i]["intDocumentCategoryId"];
                        param1[16] = ds2.Tables[0].Rows[i]["strDocumentCategory"];
                        param1[17] = ds2.Tables[0].Rows[i]["intAuthorisationId"];
                        param1[18] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intTotalRevisionno"] : 0;
                        param1[19] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionDate"].ToString() : "";
                        param1[20] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionRemarks"].ToString() : "";
                        param1[21] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intLastRevisedId"] : 0;
                        param1[22] = ds2.Tables[0].Rows[i]["intSecondaryUserId"];
                        param1[23] = ds2.Tables[0].Rows[i]["intTertiaryUserId"];
                        param1[24] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intItemId"] : 0;
                        param1[25] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intPartyId"] : 0;
                        param1[26] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["dblRateDiff"] : 0;

                        param1[27] = ds2.Tables[0].Rows[i]["strType"];
                        param1[28] = ds2.Tables[0].Rows[i]["intTypeId"];
                        param1[29] = ds2.Tables[0].Rows[i]["intDesignationId"];

                        param1[30] = ds2.Tables[0].Rows[i]["intDueDays"];
                        param1[31] = ds2.Tables[0].Rows[i]["intTotaldueDays"];
                        param1[32] = ds2.Tables[0].Rows[i]["intEsclationDays"];
                        param1[33] = dsMaxApprovalsetNo.Tables[0].Rows[0]["ApprovalsetNo"];
                        param1[34] = StrDocumentRemark1;
                        param1[35] = StrDocumentRemark2;
                        param1[36] = strEntryType;
                        param1[37] = ds2.Tables[0].Rows[i]["strRevisionFlag"];
                        param1[38] = ds2.Tables[0].Rows[i]["inttotLevel"];
                        param1[39] = ds2.Tables[0].Rows[i]["intTotRevisionLevel"];
                        param1[40] = ds2.Tables[0].Rows[i]["strApprovalType"];
                        param1[41] = ds2.Tables[0].Rows[i]["strFormName"];
                        param1[42] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["StrItemName"].ToString() : "";
                        param1[43] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["StrPartNumber"].ToString() : "";
                        param1[44] = ds2.Tables[0].Rows[i]["strFormViewFlag"];
                        param1[45] = ds2.Tables[0].Rows[i]["strDetailViewFalg"];
                        param1[46] = ds2.Tables[0].Rows[i]["strFaEffectFlag"];

                        Count += ObjDataServices.SaveQuery("ssp_InsertTblAuthorisationApproval", param1);
                    }
                }
                else if (ds.Tables[0].Rows.Count > 0)
                {
                    int Count = 0;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        object[] param1 = new object[47];
                        //object[] param1 = new object[17];
                        param1[0] = strDocType;
                        param1[1] = strDocNo;
                        param1[2] = intDocId;
                        param1[3] = strDocDate;
                        param1[4] = strPartyName;
                        param1[5] = ds.Tables[0].Rows[i]["intSequence"];
                        param1[6] = ds.Tables[0].Rows[i]["intUserId"];
                        param1[7] = ds.Tables[0].Rows[i]["strEmpId"];
                        param1[8] = ds.Tables[0].Rows[i]["intDocumentID"];
                        param1[9] = companyId;
                        param1[10] = BranchId;
                        param1[11] = FyId;
                        param1[12] = "Pending";
                        param1[13] = ds.Tables[0].Rows[i]["strOptionalFlag"];
                        param1[14] = ds.Tables[0].Rows[i]["strSuperUserFlag"];
                        param1[15] = 0;
                        param1[16] = "";
                        param1[17] = ds.Tables[0].Rows[i]["intAuthorisationId"];
                        param1[18] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intTotalRevisionno"] : 0;
                        param1[19] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionDate"].ToString() : "";
                        param1[20] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["strRevisionRemarks"].ToString() : "";
                        param1[21] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intLastRevisedId"] : 0;
                        param1[22] = ds.Tables[0].Rows[i]["intSecondaryUserId"];
                        param1[23] = ds.Tables[0].Rows[i]["intTertiaryUserId"];
                        param1[24] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intItemId"] : 0;
                        param1[25] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["intPartyId"] : 0;
                        param1[26] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["dblRateDiff"] : 0;

                        param1[27] = ds.Tables[0].Rows[i]["strType"];
                        param1[28] = ds.Tables[0].Rows[i]["intTypeId"];
                        param1[29] = ds.Tables[0].Rows[i]["intDesignationId"];

                        param1[30] = ds.Tables[0].Rows[i]["intDueDays"];
                        param1[31] = ds.Tables[0].Rows[i]["intTotaldueDays"];
                        param1[32] = ds.Tables[0].Rows[i]["intEsclationDays"];
                        param1[33] = dsMaxApprovalsetNo.Tables[0].Rows[0]["ApprovalsetNo"];
                        // param1[34] = ds2.Tables[0].Rows[i]["intLevel"];
                        param1[34] = StrDocumentRemark1;
                        param1[35] = StrDocumentRemark2;
                        param1[36] = strEntryType;
                        param1[37] = ds.Tables[0].Rows[i]["strRevisionFlag"];
                        param1[38] = ds.Tables[0].Rows[i]["inttotLevel"];
                        param1[39] = ds.Tables[0].Rows[i]["intTotRevisionLevel"];
                        param1[40] = ds.Tables[0].Rows[i]["strApprovalType"];
                        param1[41] = ds.Tables[0].Rows[i]["strFormName"]; ;
                        param1[42] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["StrItemName"].ToString() : "";
                        param1[43] = (ds3 != null && ds3.Tables[0].Rows.Count > 0) ? ds3.Tables[0].Rows[0]["StrPartNumber"].ToString() : "";
                        param1[44] = ds.Tables[0].Rows[i]["strFormViewFlag"];
                        param1[45] = ds.Tables[0].Rows[i]["strDetailViewFalg"];
                        param1[46] = ds.Tables[0].Rows[i]["strFaEffectFlag"];

                        Count += ObjDataServices.SaveQuery("ssp_InsertTblAuthorisationApproval", param1);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9, string ID10)
        {
            try
            {
                object[] param = new object[10];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;
                param[5] = ID6;
                param[6] = ID7;
                param[7] = ID8;
                param[8] = ID9;
                param[9] = ID10;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9, string ID10, string ID11)
        {
            try
            {
                object[] param = new object[11];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;
                param[5] = ID6;
                param[6] = ID7;
                param[7] = ID8;
                param[8] = ID9;
                param[9] = ID10;
                param[10] = ID11;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9, string ID10, string ID11, string ID12)
        {
            try
            {
                object[] param = new object[12];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;
                param[5] = ID6;
                param[6] = ID7;
                param[7] = ID8;
                param[8] = ID9;
                param[9] = ID10;
                param[10] = ID11;
                param[11] = ID12;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9, string ID10, string ID11, string ID12, string ID13)
        {
            try
            {
                object[] param = new object[13];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;
                param[5] = ID6;
                param[6] = ID7;
                param[7] = ID8;
                param[8] = ID9;
                param[9] = ID10;
                param[10] = ID11;
                param[11] = ID12;
                param[12] = ID13;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9, string ID10, string ID11, string ID12, string ID13, string ID14, string ID15)
        {
            try
            {
                object[] param = new object[15];
                param[0] = ID1;
                param[1] = ID2;
                param[2] = ID3;
                param[3] = ID4;
                param[4] = ID5;
                param[5] = ID6;
                param[6] = ID7;
                param[7] = ID8;
                param[8] = ID9;
                param[9] = ID10;
                param[10] = ID11;
                param[11] = ID12;
                param[12] = ID13;
                param[13] = ID14;
                param[14] = ID15;

                return ObjDataServices.FillDataSet(SP_Name, param);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        # region [FillDDLSearchDEL()]
        public void FillDDLSearchDEL(DropDownList DDLSearch, string Form_Name)
        {
            try
            {
                DataSet dsFillDDLSearch = ObjDataServices.FillDS("ssp_getDeleteFilterdataset", Form_Name);
                DDLSearch.AppendDataBoundItems = true;
                DDLSearch.Items.Add("...Select One...");
                DDLSearch.DataSource = dsFillDDLSearch.Tables[0];
                DDLSearch.DataTextField = "nvDisplayName";
                DDLSearch.DataValueField = "Common";
                DDLSearch.DataBind();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        #region GetFormWIseGridList
        public void GridColumnAdd(ref GridView ObjGridCr, string FormName, string strQueryFieldName, string QueryFieldValue)
        {
            try
            {
                DataSet DsForColumn = new DataSet();
                DsForColumn = FillDataSet("ssp_GetTblFormControlTransactionForGridColumnList", FormName, strQueryFieldName, QueryFieldValue);

                //DataSet dsTemp = FillDS("ssp_GetTblFormControlTransactionForGridColumnListSub", FormName);
                //DsForColumn.Merge(dsTemp);

                if (DsForColumn != null)
                {
                    if (DsForColumn.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < DsForColumn.Tables[0].Rows.Count; i++)
                        {
                            BoundField bc = new BoundField();
                            bc.DataField = DsForColumn.Tables[0].Rows[i]["FiledName"].ToString();
                            bc.HeaderText = DsForColumn.Tables[0].Rows[i]["ViewCaption"].ToString();
                            bc.SortExpression = DsForColumn.Tables[0].Rows[i]["FiledName"].ToString();
                            bc.ItemStyle.CssClass = "styleGridLines";
                            bc.HeaderStyle.CssClass = "styleGridLines";
                            ObjGridCr.Columns.Add(bc);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ObjDataSet.Clear();
                //return ObjDataSet;
                //throw;
            }
        }

        public void GetDataGridColum(ref GridView objDataGrd, string FormName, string strTabName, string strQueryFieldName, string QueryFieldValue)
        {
            try
            {
                DataSet DsForColumn = new DataSet();
                DsForColumn = FillDataSet("ssp_GetTblFormControlTransactionForDataGridColumnList", FormName, strTabName, strQueryFieldName, QueryFieldValue);
                //DataSet dsTemp = FillDS("ssp_GetTblFormControlTransactionForGridColumnListSub", FormName);
                //DsForColumn.Merge(dsTemp);

                if (DsForColumn != null)
                {
                    if (DsForColumn.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < DsForColumn.Tables[0].Rows.Count; i++)
                        {
                            BoundField bc = new BoundField();
                            bc.DataField = DsForColumn.Tables[0].Rows[i]["FiledName"].ToString();
                            bc.HeaderText = DsForColumn.Tables[0].Rows[i]["ViewCaption"].ToString();
                            bc.SortExpression = DsForColumn.Tables[0].Rows[i]["FiledName"].ToString();
                            bc.ItemStyle.CssClass = "styleGridLines";
                            bc.HeaderStyle.CssClass = "styleGridLines";
                            bc.Visible = true;
                            objDataGrd.Columns.Add(bc);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ObjDataSet.Clear();
                //return ObjDataSet;
                //throw;
            }
        }

        public void GetDataGridColum(ref DataGrid objDataGrd, string FormName, string strTabName, string strQueryFieldName, string QueryFieldValue)
        {
            try
            {
                DataSet DsForColumn = new DataSet();
                DsForColumn = FillDataSet("ssp_GetTblFormControlTransactionForDataGridColumnList", FormName, strTabName, strQueryFieldName, QueryFieldValue);
                //DataSet dsTemp = FillDS("ssp_GetTblFormControlTransactionForGridColumnListSub", FormName);
                //DsForColumn.Merge(dsTemp);

                if (DsForColumn != null)
                {
                    if (DsForColumn.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < DsForColumn.Tables[0].Rows.Count; i++)
                        {
                            BoundColumn bc = new BoundColumn();
                            bc.DataField = DsForColumn.Tables[0].Rows[i]["FiledName"].ToString();
                            bc.HeaderText = DsForColumn.Tables[0].Rows[i]["ViewCaption"].ToString();
                            bc.SortExpression = DsForColumn.Tables[0].Rows[i]["FiledName"].ToString();
                            bc.ItemStyle.CssClass = "styleGridLines";
                            bc.HeaderStyle.CssClass = "styleGridLines";
                            bc.Visible = true;
                            objDataGrd.Columns.Add(bc);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ObjDataSet.Clear();
                //return ObjDataSet;
                //throw;
            }
        }
        #endregion

        //# region [FillHugeDataSet()]
        //public DataSet FillHugeDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4)
        //{
        //    try
        //    {
        //        object[] param = new object[4];
        //        param[0] = ID1;
        //        param[1] = ID2;
        //        param[2] = ID3;
        //        param[3] = ID4;

        //        return ObjDataServices.FillHugeDataSet(SP_Name, param);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
        //public DataSet FillHugeDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5)
        //{
        //    try
        //    {
        //        object[] param = new object[5];
        //        param[0] = ID1;
        //        param[1] = ID2;
        //        param[2] = ID3;
        //        param[3] = ID4;
        //        param[4] = ID5;
        //        return ObjDataServices.FillHugeDataSet(SP_Name, param);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public DataSet FillHugeDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6)
        //{
        //    try
        //    {
        //        object[] param = new object[6];
        //        param[0] = ID1;
        //        param[1] = ID2;
        //        param[2] = ID3;
        //        param[3] = ID4;
        //        param[4] = ID5;
        //        param[5] = ID6;
        //        return ObjDataServices.FillHugeDataSet(SP_Name, param);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public DataSet FillHugeDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7)
        //{
        //    try
        //    {
        //        object[] param = new object[7];
        //        param[0] = ID1;
        //        param[1] = ID2;
        //        param[2] = ID3;
        //        param[3] = ID4;
        //        param[4] = ID5;
        //        param[5] = ID6;
        //        param[6] = ID7;
        //        return ObjDataServices.FillHugeDataSet(SP_Name, param);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public DataSet FillHugeDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8)
        //{
        //    try
        //    {
        //        object[] param = new object[8];
        //        param[0] = ID1;
        //        param[1] = ID2;
        //        param[2] = ID3;
        //        param[3] = ID4;
        //        param[4] = ID5;
        //        param[5] = ID6;
        //        param[6] = ID7;
        //        param[7] = ID8;
        //        return ObjDataServices.FillHugeDataSet(SP_Name, param);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public DataSet FillHugeDataSet(string SP_Name, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9)
        //{
        //    try
        //    {
        //        object[] param = new object[9];
        //        param[0] = ID1;
        //        param[1] = ID2;
        //        param[2] = ID3;
        //        param[3] = ID4;
        //        param[4] = ID5;
        //        param[5] = ID6;
        //        param[6] = ID7;
        //        param[7] = ID8;
        //        param[8] = ID9;
        //        return ObjDataServices.FillHugeDataSet(SP_Name, param);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public DataSet FillHugeDataSet(string SP_Name, object[] param)
        //{
        //    try
        //    {
        //        return ObjDataServices.FillHugeDataSet(SP_Name, param);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
        //# endregion

        #region FormWiseFillDDL()
        public void FormWiseFillDDL(DropDownList DDL, string FormID, string FormName, string FormCaption, string FieldName, string Conditionflag, string Conditionfield)
        {
            try
            {
                DataTable dt = new DataTable();
                object[] param = new object[6];
                param[0] = FormID;
                param[1] = FormName;
                param[2] = FormCaption;
                param[3] = FieldName;
                param[4] = Conditionflag;
                param[5] = Conditionfield;

                dt = ObjDataServices.GetDataSet("ssp_GetAutoFillComboFromAdmTblMasterValues", param);

                DDL.DataSource = dt;
                DDL.DataValueField = "strFieldValue";
                DDL.DataTextField = "strFieldText";
                DDL.DataBind();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region FormWiseFillTXTBox()
        public void FormWiseFillTXTBox(TextBox TXTBox, HiddenField HIDSelectID, string FormID, string FormName, string FormCaption, string FieldName, string Conditionflag, string Conditionfield)
        {
            try
            {
                DataTable dt = new DataTable();
                object[] param = new object[6];
                param[0] = FormID;
                param[1] = FormName;
                param[2] = FormCaption;
                param[3] = FieldName;
                param[4] = Conditionflag;
                param[5] = Conditionfield;

                dt = ObjDataServices.GetDataSet("ssp_GetAutoFillTXTBoXFromAdmTblMasterValues", param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    TXTBox.Text = dt.Rows[0]["strFieldText"].ToString();
                    HIDSelectID.Value = dt.Rows[0]["intId"].ToString() != "" ? dt.Rows[0]["intId"].ToString() : "0";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region FillLedgerTXTBoxAsPerInvoiceType()
        public void FillLedgerTXTBoxAsPerInvoiceType(TextBox TXTBox, string HIDPartyID, string HIDConsigneeID, string HIDBillingID, string CompanyID, string BranchID, string FYId, string FormName, string InvoiceType)
        {
            try
            {
                DataTable dt = new DataTable();
                object[] param = new object[8];
                param[0] = HIDPartyID;
                param[1] = HIDConsigneeID;
                param[2] = HIDBillingID;
                param[3] = CompanyID;
                param[4] = BranchID;
                param[5] = FYId;
                param[6] = FormName;
                param[7] = InvoiceType;

                dt = ObjDataServices.GetDataSet("ssp_GetAutoFillTXTBoXForLedgerEffectWithInCity", param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    TXTBox.Text = dt.Rows[0]["strFieldText"].ToString();
                    return;
                }
                dt = ObjDataServices.GetDataSet("ssp_GetAutoFillTXTBoXForLedgerEffectWithInState", param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    TXTBox.Text = dt.Rows[0]["strFieldText"].ToString();
                    return;
                }
                dt = ObjDataServices.GetDataSet("ssp_GetAutoFillTXTBoXForLedgerEffectWithInCountry", param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    TXTBox.Text = dt.Rows[0]["strFieldText"].ToString();
                    return;
                }
                dt = ObjDataServices.GetDataSet("ssp_GetAutoFillTXTBoXForLedgerEffectCountry", param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    TXTBox.Text = dt.Rows[0]["strFieldText"].ToString();
                    return;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region FillLedgerTXTBoxAsPerInvoiceTypeCustomerBase()
        public void FillLedgerTXTBoxAsPerInvoiceTypeCustomerBase(TextBox TXTBox, string HIDPartyID, string HIDConsigneeID, string HIDBillingID, string CompanyID, string BranchID, string FYId, string FormName, string InvoiceType)
        {
            try
            {
                DataTable dt = new DataTable();
                object[] param = new object[8];
                param[0] = HIDPartyID;
                param[1] = HIDConsigneeID;
                param[2] = HIDBillingID;
                param[3] = CompanyID;
                param[4] = BranchID;
                param[5] = FYId;
                param[6] = FormName;
                param[7] = InvoiceType;

                dt = ObjDataServices.GetDataSet("ssp_GetAutoFillTXTBoXForLedgerEffectWithInCityCustomerBase", param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    TXTBox.Text = dt.Rows[0]["strFieldText"].ToString();
                    return;
                }
                dt = ObjDataServices.GetDataSet("ssp_GetAutoFillTXTBoXForLedgerEffectWithInStateCustomerBase", param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    TXTBox.Text = dt.Rows[0]["strFieldText"].ToString();
                    return;
                }
                dt = ObjDataServices.GetDataSet("ssp_GetAutoFillTXTBoXForLedgerEffectWithInCountryCustomerBase", param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    TXTBox.Text = dt.Rows[0]["strFieldText"].ToString();
                    return;
                }
                dt = ObjDataServices.GetDataSet("ssp_GetAutoFillTXTBoXForLedgerEffectCountryCustomerBase", param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    TXTBox.Text = dt.Rows[0]["strFieldText"].ToString();
                    return;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region ///[General Master Values ]////

        public void AdmMasterValuesFillTXTBox(TextBox TXTBox1, HiddenField HIDSelectID1, TextBox TXTBox2, HiddenField HIDSelectID2, string FormCaption, string FieldName, string Conditionflag, string Conditionfield)
        {
            try
            {
                DataSet ds = new DataSet();
                object[] param = new object[4];
                param[0] = FormCaption;
                param[1] = FieldName;
                param[2] = Conditionflag;
                param[3] = Conditionfield;

                ds = FillDataSet("ssp_GetAutoFillGeneralTXTBoXFromAdmTblMasterValues", param);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    if (Conditionflag == "Y")
                    {
                        TXTBox1.Text = ds.Tables[0].Rows[0]["strFieldText"].ToString();
                        HIDSelectID1.Value = ds.Tables[0].Rows[0]["strCaption"].ToString() != "" ? ds.Tables[0].Rows[0]["strCaption"].ToString() : "0";

                        TXTBox2.Text = ds.Tables[1].Rows[0]["strFieldText"].ToString();
                        HIDSelectID2.Value = ds.Tables[1].Rows[0]["strCaption"].ToString() != "" ? ds.Tables[0].Rows[0]["strCaption"].ToString() : "0";
                    }
                    else
                    {
                        TXTBox1.Text = ds.Tables[0].Rows[0]["strFieldText"].ToString();
                        HIDSelectID1.Value = ds.Tables[0].Rows[0]["strCaption"].ToString() != "" ? ds.Tables[0].Rows[0]["strCaption"].ToString() : "0";

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void AdmMasterValuesFillTXTBox(TextBox TXTBox1, HiddenField HIDSelectID1, string FormCaption, string FieldName)
        {
            try
            {
                DataSet ds = new DataSet();
                object[] param = new object[4];
                param[0] = FormCaption;
                param[1] = FieldName;
                param[2] = "N";
                param[3] = "";

                ds = FillDataSet("ssp_GetAutoFillGeneralTXTBoXFromAdmTblMasterValues", param);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    TXTBox1.Text = ds.Tables[0].Rows[0]["strFieldText"].ToString();
                    HIDSelectID1.Value = ds.Tables[0].Rows[0]["strCaption"].ToString() != "" ? ds.Tables[0].Rows[0]["strCaption"].ToString() : "0";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region // [Convert Quantity UOM Wise] //

        public string ConvertQtyUOMWise(decimal Qty, int QtyUOMId)
        {
            try
            {
                DataSet dsUOMType = new DataSet();
                object[] param = new object[1];

                param[0] = Int32.Parse(QtyUOMId.ToString());
                dsUOMType = FillDataSet("sp_GetUnitTypeForUOM", param);
                if (dsUOMType.Tables[0].Rows[0]["strUOMType"].ToString() == "Counts")
                    return Convert.ToInt64(Qty).ToString();
                else
                    return Qty.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        # region [FillFormApproveBy()]
        public void FillFormApproveBy(Label LBLFormApproveBy, string Form_Name, string HeaderTableName, string ID_Field, string SearchField)
        {
            try
            {
                object[] param = new object[4];
                param[0] = Form_Name.ToString();
                param[1] = HeaderTableName.ToString();
                param[2] = ID_Field.ToString();
                param[3] = SearchField.ToString();
                DataSet dsFormApproveBy = ObjDataServices.FillDataSet("ssp_getFillFormApproveBy", param);

                if (dsFormApproveBy != null && dsFormApproveBy.Tables[0].Rows.Count > 0)
                {
                    LBLFormApproveBy.Text = dsFormApproveBy.Tables[0].Rows[0]["strFormApproveBy"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        # endregion

        public int getArrayCnt(ref DataSet ds, string frmName, string strQueryFieldName, string QueryFieldValue)
        {
            ds = FillDataSet("ssp_GetControlNameFromTblFormControlTransaction", frmName, strQueryFieldName, QueryFieldValue);
            int cnt = ds.Tables[0].Rows.Count;
            return cnt;
        }

        public int ReportTimeOutDataSet(string DocName, string FormName)
        {
            try
            {
                int TimeOut = 1000;
                DataSet dsReportTimeOut = new DataSet();
                dsReportTimeOut = FillDataSet("GetSP_ReportDocName", DocName.ToString().Trim(), FormName.ToString().Trim());
                if (dsReportTimeOut != null && dsReportTimeOut.Tables[0].Rows.Count > 0)
                {
                    TimeOut = Convert.ToInt32(dsReportTimeOut.Tables[0].Rows[0]["intTimeOutvalue"].ToString() != "" ? dsReportTimeOut.Tables[0].Rows[0]["intTimeOutvalue"].ToString() : "1000");
                }
                return TimeOut;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #region GridVisibility
        public void EmptyGridFix(ref GridView grdView)
        {
            // normally executes after a grid load method
            if (grdView.Rows.Count == 0 &&
                grdView.DataSource != null)
            {
                DataTable dt = null;

                // need to clone sources otherwise
                // it will be indirectly adding to 
                // the original source

                if (grdView.DataSource is DataSet)
                {
                    dt = ((DataSet)grdView.DataSource).Tables[0].Clone();
                }
                else if (grdView.DataSource is DataTable)
                {
                    dt = ((DataTable)grdView.DataSource).Clone();
                }

                if (dt == null)
                {
                    return;
                }

                if (!dt.Columns.Contains("SRNo"))
                {
                    dt.Columns.Add("SRNo");
                }

                dt.Rows.Add(dt.NewRow()); // add empty row
                grdView.DataSource = dt;
                grdView.DataBind();

                // hide row
                grdView.Rows[0].Visible = false;
                grdView.Rows[0].Controls.Clear();
            }

            // normally executes at all postbacks
            if (grdView.Rows.Count == 1 &&
                grdView.DataSource == null)
            {
                bool bIsGridEmpty = true;

                // check first row that all cells empty
                for (int i = 0; i < grdView.Rows[0].Cells.Count; i++)
                {
                    if (grdView.Rows[0].Cells[i].Text != string.Empty)
                    {
                        bIsGridEmpty = false;
                    }
                }
                // hide row
                if (bIsGridEmpty)
                {
                    grdView.Rows[0].Visible = false;
                    grdView.Rows[0].Controls.Clear();
                }
            }
        }
        #endregion

        //public  WebControl[] getControlArray()
        //{
        //    DataSet ds = new DataSet();
        //    ds = FillDS("ssp_GetControlNameFromTblFormControlTransaction", "frmOrderAcceptance");
        //    int cnt = ds.Tables[0].Rows.Count;
        //    WebControl[] cntrArray = new WebControl[cnt];

        //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //    {
        //        switch (ds.Tables[0].Rows[i]["strCntType"].ToString())
        //        {
        //            case "Label":
        //                Label ctr = new Label();
        //                ctr.ID = ds.Tables[0].Rows[i][0].ToString();
        //                cntrArray[i] = ctr;
        //                break;
        //            case "LinkButton":
        //                LinkButton ctr1 = new LinkButton();
        //                ctr1.ID = ds.Tables[0].Rows[i][0].ToString();
        //                cntrArray[i] = ctr1;
        //                break;
        //            case "ImageButton":
        //                ImageButton ctr2 = new ImageButton();
        //                ctr2.ID = ds.Tables[0].Rows[i][0].ToString();
        //                cntrArray[i] = ctr2;
        //                break;
        //            case "Button":
        //                Button ctr3 = new Button();
        //                ctr3.ID = ds.Tables[0].Rows[i][0].ToString();
        //                cntrArray[i] = ctr3;
        //                break;
        //            case "DropDownList":
        //                DropDownList ctr4 = new DropDownList();
        //                ctr4.ID = ds.Tables[0].Rows[i][0].ToString();
        //                cntrArray[i] = ctr4;
        //                break;
        //            case "Image":
        //                Image ctr5 = new Image();
        //                ctr5.ID = ds.Tables[0].Rows[i][0].ToString();
        //                cntrArray[i] = ctr5;
        //                break;
        //            case "TextBox":
        //                TextBox ctr6 = new TextBox();
        //                ctr6.ID = ds.Tables[0].Rows[i][0].ToString();
        //                cntrArray[i] = ctr6;
        //                break;
        //            case "CheckBox":
        //                CheckBox ctr7 = new CheckBox();
        //                ctr7.ID = ds.Tables[0].Rows[i][0].ToString();
        //                cntrArray[i] = ctr7;
        //                break;
        //        }  
        //    }
        //    return  cntrArray;
        //}

        #region  Fill DataBase Name

        public DataSet FillDBName()
        {
            try
            {
                return ObjDataServices.GetDataBaseName();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region  Set Connection String

        public void SetConnString(string KeyName)
        {
            try
            {
                ServerPath = KeyName;
                ObjDataServices.SetConnecString(KeyName);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion
        public string GetActualCaption(string Caption)
        {
            string NewCaption = "";
            NewCaption = Caption.Replace("<u>", "");
            NewCaption = NewCaption.Replace("</u>", "");

            return NewCaption;
        }

        #region GetServerParameter
        public string[] GetServerParameter()
        {
            return ObjDataServices.GetServerParameter();
        }
        #endregion

        #region FormWiseFillTab
        public DataSet FillTabDataSet(string FormName, ref DataSet ds)
        {
            ds = FillDS("SP_GetManagementFormTabDet", FormName);
            return ds;
        }
        #endregion

        public string GetFormID(string Form_Name, string strQueryFieldName, string QueryFieldValue)
        {
            try
            {
                string val = "0";
                DataSet ds = new DataSet();
                ds = FillDataSet("ssp_getFormIDTblFormInfo", Form_Name, strQueryFieldName, QueryFieldValue);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    val = ds.Tables[0].Rows[0][0].ToString();
                    return val;
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        #region Enum Letters
        public enum Letters
        {
            a = 25, b = 24, c = 23, d = 22, e = 21, f = 20, g = 19, h = 18, i = 17, j = 16, k = 15, l = 14, m = 13,
            n = 12, o = 11, p = 10, q = 09, r = 08, s = 07, t = 06, u = 05, v = 04, w = 03, x = 02, y = 01, z = 00,

            A = 26, B = 27, C = 28, D = 29, E = 30, F = 31, G = 32, H = 33, I = 34, J = 35, K = 36, L = 37, M = 38,
            N = 39, O = 40, P = 41, Q = 42, R = 43, S = 44, T = 45, U = 46, V = 47, W = 48, X = 49, Y = 50, Z = 51
        }
        #endregion

        #region [EncryptFieldName]

        public string EncryptFieldName(string strval)
        {
            string str = "";
            try
            {
                for (int i = 0; i < strval.Length; i++)
                {
                    str = str + EncryptFieldNameLogic(strval, i);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return str;
        }

        public string EncryptFieldNameLogic(string strval, int i)
        {
            string str = "";
            try
            {
                string sChar = strval.Substring(i, (strval.Length - (strval.Length - 1)));

                if (sChar == " ")
                {
                    str = str + "52".ToString();
                }
                else
                {
                    var ActualValue = (Int32)((Letters)Enum.Parse(typeof(Letters), sChar));

                    if (ActualValue.ToString() == sChar)
                    {
                        str = str + Enum.GetName(typeof(Letters), Convert.ToInt32(sChar)).ToString();
                    }
                    else
                    {
                        if (Convert.ToInt32(ActualValue) < 10)
                            str = str + "0" + ActualValue.ToString();
                        else
                            str = str + ActualValue.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return str;
        }
        #endregion

        #region [DecryptFieldName]

        public string DecryptFieldName(string strval)
        {
            string str = "";
            try
            {
                for (int i = 0; i < strval.Length; i++)
                {
                    str = str + DecryptFieldNameLogic(strval, i);
                    i++;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return str;
        }

        public string DecryptFieldNameLogic(string strval, int i)
        {
            string str = "";
            try
            {
                string sChar = strval.Substring(i, (strval.Length - (strval.Length - 2)));

                if (sChar == "52")
                {
                    str = str + " ".ToString();
                }
                else
                {
                    string strActualValue = "";
                    if (sChar.Contains("0") == true || sChar.Contains("1") == true || sChar.Contains("2") == true || sChar.Contains("3") == true || sChar.Contains("4") == true || sChar.Contains("5") == true || sChar.Contains("6") == true || sChar.Contains("7") == true || sChar.Contains("8") == true || sChar.Contains("9") == true)
                    {
                        var ActualValue = (Int32)((Letters)Enum.Parse(typeof(Letters), sChar));
                        strActualValue = ActualValue.ToString();
                    }

                    if (sChar.Substring(0, sChar.Length - 1).ToString() == "0")
                        strActualValue = "0" + strActualValue.ToString();

                    if (strActualValue.ToString() == sChar)
                        str = str + Enum.GetName(typeof(Letters), Convert.ToInt32(sChar)).ToString();
                    else
                        foreach (var cha in sChar)
                        {
                            var ActualValueTemp = (Int32)((Letters)Enum.Parse(typeof(Letters), cha.ToString()));
                            str = str + ActualValueTemp.ToString();
                        }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return str;
        }
        #endregion

        # region [FillRegistryDATA()]
        public DataSet FillRegistryDATA(string KeyName)
        {
            try
            {
                return ObjDataServices.FillRegistryDATANew(KeyName);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        //added for freeze grid column
        #region [FREEZE GRID COL]
        public string FreezeColumnGrid(Control Gr, int width, int ht, int no_of_column)
        {
            string SCr = "<script type=" + '"' + "text/javascript" + '"' + ">"
   + " $(document).ready(function() { "
      + "$(" + "'#" + Gr.ClientID + "').gridviewScroll({"
            + "width:" + width + ","

            + " height " + ":" + ht + ","

            + "freezesize:" + no_of_column
       + "});  "
   + "});"
   + "</script>";

            return SCr;
        }
        #endregion

        /// Added For Registry Data
        #region [REGISTRY DATA]
        public string RegApplicationName()
        {
            try
            {
                return ObjDataServices.DataServicesRegApplicationName();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string RegSuperAdminPassword()
        {
            try
            {
                return ObjDataServices.DataServicesRegSuperAdminPassword();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string RegNoOfUser()
        {
            try
            {
                return ObjDataServices.DataServicesRegNoOfUser();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string RegValidity()
        {
            try
            {
                return ObjDataServices.DataServicesRegNoOfUser();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string RegNoOfBranches()
        {
            try
            {
                return ObjDataServices.DataServicesRegNoOfBranches();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string RegCompanyName()
        {
            try
            {
                return ObjDataServices.DataServicesRegCompanyName();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string RegCompanyShortName()
        {
            try
            {
                return ObjDataServices.DataServicesRegCompanyShortName();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string RegNoOfModule()
        {
            try
            {
                return ObjDataServices.DataServicesRegNoOfModule();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string RegLicenceNo()
        {
            try
            {
                return ObjDataServices.DataServicesRegLicenceNo();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string RegUseFlag()
        {
            try
            {
                return ObjDataServices.DataServicesRegUseFlag();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string sqlConnectionString()
        {
            try
            {
                return ObjDataServices.DataServicessqlConnectionString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion
        #region [FillHugeDataSet()]
        public SqlDbType getSqlDbType(string DataType)
        {
            return DataType == "int" ? SqlDbType.Int : DataType == "varchar" ? SqlDbType.Text : DataType == "decimal" ? SqlDbType.Decimal :
                DataType == "datetime" ? SqlDbType.DateTime : DataType == "bigint" ? SqlDbType.BigInt :
                DataType == "date" ? SqlDbType.Date : DataType == "nvarchar" ? SqlDbType.NVarChar :
                DataType == "nchar" ? SqlDbType.NChar : DataType == "smallint" ? SqlDbType.SmallInt :
                DataType == "image" ? SqlDbType.Image : DataType == "binary" ? SqlDbType.Binary : DataType == "bit" ? SqlDbType.Bit :
                DataType == "char" ? SqlDbType.Char : DataType == "float" ? SqlDbType.Float :
                DataType == "money" ? SqlDbType.Money : DataType == "ntext" ? SqlDbType.NText :
                DataType == "real" ? SqlDbType.Real : DataType == "smalldatetime" ? SqlDbType.SmallDateTime :
                DataType == "smallmoney" ? SqlDbType.SmallMoney : DataType == "timestamp" ? SqlDbType.Timestamp :
                DataType == "time" ? SqlDbType.Time : DataType == "tinyint" ? SqlDbType.TinyInt :
                DataType == "varbinary" ? SqlDbType.VarBinary : SqlDbType.Text; //  Default
        }

        public object GetConvertedParameter(string DataType, string Param)
        {
            object obj = new object();
            switch (DataType)
            {
                case "int":
                    obj = Convert.ToInt32(Param != "" ? Param : "0");
                    break;
                case "bigint":
                    obj = Convert.ToInt64(Param != "" ? Param : "0");
                    break;
                case "decimal":
                    obj = Convert.ToDecimal(Param != "" ? Param : "0.0000");
                    break;
                default:
                    obj = Param.ToString();
                    break;
            }
            return obj;
        }

        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "";
                string DataType1 = "";
                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                //objCmd.Parameters[Caption1].Value = ID1.ToString();
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "";
                string DataType1 = "", DataType2 = "";
                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "";
                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "";
                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4, string ID5)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "", Caption5 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "", DataType5 = "";
                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 5)
                        {
                            Caption5 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType5 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                objCmd.Parameters.Add(Caption5, getSqlDbType(DataType5));
                objCmd.Parameters[Caption5].Value = GetConvertedParameter(DataType5, ID5.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "", Caption5 = "", Caption6 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "", DataType5 = "", DataType6 = "";
                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 5)
                        {
                            Caption5 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType5 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 6)
                        {
                            Caption6 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType6 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                objCmd.Parameters.Add(Caption5, getSqlDbType(DataType5));
                objCmd.Parameters[Caption5].Value = GetConvertedParameter(DataType5, ID5.ToString());

                objCmd.Parameters.Add(Caption6, getSqlDbType(DataType6));
                objCmd.Parameters[Caption6].Value = GetConvertedParameter(DataType6, ID6.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "", Caption5 = "", Caption6 = "", Caption7 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "", DataType5 = "", DataType6 = "", DataType7 = "";


                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 5)
                        {
                            Caption5 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType5 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 6)
                        {
                            Caption6 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType6 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }

                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 7)
                        {
                            Caption7 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType7 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                objCmd.Parameters.Add(Caption5, getSqlDbType(DataType5));
                objCmd.Parameters[Caption5].Value = GetConvertedParameter(DataType5, ID5.ToString());

                objCmd.Parameters.Add(Caption6, getSqlDbType(DataType6));
                objCmd.Parameters[Caption6].Value = GetConvertedParameter(DataType6, ID6.ToString());


                objCmd.Parameters.Add(Caption7, getSqlDbType(DataType7));
                objCmd.Parameters[Caption7].Value = GetConvertedParameter(DataType7, ID7.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "", Caption5 = "", Caption6 = "", Caption7 = "", Caption8 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "", DataType5 = "", DataType6 = "", DataType7 = "", DataType8 = "";


                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 5)
                        {
                            Caption5 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType5 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 6)
                        {
                            Caption6 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType6 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }

                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 7)
                        {
                            Caption7 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType7 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 8)
                        {
                            Caption8 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType8 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                objCmd.Parameters.Add(Caption5, getSqlDbType(DataType5));
                objCmd.Parameters[Caption5].Value = GetConvertedParameter(DataType5, ID5.ToString());

                objCmd.Parameters.Add(Caption6, getSqlDbType(DataType6));
                objCmd.Parameters[Caption6].Value = GetConvertedParameter(DataType6, ID6.ToString());


                objCmd.Parameters.Add(Caption7, getSqlDbType(DataType7));
                objCmd.Parameters[Caption7].Value = GetConvertedParameter(DataType7, ID7.ToString());

                objCmd.Parameters.Add(Caption8, getSqlDbType(DataType8));
                objCmd.Parameters[Caption8].Value = GetConvertedParameter(DataType8, ID8.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "", Caption5 = "", Caption6 = "", Caption7 = "", Caption8 = "", Caption9 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "", DataType5 = "", DataType6 = "", DataType7 = "", DataType8 = "", DataType9 = "";


                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 5)
                        {
                            Caption5 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType5 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 6)
                        {
                            Caption6 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType6 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }

                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 7)
                        {
                            Caption7 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType7 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 8)
                        {
                            Caption8 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType8 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 9)
                        {
                            Caption9 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType9 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                objCmd.Parameters.Add(Caption5, getSqlDbType(DataType5));
                objCmd.Parameters[Caption5].Value = GetConvertedParameter(DataType5, ID5.ToString());

                objCmd.Parameters.Add(Caption6, getSqlDbType(DataType6));
                objCmd.Parameters[Caption6].Value = GetConvertedParameter(DataType6, ID6.ToString());

                objCmd.Parameters.Add(Caption7, getSqlDbType(DataType7));
                objCmd.Parameters[Caption7].Value = GetConvertedParameter(DataType7, ID7.ToString());

                objCmd.Parameters.Add(Caption8, getSqlDbType(DataType8));
                objCmd.Parameters[Caption8].Value = GetConvertedParameter(DataType8, ID8.ToString());

                objCmd.Parameters.Add(Caption9, getSqlDbType(DataType9));
                objCmd.Parameters[Caption9].Value = GetConvertedParameter(DataType9, ID9.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9, string ID10)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "", Caption5 = "", Caption6 = "", Caption7 = "", Caption8 = "", Caption9 = "", Caption10 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "", DataType5 = "", DataType6 = "", DataType7 = "", DataType8 = "", DataType9 = "", DataType10 = "";

                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 5)
                        {
                            Caption5 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType5 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 6)
                        {
                            Caption6 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType6 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 7)
                        {
                            Caption7 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType7 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 8)
                        {
                            Caption8 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType8 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 9)
                        {
                            Caption9 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType9 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 10)
                        {
                            Caption10 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType10 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                objCmd.Parameters.Add(Caption5, getSqlDbType(DataType5));
                objCmd.Parameters[Caption5].Value = GetConvertedParameter(DataType5, ID5.ToString());

                objCmd.Parameters.Add(Caption6, getSqlDbType(DataType6));
                objCmd.Parameters[Caption6].Value = GetConvertedParameter(DataType6, ID6.ToString());

                objCmd.Parameters.Add(Caption7, getSqlDbType(DataType7));
                objCmd.Parameters[Caption7].Value = GetConvertedParameter(DataType7, ID7.ToString());

                objCmd.Parameters.Add(Caption8, getSqlDbType(DataType8));
                objCmd.Parameters[Caption8].Value = GetConvertedParameter(DataType8, ID8.ToString());

                objCmd.Parameters.Add(Caption9, getSqlDbType(DataType9));
                objCmd.Parameters[Caption9].Value = GetConvertedParameter(DataType9, ID9.ToString());

                objCmd.Parameters.Add(Caption10, getSqlDbType(DataType10));
                objCmd.Parameters[Caption10].Value = GetConvertedParameter(DataType10, ID10.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9, string ID10, string ID11)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "", Caption5 = "", Caption6 = "", Caption7 = "", Caption8 = "", Caption9 = "", Caption10 = "", Caption11 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "", DataType5 = "", DataType6 = "", DataType7 = "", DataType8 = "", DataType9 = "", DataType10 = "", DataType11 = "";

                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 5)
                        {
                            Caption5 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType5 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 6)
                        {
                            Caption6 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType6 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 7)
                        {
                            Caption7 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType7 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 8)
                        {
                            Caption8 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType8 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 9)
                        {
                            Caption9 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType9 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 10)
                        {
                            Caption10 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType10 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 11)
                        {
                            Caption11 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType11 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                objCmd.Parameters.Add(Caption5, getSqlDbType(DataType5));
                objCmd.Parameters[Caption5].Value = GetConvertedParameter(DataType5, ID5.ToString());

                objCmd.Parameters.Add(Caption6, getSqlDbType(DataType6));
                objCmd.Parameters[Caption6].Value = GetConvertedParameter(DataType6, ID6.ToString());

                objCmd.Parameters.Add(Caption7, getSqlDbType(DataType7));
                objCmd.Parameters[Caption7].Value = GetConvertedParameter(DataType7, ID7.ToString());

                objCmd.Parameters.Add(Caption8, getSqlDbType(DataType8));
                objCmd.Parameters[Caption8].Value = GetConvertedParameter(DataType8, ID8.ToString());

                objCmd.Parameters.Add(Caption9, getSqlDbType(DataType9));
                objCmd.Parameters[Caption9].Value = GetConvertedParameter(DataType9, ID9.ToString());

                objCmd.Parameters.Add(Caption10, getSqlDbType(DataType10));
                objCmd.Parameters[Caption10].Value = GetConvertedParameter(DataType10, ID10.ToString());

                objCmd.Parameters.Add(Caption11, getSqlDbType(DataType11));
                objCmd.Parameters[Caption11].Value = GetConvertedParameter(DataType11, ID11.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9, string ID10, string ID11, string ID12)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "", Caption5 = "", Caption6 = "", Caption7 = "", Caption8 = "", Caption9 = "", Caption10 = "", Caption11 = "", Caption12 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "", DataType5 = "", DataType6 = "", DataType7 = "", DataType8 = "", DataType9 = "", DataType10 = "", DataType11 = "", DataType12 = "";

                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 5)
                        {
                            Caption5 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType5 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 6)
                        {
                            Caption6 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType6 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 7)
                        {
                            Caption7 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType7 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 8)
                        {
                            Caption8 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType8 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 9)
                        {
                            Caption9 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType9 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 10)
                        {
                            Caption10 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType10 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 11)
                        {
                            Caption11 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType11 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 12)
                        {
                            Caption12 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType12 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                objCmd.Parameters.Add(Caption5, getSqlDbType(DataType5));
                objCmd.Parameters[Caption5].Value = GetConvertedParameter(DataType5, ID5.ToString());

                objCmd.Parameters.Add(Caption6, getSqlDbType(DataType6));
                objCmd.Parameters[Caption6].Value = GetConvertedParameter(DataType6, ID6.ToString());

                objCmd.Parameters.Add(Caption7, getSqlDbType(DataType7));
                objCmd.Parameters[Caption7].Value = GetConvertedParameter(DataType7, ID7.ToString());

                objCmd.Parameters.Add(Caption8, getSqlDbType(DataType8));
                objCmd.Parameters[Caption8].Value = GetConvertedParameter(DataType8, ID8.ToString());

                objCmd.Parameters.Add(Caption9, getSqlDbType(DataType9));
                objCmd.Parameters[Caption9].Value = GetConvertedParameter(DataType9, ID9.ToString());

                objCmd.Parameters.Add(Caption10, getSqlDbType(DataType10));
                objCmd.Parameters[Caption10].Value = GetConvertedParameter(DataType10, ID10.ToString());

                objCmd.Parameters.Add(Caption11, getSqlDbType(DataType11));
                objCmd.Parameters[Caption11].Value = GetConvertedParameter(DataType11, ID11.ToString());

                objCmd.Parameters.Add(Caption12, getSqlDbType(DataType12));
                objCmd.Parameters[Caption12].Value = GetConvertedParameter(DataType12, ID12.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9, string ID10, string ID11, string ID12, string ID13)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "", Caption5 = "", Caption6 = "", Caption7 = "", Caption8 = "", Caption9 = "", Caption10 = "", Caption11 = "", Caption12 = "", Caption13 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "", DataType5 = "", DataType6 = "", DataType7 = "", DataType8 = "", DataType9 = "", DataType10 = "", DataType11 = "", DataType12 = "", DataType13 = "";

                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 5)
                        {
                            Caption5 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType5 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 6)
                        {
                            Caption6 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType6 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 7)
                        {
                            Caption7 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType7 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 8)
                        {
                            Caption8 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType8 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 9)
                        {
                            Caption9 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType9 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 10)
                        {
                            Caption10 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType10 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 11)
                        {
                            Caption11 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType11 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 12)
                        {
                            Caption12 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType12 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 13)
                        {
                            Caption13 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType13 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                objCmd.Parameters.Add(Caption5, getSqlDbType(DataType5));
                objCmd.Parameters[Caption5].Value = GetConvertedParameter(DataType5, ID5.ToString());

                objCmd.Parameters.Add(Caption6, getSqlDbType(DataType6));
                objCmd.Parameters[Caption6].Value = GetConvertedParameter(DataType6, ID6.ToString());

                objCmd.Parameters.Add(Caption7, getSqlDbType(DataType7));
                objCmd.Parameters[Caption7].Value = GetConvertedParameter(DataType7, ID7.ToString());

                objCmd.Parameters.Add(Caption8, getSqlDbType(DataType8));
                objCmd.Parameters[Caption8].Value = GetConvertedParameter(DataType8, ID8.ToString());

                objCmd.Parameters.Add(Caption9, getSqlDbType(DataType9));
                objCmd.Parameters[Caption9].Value = GetConvertedParameter(DataType9, ID9.ToString());

                objCmd.Parameters.Add(Caption10, getSqlDbType(DataType10));
                objCmd.Parameters[Caption10].Value = GetConvertedParameter(DataType10, ID10.ToString());

                objCmd.Parameters.Add(Caption11, getSqlDbType(DataType11));
                objCmd.Parameters[Caption11].Value = GetConvertedParameter(DataType11, ID11.ToString());

                objCmd.Parameters.Add(Caption12, getSqlDbType(DataType12));
                objCmd.Parameters[Caption12].Value = GetConvertedParameter(DataType12, ID12.ToString());

                objCmd.Parameters.Add(Caption13, getSqlDbType(DataType13));
                objCmd.Parameters[Caption13].Value = GetConvertedParameter(DataType13, ID13.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9, string ID10, string ID11, string ID12, string ID13, string ID14)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "", Caption5 = "", Caption6 = "", Caption7 = "", Caption8 = "", Caption9 = "", Caption10 = "", Caption11 = "", Caption12 = "", Caption13 = "", Caption14 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "", DataType5 = "", DataType6 = "", DataType7 = "", DataType8 = "", DataType9 = "", DataType10 = "", DataType11 = "", DataType12 = "", DataType13 = "", DataType14 = "";

                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 5)
                        {
                            Caption5 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType5 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 6)
                        {
                            Caption6 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType6 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 7)
                        {
                            Caption7 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType7 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 8)
                        {
                            Caption8 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType8 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 9)
                        {
                            Caption9 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType9 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 10)
                        {
                            Caption10 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType10 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 11)
                        {
                            Caption11 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType11 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 12)
                        {
                            Caption12 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType12 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 13)
                        {
                            Caption13 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType13 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 14)
                        {
                            Caption14 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType14 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                objCmd.Parameters.Add(Caption5, getSqlDbType(DataType5));
                objCmd.Parameters[Caption5].Value = GetConvertedParameter(DataType5, ID5.ToString());

                objCmd.Parameters.Add(Caption6, getSqlDbType(DataType6));
                objCmd.Parameters[Caption6].Value = GetConvertedParameter(DataType6, ID6.ToString());

                objCmd.Parameters.Add(Caption7, getSqlDbType(DataType7));
                objCmd.Parameters[Caption7].Value = GetConvertedParameter(DataType7, ID7.ToString());

                objCmd.Parameters.Add(Caption8, getSqlDbType(DataType8));
                objCmd.Parameters[Caption8].Value = GetConvertedParameter(DataType8, ID8.ToString());

                objCmd.Parameters.Add(Caption9, getSqlDbType(DataType9));
                objCmd.Parameters[Caption9].Value = GetConvertedParameter(DataType9, ID9.ToString());

                objCmd.Parameters.Add(Caption10, getSqlDbType(DataType10));
                objCmd.Parameters[Caption10].Value = GetConvertedParameter(DataType10, ID10.ToString());

                objCmd.Parameters.Add(Caption11, getSqlDbType(DataType11));
                objCmd.Parameters[Caption11].Value = GetConvertedParameter(DataType11, ID11.ToString());

                objCmd.Parameters.Add(Caption12, getSqlDbType(DataType12));
                objCmd.Parameters[Caption12].Value = GetConvertedParameter(DataType12, ID12.ToString());

                objCmd.Parameters.Add(Caption13, getSqlDbType(DataType13));
                objCmd.Parameters[Caption13].Value = GetConvertedParameter(DataType13, ID13.ToString());

                objCmd.Parameters.Add(Caption14, getSqlDbType(DataType14));
                objCmd.Parameters[Caption14].Value = GetConvertedParameter(DataType14, ID14.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, string ID1, string ID2, string ID3, string ID4, string ID5, string ID6, string ID7, string ID8, string ID9, string ID10, string ID11, string ID12, string ID13, string ID14, string ID15)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "", Caption4 = "", Caption5 = "", Caption6 = "", Caption7 = "", Caption8 = "", Caption9 = "", Caption10 = "", Caption11 = "", Caption12 = "", Caption13 = "", Caption14 = "", Caption15 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "", DataType4 = "", DataType5 = "", DataType6 = "", DataType7 = "", DataType8 = "", DataType9 = "", DataType10 = "", DataType11 = "", DataType12 = "", DataType13 = "", DataType14 = "", DataType15 = "";

                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 4)
                        {
                            Caption4 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType4 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 5)
                        {
                            Caption5 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType5 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 6)
                        {
                            Caption6 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType6 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 7)
                        {
                            Caption7 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType7 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 8)
                        {
                            Caption8 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType8 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 9)
                        {
                            Caption9 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType9 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 10)
                        {
                            Caption10 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType10 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 11)
                        {
                            Caption11 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType11 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 12)
                        {
                            Caption12 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType12 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 13)
                        {
                            Caption13 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType13 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 14)
                        {
                            Caption14 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType14 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 15)
                        {
                            Caption15 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType15 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                objCmd.Parameters.Add(Caption4, getSqlDbType(DataType4));
                objCmd.Parameters[Caption4].Value = GetConvertedParameter(DataType4, ID4.ToString());

                objCmd.Parameters.Add(Caption5, getSqlDbType(DataType5));
                objCmd.Parameters[Caption5].Value = GetConvertedParameter(DataType5, ID5.ToString());

                objCmd.Parameters.Add(Caption6, getSqlDbType(DataType6));
                objCmd.Parameters[Caption6].Value = GetConvertedParameter(DataType6, ID6.ToString());

                objCmd.Parameters.Add(Caption7, getSqlDbType(DataType7));
                objCmd.Parameters[Caption7].Value = GetConvertedParameter(DataType7, ID7.ToString());

                objCmd.Parameters.Add(Caption8, getSqlDbType(DataType8));
                objCmd.Parameters[Caption8].Value = GetConvertedParameter(DataType8, ID8.ToString());

                objCmd.Parameters.Add(Caption9, getSqlDbType(DataType9));
                objCmd.Parameters[Caption9].Value = GetConvertedParameter(DataType9, ID9.ToString());

                objCmd.Parameters.Add(Caption10, getSqlDbType(DataType10));
                objCmd.Parameters[Caption10].Value = GetConvertedParameter(DataType10, ID10.ToString());

                objCmd.Parameters.Add(Caption11, getSqlDbType(DataType11));
                objCmd.Parameters[Caption11].Value = GetConvertedParameter(DataType11, ID11.ToString());

                objCmd.Parameters.Add(Caption12, getSqlDbType(DataType12));
                objCmd.Parameters[Caption12].Value = GetConvertedParameter(DataType12, ID12.ToString());

                objCmd.Parameters.Add(Caption13, getSqlDbType(DataType13));
                objCmd.Parameters[Caption13].Value = GetConvertedParameter(DataType13, ID13.ToString());

                objCmd.Parameters.Add(Caption14, getSqlDbType(DataType14));
                objCmd.Parameters[Caption14].Value = GetConvertedParameter(DataType14, ID14.ToString());

                objCmd.Parameters.Add(Caption15, getSqlDbType(DataType15));
                objCmd.Parameters[Caption15].Value = GetConvertedParameter(DataType15, ID15.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        public DataTable FillHugeDataTable(string SP_Name, int TimeOut, string ID1, string ID2, string ID3)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "", Caption2 = "", Caption3 = "";
                string DataType1 = "", DataType2 = "", DataType3 = "";
                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 2)
                        {
                            Caption2 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType2 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 3)
                        {
                            Caption3 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType3 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }
                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                objCmd.Parameters.Add(Caption1, getSqlDbType(DataType1));
                objCmd.Parameters[Caption1].Value = GetConvertedParameter(DataType1, ID1.ToString());

                objCmd.Parameters.Add(Caption2, getSqlDbType(DataType2));
                objCmd.Parameters[Caption2].Value = GetConvertedParameter(DataType2, ID2.ToString());

                objCmd.Parameters.Add(Caption3, getSqlDbType(DataType3));
                objCmd.Parameters[Caption3].Value = GetConvertedParameter(DataType3, ID3.ToString());

                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS.Tables[0];
                //return FillHugeDataTable(SP_Name, TimeOut, ID1, ID2, ID3).DataSet.Tables[0];
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet FillHugeDataSet(string SP_Name, int TimeOut, object[] param)
        {
            try
            {
                DataSet dsSPParameter = new DataSet();
                string Caption1 = "";
                string DataType1 = "";
                dsSPParameter = FillDS("GetSP_Information", SP_Name.ToString().Trim());
                if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSPParameter.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[i]["ORDINAL_POSITION"].ToString()) == 1)
                        {
                            Caption1 = dsSPParameter.Tables[0].Rows[i]["PARAMETER_NAME"].ToString();
                            DataType1 = dsSPParameter.Tables[0].Rows[i]["TYPE_NAME"].ToString();
                        }
                    }
                }

                SqlConnection objConn = new SqlConnection(sqlConnectionString());
                objConn.Open();

                SqlCommand objCmd = new SqlCommand(SP_Name, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = TimeOut;

                int ParamCounter = 0;
                foreach (var paramitem in param)
                {
                    if (ParamCounter == 0) ParamCounter = 1;
                    if (dsSPParameter != null && dsSPParameter.Tables[0].Rows.Count > 0 && ParamCounter < dsSPParameter.Tables[0].Rows.Count)
                        if (Convert.ToDouble(dsSPParameter.Tables[0].Rows[ParamCounter]["ORDINAL_POSITION"].ToString()) == ParamCounter)
                        {
                            objCmd.Parameters.Add(dsSPParameter.Tables[0].Rows[ParamCounter]["PARAMETER_NAME"].ToString(), getSqlDbType(dsSPParameter.Tables[0].Rows[ParamCounter]["TYPE_NAME"].ToString()));
                            objCmd.Parameters[dsSPParameter.Tables[0].Rows[ParamCounter]["PARAMETER_NAME"].ToString()].Value = GetConvertedParameter(dsSPParameter.Tables[0].Rows[ParamCounter]["TYPE_NAME"].ToString(), paramitem.ToString());
                        }
                    ParamCounter++;
                }
                SqlDataAdapter objDA = new SqlDataAdapter(objCmd);
                DataSet objDS = new DataSet("pp");

                objDA.Fill(objDS);
                objConn.Close();

                return objDS;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string ReverseString(string s)
        {
            Char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
    }

    public static class RegistryData
    {
        public static string RegApplicationName = "";
        public static string RegSuperAdminPassword = "";
        public static string RegNoOfUser = "";
        public static string RegValidity = "";
        public static string RegNoOfBranches = "";
        public static string RegCompanyName = "";
        public static string RegCompanyShortName = "";
        public static string RegNoOfModule = "";
        public static string RegLicenceNo = "";
        public static string RegUseFlag = "";

        static RegistryData()
        {
            DataSet ds = new DataSet();
            DataLayer.DataServices ObjDataServicesNew = new DataServices();
            ds = ObjDataServicesNew.FillRegistryDATANew(Library.ServerPath);

            RegApplicationName = ds.Tables[0].Rows[0]["regApplicationName"].ToString();
            RegSuperAdminPassword = ds.Tables[0].Rows[0]["regSuperAdminPassword"].ToString();
            RegNoOfUser = ds.Tables[0].Rows[0]["regNoOfUser"].ToString();
            RegValidity = ds.Tables[0].Rows[0]["regValidity"].ToString();
            RegNoOfBranches = ds.Tables[0].Rows[0]["regNoOfBranches"].ToString();
            RegCompanyName = ds.Tables[0].Rows[0]["regCompanyName"].ToString();
            RegNoOfModule = ds.Tables[0].Rows[0]["regNoOfModule"].ToString();
            RegLicenceNo = ds.Tables[0].Rows[0]["regLicenceNo"].ToString();
            RegUseFlag = ds.Tables[0].Rows[0]["regUseFlag"].ToString();
            RegCompanyShortName = ds.Tables[0].Rows[0]["regCompanyShortName"].ToString();
        }

        public static void RegistryDataDetails()
        {
            DataSet ds = new DataSet();
            DataLayer.DataServices ObjDataServicesNew = new DataServices();
            ds = ObjDataServicesNew.FillRegistryDATANew(Library.ServerPath);

            RegApplicationName = ds.Tables[0].Rows[0]["regApplicationName"].ToString();
            RegSuperAdminPassword = ds.Tables[0].Rows[0]["regSuperAdminPassword"].ToString();
            RegNoOfUser = ds.Tables[0].Rows[0]["regNoOfUser"].ToString();
            RegValidity = ds.Tables[0].Rows[0]["regValidity"].ToString();
            RegNoOfBranches = ds.Tables[0].Rows[0]["regNoOfBranches"].ToString();
            RegCompanyName = ds.Tables[0].Rows[0]["regCompanyName"].ToString();
            RegNoOfModule = ds.Tables[0].Rows[0]["regNoOfModule"].ToString();
            RegLicenceNo = ds.Tables[0].Rows[0]["regLicenceNo"].ToString();
            RegUseFlag = ds.Tables[0].Rows[0]["regUseFlag"].ToString();
            RegCompanyShortName = ds.Tables[0].Rows[0]["regCompanyShortName"].ToString();
        }
    }
}
