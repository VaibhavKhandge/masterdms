﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BusinessLayer;
using System.Globalization;

namespace eBILL.UI.UserControls
{
    public partial class FilterCriteria : System.Web.UI.UserControl
    {
        public Library objLibrary = new Library();
        public DataSet ds = new DataSet();
        public DataSet dsSearch = new DataSet();
        public HomeCommotion ObjHomeCommotion = new HomeCommotion();
        public string Where = "";

        #region [ Delegate Event declaration ]
        public delegate void OnApplyButtonClick(object sender, EventArgs e);
        public delegate void OnCancelButtonClick(object sender, EventArgs e);
        #endregion

        #region [ Event declaration ]
        public event OnApplyButtonClick btnApplyHandler;
        public event OnCancelButtonClick btnCancelHandler;
        #endregion

        #region [ Button Events ]
        protected void btnApplyold_Click(object sender, EventArgs e)
        {
            if (btnApplyHandler != null)
            {
                Where = " ";
                int cancelFg = 0;
                int RevFlag = 0;
                for (int i = 0; i < GRDSearchDetails.Rows.Count; i++)
                {
                    Label LBLColumnField = (Label)GRDSearchDetails.Rows[i].FindControl("LBLColumnField");
                    TextBox TXTSearchValue = (TextBox)GRDSearchDetails.Rows[i].FindControl("TXTSearchValue");
                    DropDownList DDlEnd = (DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlEnd");
                    DropDownList DDlConditions = (DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions");
                    switch (DDlConditions.SelectedItem.ToString())
                    {
                        case "LIKE":
                            if (TXTSearchValue.Text.ToString().Trim() != "")
                                Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '%" + TXTSearchValue.Text.ToString().TrimEnd() + "%' ";
                            break;
                        case "NOT LIKE":
                            if (TXTSearchValue.Text.ToString().Trim() != "")
                                Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '%" + TXTSearchValue.Text.ToString().TrimEnd() + "%' ";
                            break;
                        case "=":
                            if (TXTSearchValue.Text.ToString().Trim() != "")
                            {
                                if (LBLColumnField.Text.ToString().Substring(0, 3) == "str")
                                    Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '" + TXTSearchValue.Text.ToString().TrimEnd() + "' ";
                                if (LBLColumnField.Text.ToString().Substring(0, 3) == "dbl" && TXTSearchValue.Text.ToString().Trim().Length > 0)
                                    Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '" + Convert.ToDecimal(TXTSearchValue.Text.ToString().Trim()).ToString("0.00", CultureInfo.InvariantCulture) + "' ";
                                else
                                    Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " " + TXTSearchValue.Text.ToString().TrimEnd() + " ";
                            }
                            break;
                        default:
                            if (LBLColumnField.Text.ToString().ToString().Substring(0, 3) != "str")
                            {
                                if (TXTSearchValue.Text.ToString().Trim() != "" && LBLColumnField.Text.ToString().ToString().Substring(0, 3) == "dbl")
                                    Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " " + Convert.ToDecimal(TXTSearchValue.Text.ToString().Trim()).ToString("0.00", CultureInfo.InvariantCulture) + " ";
                                else if (TXTSearchValue.Text.ToString().Trim() != "")
                                    Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " " + TXTSearchValue.Text.ToString().Trim() + " ";
                            }
                            break;
                    }

                    bool CondFlag = false; int s = i + 1;
                    if (TXTSearchValue.Text.ToString().Trim() != "")
                    {
                        for (int j = i + 1; j < GRDSearchDetails.Rows.Count; j++)
                        {
                            TextBox TXTSearchValueTest = (TextBox)GRDSearchDetails.Rows[j].FindControl("TXTSearchValue"); s++;
                            if (TXTSearchValueTest.Text.ToString().Trim() != "")
                            {
                                CondFlag = true;
                                break;
                            }
                        }
                    }
                    if (DDlEnd.SelectedValue.ToString().Contains("N/A"))
                    {
                        if (CondFlag == true)
                        {
                            Where = "";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert(' Please place the right condition in between " + ((Label)GRDSearchDetails.Rows[i].FindControl("LBLColumns")).Text.ToString() + " and " + ((Label)GRDSearchDetails.Rows[s - 1].FindControl("LBLColumns")).Text.ToString() + ".');", true);
                            ModalPopupExtenderMessage.Show();
                            this.Visible = true;
                            return;
                        }
                        //else
                        //    Where += "";
                    }
                    else
                    {
                        if (CondFlag == false)
                        {
                            Where = "";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert(' Please select " + "N/A" + " for " + ((Label)GRDSearchDetails.Rows[i].FindControl("LBLColumns")).Text.ToString() + ".');", true);
                            ModalPopupExtenderMessage.Show();
                            this.Visible = true;
                            return;
                        }
                        else
                            Where += " " + DDlEnd.SelectedItem.Text.ToString() + " ";
                    }

                    if (LBLColumnField.Text.ToString().Trim() == "Cancel")
                    {
                        if (TXTSearchValue.Text.ToString().Trim() == "")
                        {
                            cancelFg = 1;
                        }
                    }
                    if (LBLColumnField.Text.ToString().Trim() == "RevisionNo")
                    {
                        if (TXTSearchValue.Text.ToString().Trim() == "")
                        {
                            RevFlag = 1;
                        }
                    }
                }
                if (cancelFg == 1)
                    if (Where != "" && Where != " ")
                        Where += " and " + " Cancel='N' ";
                    else
                        Where = " Cancel='N' ";

                if (RevFlag == 1)
                    if (Where != "" && Where != " ")
                        Where += " and " + " RevisionNo=0 ";
                    else
                        Where = " RevisionNo=0 ";

                Page.Session["Where"] = Where;
                btnApplyHandler(this, e);
            }
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            if (btnApplyHandler != null)
            {
                Where = " ";
                int cancelFg = 0;
                int RevFlag = 0;
                for (int i = 0; i < GRDSearchDetails.Rows.Count; i++)
                {
                    Label LBLColumnField = (Label)GRDSearchDetails.Rows[i].FindControl("LBLColumnField");
                    TextBox TXTSearchValue = (TextBox)GRDSearchDetails.Rows[i].FindControl("TXTSearchValue");
                    DropDownList DDlEnd = (DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlEnd");
                    DropDownList DDlConditions = (DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions");
                    HiddenField HIDMultiplflag = (HiddenField)GRDSearchDetails.Rows[i].FindControl("HIDMultiplflag");
                    HiddenField HIDSelectionflag = (HiddenField)GRDSearchDetails.Rows[i].FindControl("HIDSelectionflag");
                    switch (DDlConditions.SelectedItem.ToString())
                    {
                        case "LIKE":
                            if (TXTSearchValue.Text.ToString().Trim() != "")
                            {
                                if (HIDMultiplflag.Value.ToString().Trim() == "Y")
                                {
                                    TXTSearchValue.TextMode = TextBoxMode.MultiLine;
                                    Where += " (" + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '%" + TXTSearchValue.Text.ToString().TrimEnd() + "%') ";
                                    Where = WhereSp(Where);
                                    Where = Where.Replace(",", ("%' Or " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '%").TrimStart()).Trim();
                                }
                                else
                                    Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '%" + TXTSearchValue.Text.ToString().TrimEnd() + "%' ";
                            }
                            break;
                        case "NOT LIKE":
                            if (TXTSearchValue.Text.ToString().Trim() != "")
                            {
                                if (HIDMultiplflag.Value.ToString().Trim() == "Y")
                                {
                                    TXTSearchValue.TextMode = TextBoxMode.MultiLine;
                                    Where += " (" + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '%" + TXTSearchValue.Text.ToString().TrimEnd() + "%') ";
                                    Where = WhereSp(Where);
                                    Where = Where.Replace(",", ("%' Or " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '%").TrimStart()).Trim();
                                }
                                else
                                    Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '%" + TXTSearchValue.Text.ToString().TrimEnd() + "%' ";
                            }
                            break;
                        case "=":
                            if (TXTSearchValue.Text.ToString().Trim() != "")
                            {
                                if (LBLColumnField.Text.ToString().Substring(0, 3) == "str")
                                {
                                    if (HIDMultiplflag.Value.ToString().Trim() == "Y")
                                    {
                                        TXTSearchValue.TextMode = TextBoxMode.MultiLine;
                                        Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '" + TXTSearchValue.Text.ToString().TrimEnd() + "' ";
                                        Where = WhereSp(Where);
                                        Where = Where.Replace(",", ("' Or " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '").TrimStart()).Trim();
                                    }
                                    else
                                        Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '" + TXTSearchValue.Text.ToString().TrimEnd() + "' ";
                                }
                                if (LBLColumnField.Text.ToString().Substring(0, 3) == "dbl" && TXTSearchValue.Text.ToString().Trim().Length > 0)
                                {
                                    if (HIDMultiplflag.Value.ToString().Trim() == "Y")
                                    {
                                        TXTSearchValue.TextMode = TextBoxMode.MultiLine;
                                        Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '" + Convert.ToDecimal(TXTSearchValue.Text.ToString().Trim()).ToString("0.00", CultureInfo.InvariantCulture) + "' ";
                                        Where = WhereSp(Where);
                                        Where = Where.Replace(",", ("' Or " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + "'").TrimStart()).Trim();
                                    }
                                    else
                                        Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " '" + Convert.ToDecimal(TXTSearchValue.Text.ToString().Trim()).ToString("0.00", CultureInfo.InvariantCulture) + "' ";
                                }
                                else
                                {
                                    if (HIDMultiplflag.Value.ToString().Trim() == "Y")
                                    {
                                        TXTSearchValue.TextMode = TextBoxMode.MultiLine;
                                        Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " " + TXTSearchValue.Text.ToString().TrimEnd() + " ";
                                        Where = WhereSp(Where);
                                        Where = Where.Replace(",", (" Or " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " ").TrimStart()).Trim();
                                    }
                                    else
                                        Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " " + TXTSearchValue.Text.ToString().TrimEnd() + " ";
                                }
                            }
                            break;
                        default:
                            if (LBLColumnField.Text.ToString().ToString().Substring(0, 3) != "str")
                            {
                                if (TXTSearchValue.Text.ToString().Trim() != "" && LBLColumnField.Text.ToString().ToString().Substring(0, 3) == "dbl")
                                {
                                    if (HIDMultiplflag.Value.ToString().Trim() == "Y")
                                    {
                                        Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " " + Convert.ToDecimal(TXTSearchValue.Text.ToString().Trim()).ToString("0.00", CultureInfo.InvariantCulture) + " ";
                                        Where = WhereSp(Where);
                                        Where = Where.Replace(",", (" Or " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " ").TrimStart()).Trim();
                                    }
                                    else
                                        Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " " + Convert.ToDecimal(TXTSearchValue.Text.ToString().Trim()).ToString("0.00", CultureInfo.InvariantCulture) + " ";
                                }
                                else if (TXTSearchValue.Text.ToString().Trim() != "")
                                {
                                    if (HIDMultiplflag.Value.ToString().Trim() == "Y")
                                    {
                                        Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " " + TXTSearchValue.Text.ToString().Trim() + " ";
                                        Where = WhereSp(Where);
                                        Where = Where.Replace(",", (" Or " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " ").TrimStart()).Trim();
                                    }

                                    else
                                        Where += " " + LBLColumnField.Text.ToString() + " " + DDlConditions.SelectedItem.ToString() + " " + TXTSearchValue.Text.ToString().Trim() + " ";

                                }
                            }
                            break;
                    }

                    bool CondFlag = false; int s = i + 1;
                    if (TXTSearchValue.Text.ToString().Trim() != "")
                    {
                        for (int j = i + 1; j < GRDSearchDetails.Rows.Count; j++)
                        {
                            TextBox TXTSearchValueTest = (TextBox)GRDSearchDetails.Rows[j].FindControl("TXTSearchValue"); s++;
                            if (TXTSearchValueTest.Text.ToString().Trim() != "")
                            {
                                CondFlag = true;
                                break;
                            }
                        }
                    }
                    if (DDlEnd.SelectedValue.ToString().Contains("N/A"))
                    {
                        if (CondFlag == true)
                        {
                            Where = "";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert(' Please place the right condition in between " + ((Label)GRDSearchDetails.Rows[i].FindControl("LBLColumns")).Text.ToString() + " and " + ((Label)GRDSearchDetails.Rows[s - 1].FindControl("LBLColumns")).Text.ToString() + ".');", true);
                            ModalPopupExtenderMessage.Show();
                            this.Visible = true;
                            return;
                        }
                        //else
                        //    Where += "";
                    }
                    else
                    {
                        if (CondFlag == false)
                        {
                            Where = "";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert(' Please select " + "N/A" + " for " + ((Label)GRDSearchDetails.Rows[i].FindControl("LBLColumns")).Text.ToString() + ".');", true);
                            ModalPopupExtenderMessage.Show();
                            this.Visible = true;
                            return;
                        }
                        else
                            Where += " " + DDlEnd.SelectedItem.Text.ToString() + " ";
                    }

                    if (LBLColumnField.Text.ToString().Trim() == "Cancel")
                    {
                        if (TXTSearchValue.Text.ToString().Trim() == "")
                        {
                            cancelFg = 1;
                        }
                    }
                    if (LBLColumnField.Text.ToString().Trim() == "RevisionNo")
                    {
                        if (TXTSearchValue.Text.ToString().Trim() == "")
                        {
                            RevFlag = 1;
                        }
                    }

                }
                if (cancelFg == 1)
                    if (Where != "" && Where != " ")
                        Where += " and " + " Cancel='N' ";
                    else
                        Where = " Cancel='N' ";

                if (RevFlag == 1)
                    if (Where != "" && Where != " ")
                        Where += " and " + " RevisionNo=0 ";
                    else
                        Where = " RevisionNo=0 ";

                Page.Session["Where"] = Where;
                btnApplyHandler(this, e);
            }
        }

        protected string WhereSp(string Where)
        {
        A3:
            if (Where.Contains(" ,")) Where = Where.Replace(" ,", ",");
            if (Where.Contains(", ")) Where = Where.Replace(", ", ",");
            if (Where.Contains(" ,") || Where.Contains(", "))
                goto A3;
            return Where;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //if (btnCancelHandler != null)
            //    btnCancelHandler(this, e);
        }
        #endregion

        #region Load
        protected void Page_Load(object sender, EventArgs e)
        {
            btnApply.Click += new EventHandler(btnApply_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
            btnApplyDummy.Click += new EventHandler(btnApply_Click);
            btnCancelDummy.Click += new EventHandler(btnCancel_Click);
        }
        #endregion

        #region Show control
        public void Show(string formname, int height, int width)
        {
            ds = new DataSet();
            ds = objLibrary.FillDS("ssp_GetBindFilterCriteriaDataset", formname);
            GRDSearchDetails.DataSource = ds;
            GRDSearchDetails.DataBind();
            for (int i = 0; i < GRDSearchDetails.Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["strMultiplflag"].ToString() == "Y")
                {
                    ((TextBox)GRDSearchDetails.Rows[i].FindControl("TXTSearchValue")).TextMode = TextBoxMode.MultiLine;
                }
                if (ds.Tables[0].Rows[i]["nvDataType"].ToString() == "A")
                {
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("LIKE");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[0].Value = "Like";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("NOT LIKE");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[1].Value = "NOT LIKE";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).DataBind();
                }
                else
                {
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("=");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[0].Value = "E";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("<");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[1].Value = "L";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add(">");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[2].Value = "G";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add(">=");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[3].Value = "GE";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("<=");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[4].Value = "LE";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).DataBind();
                }
            }
            MessageBox.Height = height;
            MessageBox.Width = width;
            ModalPopupExtenderMessage.Show();
            this.Visible = true;
        }

        public void ShowNew(string formname, int height, int width)
        {
            ds = new DataSet();
            ds = objLibrary.FillDS("ssp_GetBindFilterCriteriaDatasetNew", formname);
            GRDSearchDetails.DataSource = ds;
            GRDSearchDetails.DataBind();
            for (int i = 0; i < GRDSearchDetails.Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["nvDataType"].ToString() == "A")
                {
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("LIKE");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[0].Value = "Like";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("NOT LIKE");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[1].Value = "NOT LIKE";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).DataBind();
                }
                else
                {
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("=");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[0].Value = "E";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("<");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[1].Value = "L";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add(">");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[2].Value = "G";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add(">=");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[3].Value = "GE";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("<=");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[4].Value = "LE";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).DataBind();
                }
            }
            MessageBox.Height = height;
            MessageBox.Width = width;
            ModalPopupExtenderMessage.Show();
            this.Visible = true;
        }
        #endregion

        #region ShowD control
        public void ShowD(string formname, int height, int width)
        {
            ds = new DataSet();
            ds = objLibrary.FillDS("ssp_GetBindFilterCriteriaDatasetForDeleteTransaction", formname);
            GRDSearchDetails.DataSource = ds;
            GRDSearchDetails.DataBind();
            for (int i = 0; i < GRDSearchDetails.Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["nvDataType"].ToString() == "A")
                {
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("LIKE");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[0].Value = "Like";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("NOT LIKE");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[1].Value = "NOT LIKE";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).DataBind();
                }
                else
                {
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("=");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[0].Value = "E";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("<");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[1].Value = "L";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add(">");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[2].Value = "G";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add(">=");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[3].Value = "GE";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items.Add("<=");
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).Items[4].Value = "LE";
                    ((DropDownList)GRDSearchDetails.Rows[i].FindControl("DDlConditions")).DataBind();
                }
            }
            MessageBox.Height = height;
            MessageBox.Width = width;
            ModalPopupExtenderMessage.Show();
            this.Visible = true;
        }
        #endregion
    }
}