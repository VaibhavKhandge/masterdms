﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace eBILL.UI.UserControls
{
    public partial class AfterSaveMessageBox : System.Web.UI.UserControl
    {
        #region [ Delegate Event declaration ]
        public delegate void OnOKButtonClick(object sender, EventArgs e);
        public delegate void OnCancelButtonClick(object sender, EventArgs e);
        public delegate void OnCHKPrintChecked(object sender, EventArgs e);
        public delegate void OnCHKEmailChecked(object sender, EventArgs e);
        public delegate void OnCHKSMSChecked(object sender, EventArgs e);
        #endregion

        #region [ Event declaration ]
        public event OnOKButtonClick btnOKHandler;
        public event OnCancelButtonClick btnCancelHandler;
        public event OnCHKPrintChecked CHKPrintHandler;
        public event OnCHKEmailChecked CHKEmailHandler;
        public event OnCHKSMSChecked CHKSMSHandler;
        #endregion

        #region [ Button Events ]
        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (btnOKHandler != null)
            {
                btnOKHandler(this, e);
                if (CHKPrint.Checked)
                    CHKPrintHandler(this, e);
                if (CHKEmail.Checked)
                    CHKEmailHandler(this, e);
                if (CHKSms.Checked)
                    CHKSMSHandler(this, e);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (btnCancelHandler != null)
                btnCancelHandler(this, e);
        }

        protected void CHKPrint_CheckedChanged(object sender, EventArgs e)
        {
            if (CHKPrintHandler != null)
                CHKPrintHandler(this, e);
        }

        protected void CHKEmail_CheckedChanged(object sender, EventArgs e)
        {
            if (CHKEmailHandler != null)
                CHKEmailHandler(this, e);
        }

        protected void CHKSMS_CheckedChanged(object sender, EventArgs e)
        {
            if (CHKSMSHandler != null)
                CHKSMSHandler(this, e);
        }
        #endregion

        #region Load
        protected void Page_Load(object sender, EventArgs e)
        {
            btnOK.Click += new EventHandler(btnOK_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
        }
        #endregion

        #region Show control
        public void Show(MessageType messageType, string message, int height, int width, int Print, int Email, int SMS)
        {
            litMessage.Text = message;
            MessageBox.Height = height;
            MessageBox.Width = width;

            CHKPrint.Visible = Print == 0 ? false : true;
            CHKEmail.Visible = Email == 0 ? false : true;
            CHKSms.Visible = SMS == 0 ? false : true;

            MessageBox.CssClass = messageType.ToString().ToLower();
            ModalPopupExtenderMessage.Show();
            this.Visible = true;
        }
        #endregion

        #region Enum
        public enum MessageType
        {
            Error = 1,
            Info = 2,
            Success = 3,
            Warning = 4
        }
        #endregion
    }
}