﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data;

namespace BusinessLayer
{
   public class HomeConstant
    {
        # region [dsHeader Property]

        public DataSet dsHeader = null;

        # endregion

        # region [IDField Property]
        public string ID_Field
        {
            get
            {
                return "strUserID";
            }
        }
        # endregion

        # region [SP_Authontication Property]
        public string SP_Authontication
        {
            get
            {
                return "SSP_GetValidUser";
            }

        }
        # endregion

        # region [Sp_SaveUserLoginDetailsInformation Property]
        public string Sp_SaveUserLoginDetailsInformation
        {
            get
            {
                return "Sp_SaveUserLoginDetailsInformation";
            }

        }
        # endregion

        # region [SP_GetRoleId Property]
        public string SP_GetRoleId
        {
            get
            {
                return "SSP_GetRoleId";
            }

        }
        # endregion

        # region [SP_VerifyPassword Property]
        public string SP_VerifyPassword
        {
            get
            {
                return "SSP_GetVerifyPassword";
            }

        }
        # endregion

        # region [SP_SaveChangePassword Property]
        public string SP_SaveChangePassword
        {
            get
            {
                return "ausp_SaveChangePassword";
            }

        }
        # endregion

        # region [XMLFile_Name Property]
        public string XMLFile_Name
        {
            get
            {
                return "ProfitCenter_XML";
            }

        }
        # endregion

        # region [FYXMLFile_Name Property]
        public string FinancialYearXMLFile_Name
        {
            get
            {
                return "FinancialYear_XML";
            }

        }
        # endregion

        # region [SP_BindProfitCenter]
        public string SP_BindProfitCenter
        {
            get
            {
                return "ssp_getBranch";
            }

        }
        # endregion

        # region [SP_BindFinancialYear]
        public string SP_BindFinancialYear
        {
            get
            {
                return "ssp_getFinancialYear";
            }

        }
        # endregion

        # region [SP_CompanyAuthontication Property]
        public string SP_CompanyAuthontication
        {
            get
            {
                return "SSP_GetValidOrganization";
            }

        }
        # endregion

        # region [SP_CompanyHelp]
        public string SP_CompanyHelp
        {
            get
            {
                return "ssp_getOrganizationHelp";
            }

        }
        # endregion
    }
}
