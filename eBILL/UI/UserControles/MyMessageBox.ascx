﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyMessageBox.ascx.cs"
    Inherits="eBILL.UI.UserControls.MyMessageBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<style type="text/css">
    .style1
    {
        width: 174px;
    }
    .style2
    {
        width: 727px;
    }
    .style3
    {
        width: 526px;
    }
    .style4
    {
        width: 743px;
    }
    .updateProgressMessage
    {
        margin: 3px;
        font-family: Trebuchet MS;
        font-size: small;
        vertical-align: middle;
    }
    .modalBackground
    {
        background-color: Gray;
        filter: alpha(opacity=60);
        opacity: 0.60;
    }
    .updateProgress
    {
        border-width: 1px;
        border-style: solid;
        background-color: #FFFFFF;
        position: absolute;
        width: 150px;
        height: 50px;
    }
</style>
<asp:LinkButton ID="LinkButtonTargetControl" runat="server"></asp:LinkButton>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtenderMessage" runat="server" TargetControlID="LinkButtonTargetControl"
    CancelControlID="CloseButton" PopupControlID="MessageBox" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="MessageBox" CssClass="updateProgress" runat="server" Width="100%"
    Height="100%" Style="display: none;" BorderStyle="Solid" BorderWidth="2" BorderColor="#410000"
    BackColor="#DCDCD1">
    <%--BackImageUrl="~\Images\Tanspecks.jpg" #DCDCD1--%>
    <table cellspacing="0" width="100%" style="height: 100%;">
        <tr>
            <td colspan="3" width="100%" bgcolor="#0000A0" align="center">
                <asp:Label ID="LabelPopupHeader" runat="server" Text="Message" Font-Bold="false"
                    ForeColor="White" Font-Names="verdana" Font-Size="8pt" />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: auto; border-top-style: solid; border-top-width: 1px;"
                width="100%">
                <div>
                    <p id="PID" runat="server" style="font-family: verdana; font-size: 8pt; text-align:center">
                        <%--<asp:Literal ID="litMessage" runat="server"></asp:Literal>--%>
                        <%--<asp:Label ID="PID" runat="server" Text="" Font-Bold="false" ForeColor="White"
                            Font-Names="verdana" Font-Size="9pt" />--%>
                    </p>
                </div>
            </td>
        </tr>
        <tr>
            <%--<td width="25%">
                <asp:Button ID="btnd" runat="server" Height="0px" Text="" Width="0px" />
            </td>--%>
            <td width="33.33%">
                <asp:Button ID="btnYes" runat="server" Style="width: 99%; color: #0000A0; font-family: Segoe UI;
                    font-size: 8pt" Text="Yes" />
            </td>
            <td width="33.33%">
                <asp:Button ID="btnNo" runat="server" Style="width: 99%; color: #0000A0; font-family: Segoe UI;
                    font-size: 8pt" Text="No" />
            </td>
            <td width="33.33%">
                <asp:Button ID="btnCancel" runat="server" Style="width: 99%; color: #0000A0; font-family: Segoe UI;
                    font-size: 8pt" Text="Close" />
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="3">
                <asp:HyperLink ID="CloseButton" runat="server">
                    <asp:Image ID="Image1" runat="server" Visible="false" AlternateText="Click here to close this message"
                        ImageUrl="~/Images/Close1.gif" />
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Panel>
