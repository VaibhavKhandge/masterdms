﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DataLayer
{
    public class DataServices
    {
        public DataLayer.DataLinker objDataLinker = new DataLayer.DataLinker();

        # region [BindGrid()]
        public DataTable BindGrid(string SP_Name, string Company_ID)
        {
            try
            {
                DataSet lds = new DataSet();
                lds = objDataLinker.FillDS(SP_Name, Company_ID);
                return lds.Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        # region [FillDT()]
        public DataTable FillDT(string SP_Name)
        {
            try
            {
                DataSet lds = new DataSet();
                lds = objDataLinker.GetDs(SP_Name);
                return lds.Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
        }
        # endregion

        #region [ SaveDetail() ]
        public int SaveDetail(ref DataSet ds)
        {
            try
            {
                return objDataLinker.SaveDetail(ref  ds);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
       
        # region [GetDataSet()]
        public DataTable GetDataSet(string SP_Name, object[] param)
        {
            try
            {
                DataSet lds = new DataSet();
                lds = objDataLinker.GetDs(SP_Name, param);
                return lds.Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void GetDataSet1(string SP_Name, object[] param)
        {
            try
            {
                objDataLinker.GetDs1(SP_Name, param);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        # region [GetDataSet()]
        public DataSet FillDataSet(string SP_Name, object[] param)
        {
            try
            {
                return objDataLinker.GetDs(SP_Name, param);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [FillDataSet]
        public DataSet FillDataSet(string SP_Name, string formaname, string shortname)
        {
            try
            {
                return objDataLinker.GetDs(SP_Name, formaname, shortname);

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [GetDataSet]
        public DataSet GetDataSet(string SP_Name)
        {
            try
            {
                return (objDataLinker.GetDs(SP_Name));
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [FillDS()]
        public DataSet FillDS(string SP_Name, string SearchField)
        {
            try
            {
                return objDataLinker.FillDS(SP_Name, SearchField);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        # region [FillDS()]
        public DataSet FillDS(string SP_Name)
        {
            try
            {
                return objDataLinker.GetDs(SP_Name);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        # region [GetCode()]
        public string[] GetCode(string Form_Name, string Form_Type, string Getcode_SP_Name, string Branch_Code, int FY_ID)
        {
            try
            {
                return objDataLinker.GetCode(Form_Name, Form_Type, Getcode_SP_Name, Branch_Code, FY_ID);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [DataSet Table Matching()]
        public DataSet DSTableMatching(string TblName)
        {
            try
            {
                return objDataLinker.DSTableMatching(TblName);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region [SaveData()]
        public string[] SaveData(DataSet ds, string SP_Name)
        {
            try
            {
                return objDataLinker.SaveData(ds, SP_Name);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [Update()]

        public int SavePlanning(DataSet dsHeader, int Id, string ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return objDataLinker.SavePlanning(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Save(DataSet dsHeader, int Id, string ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return objDataLinker.Save(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Save(DataSet dsHeader, string Id, string ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return objDataLinker.Save(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Save(DataSet dsHeader, int Id, string ID_Field, string SP_DeleteDetails, bool blRevision)
        {
            try
            {
                return objDataLinker.Save(dsHeader, Id, ID_Field, SP_DeleteDetails, blRevision);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [Save()]
        public string[] Save(DataSet dsHeader, string ID_Field)
        {
            try
            {
                return objDataLinker.Save(dsHeader, ID_Field);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] SaveAssetData(DataSet dsHeader, string ID, bool AddMode, string SP_DeleteDetails)
        {
            try
            {
                return objDataLinker.SaveAssetData(dsHeader, ID, AddMode, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [SaveEnclosure()]
        public int SaveEnclosure(DataSet dsHeader, int[] Id, string[] ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return objDataLinker.SaveEnclosure(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int SaveEnclosureCA(DataSet dsHeader, int[] Id, string[] ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return objDataLinker.SaveEnclosureCA(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }
    
        public int SaveEnclosure(DataSet dsHeader, string[] Id, string[] ID_Field, string SP_DeleteDetails)
        {
            try
            {
                return objDataLinker.SaveEnclosure(dsHeader, Id, ID_Field, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region SaveForSelectedBranch
        public int SaveForBranch(DataSet dsHeader)
        {
            try
            {
                return objDataLinker.SaveForBranch(dsHeader);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int SaveForBranchFinal(DataSet dsHeader)
        {
            try
            {
                return objDataLinker.SaveForBranchFinal(dsHeader);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int SaveDepreciation(DataSet dsHeader, bool AddMode)
        {
            try
            {
                return objDataLinker.SaveDepreciation(dsHeader, AddMode);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [chkDuplication()]
        public DataSet chkDuplication(string TableName, string Condition, string SP_Name)
        {
            try
            {
                return objDataLinker.chkDuplication(TableName, Condition, SP_Name);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [Delete()]
        public int Delete(string ID_Field, int Id, string SP_DeleteDetails)
        {
            try
            {
                return objDataLinker.Delete(ID_Field, Id, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
                //return 0;
            }
        }

        public int Delete(string ID_Field, string Id, string SP_DeleteDetails)
        {
            try
            {
                return objDataLinker.Delete(ID_Field, Id, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
                //return 0;
            }
        }

        public int Delete(string[] ID_Field, int[] Id, string SP_DeleteDetails)
        {
            try
            {
                return objDataLinker.Delete(ID_Field, Id, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
                //return 0;
            }
        }

        public int DeleteCA(string[] ID_Field, int[] Id, string SP_DeleteDetails)
        {
            try
            {
                return objDataLinker.DeleteCA(ID_Field, Id, SP_DeleteDetails);
            }
            catch (Exception)
            {
                throw;
                //return 0;
            }
        }

        public int Delete(string ID_Field, int Id, string SP_DeleteDetails, bool blRevision)
        {
            try
            {
                return objDataLinker.Delete(ID_Field, Id, SP_DeleteDetails, blRevision);
            }
            catch (Exception)
            {
                throw;
                //return 0;
            }
        }
        #endregion

        #region [ErrorLog()]
        public string ErrorLog(string Error_Message, string Form_Name, int User_Id, int Branch_Id, string Company_ID, string SP_ErrorLog)
        {
            try
            {
                object[] param = new object[5];
                param[0] = Error_Message;
                param[1] = Form_Name;
                param[2] = User_Id;
                param[3] = Branch_Id;
                param[4] = Company_ID;
                DataSet ds = objDataLinker.GetDs(SP_ErrorLog, param);
                return ds != null ? ds.Tables[0].Rows[0][0].ToString() : "";
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [FincialAccountReport()]
        public DataSet FincialAccountReport(string SP_ReportName, int intOrganizationId, int intBranchId, int intFyId, string strDate)
        {
            try
            {
                object[] param = new object[4];
                param[0] = intOrganizationId;
                param[1] = intBranchId;
                param[2] = intFyId;
                param[3] = strDate;
                DataSet ds = objDataLinker.GetDs(SP_ReportName, param);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [FincialAccountReport()]
        public DataSet FincialAccountReport(string SP_ReportName, int intOrganizationId, int intBranchId, int intFyId, string strDate, string strPrintFlag)
        {
            try
            {
                object[] param = new object[5];
                param[0] = intOrganizationId;
                param[1] = intBranchId;
                param[2] = intFyId;
                param[3] = strDate;
                param[4] = strPrintFlag;
                DataSet ds = objDataLinker.GetDs(SP_ReportName, param);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet FincialAccountReport(string SP_ReportName, object[] param)
        {
            try
            {
                DataSet ds = objDataLinker.GetDs(SP_ReportName, param);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        # region [SaveQuery()]
        public int SaveQuery(string SP_Name, object[] param)
        {
            try
            {
                int count = 0;
                count = objDataLinker.SaveQuery(SP_Name, param);
                return count;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [SAVE Reports]
        public int SaveData(string Query)
        {
            try
            {
                return objDataLinker.SaveData(Query);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [SAVE Reports DS]
        public int SaveDataReport(DataSet ds, String SPName)
        {
            try
            {
                return objDataLinker.SaveDataReport(ds, SPName);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [BuildReport()]
        public void BuildReport(string WhereClause, string ViewName, string SP_Name)
        {
            try
            {
                objDataLinker.BuildReport(WhereClause, ViewName, SP_Name);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BuildReport(int FId, int BranchId, string WhereClause, string ViewName, string SP_Name, string FormDate, string ToDate)
        {
            try
            {
                objDataLinker.BuildReport(FId, BranchId, WhereClause, ViewName, SP_Name, FormDate, ToDate);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void BuildReport(int FId, int BranchId, string ViewName, string SP_Name)
        {
            try
            {
                objDataLinker.BuildReport(FId, BranchId, ViewName, SP_Name);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region [Posting()]
        public void Posting(string SP_Name, string strTblName, string strDate, string strEmpId, string strClause)
        {
            try
            {
                objDataLinker.Posting(SP_Name, strTblName, strDate, strEmpId, strClause);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [ChkUseFlag()]
        public Boolean ChkUseFlag(string SP_Name, string strTblName, string strFldName, Int32 intID)
        {
            try
            {
                return objDataLinker.ChkUseFlag(SP_Name, strTblName, strFldName, intID);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region [ChkUseFlagBranchwise()]
        public Boolean ChkUseFlagBranchwise(string SP_Name, string strTblName, string strFldName, Int32 intID, Int32 intBranchID)
        {
            try
            {
                return objDataLinker.ChkUseFlagBranchwise("ssp_ChkUseFlagForBranch", strTblName, strFldName, intID, intBranchID);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetScript
        public string GetScript(string SP_Script)
        {
            return objDataLinker.GetScript(SP_Script);
        }
        #endregion

        #region GetFilterchar
        public string GetFilterchar(string SP_Name, string TableName, string Field, string FromChar, string ToChar)
        {
            return objDataLinker.GetFilterchar(SP_Name, TableName, Field, FromChar, ToChar);
        }
        #endregion

        #region GetFromMailId
        public string GetFromMailId(string SP_Name, string strCompanyID, string BranchID, string UserID)
        {
            return objDataLinker.GetFromMailId(SP_Name, strCompanyID, BranchID, UserID);
        }
        #endregion

        #region GetServerParameter
        public string[] GetServerParameter()
        {
            return objDataLinker.GetServerParameter();
        }
        #endregion

        #region [SAVE BULKY DETAIL DATA]
        /// <summary>
        /// Created by To save bulky data to avoid request time out exception
        /// 
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="strTableName"></param>
        public void SaveBulkyDetailData(DataSet ds, String strTableName)
        {
            objDataLinker.SaveBulkyDetailData(ds, strTableName);
        }
        #endregion
                          
        #region GetDataBaseName
        public DataSet GetDataBaseName()
        {
            DataSet ds;
            try
            {
                ds = objDataLinker.GetDBName();
            }
            catch (Exception)
            {
                throw;
            }
            return ds;
        }
        #endregion

        #region SetConnecString
        public void SetConnecString(string KeyName)
        {
            try
            {
                objDataLinker.SetConnectionString(KeyName);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        # region [FillRegistryDATA()]
        public DataSet FillRegistryDATA(string KeyName)
        {
            try
            {
                return objDataLinker.FillRegistryDATA(KeyName);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        # region [FillRegistryDATANew()]
        public DataSet FillRegistryDATANew(string KeyName)
        {
            try
            {
                return objDataLinker.FillRegistryDATA(KeyName);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        /// For the registry creation
       # region [registry creation]
        public string DataServicesRegApplicationName()
        {
            try
            {
                return objDataLinker.DataLinkerRegApplicationName();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DataServicesRegSuperAdminPassword()
        {
            try
            {
                return objDataLinker.DataLinkerRegSuperAdminPassword();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DataServicesRegNoOfUser()
        {
            try
            {
                return objDataLinker.DataLinkerRegNoOfUser();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DataServicesRegValidity()
        {
            try
            {
                return objDataLinker.DataLinkerRegValidity();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DataServicesRegNoOfBranches()
        {
            try
            {
                return objDataLinker.DataLinkerRegNoOfBranches();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DataServicesRegCompanyName()
        {
            try
            {
                return objDataLinker.DataLinkerRegCompanyName();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DataServicesRegCompanyShortName()
        {
            try
            {
                return objDataLinker.DataLinkerRegCompanyShortName();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DataServicesRegNoOfModule()
        {
            try
            {
                return objDataLinker.DataLinkerRegNoOfModule();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DataServicesRegLicenceNo()
        {
            try
            {
                return objDataLinker.DataLinkerRegLicenceNo();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DataServicesRegUseFlag()
        {
            try
            {
                return objDataLinker.DataLinkerRegUseFlag();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DataServicessqlConnectionString()
        {
            try
            {
                return objDataLinker.DataLinkersqlConnectionString();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
