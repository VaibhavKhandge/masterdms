﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FilterCriteria.ascx.cs"
    Inherits="eBILL.UI.UserControls.FilterCriteria" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<style type="text/css">
    .style1 {
        width: 174px;
    }

    .style2 {
        width: 727px;
    }

    .style3 {
        width: 526px;
    }

    .style4 {
        width: 743px;
    }

    .updateProgressMessage {
        margin: 3px;
        font-family: Trebuchet MS;
        font-size: small;
        vertical-align: middle;
    }

    .modalBackground {
        background-color: Gray;
        filter: alpha(opacity=60);
        opacity: 0.60;
    }

    .updateProgress {
        border-width: 1px;
        border-style: solid;
        background-color: #FFFFFF;
        position: absolute;
        width: 150px;
        height: 50px;
    }
</style>

<script type="text/javascript" language="javascript">
    function ViewExistFY(form, control0, control1, controlName, Fld) {
        document.getElementById(control0).value = '0';
        window.showModalDialog('frmSelection.aspx?form=' + form + '&control0=' + control0 + '&control1=' + control1 + '&control=' + controlName + '&Fld=' + Fld + '&StrVal=' + document.getElementById(control1).value + '', window, 'dialogWidth=500px;dialogHeight=500px');
    }
</script>

<asp:LinkButton ID="LinkButtonTargetControl" runat="server"></asp:LinkButton>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtenderMessage" runat="server" TargetControlID="LinkButtonTargetControl"
    PopupControlID="MessageBox" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="MessageBox" CssClass="updateProgress" runat="server" Width="100%"
    Style="display: none;" BorderStyle="Solid" BorderWidth="5" BorderColor="#410000"
    BackImageUrl="~\Images\Tanspecks.jpg">
    <div id="DSearch" runat="server" style="border-style: solid; border-width: 1px; background-color: aliceblue; vertical-align: top; overflow: auto; width: 100%; height: 100%; text-align: left; table-layout: fixed;">
        <table width="100%" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="Label9" runat="server" BackColor="#0000A0" Font-Bold="True" Font-Names="verdana"
                        Font-Size="8pt" ForeColor="White" Style="font-family: verdana; text-align: center"
                        Text="Search" Width="100%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnApplyDummy" runat="server" Text="Apply" Font-Names="verdana" BorderStyle="Ridge" />

                    <asp:Button ID="btnCancelDummy" runat="server" Text="Cancel" Font-Names="verdana"
                        BorderStyle="Ridge" />
                </td>
            </tr>
            <tr>
                <td>
                    <div id="Div1" runat="server" style="border-style: solid; border-width: 1px; background-color: aliceblue; vertical-align: top; overflow: auto; width: 100%; height: 99%; text-align: left; table-layout: fixed;">
                        <asp:GridView ID="GRDSearchDetails" runat="server" EmptyDataText="There is no record to display"
                            CellPadding="4" ForeColor="#333333" GridLines="Both" HeaderStyle-Wrap="false"
                            AutoGenerateColumns="False" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                            ShowFooter="false" Width="100%" Font-Names="verdana" Font-Size="8pt">
                            <HeaderStyle BorderWidth="1px" BackColor="#0066CC" Font-Bold="True" ForeColor="White"
                                Wrap="false" />
                            <PagerStyle BorderWidth="1px" BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <AlternatingRowStyle BorderWidth="1px" BackColor="White" Wrap="false" Font-Names="verdana"
                                Font-Size="8pt" />
                            <SelectedRowStyle BorderWidth="1px" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"
                                Wrap="false" Font-Names="verdana" Font-Size="8pt" />
                            <RowStyle BorderWidth="1px" BackColor="#EFF3FB" Wrap="false" Font-Names="verdana"
                                Font-Size="8pt" />
                            <Columns>
                                <asp:TemplateField HeaderText="Column" HeaderStyle-CssClass="styleGridLines" ItemStyle-CssClass="styleGridLines">
                                    <ItemTemplate>
                                        <asp:Label ID="LBLColumns" runat="server" Text='<%# bind("nvDisplayName") %>'></asp:Label>
                                        <asp:Label ID="LBLColumnField" runat="server" Text='<%# bind("nvColumnName") %>'
                                            Visible="false"></asp:Label>
                                        <asp:HiddenField ID="HIDMultiplflag" runat="server" Value='<%# bind("strMultiplflag") %>' />
                                        <asp:HiddenField ID="HIDSelectionflag" runat="server" Value='<%# bind("strSelectionflag") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Condition" HeaderStyle-CssClass="styleGridLines" ItemStyle-CssClass="styleGridLines">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="DDlConditions" runat="server" SkinID="DDLSkin" BorderStyle="Solid"
                                            BorderWidth="1px" BorderColor="Gray" Width="70px">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Search Value" HeaderStyle-CssClass="styleGridLines"
                                    ItemStyle-CssClass="styleGridLines">
                                    <ItemTemplate>
                                        <asp:TextBox ID="TXTSearchValue" runat="server" SkinID="TXTSkin"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="styleGridLines" ItemStyle-CssClass="styleGridLines">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="DDlEnd" runat="server" SkinID="DDLSkin" BorderStyle="Solid"
                                            BorderWidth="1px" BorderColor="Gray">
                                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                                            <asp:ListItem Value="OR">OR</asp:ListItem>
                                            <asp:ListItem Value="AND">AND</asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnApply" runat="server" Text="Apply" Font-Names="verdana" BorderStyle="Ridge" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Font-Names="verdana" BorderStyle="Ridge" />
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
