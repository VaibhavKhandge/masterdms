﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmSalesExecutiveMasterView.aspx.cs" Inherits="eBILL.UI.frmSalesExecutiveMasterView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sales Executive Master View</title>
    <!-- bootstrap 3.0.2 -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="CSS/AdminLTE.css" rel="stylesheet" />
    <link href="CSS/colorbox.css" rel="stylesheet" />
    <!-- Theme style -->
    <script src="JS/jquery.min.js" type="text/javascript"></script>
    <script src="../Script/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="JS/jquery.colorbox.js" type="text/javascript"></script>
    <script src="../Script/KeyPress.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
        });

        function pageLoad(sender, args) {
            if (args.get_isPartialLoad()) {
                $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
            }
        }
        function loadPopup() {
            $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
        }

        function LoadPage() {
            $.colorbox.close();
            document.getElementById('LinkButton1').click();
        }

        function ConfirmEDelete() {
            if (confirm('Are you sure want to Delete this record?') == true) {
                document.getElementById("ConfirmDelete").value = "true";
                return true;
            }
            else {
                document.getElementById("ConfirmDelete").value = "false";
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server"
                    DynamicLayout="true">
                    <ProgressTemplate>
                        <div id="updPleaseWait" style="text-align: right; color: White; vertical-align: top; position: fixed; top: 300px; left: 480px">
                            <table style="background-color: #7cc3ef; width: 100%">
                                <tr>
                                    <td colspan="2" style="text-align: center; width: 100%">
                                        <b><font color="white">Please Wait...</font></b>
                                        <br />
                                        <img src="../vendor/lightbox2/src/images/loading.gif" alt="Please wait...." title="Please wait...." />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div class="col-md-12" style="background: linear-gradient(45deg, #b74aef 0%, #da4726 100%);">
                    <div class="col-md-5">
                        <ol class="breadcrumb">
                            <li>
                                <a href="frmSalesExecutiveMaster.aspx" class="iframe">
                                    <asp:Button Text="Add New" ID="btnAdd" runat="server" CssClass="btn btn-primary btn-sm" />
                                </a>
                                <asp:Button ID="btnPrint" runat="server" Text="Export" CssClass="btn btn-primary btn-sm" OnCommand="Page_Command" CommandName="btnPrint" />
                            </li>
                        </ol>
                    </div>
                    <div class="col-md-3" style="padding-top: 0px">
                        <span style="color: #fff">
                            <h4>Sales Executive Master View</h4>
                        </span>
                    </div>
                    <div class="col-md-3" style="padding-top: 7px; margin-left: 100px;"><span style="color: #fff">Date/ Time :</span><span style="color: #fff; font-size: 12px;"> <%=System.DateTime.Now.ToString("dd-MMM-yyyy hh:mm") %></span></div>
                    <div class="col-md-1" style="padding-top: 7px"><span style="color: #fff"></span><span style="color: #fff; font-size: 12px;" runat="server" id="TimeMinu"></span></div>
                </div>
                <div class="col-md-12" runat="server">
                    <div class="row" style="overflow: scroll;" runat="server">
                        <asp:GridView ID="GRDDist" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                            PageSize="2000" Width="100%" HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundField DataField="SrNO" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="SrNO" Visible="True" HeaderText="Sr No." HeaderStyle-Width="4%" ItemStyle-Width="4%"
                                    FooterStyle-Width="4%"></asp:BoundField>
                                <asp:TemplateField HeaderText="View" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    HeaderStyle-Width="4%" ItemStyle-Width="4%" FooterStyle-Width="4%">
                                    <ItemTemplate>
                                        <a href='../UI/frmSalesExecutiveMaster.aspx?intSalesExecutiveID=<%# Eval("intSalesExecutiveID") %>' class="iframe">
                                            <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Images/Link.gif" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Print" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines" Visible="false">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgPrint" runat="server" CommandName="Print" ImageUrl="~/Images/print1.gif"
                                            CommandArgument='<%# Bind("intSalesExecutiveID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="intSalesExecutiveID" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="intSalesExecutiveID" Visible="False" HeaderText="Dist ID"
                                    HeaderStyle-Width="4%" ItemStyle-Width="4%" FooterStyle-Width="4%"></asp:BoundField>

                                <asp:BoundField DataField="strSalesExecutiveCode" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="strSalesExecutiveCode" Visible="True" HeaderText="Code" ControlStyle-Width="30"
                                    HeaderStyle-Width="10%" ItemStyle-Width="10%" FooterStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="strSalesExecutiveName" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="strSalesExecutiveName" Visible="True" HeaderText="Name"
                                    HeaderStyle-Width="15%" ItemStyle-Width="15%" FooterStyle-Width="15%"></asp:BoundField>
                                <asp:BoundField DataField="strCompanyName" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="strCompanyName" Visible="True" HeaderText="Company"
                                    HeaderStyle-Width="15%" ItemStyle-Width="15%" FooterStyle-Width="15%"></asp:BoundField>
                                <asp:BoundField DataField="strDistributerName" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="strDistributerName" Visible="True" HeaderText="Distributor"
                                    HeaderStyle-Width="10%" ItemStyle-Width="10%" FooterStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="strSalesExecutiveAddress" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="strSalesExecutiveAddress" Visible="True" HeaderText="Address"
                                    HeaderStyle-Width="15%" ItemStyle-Width="15%" FooterStyle-Width="15%"></asp:BoundField>
                                <asp:BoundField DataField="strSalesExecutivePhoneNo" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="strSalesExecutivePhoneNo" Visible="True" HeaderText="Phone No"
                                    HeaderStyle-Width="15%" ItemStyle-Width="15%" FooterStyle-Width="15%"></asp:BoundField>
                                <asp:BoundField DataField="strSalesExecutiveMobileNo" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="strSalesExecutiveMobileNo" Visible="True" HeaderText="Mobile"
                                    HeaderStyle-Width="10%" ItemStyle-Width="10%" FooterStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="strSalesExecutiveEmail" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="strSalesExecutiveEmail" Visible="True" HeaderText="Email"
                                    HeaderStyle-Width="10%" ItemStyle-Width="10%" FooterStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="strCloseFlag" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="strCloseFlag" Visible="True" HeaderText="Close"
                                    HeaderStyle-Width="15%" ItemStyle-Width="15%" FooterStyle-Width="15%"></asp:BoundField>
                                <asp:BoundField DataField="strCloseDate" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="strCloseDate" Visible="True" HeaderText="Date"
                                    HeaderStyle-Width="10%" ItemStyle-Width="10%" FooterStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="strCloseRemark" ItemStyle-CssClass="styleGridLines" HeaderStyle-CssClass="styleGridLines"
                                    SortExpression="strCloseRemark" Visible="True" HeaderText="Remark"
                                    HeaderStyle-Width="15%" ItemStyle-Width="15%" FooterStyle-Width="15%"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click"></asp:LinkButton>
            </ContentTemplate>
             <Triggers>
                <asp:PostBackTrigger ControlID="btnPrint" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
