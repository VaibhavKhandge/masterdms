﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/NewFormMaster.Master" AutoEventWireup="true" CodeBehind="frmWorkFlow.aspx.cs" Inherits="eBILL.UI.frmWorkFlow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 4px;
            line-height: 1;
        }

        .transHeight {
            max-height: 540px;
        }

        .x_title h2 {
            font-size: 12px;
        }

        .form-control {
            font-size: 12px;
            z-index: 1;
        }

        .btn {
            padding: 3px 12px;
            z-index: 1;
        }

        .x_panel {
            position: relative !important;
            width: 100%;
            margin-bottom: 10px;
            padding: 5px 5px;
            display: inline-block;
            background: none repeat scroll 0% 0% #FFF;
            border: 1px solid #E6E9ED;
            opacity: 1;
            transition: all 0.2s ease 0s;
        }

        .HeaderInnerMenu {
            border: 2px solid #E6E9ED;
            padding: 1px 5px 6px;
            margin-bottom: 10px;
            border-radius: 10px;
            background-color: #0000A0;
            height: 30px;
        }

        .divtable {
            vertical-align: top;
            position: absolute;
            overflow: auto;
            table-layout: fixed;
            height: 190px;
            width: 100%;
            margin-left: -5px;
            margin-top: -7px;
        }

        .divtableNew {
            vertical-align: top;
            position: absolute;
            overflow: auto;
            table-layout: fixed;
            height: 250px;
            width: 100%;
            margin-left: -5px;
            margin-top: -7px;
        }

        .divtableNew2 {
            vertical-align: top;
            position: absolute;
            overflow: auto;
            table-layout: fixed;
            height: 380px;
            width: 100%;
            margin-left: -5px;
            margin-top: -7px;
        }

        .buttoncolor {
            background-color: Green;
        }
    </style>

    <script type="text/javascript">
        function getSortedData() {
            document.getElementById("bodyCtnPlaceHolder_hdnSearchValueForTranForm").value = document.getElementById("bodyCtnPlaceHolder_txtSearch").value;
            document.getElementById("bodyCtnPlaceHolder_lnkForMasterFormSort").click();
        }

        function getSortedDataforTransaction() {
            document.getElementById("bodyCtnPlaceHolder_hdnSearchValueForTranForm").value = document.getElementById("txtSearchTran").value;
            document.getElementById("bodyCtnPlaceHolder_lnkForTransactionFormSort").click();
        }

        function getSortedDataforAlerts() {
            document.getElementById("bodyCtnPlaceHolder_hdnSearchValueForAlerts").value = document.getElementById("txtSearchAlerts").value;
            document.getElementById("bodyCtnPlaceHolder_lnkForAlerts").click();
        }

        function getSortedDataforReports() {
            document.getElementById("bodyCtnPlaceHolder_hdnSearchValueForReports").value = document.getElementById("txtSearchReports").value;
            document.getElementById("bodyCtnPlaceHolder_lnkForReports").click();
        }

        function getSortedDataforViews() {
            document.getElementById("bodyCtnPlaceHolder_hdnSearchValueForViews").value = document.getElementById("txtSearchViews").value;
            document.getElementById("bodyCtnPlaceHolder_lnkForViews").click();
        }

        function setDivPosition(values) {
            document.getElementById(values).style.display = 'none';
        }

        function openDivFull(str) {
            document.getElementById("bodyCtnPlaceHolder_hdnDivName").value = str.id;
            document.getElementById("bodyCtnPlaceHolder_hdnIsMax").value = "true";
            document.getElementById("bodyCtnPlaceHolder_lnkOpenDivFullScreen").click();
        }

        // functions to get alerts print in Excel Format
        function getAlertPrint(str) {
            document.getElementById("bodyCtnPlaceHolder_hdnPrintAlertID").value = str.id;
            document.getElementById("bodyCtnPlaceHolder_lnkPrintAlert").click();
        }

        function openWindow() {
            window.open('frmShowExcelReport.aspx', '_new', 'toolbar=1,status=1,menubar=1,width=700px,height=600px,scrollbars=1,resizable=1', 1);
        }

        // Function to view Alerts in detail...
        function getAlertDetails(str) {
            document.getElementById("bodyCtnPlaceHolder_hdnPrintAlertID").value = str.id;
            document.getElementById("bodyCtnPlaceHolder_lnkViewAlerts").click();
        }

        function Click(btn) { var btn; $get(btn).click(); }
        function timeout() { window.setTimeout(window.close, 1000); }
        function GetBrowser() { $get('HIDBrowser').value = Sys.Browser.name.toString(); }

        function openView(str) {
            document.getElementById("bodyCtnPlaceHolder_hdnOpenViewId").value = str.id;
            document.getElementById("bodyCtnPlaceHolder_hdnOpenViewIndex").value = str.name;
            document.getElementById("bodyCtnPlaceHolder_lnkOpenView").click();
        }

        function Reset() {
            document.getElementById("bodyCtnPlaceHolder_ResetLink").click();
        }

        function btnvalue(x) {
            if (document.getElementById(x).checked == true) {
                location.href = "../Ui/Dashboard.aspx";
            }
            else {
                location.href = "../Ui/frmWorkFlow.aspx";
            }
        }

        function btnview(x) {
            if (document.getElementById(x).checked == true) {
                location.href = "../Ui/Default.aspx";
            }
            else {
                location.href = "../Ui/frmWorkFlow.aspx";
            }
        }

        function OpenClosed(values) {
            for (var i = 0; i < 5; i++) {
                if (values != i) {
                    var divName1 = 'DivT' + i;
                    var divName2 = 'divS' + i;
                    if (document.getElementById(divName1) != null) {
                        document.getElementById(divName1).style.height = 'auto';
                        document.getElementById(divName2).style.display = 'none';
                    }
                }
            }
        }

        function OpenReport(Values) {
            document.getElementById("bodyCtnPlaceHolder_LinkButton1").click();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-4 col-sm-10 col-xs-8">
            <h2 style="font-size: 14px; margin-top: 0px; margin-left: 0px; float: left;">Module :
                <asp:Label ID="lblModuleName" runat="server" Style="color: Maroon"></asp:Label></h2>
            <div style="float: right">
                <label class="switch">
                    <input id="MISFlow" onchange="btnvalue('MISFlow')" type="checkbox" class="switch-input" unchecked>
                    <span class="switch-label active" data-on="MIS" data-off="Flow"></span>
                    <span class="switch-handle"></span>
                </label>
            </div>
        </div>

        <div class='col-md-4 input-group'>
            <div style="float: left; display: none;">
                <label class="switch">
                    <input id="View1&2" onchange="btnview('View1&2')" type="checkbox" class="switch-input" unchecked>
                    <span class="switch-label" data-on="View 1" data-off="View 2"></span>
                    <span class="switch-handle"></span>
                </label>
            </div>
            <input type='text' style="float: left" class='form-control' runat='server' id='txtSearch' placeholder='Search for...'>
            <span class='input-group-btn' style="float: left">
                <button class='btn btn-primary' type='button' onclick='javascript:return getSortedData()'>Go!</button>
            </span>
            <a href="#" onclick='javascript:return Reset();' title="Refresh">
                <i class="glyphicon glyphicon-refresh" style="float: left; color: Maroon; margin-top: 5px; padding-right: 10px; padding-left: 50px; font-size: 14px"
                    id="btnReload"></i>
            </a>

            <asp:Button ID="Button1" runat="server" autopostback="true" Text="Reset" Style="margin-left: 1px; float: right; padding: 0px 1px 1px 1px; margin-top: 0px; font-size: 12px; display: none;"
                class="btn btn-warning"
                OnClick="btnReload_Click" />&nbsp;&nbsp;
                 <asp:LinkButton ID="ResetLink" OnClick="ResetLink_Click" runat="server"></asp:LinkButton>
        </div>

        <div runat="server" id="divMain" class="col-md-12 col-sm-12 col-xs-12">
        </div>
        <asp:LinkButton runat="server" ID="lnkForMasterFormSort" OnClick="lnkForMasterFormSort_Click"></asp:LinkButton>
        <asp:LinkButton runat="server" ID="lnkOpenDivFullScreen" OnClick="lnkOpenDivFullScreen_Click"></asp:LinkButton>
        <asp:HiddenField runat="server" ID="hdnSerchValueForMasterForm" />
        <asp:LinkButton runat="server" ID="lnkForTransactionFormSort" OnClick="lnkForTransactionFormSort_Click"></asp:LinkButton>
        <asp:HiddenField runat="server" ID="hdnSearchValueForTranForm" />
        <asp:LinkButton runat="server" ID="lnkForAlerts" OnClick="lnkForAlerts_Click"></asp:LinkButton>
        <asp:HiddenField runat="server" ID="hdnSearchValueForAlerts" />
        <asp:LinkButton runat="server" ID="lnkForSetPosition" OnClick="lnkForSetPosition_Click"></asp:LinkButton>
        <asp:HiddenField runat="server" ID="hdnDivName" />
        <asp:HiddenField runat="server" ID="hdnMasterIsClose" />
        <asp:HiddenField runat="server" ID="hdnTranIsClose" />
        <asp:HiddenField runat="server" ID="hdnAlertIsClose" />
        <asp:HiddenField runat="server" ID="hdnReportsIsClose" />
        <asp:HiddenField runat="server" ID="hdnViewIsClose" />
        <asp:LinkButton runat="server" ID="lnkForReports" OnClick="lnkForReports_Click"></asp:LinkButton>
        <asp:HiddenField runat="server" ID="hdnSearchValueForReports" />
        <asp:LinkButton runat="server" ID="lnkForViews" OnClick="lnkForViews_Click"></asp:LinkButton>
        <asp:HiddenField runat="server" ID="hdnSearchValueForViews" />
        <asp:HiddenField runat="server" ID="hdnIsMax" />
        <asp:HiddenField runat="server" ID="hdnPrintAlertID" />
        <asp:LinkButton runat="server" ID="lnkPrintAlert" OnClick="lnkPrintAlert_Click"></asp:LinkButton>
        <asp:HiddenField runat="server" ID="hdnPrintAlertType" />
        <asp:LinkButton runat="server" ID="lnkViewAlerts" OnClick="lnkViewAlerts_Click"></asp:LinkButton>
        <asp:HiddenField ID="HIDBrowser" runat="server" />
        <asp:HiddenField runat="server" ID="hdnOpenViewId" />
        <asp:HiddenField runat="server" ID="hdnOpenViewIndex" />
        <asp:LinkButton runat="server" ID="lnkOpenView" OnClick="lnkOpenView_Click"></asp:LinkButton>
        <asp:LinkButton runat="server" ID="LinkButton1" OnClick="LinkButton1_Click"></asp:LinkButton>
        <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick"></asp:Timer>
    </div>
</asp:Content>
