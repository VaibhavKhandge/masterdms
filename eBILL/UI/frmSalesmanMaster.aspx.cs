﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.Data;
using System.Data.SqlClient;

namespace eBILL.UI
{
    public partial class frmSalesmanMaster : System.Web.UI.Page
    {
        # region [Branch_ID Property]
        protected int Branch_ID
        {
            get { return ViewState["Branch_ID"] != null ? (Int32)ViewState["Branch_ID"] : 0; }
            set { ViewState["Branch_ID"] = value; }
        }
        # endregion

        # region [Company_ID Property]
        protected int Company_ID
        {
            get { return ViewState["Company_ID"] != null ? (Int32)ViewState["Company_ID"] : 0; }
            set { ViewState["Company_ID"] = value; }
        }
        # endregion

        # region [FY_ID Property]
        protected int FY_ID
        {
            get { return ViewState["FY_ID"] != null ? (Int32)ViewState["FY_ID"] : 0; }
            set { ViewState["FY_ID"] = value; }
        }
        # endregion

        # region [Role_ID Property]
        protected int Role_ID
        {
            get { return ViewState["Role_ID"] != null ? (Int32)ViewState["Role_ID"] : 0; }
            set { ViewState["Role_ID"] = value; }
        }
        # endregion

        # region [dsHeader Property]
        public DataTable dsHeader
        {
            get { return ViewState["dsHeader"] != null ? (DataTable)ViewState["dsHeader"] : null; }
            set { ViewState["dsHeader"] = value; }
        }

        #endregion

        ManageSalesmanMaster OBJManageSalesmanMaster = new ManageSalesmanMaster();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["intSalesmanID"] != null && Request.QueryString["intSalesmanID"] != "")
                    {
                        Branch_ID = Convert.ToInt32(Session["BranchID"].ToString());
                        Company_ID = Convert.ToInt32(Session["CompanyID"].ToString());
                        FY_ID = Convert.ToInt32(Session["FinancialYearId"].ToString());
                        Role_ID = Convert.ToInt32(Session["RoleId"].ToString());
                        txtCloseDate.Text = System.DateTime.Now.ToString("dd/MMM/yyyy");

                        ControlEnableDisable(false);
                        btnSave.Visible = false;
                        btnUpdate.Visible = true;

                        HIDSalesmanID.Value = Request.QueryString["intSalesmanID"].ToString();
                        DataTable aGetSalesmanDetails = new DataTable();
                        OBJManageSalesmanMaster.intSalesmanID = Convert.ToInt32(Request.QueryString["intSalesmanID"].ToString());
                        aGetSalesmanDetails = OBJManageSalesmanMaster.GettblSalesmanMaster_View();

                        if (aGetSalesmanDetails.Rows.Count > 0)
                        {
                            HIDSalesmanID.Value = aGetSalesmanDetails.Rows[0]["intSalesmanID"].ToString();
                            HIDSalesmanExecutiveID.Value = aGetSalesmanDetails.Rows[0]["intSalesExecutiveID"].ToString();
                            HIDExecutiveDistID.Value = aGetSalesmanDetails.Rows[0]["intDistributerID"].ToString();
                            HIDCompanyID.Value = aGetSalesmanDetails.Rows[0]["intCompanyID"].ToString();

                            txtSalesmanCode.Text = aGetSalesmanDetails.Rows[0]["strSalesmanCode"].ToString();
                            txtSalesmanName.Text = aGetSalesmanDetails.Rows[0]["strSalesmanName"].ToString();

                            txtSalesmanExecutive.Text = aGetSalesmanDetails.Rows[0]["strSalesExecutiveName"].ToString();
                            txtSalesmanDistributor.Text = aGetSalesmanDetails.Rows[0]["strDistributerName"].ToString();
                            txtCompany.Text = aGetSalesmanDetails.Rows[0]["strCompanyName"].ToString();
                            txtSalesmanAddress.Text = aGetSalesmanDetails.Rows[0]["strSalesmanAddress"].ToString();

                            txtSalesmanPhone.Text = aGetSalesmanDetails.Rows[0]["strSalesmanPhoneNo"].ToString();
                            txtSalesmanMobile.Text = aGetSalesmanDetails.Rows[0]["strSalesmanMobileNo"].ToString();
                            txtSalesmanEmail.Text = aGetSalesmanDetails.Rows[0]["strSalesmanEmail"].ToString();
                            
                            chkSalesmanClose.Checked = aGetSalesmanDetails.Rows[0]["strCloseFlag"].ToString() == "Y" ? true : false;
                            if (chkSalesmanClose.Checked == true)
                            {
                                chkSalesmanClose_CheckedChanged(null, null);
                                txtCloseDate.Text = aGetSalesmanDetails.Rows[0]["strCloseDate"].ToString();
                                txtCloseRemark.Text = aGetSalesmanDetails.Rows[0]["strCloseRemark"].ToString();
                            }
                            else
                            {
                                txtCloseDate.Text = System.DateTime.Now.ToString("dd/MMM/yyyy");
                                txtCloseRemark.Text = "";
                            }
                        }
                    }
                }
                setSearchButton();
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void Page_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string aStatus = string.Empty;
                if (e.CommandName == "btnSave")
                {
                    OBJManageSalesmanMaster.intSalesmanID = HIDSalesmanID.Value == "" ? 0 : Convert.ToInt32(HIDSalesmanID.Value);
                    OBJManageSalesmanMaster.strSalesmanName = txtSalesmanName.Text != "" ? txtSalesmanName.Text.Trim() : "";
                    OBJManageSalesmanMaster.intSalesExecutiveID = HIDSalesmanExecutiveID.Value == "" ? 0 : Convert.ToInt32(HIDSalesmanExecutiveID.Value);
                    OBJManageSalesmanMaster.intDistributerID = HIDExecutiveDistID.Value == "" ? 0 : Convert.ToInt32(HIDExecutiveDistID.Value);
                    OBJManageSalesmanMaster.intCompanyID = HIDCompanyID.Value == "" ? 0 : Convert.ToInt32(HIDCompanyID.Value);
                    OBJManageSalesmanMaster.strSalesmanAddress = txtSalesmanAddress.Text != "" ? txtSalesmanAddress.Text.Trim() : "";
                    OBJManageSalesmanMaster.strSalesmanPhoneNo = txtSalesmanPhone.Text != "" ? txtSalesmanPhone.Text.Trim() : "";
                    OBJManageSalesmanMaster.strSalesmanMobileNo = txtSalesmanMobile.Text != "" ? txtSalesmanMobile.Text.Trim() : "";
                    OBJManageSalesmanMaster.strSalesmanEmail = txtSalesmanEmail.Text != "" ? txtSalesmanEmail.Text.Trim() : "";
                    OBJManageSalesmanMaster.strCloseFlag = chkSalesmanClose.Checked ? "Y" : "N";
                    if (chkSalesmanClose.Checked == true)
                    {
                        OBJManageSalesmanMaster.strCloseByID = Convert.ToInt32(Role_ID);
                        OBJManageSalesmanMaster.strCloseDate = txtCloseDate.Text.Trim();
                        OBJManageSalesmanMaster.strCloseRemark = txtCloseRemark.Text != "" ? txtCloseRemark.Text.Trim() : "";
                    }
                    else
                    {
                        OBJManageSalesmanMaster.strCloseByID = 0;
                        OBJManageSalesmanMaster.strCloseDate = "";
                        OBJManageSalesmanMaster.strCloseRemark = "";
                    }
                    aStatus = OBJManageSalesmanMaster.ManagetblSalesManMaster();
                    if (aStatus == "Duplicate")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(3);", true);
                        return;
                    }
                    else if (aStatus != "")
                    {
                        if (aStatus == HIDSalesmanID.Value)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(1);", true);
                        }
                        else if (aStatus != "")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(2);", true);
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "CloseDiv();", true);
                    }
                }
                else if (e.CommandName == "btnClear")
                {
                    txtSalesmanName.Text = string.Empty;
                    HIDSalesmanID.Value = "0";
                    HIDSalesmanExecutiveID.Value = "0";
                    HIDExecutiveDistID.Value = "0";
                    HIDCompanyID.Value = "0";

                    txtSalesmanAddress.Text = string.Empty;
                    txtSalesmanPhone.Text = string.Empty;
                    txtSalesmanMobile.Text = string.Empty;
                    txtSalesmanEmail.Text = string.Empty;

                    chkSalesmanClose.Checked = false;
                    txtCloseDate.Text = System.DateTime.Now.ToString("dd/MMM/yyyy");
                    txtCloseRemark.Text = string.Empty;
                }
                else if (e.CommandName == "btnUpdate")
                {
                    OBJManageSalesmanMaster.intSalesmanID = HIDSalesmanID.Value == "" ? 0 : Convert.ToInt32(HIDSalesmanID.Value);
                    OBJManageSalesmanMaster.strSalesmanName = txtSalesmanName.Text != "" ? txtSalesmanName.Text.Trim() : "";
                    OBJManageSalesmanMaster.intSalesExecutiveID = HIDSalesmanExecutiveID.Value == "" ? 0 : Convert.ToInt32(HIDSalesmanExecutiveID.Value);
                    OBJManageSalesmanMaster.intDistributerID = HIDExecutiveDistID.Value == "" ? 0 : Convert.ToInt32(HIDExecutiveDistID.Value);
                    OBJManageSalesmanMaster.intCompanyID = HIDCompanyID.Value == "" ? 0 : Convert.ToInt32(HIDCompanyID.Value);
                    OBJManageSalesmanMaster.strSalesmanAddress = txtSalesmanAddress.Text != "" ? txtSalesmanAddress.Text.Trim() : "";
                    OBJManageSalesmanMaster.strSalesmanPhoneNo = txtSalesmanPhone.Text != "" ? txtSalesmanPhone.Text.Trim() : "";
                    OBJManageSalesmanMaster.strSalesmanMobileNo = txtSalesmanMobile.Text != "" ? txtSalesmanMobile.Text.Trim() : "";
                    OBJManageSalesmanMaster.strSalesmanEmail = txtSalesmanEmail.Text != "" ? txtSalesmanEmail.Text.Trim() : "";
                    OBJManageSalesmanMaster.strCloseFlag = chkSalesmanClose.Checked ? "Y" : "N";
                    if (chkSalesmanClose.Checked == true)
                    {
                        OBJManageSalesmanMaster.strCloseByID = Convert.ToInt32(Role_ID);
                        OBJManageSalesmanMaster.strCloseDate = txtCloseDate.Text.Trim();
                        OBJManageSalesmanMaster.strCloseRemark = txtCloseRemark.Text != "" ? txtCloseRemark.Text.Trim() : "";
                    }
                    else
                    {
                        OBJManageSalesmanMaster.strCloseByID = 0;
                        OBJManageSalesmanMaster.strCloseDate = "";
                        OBJManageSalesmanMaster.strCloseRemark = "";
                    }
                    aStatus = OBJManageSalesmanMaster.ManagetblSalesManMaster();
                    if (aStatus == "Duplicate")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(3);", true);
                        return;
                    }
                    else if (aStatus != "")
                    {
                        if (aStatus == HIDSalesmanID.Value)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(1);", true);
                        }
                        else if (aStatus != "")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(2);", true);
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "CloseDiv();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            DataTable aGetExecutive = new DataTable();
            OBJManageSalesmanMaster.intSalesExecutiveID = HIDSalesmanExecutiveID.Value != "" ? Convert.ToInt32(HIDSalesmanExecutiveID.Value) : 0;
            aGetExecutive = OBJManageSalesmanMaster.aGetSalesExecutive_Bind();
            if (aGetExecutive.Rows.Count > 0)
            {
                txtSalesmanDistributor.Text = aGetExecutive.Rows[0]["strDistributerName"].ToString();
                HIDExecutiveDistID.Value = aGetExecutive.Rows[0]["intDistributerID"].ToString();

                txtCompany.Text = aGetExecutive.Rows[0]["strCompanyName"].ToString();
                HIDCompanyID.Value = aGetExecutive.Rows[0]["intCompanyID"].ToString();
            }
        }

        private void setSearchButton()
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "setsearchButton('frmSalesmanMaster','" + HIDSalesmanExecutiveID.ClientID + "','" + txtSalesmanExecutive.ClientID + "','strSalesExecutiveName','SelectSalesmanExecutive');", true);
        }

        protected void chkSalesmanClose_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSalesmanClose.Checked == true)
                divClose.Visible = true;
            else
                divClose.Visible = false;
        }

        protected void ControlEnableDisable(bool bVal)
        {
            txtSalesmanName.Enabled = bVal;
            btnSalesmanExecutive.Visible = bVal;
            txtSalesmanExecutive.Enabled = bVal;
        }
    }
}