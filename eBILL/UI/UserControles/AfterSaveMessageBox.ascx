﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AfterSaveMessageBox.ascx.cs"
    Inherits="eBILL.UI.UserControls.AfterSaveMessageBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<style type="text/css">
    .style1 {
        width: 174px;
    }

    .style2 {
        width: 727px;
    }

    .style3 {
        width: 526px;
    }

    .style4 {
        width: 743px;
    }

    .updateProgressMessage {
        margin: 3px;
        font-family: Trebuchet MS;
        font-size: small;
        vertical-align: middle;
    }

    .modalBackground {
        background-color: Gray;
        filter: alpha(opacity=60);
        opacity: 0.60;
    }

    .updateProgress {
        border-width: 1px;
        border-style: solid;
        background-color: #FFFFFF;
        position: absolute;
        width: 150px;
        height: 50px;
    }
</style>
<asp:LinkButton ID="LinkButtonTargetControl" runat="server"></asp:LinkButton>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtenderMessage" runat="server" TargetControlID="LinkButtonTargetControl"
    PopupControlID="MessageBox" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="MessageBox" CssClass="updateProgress" runat="server" Width="300px"
    Style="display: none;" BorderStyle="Solid" BorderWidth="5" BorderColor="#410000"
    BackImageUrl="~\Images\Tanspecks.jpg">
    <div>
        <table>
            <tr>
                <td colspan="3">
                    <asp:Label ID="LabelPopupHeader" runat="server" Text="Message" Font-Bold="true" ForeColor="Black" />
                </td>
                <td class="style1"></td>
            </tr>
            <tr>
                <td colspan="4" style="height: auto" align="center">
                    <div>
                        <p>
                            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                        </p>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:CheckBox ID="CHKPrint" Text="Print" runat="server" Checked="false" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:CheckBox ID="CHKEmail" Text="Email" runat="server" Checked="false" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:CheckBox ID="CHKSms" Text="SMS" runat="server" Checked="false" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
                <td>&nbsp;
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;
                </td>
                <td>&nbsp;&nbsp;&nbsp;
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnOK" runat="server" Text="OK" Width="80px" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="80px" />
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
