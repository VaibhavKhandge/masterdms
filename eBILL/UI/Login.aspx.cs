﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataLayer;
using BusinessLayer;
using System.IO;
using System.Net;

namespace eBILL.UI
{
    public partial class Login : System.Web.UI.Page
    {
        public Library objLibrary = new Library();
        public HomeConstant ObjHomeConstant = new HomeConstant();
        public HomeCommotion ObjHomeCommotion = new HomeCommotion();

        # region [dsHeader Property]        
        public DataSet dsHeader
        {
            get { return ViewState["dsHeader"] != null ? (DataSet)ViewState["dsHeader"] : null; }
            set { ViewState["dsHeader"] = value; }
        }
        # endregion
        # region [dsUser Property]        
        public DataSet dsUser
        {
            get { return ViewState["dsUser"] != null ? (DataSet)ViewState["dsUser"] : null; }
            set { ViewState["dsUser"] = value; }
        }
        # endregion
        # region [SearchFieldForBranch Property]
        public string SearchFieldForBranch
        {
            get { return ViewState["SearchFieldForBranch"] != null ? (string)ViewState["SearchFieldForBranch"] : ""; }
            set { ViewState["SearchFieldForBranch"] = value; }
        }
        # endregion
        # region [FYRoleName Property]
        public string FYRoleName
        {
            get { return ViewState["FYRoleName"] != null ? (string)ViewState["FYRoleName"] : ""; }
            set { ViewState["FYRoleName"] = value; }
        }
        # endregion
        # region [strPreviousYearAccess Property]
        public string strPreviousYearAccess
        {
            get { return ViewState["strPreviousYearAccess"] != null ? (string)ViewState["strPreviousYearAccess"] : ""; }
            set { ViewState["strPreviousYearAccess"] = value; }
        }
        # endregion
        # region [dsCssDet Property]
        public DataSet dsCssDet
        {
            get { return ViewState["dsCssDet"] != null ? (DataSet)ViewState["dsCssDet"] : null; }
            set
            {
                ViewState["dsCssDet"] = value;
            }
        }
        # endregion
        # region [dsSkinDet Property]
        public DataSet dsSkinDet
        {
            get { return ViewState["dsSkinDet"] != null ? (DataSet)ViewState["dsSkinDet"] : null; }
            set
            {
                ViewState["dsSkinDet"] = value;
            }
        }
        # endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtUser.Focus();
                sqlFyId.ConnectionString = DataLayer.DataLinker.sqlConnectionString;
                if (!IsPostBack)
                {
                    Session["CompanyHeaderFlag"] = "Y";
                    Session["strExcelReportflag"] = "Y";
                    Session["format"] = "xls";
                    txtUser.Text = String.Empty;
                    txtPassword.Text = "";

                    //ddlFY.DataBind();
                    BtnCompanyOK_Click(sender, e);
                    string strCurrentFY = string.Empty;
                    DataTable aGetFyId = new DataTable();
                    General ObjGen = new General();
                    aGetFyId = ObjGen.GetCurrentFYIdForLoginPage();
                    if (aGetFyId != null)
                    {
                        if (aGetFyId.Rows.Count > 0)
                        {
                            strCurrentFY = aGetFyId.Rows[0]["strFYId"].ToString();
                        }
                    }

                    int aError = 0;
                    ddlFY.SelectedValue = strCurrentFY;
                    if (strCurrentFY == ddlFY.SelectedValue)
                    {
                        aError = 1;
                    }

                    if (aError == 0)
                    {
                        ddlFY.SelectedValue = strCurrentFY;

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('You donot have access to new financial year.');", true);
                    }
                    txtUser_TextChanged(sender, e);
                    txtUser.Focus();
                    if (FYRoleName == "admin" || FYRoleName == "Admin" || FYRoleName == "ADMIN")
                        ddlFY.Enabled = true;
                    else
                        ddlFY.Enabled = false;
                }
            }
            catch(Exception Ex)
            {
                ExceptionHandler.LogError(Ex);
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                ManageCQM ObjMa = new ManageCQM();
                DataTable aCDetails = new DataTable();
                //string aEncryptString = "MSIL2018$MINDA STONERIDGE INSTRUMENTS LTD$31-Mar-2021$30$100";
                //                            //"eBILL2021 VAIBHAV 31-Mar-2021 10 50";
                //string aDecryptString = RBEDS.RBDE.doe(aEncryptString);
                //string aDesc = RBEDS.RBDE.dod(aDecryptString);

                aCDetails = ObjMa.GetCompanyDetailsForLicensMatch();
                int CNerror = 0, VDError = 0, NUError = 0, NBError = 0, CLerror = 0;
                if (aCDetails.Rows.Count > 0)
                {
                    DataTable aGetMavedInfoDetails = new DataTable();
                    aGetMavedInfoDetails = ObjMa.GettblMavedInfo();
                    if (aGetMavedInfoDetails.Rows.Count > 0)
                    {
                        string values = RBEDS.RBDE.dod(aGetMavedInfoDetails.Rows[0][RBEDS.RBDE.dod("X3qao+c3wZx2BOmSbHBWqg==")].ToString().Trim());
                        string[] DecryptedString = values.Split('$');

                        if (aCDetails.Rows[0][RBEDS.RBDE.dod("4gpw9TT2xThvLjQbwDN8kQ==")].ToString().Trim() == DecryptedString[0].ToString().Trim())
                        {
                            CLerror = 1;
                        }
                        else
                        {
                            CLerror = 0;
                        }

                        if (aCDetails.Rows[0][RBEDS.RBDE.dod("qE00G4OqRiQAIiegpngTDw==")].ToString().Trim() == DecryptedString[1].ToString().Trim())
                        {
                            CNerror = 1;
                        }
                        else
                        {
                            CNerror = 0;
                        }

                        if (Convert.ToDateTime(DecryptedString[2]) <= DateTime.Now.Date)
                        {
                            VDError = 0;
                        }
                        else
                        {
                            VDError = 1;
                        }

                        if (Convert.ToInt32(aCDetails.Rows[0][RBEDS.RBDE.dod("QY0kAU/ZedppxsYhpEevQg==")].ToString()) <= Convert.ToInt32(DecryptedString[3]))
                        {
                            NBError = 1;
                        }
                        else
                        {
                            NBError = 0;
                        }

                        if (Convert.ToInt32(aCDetails.Rows[0][RBEDS.RBDE.dod("AW1VlQ1gbU8/gZKEK6MYuQ==")].ToString()) <= Convert.ToInt32(DecryptedString[4]))
                        {
                            NUError = 1;
                        }
                        else
                        {
                            NUError = 0;
                        }
                    }
                    else
                    {
                        CNerror = 0;
                        VDError = 0;
                        NUError = 0;
                        NBError = 0;
                        CLerror = 0;
                    }
                }
                else
                {
                    CNerror = 0;
                    VDError = 0;
                    NUError = 0;
                    NBError = 0;
                    CLerror = 0;
                }

                if (CNerror == 1 && VDError == 1 && NUError == 1 && NBError == 1 && CLerror == 1)
                {
                    int RoleId = 0;
                    string RoleName = string.Empty;
                    string AllBranch = string.Empty;
                    int UserEmployeeId = 0;
                    string UserEmployeeName = "";

                    int valid = ObjHomeCommotion.UserValidation(txtUser.Text.Trim(), txtPassword.Text, int.Parse(HIDCompanyID.Value), Convert.ToInt32(ddlProfit.SelectedValue), ref RoleId, ref RoleName, ref UserEmployeeId, ref UserEmployeeName, ref AllBranch);
                    if (valid > 0)
                    {
                        Page.Session["UserID"] = valid;
                        Page.Session["UserName"] = txtUser.Text.Trim();
                        Page.Session["BranchID"] = ddlProfit.SelectedValue;
                        Page.Session["ImageFlag"] = "N";
                        Page.Session["ISMDReport"] = "N";
                        Session["MISRptId"] = "0";
                        Session["NewHeaderFlag"] = "N";
                        Session["AutoMISReportID"] = "0";
                        Session["strPrintPdfFlag"] = "N";
                        Session["strPrintExcelFlag"] = "N";
                        Session["strPrintWordFlag"] = "N";
                        Session["wheresort"] = "N";
                        Page.Session["BranchName"] = ddlProfit.SelectedItem;
                        Page.Session["FinancialYear"] = ddlFY.SelectedItem;
                        Page.Session["FinancialYearId"] = ddlFY.SelectedItem.Value;
                        Page.Session["RoleId"] = RoleId;
                        Page.Session["RoleName"] = RoleName;
                        Page.Session["CompanyID"] = HIDCompanyID.Value;
                        //Page.Session["OrganizationName"] = LBLCompanyName.Text.Trim();
                        Page.Session["CRM"] = "NOT";
                        Session["ERM"] = "NOT";
                        Page.Session["UserEmployeeId"] = UserEmployeeId;
                        Page.Session["UserEmployeeName"] = UserEmployeeName;
                        Page.Session["AllBranch"] = AllBranch;
                        Page.Session["strBarCodeFlag"] = "N";
                        Session["intEmployeeID"] = UserEmployeeId;

                        ManageCQM ObjManageCQM = new ManageCQM();

                        ObjManageCQM.strFYID = Convert.ToInt32(ddlFY.SelectedValue); //GetFinancialYearFirstDate
                        DataTable aGetFinancialYearFirstDate = new DataTable();
                        aGetFinancialYearFirstDate = ObjManageCQM.GetFinancialYearFirstDate();

                        if (aGetFinancialYearFirstDate != null)
                        {
                            if (aGetFinancialYearFirstDate.Rows.Count > 0)
                            {
                                Session["YearStartDate"] = aGetFinancialYearFirstDate.Rows[0]["str1stHalfStartDate"].ToString();
                                Session["YearEndDate"] = aGetFinancialYearFirstDate.Rows[0]["str2ndHalfSEndDate"].ToString();
                            }
                        }

                        ManageMISReport ObjManageRolePermission = new ManageMISReport();
                        ObjManageRolePermission.intRoleId = RoleId;
                        DataTable dtPermissionInfo = ObjManageRolePermission.AdmTblTabRightMasterForPermissionInfo();
                        Page.Session["PermissionInfo"] = dtPermissionInfo;
                        ObjManageRolePermission.intRoleId = RoleId;
                        DataTable dtPagewiseRolePermission = ObjManageRolePermission.GetRoleWisePermission();
                        Page.Session["PermissionInfoPageWise"] = dtPagewiseRolePermission;

                        DataSet dsForFilePath = objLibrary.FillDataSet("ausp_GetDisplayPath", HIDCompanyID.Value, "0");
                        if (dsForFilePath.Tables[0].Rows[0]["strTempararyReportPath"].ToString().Length > 0)
                        {
                            Session["TempararyReportPath"] = dsForFilePath.Tables[0].Rows[0]["strTempararyReportPath"].ToString();
                        }
                        else
                        {
                            Session["TempararyReportPath"] = "C:\\TempararyReportPath\\";
                            if (Directory.Exists(Session["TempararyReportPath"].ToString()))
                            {
                            }
                            else
                            {
                                Directory.CreateDirectory(Session["TempararyReportPath"].ToString());
                            }
                        }

                        if (dsForFilePath.Tables[0].Rows[0]["strImagePath"].ToString().Length > 0)
                            Session["strImagePath"] = dsForFilePath.Tables[0].Rows[0]["strImagePath"].ToString();
                        else
                            Session["strImagePath"] = "D:\\Project\\eBillSolnDocuments\\";

                        if (dsForFilePath.Tables[0].Rows[0]["strthumbSizeHeight"].ToString().Length > 0)
                            Session["strthumbSizeHeight"] = dsForFilePath.Tables[0].Rows[0]["strthumbSizeHeight"].ToString();
                        else
                            Session["strthumbSizeHeight"] = "200";

                        if (dsForFilePath.Tables[0].Rows[0]["strthumbSizeWidth"].ToString().Length > 0)
                            Session["strthumbSizeWidth"] = dsForFilePath.Tables[0].Rows[0]["strthumbSizeWidth"].ToString();
                        else
                            Session["strthumbSizeWidth"] = "200";

                        if (dsForFilePath.Tables[0].Rows[0]["strImageSize"].ToString().Length > 0)
                            Session["strImageSize"] = dsForFilePath.Tables[0].Rows[0]["strImageSize"].ToString();
                        else
                            Session["strImageSize"] = "600";

                        if (dsForFilePath.Tables[0].Rows[0]["strQCFileSize"].ToString().Length > 0)
                            Session["strQCFileSize"] = dsForFilePath.Tables[0].Rows[0]["strQCFileSize"].ToString();
                        else
                            Session["strQCFileSize"] = "600";

                        Session["strCompanyProfilePath"] = dsForFilePath.Tables[0].Rows[0]["strCompanyProfilePath"].ToString();

                        if (Directory.Exists(Session["TempararyReportPath"].ToString() + Session["UserName"].ToString() + "\\"))
                        {
                            string[] files = Directory.GetFiles(Session["TempararyReportPath"].ToString() + Session["UserName"].ToString() + "\\");
                            foreach (string file in files)
                            {
                                File.Delete(file.ToString());
                            }
                            Directory.Delete(Session["TempararyReportPath"].ToString() + Session["UserName"].ToString() + "\\");
                        }
                        else
                        {
                            Directory.CreateDirectory(Session["TempararyReportPath"].ToString() + Session["UserName"].ToString() + "\\");
                        }

                        DataSet ds = objLibrary.FillDataSet("ausp_SaveGenTblAutoCode", HIDCompanyID.Value, ddlProfit.SelectedValue, ddlFY.SelectedItem.Value.ToString());

                        DataSet dSet = new DataSet();
                        dSet = objLibrary.FillDataSet("ssp_GetBranchDisplayName", HIDCompanyID.Value.ToString(), ddlProfit.SelectedValue.ToString());
                        Page.Session["BranchDisplayName"] = (dSet != null && dSet.Tables[0].Rows.Count > 0) ? dSet.Tables[0].Rows[0]["strDisplayName"].ToString() : "";
                        Page.Session["SuperAdminPassword"] = null;

                        DataSet ddSet = new DataSet();
                        ddSet = objLibrary.FillDataSet("ssp_GetDepartmentSubDepartment_UserWise", valid.ToString(), HIDCompanyID.Value.ToString(), ddlProfit.SelectedValue.ToString());
                        Page.Session["intdepartmentid"] = (ddSet != null && ddSet.Tables[0].Rows.Count > 0) ? ddSet.Tables[0].Rows[0]["intdepartmentid"].ToString() : "0";
                        Page.Session["intSubdepartmentid"] = (ddSet != null && ddSet.Tables[0].Rows.Count > 0) ? ddSet.Tables[0].Rows[0]["intSubdepartmentid"].ToString() : "0";
                        Page.Session["intReportToId"] = (ddSet != null && ddSet.Tables[0].Rows.Count > 0) ? ddSet.Tables[0].Rows[0]["intReportToId"].ToString() : "0";
                        Page.Session["strDesignationName"] = (ddSet != null && ddSet.Tables[0].Rows.Count > 0) ? ddSet.Tables[0].Rows[0]["strDesignationName"].ToString() : "";
                        Page.Session["SubDeptInchargeId"] = (ddSet != null && ddSet.Tables[0].Rows.Count > 0) ? ddSet.Tables[0].Rows[0]["SubDeptInchargeId"].ToString() : "0";

                        //Page.Session["ProductName"] = Label1.Text;

                        string LoginDay = Convert.ToString(DateTime.Now.Date.Day);
                        if (LoginDay.Length == 1)
                        {
                            LoginDay = "0" + LoginDay;
                        }
                        string LoginMonth = Convert.ToString(DateTime.Now.Date.Month);
                        string LoginYear = Convert.ToString(DateTime.Now.Date.Year);
                        string LoginDate = LoginDay + "-" + LoginMonth + "-" + LoginYear;
                        string LoginTime = Convert.ToString(DateTime.Now.Hour) + ":" + Convert.ToString(DateTime.Now.Minute) +
                            ":" + Convert.ToString(DateTime.Now.Second);
                        string LogOffDate = "";
                        string LogOffTime = "";

                        Session["UserLoginDate"] = LoginDate;
                        Session["LoginUserId"] = valid;

                        int Count = ObjHomeCommotion.SaveUserLoginDetails(valid, GetIP().ToString().Trim(),
                            int.Parse(HIDCompanyID.Value), int.Parse(ddlProfit.SelectedValue), LoginDate, LoginTime, LogOffDate, LogOffTime);

                        try
                        {
                            string filePath = Server.MapPath("..") + "\\App_Themes\\" + Page.Session["UserName"].ToString() + "\\" + Page.Session["UserName"].ToString();

                            if (File.Exists(filePath + ".skin"))
                            {
                                Session["ThemeFileName"] = Page.Session["UserName"].ToString();
                            }
                            else
                            {
                                Session["ThemeFileName"] = "SkinFile";
                            }

                            if (File.Exists(filePath + ".css"))
                            {
                                Session["CSSFileName"] = Page.Session["UserName"].ToString();
                            }
                            else
                            {
                                Session["CSSFileName"] = "Global";
                            }
                        }
                        catch (Exception)
                        {
                            Session["ThemeFileName"] = "SkinFile";
                            Session["CSSFileName"] = "Global";
                        }
                        DataSet defaultPageDS = objLibrary.FillDataSet("ssp_getDefaultPageByRoleAfterLogin", Page.Session["CompanyID"].ToString(), Page.Session["RoleId"].ToString());
                        switch (Convert.ToString(Page.Session["RoleName"]).ToUpper())
                        {
                            case "SCM":
                                Response.Redirect("LoginByRole.aspx?Role=SCM");
                                break;
                            case "CRM":
                                Response.Redirect("LoginByRole.aspx?Role=CRM");
                                break;
                            case "EMP":
                                Response.Redirect("LoginByRole.aspx?Role=EMP");
                                break;
                            default:
                                if (defaultPageDS.Tables[0].Rows.Count > 0)
                                {
                                    if (defaultPageDS.Tables[0].Rows[0]["strDefaultHomePage"].ToString() == "Dashboard")
                                    {
                                        Response.Redirect("../Ui/Dashboard.aspx", false);
                                    }
                                    else
                                    {
                                        Response.Redirect("../Ui/frmWorkFlow.aspx", false);
                                    }
                                }
                                else
                                {
                                    Response.Redirect("../Ui/Default.aspx", false);
                                }
                                break;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Invalid User !!!');", true);
                    }
                }
                else
                {
                    if (CNerror == 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Registered Company Name is not valid');", true);
                    }
                    else if (VDError == 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Subscription Date has been Expired');", true);
                    }
                    else if (NUError == 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Subscribed Number of Users Exceeded');", true);
                    }
                    else if (NBError == 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Subscribed Number of Branches/Units Exceeded');", true);
                    }
                    else if (CLerror == 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('License key is not valid');", true);
                    }
                }
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('System does not allow to work becouse current financial year not create.');", true);
                //}
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Registered Company Name is not valid');", true);
                string aEx = ex.ToString();
            }
        }

        protected void BtnCompanyOK_Click(object sender, EventArgs e)
        {
            string strCompany = "";
            string strCompanyInformation = "";
            string strWebGridFlag = "";
            string strEnclosurePath = "";
            int valid = ObjHomeCommotion.CompanyValidation(TxtCompanyID.Text.Trim(), TxtCompanyPassword.Text.Trim(), ref strCompany, ref strCompanyInformation, ref strWebGridFlag, ref strEnclosurePath);
            if (valid >= 0)
            {
                Page.Session["WebGridFlag"] = strWebGridFlag;
                HIDCompanyID.Value = Convert.ToString(valid);
                Session["strEnclosurePath"] = strEnclosurePath;
                ObjHomeCommotion.FillCombo(ref ddlProfit, Server.MapPath("..").ToString(), int.Parse(HIDCompanyID.Value));
                try
                {
                    DataSet dsCompanyLogo = new DataSet();
                    object[] param = new object[1];
                    param[0] = int.Parse(HIDCompanyID.Value);
                    dsCompanyLogo = objLibrary.FillDataSet("SSP_GetHomeCompanyImage", param);
                    string filename = dsCompanyLogo.Tables[0].Rows[0]["strBranchLogoName"].ToString();
                    if (dsCompanyLogo.Tables[0].Rows[0]["imgBranchLogo"]!=null)
                    {
                        byte[] file = (byte[])dsCompanyLogo.Tables[0].Rows[0]["imgBranchLogo"];
                        if (filename.ToString() != "")
                        {
                            //imgCompanyLogo.ImageUrl = "~//UI//UploadedFiles//" + filename;
                            //imgCompanyLogo.Width = new Unit(200, UnitType.Pixel);
                            //imgCompanyLogo.Height = new Unit(60, UnitType.Pixel);
                        }
                        else
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Select File Name Please.!');", true);
                    }
                }
                catch (Exception ex)
                {
                    ExceptionHandler.LogError(ex);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Invalid Company ID !!!');", true);
            }
        }

        protected void txtUser_TextChanged(object sender, EventArgs e)
        {
            FYRoleName = "";
            strPreviousYearAccess = string.Empty;
            dsUser = objLibrary.FillDataSet("ssp_GetDisplayBranchUserWise", txtUser.Text.Trim().ToString(), int.Parse(HIDCompanyID.Value).ToString());
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                FYRoleName = dsUser.Tables[0].Rows[0]["strRoleName"].ToString();
                strPreviousYearAccess = dsUser.Tables[0].Rows[0]["strPreviousYearAccess"].ToString();
                ddlProfit.DataSource = dsUser;
                ddlProfit.DataValueField = "intBranchid";
                ddlProfit.DataTextField = "strBranchname";
                ddlProfit.DataBind();
                string aAllBranchIds = string.Empty;
                for (int l = 0; l < dsUser.Tables[0].Rows.Count; l++)
                {
                    if (aAllBranchIds == "")
                        aAllBranchIds = dsUser.Tables[0].Rows[l]["intBranchid"].ToString();
                    else
                        aAllBranchIds += "|" + dsUser.Tables[0].Rows[l]["intBranchid"].ToString();
                }

                Session["aAllBranchIds"] = aAllBranchIds;
            }
            else
            {
                txtPassword.Focus();
            }
            txtPassword.Focus();

            if (FYRoleName == "admin" || FYRoleName == "Admin" || FYRoleName == "ADMIN" || strPreviousYearAccess == "Y")
                ddlFY.Enabled = true;
            else
                ddlFY.Enabled = false;
        }

        private string GetIP()
        {
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();
        }
    }
}