﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmSalesExecutiveMaster.aspx.cs" Inherits="eBILL.UI.frmSalesExecutiveMaster" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sales Executive Master</title>
    <!-- bootstrap 3.0.2 -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="CSS/AdminLTE.css" rel="stylesheet" />
    <link href="CSS/colorbox.css" rel="stylesheet" />
    <!-- Theme style -->
    <script src="JS/jquery.min.js" type="text/javascript"></script>
    <script src="../Script/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="JS/jquery.colorbox.js" type="text/javascript"></script>
    <script src="../Script/KeyPress.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
        });

        function pageLoad(sender, args) {
            if (args.get_isPartialLoad()) {
                $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
            }
        }
        function loadPopup() {
            $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
        }

        function LoadPage() {
            $.colorbox.close();
            document.getElementById('LinkButton1').click();
        }

        function ConfirmEDelete() {
            if (confirm('Are you sure want to Delete this record?') == true) {
                document.getElementById("ConfirmDelete").value = "true";
                return true;
            }
            else {
                document.getElementById("ConfirmDelete").value = "false";
                return false;
            }
        }

        function setValues(Values) {
            var ActualValues = Values.split(',');
            var HdnFieldName = "";
            for (var i = 0; i < ActualValues.length; i++) {
                var result = ActualValues[i].split('$');
                if (i == 0) {
                    HdnFieldName = result[0];
                }
                if (HdnFieldName == 'HIDExeCompID') {
                    document.getElementById(result[0]).value = result[1];
                }

                if (HdnFieldName == 'HIDExecutiveDistID') {
                    document.getElementById(result[0]).value = result[1];
                }
            }
            if (HdnFieldName == 'HIDExeCompID') {
                var s_a = document.getElementById('SelectExecutiveCompany');
                s_a.href = 'frmSelection.aspx?form=' + 'frmSalesExecutiveMaster' + '&control0=' + 'HIDExeCompID' + '&control1=' + 'txtExeCompany' + '&Fld=' + 'strCompanyName' + '&StrVal=' + document.getElementById('txtExeCompany').value + '&SearchFire=Y';
            }

            if (HdnFieldName == 'HIDExecutiveDistID') {
                var s_a = document.getElementById('SelectExecutiveDistibutor');
                s_a.href = 'frmSelection.aspx?form=' + 'frmSalesExecutiveMaster' + '&control0=' + 'HIDExecutiveDistID' + '&control1=' + 'txtExecutiveDist' + '&Fld=' + 'strDistributerName' + '&StrVal=' + document.getElementById('txtExecutiveDist').value + '&SearchFire=Y';
            }
            $.colorbox.close();

            //if (HdnFieldName == 'HIDDistCompany') {
            //    document.getElementById('LinkButton1').click();
            //}
        }

        function setsearchButton(form, control0, control1, Fld, hrefid) {
            if (hrefid == 'SelectExecutiveCompany') {
                if (document.getElementById('SelectExecutiveCompany') != null) {
                    var s_a = document.getElementById('SelectExecutiveCompany');
                    s_a.href = 'frmSelection.aspx?form=' + form + '&control0=' + control0 + '&control1=' + control1 + '&Fld=' + Fld + '&StrVal=' + document.getElementById(control1).value + '&SearchFire=Y';
                }
            }

            if (hrefid == 'SelectExecutiveDistibutor') {
                if (document.getElementById('SelectExecutiveDistibutor') != null) {
                    var s_a = document.getElementById('SelectExecutiveDistibutor');
                    s_a.href = 'frmSelection.aspx?form=' + form + '&control0=' + control0 + '&control1=' + control1 + '&Fld=' + Fld + '&StrVal=' + document.getElementById(control1).value + '&SearchFire=Y';
                }
            }
        }

        function ClosePopup(values) {
            if (values == "1") {
                alert('Record Update SuccessFully....');
            }
            if (values == "2") {
                alert('Record save SuccessFully...');
            }
            if (values == "3") {
                alert('Distributor can not be duplicated,enter another Distributor...');
            }
        }

        function CloseDiv() {
            parent.LoadPage();
        }

        function Validation() {
            if (document.getElementById('txtExeName').value == "") {
                alert('Please enter executive name...!');
                return false;
            }
            else if (document.getElementById('HIDExeCompID').value == "") {
                alert('Please select company...!');
                document.getElementById('txtExeDistributor').focus();
                return false;
            }
            else if (document.getElementById('HIDExecutiveDistID').value == "") {
                alert('Please select distributor...!');
                document.getElementById('txtExecutiveDist').focus();
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server"
                    DynamicLayout="true">
                    <ProgressTemplate>
                        <div id="updPleaseWait" style="text-align: right; color: White; vertical-align: top; position: fixed; top: 300px; left: 600px">
                            <table style="background-color: #7cc3ef; width: 100%">
                                <tr>
                                    <td colspan="2" style="text-align: center; width: 100%">
                                        <b><font color="white">Please Wait...</font></b>
                                        <br />
                                        <img src="../vendor/lightbox2/dist/images/loading.gif" alt="Please wait...." title="Please wait...." />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div class="col-md-12" style="background: linear-gradient(45deg, #b74aef 0%, #da4726 100%);">
                    <div class="col-md-5">
                        <ol class="breadcrumb">
                            <li>
                                <asp:Button Text="Save" ID="btnSave" runat="server" CssClass="btn btn-primary btn-sm" OnCommand="Page_Command" CommandName="btnSave" OnClientClick="return Validation()" />
                                <asp:Button Text="Update" ID="btnUpdate" runat="server" CssClass="btn btn-primary btn-sm" OnCommand="Page_Command" CommandName="btnUpdate" OnClientClick="return Validation()" Visible="false" />
                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-primary btn-sm" OnCommand="Page_Command" CommandName="btnClear" />
                            </li>
                        </ol>
                    </div>
                    <div class="col-md-3" style="padding-top: 0px">
                        <span style="color: #fff">
                            <h4>Sales Executive Master</h4>
                        </span>
                    </div>
                    <div class="col-md-3" style="padding-top: 7px; margin-left: 100px;"><span style="color: #fff">Date/ Time :</span><span style="color: #fff; font-size: 12px;"> <%=System.DateTime.Now.ToString("dd-MMM-yyyy hh:mm") %></span></div>
                    <div class="col-md-1" style="padding-top: 7px"><span style="color: #fff"></span><span style="color: #fff; font-size: 12px;" runat="server" id="TimeMinu"></span></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Executive Code :</label>
                                    <asp:TextBox ID="txtExeCode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Executive Name :</label>
                                    <asp:TextBox ID="txtExeName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="textwithselect form-group" id="Div1" runat="server">
                                        <label>Executive Company :</label>
                                        <asp:TextBox ID="txtExeCompany" runat="server" CssClass="form-control" AutoPostBack="true" ToolTip="Executive Company"></asp:TextBox>
                                        <a href="" id="SelectExecutiveCompany" class="iframe">
                                            <asp:Button ID="btnExecutiveCompany" runat="server" CssClass="btn btn-primary btn-sm" Text="Select"
                                                TabIndex="6" ToolTip="Select Executive Company"></asp:Button></a>
                                        <asp:HiddenField ID="HIDExeCompID" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="textwithselect form-group" id="Div2" runat="server">
                                        <label>Executive Distributer :</label>
                                        <asp:TextBox ID="txtExecutiveDist" runat="server" CssClass="form-control" AutoPostBack="true" ToolTip="Executive Distibutor"></asp:TextBox>
                                        <a href="" id="SelectExecutiveDistibutor" class="iframe">
                                            <asp:Button ID="btnExeDistributor" runat="server" CssClass="btn btn-primary btn-sm" Text="Select"
                                                TabIndex="6" ToolTip="Select Executive Distibutor"></asp:Button></a>
                                        <asp:HiddenField ID="HIDExecutiveDistID" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Executive Address :</label>
                                    <asp:TextBox ID="txtExeAddress" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Phone No :</label>
                                    <asp:TextBox ID="txtExePhone" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Mobile No :</label>
                                    <asp:TextBox ID="txtExeMobile" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Email :</label>
                                    <asp:TextBox ID="txtExeEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Close :</label>
                                    <asp:CheckBox ID="chkExeClose" runat="server" OnCheckedChanged="chkExeClose_CheckedChanged" CssClass="form-control" AutoPostBack="true" />
                                </div>
                            </div>
                        </div>
                        <div id="divClose" runat="server" visible="false">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body">
                                        <label>Close Date :</label>
                                        <asp:TextBox ID="txtExeCloseDate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body">
                                        <label>Close Remark :</label>
                                        <asp:TextBox ID="txtExeCloseRemark" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <asp:LinkButton ID="LinkButton1" runat="server"></asp:LinkButton>
                <asp:HiddenField runat="server" ID="HIDExecutive" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
