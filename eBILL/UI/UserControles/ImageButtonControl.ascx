﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageButtonControl.ascx.cs"
    Inherits="eBILL.UI.UserControls.ImageButtonControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script type="text/javascript" language="javascript">
    var _popup;

    function validate1() {
        //  find the popup behavior
        this._popup = $find('mdlPopup');
        // show the popup
        this._popup.show();
    }
    function LoadPage() {
        $.colorbox.close();
        document.getElementById('').click();

    }
</script>

<style type="text/css">
    .bgPopup {
        background-color: gray;
        filter: alpha(opacity=50);
        opacity: .20;
        table-layout: fixed;
        background-image: none;
        cursor: auto;
        border-collapse: collapse;
    }

    .modalBackground {
        background-color: Gray;
        filter: alpha(opacity=60);
        opacity: 0.60;
    }

    .updateProgress {
        border-width: 1px;
        border-style: solid;
        background-color: #FFFFFF;
        position: absolute;
        width: 150px;
        height: 50px;
    }

    .updateProgressMessage {
        margin: 3px;
        font-family: Trebuchet MS;
        font-size: small;
        vertical-align: middle;
    }
</style>
<div>
    <table cellspacing="0">
        <tr>
            <td style="display: none;">
                <asp:Menu ID="MnuFile" runat="server" BackColor="#DCDCD1" DynamicHorizontalOffset="2"
                    Font-Names="Segoe UI" Font-Size="8pt" ForeColor="Blue" Orientation="Horizontal"
                    StaticSubMenuIndent="10px" OnMenuItemClick="MnuFile_MenuItemClick">
                    <StaticSelectedStyle BackColor="#DCDCD1" ForeColor="Red" />
                    <StaticMenuItemStyle HorizontalPadding="15px" VerticalPadding="2px" />
                    <DynamicHoverStyle BackColor="#DCDCD1" ForeColor="Red" />
                    <DynamicMenuStyle BackColor="#DCDCD1" />
                    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                    <StaticHoverStyle BackColor="#DCDCD1" ForeColor="White" />
                    <StaticMenuItemStyle HorizontalPadding="15px" VerticalPadding="2px" />
                    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                    <Items>
                        <asp:MenuItem Text="File" Value="File">
                            <asp:MenuItem Text="New&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" Value="New"></asp:MenuItem>
                            <asp:MenuItem Text="Edit       " Value="Edit"></asp:MenuItem>
                            <asp:MenuItem Text="Delete     " Value="Delete"></asp:MenuItem>
                            <asp:MenuItem Text="Save       " Value="Save"></asp:MenuItem>
                            <asp:MenuItem Text="Save As      " Value="SaveAs"></asp:MenuItem>
                            <asp:MenuItem Text="Cancel      " Value="Cancel"></asp:MenuItem>
                            <asp:MenuItem Text="List       " Value="List"></asp:MenuItem>
                            <asp:MenuItem Text="Filter       " Value="Filter"></asp:MenuItem>
                            <asp:MenuItem Text="Print      " Value="Print"></asp:MenuItem>
                            <asp:MenuItem Text="EMail      " Value="EMail"></asp:MenuItem>
                            <asp:MenuItem Text="Refresh    " Value="Refresh"></asp:MenuItem>
                            <asp:MenuItem Text="Help       " Value="Help"></asp:MenuItem>
                            <asp:MenuItem Text="SMS      " Value="SMS"></asp:MenuItem>
                            <asp:MenuItem Text="Close      " Value="Close"></asp:MenuItem>
                            <asp:MenuItem Text="PrintExcel   " Value="PrintExcel"></asp:MenuItem>
                            <asp:MenuItem Text="Fill Detail   " Value="FillDetail"></asp:MenuItem>
                        </asp:MenuItem>
                    </Items>
                </asp:Menu>
            </td>
            <td>
                <asp:PlaceHolder ID="PHOptions" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
        <tr>
            <td>
                <cc1:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="pnlPopup" PopupControlID="pnlPopup"
                    BackgroundCssClass="modalBackground" />
                <asp:Panel ID="pnlPopup" runat="server" CssClass="updateProgress" Style="display: none">
                    <div align="center" style="margin-top: 13px;">
                        <span class="updateProgressMessage">Saving Data. Please wait.....</span>
                        <cc1:ModalPopupExtender ID="modpopup" runat="server" BackgroundCssClass="bgPopup"
                            TargetControlID="imgbtnConfigure" PopupControlID="pnl" PopupDragHandleControlID="trDrag"
                            CancelControlID="imgbtnClose">
                        </cc1:ModalPopupExtender>
                    </div>
                </asp:Panel>
            </td>
            <td></td>
        </tr>
    </table>
</div>
