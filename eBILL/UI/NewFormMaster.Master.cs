﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.Data;
using System.Data.SqlClient;

namespace eBILL.UI
{
    public partial class NewFormMaster : System.Web.UI.MasterPage
    {
        public Library objLibrary = new Library();
        public MessageCommotion ObjMessageCommotion = new MessageCommotion();
        public static int i = 0;
        public static int count = 0;

        # region [Branch_ID Property]
        protected int Branch_ID
        {
            get { return ViewState["Branch_ID"] != null ? (Int32)ViewState["Branch_ID"] : 0; }
            set { ViewState["Branch_ID"] = value; }
        }
        # endregion

        # region [Company_ID Property]
        protected int Company_ID
        {
            get { return ViewState["Company_ID"] != null ? (Int32)ViewState["Company_ID"] : 0; }
            set { ViewState["Company_ID"] = value; }
        }
        # endregion

        # region [FY_ID Property]
        protected int FY_ID
        {
            get { return ViewState["FY_ID"] != null ? (Int32)ViewState["FY_ID"] : 0; }
            set { ViewState["FY_ID"] = value; }
        }
        # endregion

        # region [Role_ID Property]
        protected int Role_ID
        {
            get { return ViewState["Role_ID"] != null ? (Int32)ViewState["Role_ID"] : 0; }
            set { ViewState["Role_ID"] = value; }
        }
        #endregion

        #region [UserName Property]
        protected string UserName
        {
            get { return ViewState["UserName"] != null ? (string)ViewState["UserName"] : ""; }
            set { ViewState["UserName"] = value; }
        }
        # endregion

        protected string LogoUrl = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    UserName = Session["UserName"].ToString();
                    lblUserName.Text = UserName;

                    Branch_ID = Convert.ToInt32(Session["BranchID"].ToString());
                    Company_ID = Convert.ToInt32(Session["CompanyID"].ToString());
                    FY_ID = Convert.ToInt32(Session["FinancialYearId"].ToString());
                    Role_ID = Convert.ToInt32(Session["RoleId"].ToString());

                    BindLeftSideMenues();
                    setCheck();
                }

                DataSet dsCompanyLogo = new DataSet();
                object[] param = new object[1];
                if (Session["CompanyID"].ToString() != null)
                {
                    param[0] = int.Parse(Session["CompanyID"].ToString());
                }
                else
                {
                    param[0] = 0;
                }
                dsCompanyLogo = objLibrary.FillDataSet("SSP_GetHomeCompanyImage", param);
                if (dsCompanyLogo.Tables[0].Rows.Count > 0)
                {
                    if (dsCompanyLogo.Tables[0].Rows[0]["strBranchLogoName"].ToString() != "")
                    {
                        LogoUrl = "UploadedFiles/" + dsCompanyLogo.Tables[0].Rows[0]["strBranchLogoName"].ToString();
                    }
                }
            }
            catch(Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void lnksetCheckFlag_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Function to bind menues to left slider
        /// </summary>
        protected void BindLeftSideMenues()
        {
            DataSet moduleDataset = new DataSet();
            DataTable moduleDataTable = new DataTable();
            DataTable moduleCounter = new DataTable();
            int innerModuleCunter = 0;
            string childMenuCounter = string.Empty;
            try
            {
                moduleDataset = objLibrary.FillDataSet("ssp_GetAllModuleNamesForMenuBinding", Company_ID.ToString(), Role_ID.ToString());

                DataTable ModuleHeaderDataTable = new DataTable();
                ModuleHeaderDataTable = moduleDataset.Tables[0];
                moduleDataTable = moduleDataset.Tables[1];
                moduleCounter = moduleDataset.Tables[2];
                innerModuleCunter = moduleCounter.Rows.Count;
                for (int n = 0; n < moduleCounter.Rows.Count; n++)
                {
                    childMenuCounter += moduleCounter.Rows[n]["headerCount"].ToString() + "/";
                }
                string strMenuBinder = string.Empty;
                string header = string.Empty;
                string middle = string.Empty;
                string end = string.Empty;
                string ulHeader = string.Empty;
                string liHeader = string.Empty;

                header = "<ul>" +
                  "<li class='fa fa-edit ulheading'>&nbsp;&nbsp;&nbsp;Modules" +
                  "<ul class='childUl'>";
                i = 0;
                count = 0;

                if (hdnModuleNameDisplay.Value == "0")
                {
                    for (int a = i; a < moduleDataTable.Rows.Count; a++)
                    {
                        count++;
                        liHeader = liHeader + "<li class='childLi');'><a id='M" + (a + 1).ToString() + count.ToString() + "' href='#' onclick='javascript:return getMenuName(this)'>" + moduleDataTable.Rows[a]["strModuleName"].ToString() + "</a></li>";                      
                    }
                }
                else
                {
                    liHeader = string.Empty;
                    for (int k = 0; k < 3; k++)
                    {
                        if (k == 0)
                            liHeader = liHeader + "<li class='childLi');'><a id='M" + (k).ToString() + "' href='../Ui/frmMISReportCqm.aspx' target='_blank'>MIS</a></li>";
                        else if (k == 1)
                            liHeader = liHeader + "<li class='childLi');'><a id='M" + (k).ToString() + "' href='../Ui/frmPendingApproval.aspx' target='_blank'>Pending Approval</a></li>";
                        else if (k == 2)
                            liHeader = liHeader + "<li class='childLi');'><a id='M" + (k).ToString() + "' href='../Ui/DashboardNew.aspx' target='_blank'>Dashboard</a></li>";
                    }
                }
                ulHeader += liHeader + "</ul></li>";
                liHeader = "";
                end = "</ul></li></ul>";

                strMenuBinder = header + ulHeader + end;
                leftsideInnerMenuDiv.InnerHtml = strMenuBinder;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Set Checkbox according to session...
        /// </summary>
        protected void setCheck()
        {
            try
            {
                if (Session["MasterFormChkVal"] != null && Session["MasterFormChkVal"].ToString() != "")
                {
                    chkMasterForms.Checked = Convert.ToBoolean(Session["MasterFormChkVal"].ToString());
                }
                else
                {
                    chkMasterForms.Checked = true;
                }

                if (Session["TranFormChkVal"] != null && Session["TranFormChkVal"].ToString() != "")
                {
                    chkTranFroms.Checked = Convert.ToBoolean(Session["TranFormChkVal"].ToString());
                }
                else
                {
                    chkTranFroms.Checked = true;
                }

                if (Session["chkAlertsVal"] != null && Session["chkAlertsVal"].ToString() != "")
                {
                    chkAlerts.Checked = Convert.ToBoolean(Session["chkAlertsVal"].ToString());
                }
                else
                {
                    chkAlerts.Checked = true;
                }

                if (Session["chkReportsVal"] != null && Session["chkReportsVal"].ToString() != "")
                {
                    chkReports.Checked = Convert.ToBoolean(Session["chkReportsVal"].ToString());
                }
                else
                {
                    chkReports.Checked = true;
                }

                if (Session["chkViewsVal"] != null && Session["chkViewsVal"].ToString() != "")
                {
                    chkViews.Checked = Convert.ToBoolean(Session["chkViewsVal"].ToString());
                }
                else
                {
                    chkViews.Checked = true;
                }

                string str = Session["chkDashInfoVal"] != null ? Session["chkDashInfoVal"].ToString() : "Error";

                if (Session["chkDashInfoVal"] != null && Session["chkDashInfoVal"].ToString() != "")
                {
                    chkDashInfo.Checked = Convert.ToBoolean(Session["chkDashInfoVal"].ToString());
                }
                else
                {
                    chkDashInfo.Checked = true;
                }

                if (Session["chkDashGraphVal"] != null && Session["chkDashGraphVal"].ToString() != "")
                {
                    chkDashGraph.Checked = Convert.ToBoolean(Session["chkDashGraphVal"].ToString());
                }
                else
                {
                    chkDashGraph.Checked = true;
                }

                if (Session["chkDashAnalysisVal"] != null && Session["chkDashAnalysisVal"].ToString() != "")
                {
                    chkDashAnalysis.Checked = Convert.ToBoolean(Session["chkDashAnalysisVal"].ToString());
                }
                else
                {
                    chkDashAnalysis.Checked = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Method to create Workflow divs dynamically...
        /// this method called by Javascript function getMenuName(str)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        #region Module Wise Form
        protected void lnkModuleCall_Click(object sender, EventArgs e)
        {
            try
            {
                Session["ModuleName"] = hdnModuleName.Value.ToString();
                Response.Redirect("frmWorkFlow.aspx");
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }
        #endregion

        #region Check Box Change
        protected void chkMasterForms_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkMasterForms.Checked == true)
                {
                    Session["MasterFormChkVal"] = "true";
                }
                else
                {
                    Session["MasterFormChkVal"] = "false";
                }
                Response.Redirect("frmWorkFlow.aspx", false);
            }
            catch(Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void chkTranFroms_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkTranFroms.Checked == true)
                {
                    Session["TranFormChkVal"] = "true";
                }
                else
                {
                    Session["TranFormChkVal"] = "false";
                }
                Response.Redirect("frmWorkFlow.aspx", false);
            }
            catch(Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void chkAlerts_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkAlerts.Checked == true)
                {
                    Session["chkAlertsVal"] = "true";
                }
                else
                {
                    Session["chkAlertsVal"] = "false";
                }
                Response.Redirect("frmWorkFlow.aspx", false);
            }
            catch(Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void chkViews_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkViews.Checked == true)
                {
                    Session["chkViewsVal"] = "true";
                }
                else
                {
                    Session["chkViewsVal"] = "false";
                }
                Response.Redirect("frmWorkFlow.aspx", false);
            }
            catch(Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void chkReports_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkReports.Checked == true)
                {
                    Session["chkReportsVal"] = "true";
                }
                else
                {
                    Session["chkReportsVal"] = "false";
                }
                Response.Redirect("frmWorkFlow.aspx", false);
            }
            catch(Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void chkDashInfo_CheckedChanged(object sender, EventArgs e)
        {
            Response.Redirect("frmWorkFlow.aspx", false);
        }

        protected void chkDashGraph_CheckedChanged(object sender, EventArgs e)
        {
            Response.Redirect("frmWorkFlow.aspx", false);
        }

        protected void chkDashAnalysis_CheckedChanged(object sender, EventArgs e)
        {
            Response.Redirect("frmWorkFlow.aspx", false);
        }
        #endregion

        // Code to update user login details on logout....
        #region Log Out
        protected void lnkLogOut_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateLoginDetails();
                Session.Abandon();
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("Login.aspx");
            }
            catch(Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        private void UpdateLoginDetails()
        {
            try
            {
                string LogOffDay = Convert.ToString(DateTime.Now.Date.Day);
                if (LogOffDay.Length == 1)
                {
                    LogOffDay = "0" + LogOffDay;
                }
                string LogOffMonth = Convert.ToString(DateTime.Now.Date.Month);
                string LogOffYear = Convert.ToString(DateTime.Now.Date.Year);
                string LogOffDate = LogOffDay + "-" + LogOffMonth + "-" + LogOffYear;
                string LogOffTime = Convert.ToString(DateTime.Now.Hour) + ":" + Convert.ToString(DateTime.Now.Minute) +
                    ":" + Convert.ToString(DateTime.Now.Second);
                string LoginDate = Session["UserLoginDate"].ToString();

                int count = ObjMessageCommotion.UpdateUserLoginDetails(Session["LoginUserId"].ToString(), LoginDate, LogOffDate, LogOffTime);
            }
            catch(Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }
        #endregion
    }
}