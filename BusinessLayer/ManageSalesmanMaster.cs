﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLayer
{
    public class ManageSalesmanMaster
    {
        public int intSalesmanID, intSalesExecutiveID, intCompanyID, intDistributerID, strCloseByID;
        public string strSalesmanName, strSalesmanAddress, strSalesmanPhoneNo,
        strSalesmanMobileNo, strSalesmanEmail, strCloseFlag, strCloseDate, strCloseRemark;

        public string ManagetblSalesManMaster()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[13];
                int i = 0;
                parm[i++] = new SqlParameter("@intSalesmanID", intSalesmanID);
                parm[i++] = new SqlParameter("@intSalesExecutiveID", intSalesExecutiveID);
                parm[i++] = new SqlParameter("@intCompanyID", intCompanyID);
                parm[i++] = new SqlParameter("@intDistributerID", intDistributerID);
                parm[i++] = new SqlParameter("@strCloseByID", strCloseByID);
                parm[i++] = new SqlParameter("@strSalesmanName", strSalesmanName);
                parm[i++] = new SqlParameter("@strSalesmanAddress", strSalesmanAddress);
                parm[i++] = new SqlParameter("@strSalesmanPhoneNo", strSalesmanPhoneNo);
                parm[i++] = new SqlParameter("@strSalesmanMobileNo", strSalesmanMobileNo);
                parm[i++] = new SqlParameter("@strSalesmanEmail", strSalesmanEmail);
                parm[i++] = new SqlParameter("@strCloseFlag", strCloseFlag);
                parm[i++] = new SqlParameter("@strCloseDate", strCloseDate);
                parm[i++] = new SqlParameter("@strCloseRemark", strCloseRemark);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblSalesmanMaster", parm);                
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GettblSalesmanMaster_View()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intSalesmanID", intSalesmanID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblSalesmanMaster_View", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GettblSalesmanMaster()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intSalesmanID", intSalesmanID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblSalesmanMaster", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable aGetSalesExecutive_Bind()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intSalesExecutiveID", intSalesExecutiveID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("aGetSalesExecutive_Bind", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }
    }
}