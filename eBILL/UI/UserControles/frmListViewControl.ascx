﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~/UI/UserControls/frmListViewControl.ascx"
    Inherits="eBILL.UI.UserControls.frmListViewControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<div id="DIVListView" runat="server">
    <table cellspacing="0">
        <tbody>
            <tr>
                <td align="right">
                    <asp:Label ID="Label4" runat="server" Font-Names="verdana" Font-Size="8pt" ForeColor="#404040"
                        Text="Records :" Visible="true" Width="100%"></asp:Label>
                </td>
                <td align="left">
                    <asp:Label ID="Label2" runat="server" Font-Names="verdana" Font-Size="8pt" ForeColor="#404040"
                        Text="" Visible="true" Width="100%" Font-Bold="true"></asp:Label>
                </td>
                <td align="right">
                    <asp:Label ID="Label1" runat="server" Font-Names="verdana" Font-Size="8pt" ForeColor="#404040"
                        Text="Pages :" Visible="true" Width="100%"></asp:Label>
                </td>
                <td align="left">
                    <asp:Label ID="Label3" runat="server" Font-Names="verdana" Font-Size="8pt" ForeColor="#404040"
                        Text="Pages" Visible="true" Width="100%" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    <table cellspacing="0" style="border-style: solid; border-width: 1px; border-color: Black;">
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgFirst" runat="server" ImageUrl="~/Images/first_over.gif"
                                    OnClick="btnImage_Click" ToolTip="First" />
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/first_over.gif"
                                    Style="display: none;" />
                            </td>
                            <td>
                                <asp:ImageButton ID="imgBack" runat="server" ImageUrl="~/Images/prev_over.gif" OnClick="btnImage_Click"
                                    ToolTip="Back" />
                            </td>
                            <td>
                                <asp:TextBox ID="TXTPageNumber" runat="server" SkinID="TXTSkin" AutoPostBack="true"
                                    Font-Size="8pt" Font-Names="verdana" Width="30px" OnTextChanged="TXTPageNumber_OnTextChanged"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTEOANumber" runat="server" TargetControlID="TXTPageNumber"
                                    FilterType="Numbers" FilterMode="ValidChars" InvalidChars="123456789" />
                            </td>
                            <td>
                                <asp:ImageButton ID="imgForword" runat="server" ImageUrl="~/Images/next_over.gif"
                                    OnClick="btnImage_Click" ToolTip="Next" />
                            </td>
                            <td>
                                <asp:ImageButton ID="imgLast" runat="server" ImageUrl="~/Images/last_over.gif" OnClick="btnImage_Click"
                                    ToolTip="Last" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>
