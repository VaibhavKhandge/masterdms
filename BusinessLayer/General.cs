﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using DataLayer;
using System.Web;

namespace BusinessLayer
{
    public class General
    {
        public int intDataReportId, intFyId;
        public DataTable GettblDataReportFilterDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intDataReportId", intDataReportId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblDataReportFilterDetails", parm);
            }
            catch (Exception)
            {
                ds = null;
            }
            return ds;
        }

        public DataTable GetFinancialYear()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@IntFYId", intFyId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("tblGetFinancialYearStartDateandEndDate", parm);
            }
            catch (Exception)
            {
                throw;
            }
            return ds;
        }

        public DataTable GetDataReportMaster()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intDataReportId", intDataReportId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetDataReportMaster", parm);
            }
            catch (Exception)
            {
                ds = null;
            }
            return ds;
        }

        public DataTable DataReportSPEXECUTE(string SPName, int intBranchId, int intFyId, string FromDate, string Todate, string Status, string GroupBy, string Div1, string Div2, string Div3, string Div4, string Div5, string Div6, string Div7, string Div8, string Div9, string Div10, string Div11, string Div12, string Div13, string Div14, string Div15, string Div16, string Div17, string Div18, string Div19, string Div20)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[26];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@Status", Status);
                parm[i++] = new SqlParameter("@GroupBy", GroupBy);
                parm[i++] = new SqlParameter("@Div1", Div1);
                parm[i++] = new SqlParameter("@Div2", Div2);
                parm[i++] = new SqlParameter("@Div3", Div3);
                parm[i++] = new SqlParameter("@Div4", Div4);
                parm[i++] = new SqlParameter("@Div5", Div5);
                parm[i++] = new SqlParameter("@Div6", Div6);
                parm[i++] = new SqlParameter("@Div7", Div7);
                parm[i++] = new SqlParameter("@Div8", Div8);
                parm[i++] = new SqlParameter("@Div9", Div9);
                parm[i++] = new SqlParameter("@Div10", Div10);
                parm[i++] = new SqlParameter("@Div11", Div11);
                parm[i++] = new SqlParameter("@Div12", Div12);
                parm[i++] = new SqlParameter("@Div13", Div13);
                parm[i++] = new SqlParameter("@Div14", Div14);
                parm[i++] = new SqlParameter("@Div15", Div15);
                parm[i++] = new SqlParameter("@Div16", Div16);
                parm[i++] = new SqlParameter("@Div17", Div17);
                parm[i++] = new SqlParameter("@Div18", Div18);
                parm[i++] = new SqlParameter("@Div19", Div19);
                parm[i++] = new SqlParameter("@Div20", Div20);
                ds = DataLayer.DataLinker.ExecuteSPDataTable(SPName, parm);
            }
            catch (Exception)
            {
                ds = null;
            }
            return ds;
        }

        public DataTable GetFinancialYearStartDateandEndDate()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetFinancialYearStartDateandEndDate", parm);
            }
            catch (Exception)
            {
                ds = null;
            }
            return ds;
        }

        public DataTable GetCurrentFYIdForLoginPage()
        {
            DataTable ds = new DataTable();
            try
            {            
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCurrentFYIdForLoginPage");
            }
            catch (Exception)
            {
                ds = null;
            }
            return ds;
        }
    }
}
