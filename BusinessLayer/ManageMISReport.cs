﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using DataLayer;
using System.Web;

namespace BusinessLayer
{
    public class ManageMISReport
    {
        public int FYId, intApuid, intProductId, incustomerId, intChampionId, intSegment;
        public string FromDate, Todate, ComplainType, ForReportGridflag, formName;
        public DataTable ExecuteMisSPForGridandChat(string SPName)
        {
            DataTable ds = new DataTable();
            try
            {
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                SqlParameter[] parm = new SqlParameter[11];
                int i = 0;
                parm[i++] = new SqlParameter("@FYId", FYId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@intApuid", intApuid);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@incustomerId", incustomerId);
                parm[i++] = new SqlParameter("@intChampionId", intChampionId);
                parm[i++] = new SqlParameter("@ComplainType", ComplainType);
                parm[i++] = new SqlParameter("@ForReportGridflag", ForReportGridflag);
                parm[i++] = new SqlParameter("@intSegment", intSegment);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable(SPName, parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intBranchId;
        public string SearchType;
        public DataTable ExecuteMisSPForGridandChatForMoreFilter(string SPName)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[12];
                int i = 0;
                parm[i++] = new SqlParameter("@FYId", FYId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@intApuid", intApuid);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@incustomerId", incustomerId);
                parm[i++] = new SqlParameter("@intChampionId", intChampionId);
                parm[i++] = new SqlParameter("@ComplainType", ComplainType);
                parm[i++] = new SqlParameter("@ForReportGridflag", ForReportGridflag);
                parm[i++] = new SqlParameter("@intSegment", intSegment);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@SearchType", SearchType);
                ds = DataLayer.DataLinker.ExecuteSPDataTable(SPName, parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetGraphSeting()
        {
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@formName", formName);
                return DataLayer.DataLinker.ExecuteSPDataTable("GetGraphSeting", parm);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataTable AdmTblTabRightMasterForBlankDS()
        {
            try
            {
                return DataLayer.DataLinker.ExecuteSPDataTable("AdmTblTabRightMasterForBlankDS");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string strTabName, strTabActualId, strView, strInsert, strUpdate, strDelete, FormDesc;
        public int intRoleId;
        public string ManageAdmTblTabRightMaster()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@strTabName", strTabName);
                parm[i++] = new SqlParameter("@strTabActualId", strTabActualId);
                parm[i++] = new SqlParameter("@FormName", formName);
                parm[i++] = new SqlParameter("@intRoleId", intRoleId);
                parm[i++] = new SqlParameter("@strView", strView);
                parm[i++] = new SqlParameter("@strInsert", strInsert);
                parm[i++] = new SqlParameter("@strUpdate", strUpdate);
                parm[i++] = new SqlParameter("@strDelete", strDelete);
                parm[i++] = new SqlParameter("@FormDesc", FormDesc);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageAdmTblTabRightMaster", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable AdmTblTabRightMasterForPermissionInfo()
        {
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intRoleId", intRoleId);
                return DataLayer.DataLinker.ExecuteSPDataTable("AdmTblTabRightMasterForPermissionInfo", parm);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int intReportTypeId, FormTag;
        public DataTable GetMisReportSPName()
        {
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intReportTypeId", intReportTypeId);
                return DataLayer.DataLinker.ExecuteSPDataTable("GetRegisterReportSPName", parm);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetResiterDetailsData(string SPName)
        {
            try
            {
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intFyiD", Convert.ToInt32(intFyId));
                parm[i++] = new SqlParameter("@intBranchId", Convert.ToInt32(intBranchId));
                parm[i++] = new SqlParameter("@intCompanyId", Convert.ToInt32(intCompanyId));
                return DataLayer.DataLinker.ExecuteSPDataSet(SPName, parm);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataTable GetCrystalReportFileName()
        {
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@FormTag", FormTag);
                return DataLayer.DataLinker.ExecuteSPDataTable("GetCrystalReportFile", parm);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataTable GetMDGraphReportCriteria()
        {
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intMDReportId", intReportTypeId);
                return DataLayer.DataLinker.ExecuteSPDataTable("GetMDGraphReportCriteria", parm);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataTable GetRoleWisePermission()
        {
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intRoleId", intRoleId);
                return DataLayer.DataLinker.ExecuteSPDataTable("GetRoleWisePermission", parm);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int intMDReportId;
        public DataTable GetReferanceIdReport()
        {
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intMDReportId", intMDReportId);
                return DataLayer.DataLinker.ExecuteSPDataTable("GetReferanceIdReport", parm);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string DrillDownValues;
        public DataTable ExecuteMisSPForGridandChatForMoreFilterForDrilDown(string SPName)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[13];
                int i = 0;
                parm[i++] = new SqlParameter("@FYId", FYId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@intApuid", intApuid);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@incustomerId", incustomerId);
                parm[i++] = new SqlParameter("@intChampionId", intChampionId);
                parm[i++] = new SqlParameter("@ComplainType", ComplainType);
                parm[i++] = new SqlParameter("@ForReportGridflag", ForReportGridflag);
                parm[i++] = new SqlParameter("@intSegment", intSegment);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@SearchType", SearchType);
                parm[i++] = new SqlParameter("@DrillDownValues", DrillDownValues);
                ds = DataLayer.DataLinker.ExecuteSPDataTable(SPName, parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intCompanyId, intFYId;
        public string strTodate;
        public DataTable GetGraphDetailsForDashBoard()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFYId);
                parm[i++] = new SqlParameter("@intRoleId", intRoleId);
                parm[i++] = new SqlParameter("@strCurrentDate", strTodate);

                return DataLayer.DataLinker.ExecuteSPDataTable("GetGraphDetailsForDashBoard");
            }
            catch (Exception ex)
            {
                ds = null;
                ExceptionHandler.LogError(ex);
            }
            return ds;
        }

        public int intGroupDetailsId;
        public DataTable ExecuteMisSPForConsolidateAllCSRParameter(string SPName)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYID", FYId);
                parm[i++] = new SqlParameter("@strFromDate", FromDate);
                parm[i++] = new SqlParameter("@strToDate", Todate);
                parm[i++] = new SqlParameter("@intGroupDetailsId", intGroupDetailsId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable(SPName, parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable ExecuteMisSPForParameterWiseAllGroup(string SPName)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYID", FYId);
                parm[i++] = new SqlParameter("@strFromDate", FromDate);
                parm[i++] = new SqlParameter("@strToDate", Todate);
                parm[i++] = new SqlParameter("@intGroupId", intGroupDetailsId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable(SPName, parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intGroupId;
        public DataTable ExecuteMisSPForParameterWiseIndividualGroup(string SPName)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intFYID", FYId);
                parm[i++] = new SqlParameter("@strFromDate", FromDate);
                parm[i++] = new SqlParameter("@strToDate", Todate);
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                parm[i++] = new SqlParameter("@intGroupDetailsId", intGroupDetailsId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable(SPName, parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetBranchNameAccordingGroup()
        {
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intGroupId", intGroupId);
                return DataLayer.DataLinker.ExecuteSPDataTable("GetBranchNameAccordingGroup", parm);
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
                return null;
            }
        }
    }
}
