﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmSelection.aspx.cs" Inherits="eBILL.UI.frmSelection" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="None" %>
<%@ Register Src="~/UI/UserControles/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/UserControles/frmListViewControl.ascx" TagName="ListFilter" TagPrefix="LV" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Grid Selection</title>
    <base id="Base1" target="_self" runat="server" />
    <script src="../Script/norightclk.js" language="javascript" type="text/javascript"></script>
    <script src="../Script/SetFocus.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
        function ViewExistItem1E(form, control0, control1, control2, Fld) {
            window.showModalDialog('frmSelection.aspx?form=' + form +
                '&control0=' + control0 +
                '&control1=' + control1 +
                '&control2=' + control2 +
                '&Fld=' + Fld + '&StrVal=' + document.getElementById(control1).value + '', window, 'dialogWidth=500px;dialogHeight=500px');
        }
        function DisplayHelpHeader(SP_Name, control, Caption) {
            if (control == '0' || control == '') {
                alert('No Record Found');
            }
            else {
                window.open('frmHelp.aspx?SP_Name=' + SP_Name + '&StrVal=' + control + '&Caption=' + 'Help' + '', '_new', 'width=500px,height=500px', 1);
            }
        }

        function CallParent(values) {
            parent.setValues(values);
        }
    </script>

    <script type="text/javascript">
        function timeout() { window.setTimeout(window.close, 1000); }
        function Click(btn) { var btn; $get(btn).click(); }
        function GetBrowser() { $get('HIDBrowser').value = Sys.Browser.name.toString(); }
    </script>
</head>
<body class="CSSBody" onkeydown="Javascript:if( window.event.keyCode==13) window.event.keyCode=9;">
    <form id="form1" runat="server" method="post" onkeyup="highlight(event)" onclick="highlight(event)">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table width="100%" style="table-layout: fixed;" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="LBLBtnText" runat="server" Font-Bold="True" Font-Size="9pt" Text=""
                        SkinID="LBLDetSkinR"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="border-style: solid; border-width: 1px; vertical-align: top; overflow: auto; width: 99%; height: 430px; text-align: left">
                        <table style="table-layout: fixed;" width="99%" cellspacing="0">
                            <tr>
                                <td colspan="2" style="background-color: navy; height: 15px; width: 100%;">
                                    <asp:Label ID="LBLItemDetail" runat="server" Font-Bold="True"
                                        Font-Names="Segoe UI" Font-Size="9pt" ForeColor="White" Style="font-family: Segoe UI; text-align: center; 
                                        background: linear-gradient(45deg, #b74aef 0%, #da4726 100%);"
                                        Text="Selection List" Width="100%"></asp:Label>
                                    <asp:HiddenField ID="HIDBrowser" runat="server" />
                                    <uc1:MyMessageBox ID="MyMessageBoxInfo" runat="server" ShowCloseButton="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellspacing="0" width="99%">
                                        <tr>
                                            <td style="width: 45%;">
                                                <asp:Label ID="lblSearchField" runat="server" SkinID="LBLSELCRI" Text="SearchField :"></asp:Label>
                                            </td>
                                            <td style="width: 54%;">
                                                <asp:Label ID="lblSearchText" runat="server" SkinID="LBLSELCRI" Text="SearchText :"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellspacing="0" width="99%">
                                        <tr>
                                            <td style="width: 45%;">
                                                <asp:DropDownList ID="ddlSearchField" runat="server" SkinID="DDLSearchField" Width="100px"
                                                    OnSelectedIndexChanged="ddlSearchField_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 54%;">
                                                <table cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged"
                                                                            SkinID="TXTSELDESC" Width="200px"></asp:TextBox>
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnSelectItemName1" runat="server" ImageUrl="~/Images/search.png"
                                                                ToolTip="Select" TabIndex="9" Visible="false" />
                                                            <asp:HiddenField ID="HIDItemGroupID" runat="server" Value="0" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnFill" runat="server" Font-Bold="True" Text="Fill" SkinID="BtnSkinDetails"
                                                                OnClick="btnFill_Click" Visible="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btnSearchTxt" runat="server" AccessKey="g" ImageUrl="~/Images/Go.gif"
                                                    SkinID="BTNGo" ToolTip="Search" Visible="True" OnClick="btnSearchTxt_Click" OnClientClick="validate()" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <LV:ListFilter ID="LSFilter" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9"></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: left; width: 99%;">
                                    <div style="overflow: auto; width: 100%; height: 360px; text-align: center; border-style: solid; border-width: 1px;">
                                        <asp:DataGrid ID="GrdCommon" runat="server" OnRowCommand="GrdCommon_RowCommand"
                                            OnItemCommand="GrdCommon_ItemCommand" SkinID="GRDTabSkin" AutoGenerateColumns="true"
                                            Visible="true" AllowPaging="true" ItemStyle-HorizontalAlign="Left" PagerStyle-Visible="false" PageSize="10">

                                            <SelectedItemStyle ForeColor="Red" />
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="CHKSelect" runat="server" SkinID="CHKSkin" OnCheckedChanged="CHKSelect_CheckedChanged"
                                                            AutoPostBack="true"></asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CHKSelectSingle" runat="server" SkinID="CHKSkin" OnCheckedChanged="CHKSelectSingle_CheckedChanged"
                                                            AutoPostBack="true"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Sel" HeaderStyle-Width="2%">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" Width="20px" runat="server" Text="Sel" CommandName="Select"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>

                                                <asp:TemplateColumn HeaderText="Help">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnHelpTaxGroup" runat="server" CommandName="Help" ImageUrl="~/Images/HelpMenu.gif"
                                                            TabIndex="24" ToolTip="Detail" Visible="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Sr No." HeaderStyle-Width="2%">
                                                    <ItemTemplate>
                                                        <%#Container.DataSetIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>

                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnSelect" runat="server" Font-Bold="True" Text="Select" SkinID="BtnSkinDetails"
                        OnClick="btnSelect_Click" Visible="false" />
                    <asp:Button ID="ButtCancel" runat="server" Font-Bold="True" Text="Cancel" SkinID="BtnSkinDetails" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
