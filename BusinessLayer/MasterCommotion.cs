﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using BusinessLayer;
using DataLayer;
using System.Web;

namespace BusinessLayer
{
     public class MasterCommotion : MasterConstant
    {
        public Library ObjLibrary = new Library();
        public DataServices ObjDataServices = new DataServices();

        public DataTable GetRoleWiseMenus(int CompanyID, int BranchID, int RoleID)
        {
            try
            {
                object[] param = new object[3];
                param[0] = CompanyID;
                param[1] = BranchID;
                param[2] = RoleID;
                DataTable dt = ObjDataServices.GetDataSet(SP_GetRolewiseMenus, param);
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataTable GetRoleWiseMenusWTBranch(int CompanyID, int RoleID)
        {
            try
            {
                object[] param = new object[2];
                param[0] = CompanyID;
                param[1] = RoleID;
                DataTable dt = ObjDataServices.GetDataSet("ssp_GetDisplayRoleWiseMenuWithOutBranch", param);
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<string> MenuNameLs()
        {
            List<string> LsMenuList = new List<string>();
            LsMenuList.Add("Administration");
            LsMenuList.Add("References");
            LsMenuList.Add("Marketing");
            LsMenuList.Add("Purchase");
            LsMenuList.Add("Financial Accounting");
            LsMenuList.Add("Inventory");
            LsMenuList.Add("Dispatch");
            LsMenuList.Add("Planning");
            LsMenuList.Add("Production");
            LsMenuList.Add("Services");
            LsMenuList.Add("QC");
            LsMenuList.Add("Logistics");
            LsMenuList.Add("Excise");
            LsMenuList.Add("Capital Project");
            LsMenuList.Add("Export-Import");
            LsMenuList.Add("Tendering");
            LsMenuList.Add("HR & Payroll");
            LsMenuList.Add("Maintenance & Calibration");
            LsMenuList.Add("Security and Gate pass");
            LsMenuList.Add("Document Management");
            LsMenuList.Add("Event");
            LsMenuList.Add("Resource");
            LsMenuList.Add("CRM");
            LsMenuList.Add("SCM");
            LsMenuList.Add("MIS");
            LsMenuList.Add("Complaint Management");
            LsMenuList.Add("Asset");

            return LsMenuList;
        }

        public DataTable GetTblFavoriteMenu()
        {
            string theintCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
            SqlParameter[] parm = new SqlParameter[1];
            int i = 0;
            parm[i++] = new SqlParameter("@IntUserId", theintCreatedId);
            return DataLayer.DataLinker.ExecuteSPDataTable("ssp_GetTblFavoriteMenu", parm);
        }
    }
}
 
