﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

using DataLayer;

namespace BusinessLayer
{
    public class MessageCommotion : MessageConstant
    {
        public DataServices ObjDataServices = new DataServices();
        public Library ObjLibrary = new Library();

        #region Save()
        public int SaveMessageDetails(DataSet dsChattingUserId, string Message, int User_Id, string MessageDate, string MessageTime, string Status, string Flag)
        {
            try
            {
                int Count = 0;
                for (int i = 0; i < dsChattingUserId.Tables[0].Rows.Count; i++)
                {
                    object[] param = new object[7];
                    param[0] = User_Id;
                    param[1] = Message;
                    param[2] = Int32.Parse(dsChattingUserId.Tables[0].Rows[i]["UserId"].ToString());
                    param[3] = MessageDate;
                    param[4] = MessageTime;
                    param[5] = Status;
                    param[6] = Flag;
                    Count += ObjDataServices.SaveQuery(sp_InsertMessageDetails, param);
                }
                return Count;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int SaveMessageReplyDetails(int ChattingUser_Id, string Message, int User_Id, string MessageDate, string MessageTime, string Status, string Flag)
        {
            try
            {
                object[] param = new object[7];
                param[0] = User_Id;
                param[1] = Message;
                param[2] = ChattingUser_Id;
                param[3] = MessageDate;
                param[4] = MessageTime;
                param[5] = Status;
                param[6] = Flag;
                int Count = ObjDataServices.SaveQuery(sp_InsertMessageDetails, param);
                return Count;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region [Update]
        public int UpdateMessageDetails(int MessageDetail_Id, string Flag)
        {
            try
            {
                object[] param = new object[2];
                param[0] = MessageDetail_Id;
                param[1] = Flag;
                int Count = ObjDataServices.SaveQuery(sp_UpdateMessageDetails, param);
                return Count;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int UpdateUserLoginDetails(string User_Id, string strLoginDate, string strLogOffDate, string strLogOffTime)
        {
            try
            {
                object[] param = new object[4];
                param[0] = int.Parse(User_Id);
                param[1] = strLoginDate;
                param[2] = strLogOffDate;
                param[3] = strLogOffTime;
                int Count = ObjDataServices.SaveQuery(sp_UpdateUserLoginDetails, param);
                return Count;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region [Delete]

        public int DeleteMessageDetails(int MessageDetail_Id, string Flag)
        {
            try
            {
                object[] param = new object[2];
                param[0] = MessageDetail_Id;
                param[1] = Flag;
                int Count = ObjDataServices.SaveQuery(sp_deleteMessageDetails, param);

                return Count;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        public DataTable FillMessageDetailsGrid(int UserId, string UserName, string FromDate, string ToDate, string Message, string MessageType)
        {
            try
            {
                object[] param = new object[6];
                param[0] = UserId;
                param[1] = UserName;
                param[2] = FromDate;
                param[3] = ToDate;
                param[4] = Message;
                param[5] = MessageType;

                DataTable dt = ObjDataServices.GetDataSet("sp_GetMessageForUsers", param);
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
