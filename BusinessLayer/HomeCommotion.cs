﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataLayer;
using System.Web.UI.WebControls;
using System.IO;

namespace BusinessLayer
{
    public class HomeCommotion : HomeConstant
    {
        public DataServices ObjDataServices = new DataServices();
        public Library ObjLibrary = new Library();

        #region UserValidation()
        public int UserValidation(string User_Name, string Password, int Company_ID, int ProfitCenter, ref int RoleId, ref string RoleName, ref int UserEmployeeId, ref string UserEmployeeName, ref string AllBranch)
        {
            try
            {
                object[] param = new object[4];
                param[0] = User_Name;
                param[1] = ObjLibrary.Encrypt(Password);
                param[2] = Company_ID;
                param[3] = ProfitCenter;
                DataTable dt = ObjDataServices.GetDataSet(SP_Authontication, param);
                if (dt.Rows.Count > 0)
                {
                    RoleId = int.Parse(dt.Rows[0]["RoleID"].ToString());
                    RoleName = dt.Rows[0]["strRoleName"].ToString();
                    UserEmployeeId = int.Parse(dt.Rows[0]["intEmployeeId"].ToString());
                    UserEmployeeName = dt.Rows[0]["strEmpName"].ToString();
                    AllBranch = dt.Rows[0]["strAllBranchFlag"].ToString();
                    return int.Parse(dt.Rows[0][0].ToString());
                }
                else
                    return 0;
                // return dt.Rows.Count > 0 ? int.Parse (dt.Rows [0][0].ToString ()) : 0;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region VerifyPassword()
        public int VerifyPassword(string User_Name, string Password, int Company_ID)
        {
            try
            {
                object[] param = new object[3];
                param[0] = User_Name;
                param[1] = ObjLibrary.Encrypt(Password);
                param[2] = Company_ID;
                DataTable dt = ObjDataServices.GetDataSet(SP_VerifyPassword, param);
                return dt.Rows.Count > 0 ? int.Parse(dt.Rows[0][0].ToString()) : 0;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region FillCombo()
        public void FillCombo(ref DropDownList DDL, string Path, int Company_ID)
        {
            try
            {
                object[] param = new object[1];
                param[0] = Company_ID;
                DataTable dt = new DataTable();
                if (File.Exists(Path.Trim() + "\\XML_DOC\\" + XMLFile_Name.ToString().Trim() + ".XML"))
                {
                    dt.ReadXmlSchema(Path.Trim() + "\\XML_DOC\\" + XMLFile_Name.ToString().Trim() + "Schema.XML");
                    dt.ReadXml(Path.Trim() + "\\XML_DOC\\" + XMLFile_Name.ToString().Trim() + ".XML");
                }
                else
                {
                    dt = ObjDataServices.GetDataSet(SP_BindProfitCenter, param);
                    dt.WriteXmlSchema(Path.Trim() + "\\XML_DOC\\" + XMLFile_Name.ToString().Trim() + "Schema.XML");
                    dt.WriteXml(Path.Trim() + "\\XML_DOC\\" + XMLFile_Name.ToString().Trim() + ".XML");
                }
                DDL.DataSource = dt;
                DDL.DataValueField = "intBranchid";
                DDL.DataTextField = "strBranchname";
                DDL.DataBind();

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region FillFinancialYear()
        public void FillFinancialYear(ref DropDownList DDL, string Path, int Company_ID)
        {
            try
            {
                object[] param = new object[1];
                param[0] = Company_ID;
                DataTable dt = new DataTable();
                if (File.Exists(Path.Trim() + "\\XML_DOC\\" + FinancialYearXMLFile_Name.ToString().Trim() + ".XML"))
                {
                    dt.ReadXmlSchema(Path.Trim() + "\\XML_DOC\\" + FinancialYearXMLFile_Name.ToString().Trim() + "Schema.XML");
                    dt.ReadXml(Path.Trim() + "\\XML_DOC\\" + FinancialYearXMLFile_Name.ToString().Trim() + ".XML");
                }
                else
                {
                    dt = ObjDataServices.GetDataSet(SP_BindFinancialYear, param);
                    dt.WriteXmlSchema(Path.Trim() + "\\XML_DOC\\" + FinancialYearXMLFile_Name.ToString().Trim() + "Schema.XML");
                    dt.WriteXml(Path.Trim() + "\\XML_DOC\\" + FinancialYearXMLFile_Name.ToString().Trim() + ".XML");
                }
                DDL.DataSource = dt;
                DDL.DataValueField = "strFYId";
                DDL.DataTextField = "FY";
                DDL.DataBind();
                string strCurrentFY;

                if (Convert.ToString(DateTime.Today.Month) == "1" || Convert.ToString(DateTime.Today.Month) == "2" || Convert.ToString(DateTime.Today.Month) == "3")
                {
                    strCurrentFY = Convert.ToString((Convert.ToInt64(DateTime.Today.Year) - 1)) + "-" + Convert.ToString((Convert.ToInt64(DateTime.Today.Year)));
                }
                else
                {
                    strCurrentFY = Convert.ToString(DateTime.Today.Year) + "-" + Convert.ToString((Convert.ToInt64(DateTime.Today.Year)) + 1);

                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (String.Compare(dt.Rows[i]["FY"].ToString(), strCurrentFY) == 0)
                    {
                        DDL.SelectedIndex = i;
                        break;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Save()
        public int SaveChangePassword(int User_Id, string Password, int Company_ID)
        {
            try
            {
                object[] param = new object[3];
                param[0] = User_Id;
                param[1] = ObjLibrary.Encrypt(Password);
                param[2] = Company_ID;
                int Count = ObjDataServices.SaveQuery(SP_SaveChangePassword, param);
                return Count;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int SaveUserLoginDetails(int User_Id, string IPAdress, int Company_ID, int Branch_Id, string strLoginDate, string strLoginTime, string strLogOffDate, string strLogOffTime)
        {
            try
            {
                object[] param = new object[9];
                param[0] = User_Id;
                param[1] = IPAdress;
                param[2] = Company_ID;
                param[3] = Branch_Id;
                param[4] = strLoginDate;
                param[5] = strLoginTime;
                param[6] = strLogOffDate;
                param[7] = strLogOffTime;
                param[8] = "OnLine";
                int Count = ObjDataServices.SaveQuery(Sp_SaveUserLoginDetailsInformation, param);
                return Count;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region CompanyValidation()
        public int CompanyValidation(string Company_ID, string Password, ref string strCompany, ref string strCompanyInformation, ref string strWebGridFlag)
        {
            try
            {
                object[] param = new object[2];
                param[0] = Company_ID;
                param[1] = Password;
                DataTable dt = ObjDataServices.GetDataSet(SP_CompanyAuthontication, param);

                if (dt.Rows.Count > 0)
                {
                    strCompany = dt.Rows[0]["strCompanyName"].ToString();
                    strCompanyInformation = dt.Rows[0]["strcompanyInformation"].ToString();
                    strWebGridFlag = dt.Rows[0]["strWebGridFlag"].ToString();
                    return int.Parse(dt.Rows[0][0].ToString());
                }
                else
                    return -1;

            }
            catch (Exception)
            {
                throw;
            }
        }
        public int CompanyValidation(string Company_ID, string Password, ref string strCompany, ref string strCompanyInformation, ref string strWebGridFlag, ref string strEnclosurePath)
        {
            try
            {
                object[] param = new object[2];
                param[0] = Company_ID;
                param[1] = Password;
                DataTable dt = ObjDataServices.GetDataSet(SP_CompanyAuthontication, param);

                if (dt.Rows.Count > 0)
                {
                    strCompany = dt.Rows[0]["strCompanyName"].ToString();
                    strCompanyInformation = dt.Rows[0]["strcompanyInformation"].ToString();
                    strWebGridFlag = dt.Rows[0]["strWebGridFlag"].ToString();
                    strEnclosurePath = dt.Rows[0]["strEnclosurePath"].ToString();
                    return int.Parse(dt.Rows[0][0].ToString());
                }
                else
                    return -1;

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
