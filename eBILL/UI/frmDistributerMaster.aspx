﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmDistributerMaster.aspx.cs" Inherits="eBILL.UI.frmDistributerMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Distributer Master</title>
    <!-- bootstrap 3.0.2 -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="CSS/AdminLTE.css" rel="stylesheet" />
    <link href="CSS/colorbox.css" rel="stylesheet" />
    <!-- Theme style -->
    <script src="JS/jquery.min.js" type="text/javascript"></script>
    <script src="../Script/jquery-1.8.3.js" type="text/javascript"></script>
    <%--<script src="../Script/jquery-ui-1.9.2.js" type="text/javascript"></script>--%>
    <%--<script src="../js/bootstrap.min.js" type="text/javascript"></script>--%>
    <script src="JS/jquery.colorbox.js" type="text/javascript"></script>
    <script src="../Script/KeyPress.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
        });

        function pageLoad(sender, args) {
            if (args.get_isPartialLoad()) {
                $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
            }
        }
        function loadPopup() {
            $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
        }

        function LoadPage() {
            $.colorbox.close();
            document.getElementById('LinkButton1').click();
        }

        function setValues(Values) {
            var ActualValues = Values.split(',');
            var HdnFieldName = "";
            for (var i = 0; i < ActualValues.length; i++) {
                var result = ActualValues[i].split('$');
                if (i == 0) {
                    HdnFieldName = result[0];
                }
                if (HdnFieldName == 'HIDDistCompany') {
                    document.getElementById(result[0]).value = result[1];
                }
            }
            if (HdnFieldName == 'HIDDistCompany') {
                var s_a = document.getElementById('SelectDistibutorCompany');
                s_a.href = 'frmSelection.aspx?form=' + 'frmDistributerMaster' + '&control0=' + 'HIDDistCompany' + '&control1=' + 'txtDistCompany' + '&Fld=' + 'strCompanyName' + '&StrVal=' + document.getElementById('txtDistCompany').value + '&SearchFire=Y';
            }
            $.colorbox.close();

            if (HdnFieldName == 'HIDDistCompany') {
                document.getElementById('LinkButton1').click();
            }
        }

        function setsearchButton(form, control0, control1, Fld, hrefid) {
            if (hrefid == 'SelectDistibutorCompany') {
                if (document.getElementById('SelectDistibutorCompany') != null) {
                    var s_a = document.getElementById('SelectDistibutorCompany');
                    s_a.href = 'frmSelection.aspx?form=' + form + '&control0=' + control0 + '&control1=' + control1 + '&Fld=' + Fld + '&StrVal=' + document.getElementById(control1).value + '&SearchFire=Y';
                }
            }
        }

        function ClosePopup(values) {
            if (values == "1") {
                alert('Record Update SuccessFully....');
            }
            if (values == "2") {
                alert('Record save SuccessFully...');
            }
            if (values == "3") {
                alert('Distributor can not be duplicated,enter another Distributor...');
            }
        }

        function CloseDiv() {
            parent.LoadPage();
        }

        function Validation() {
            if (document.getElementById('txtDistName').value == "") {
                alert('Please enter distributor name...!');
                return false;
            }
            else if (document.getElementById('HIDDistCompany').value == "") {
                alert('Please select company...!');
                document.getElementById('txtDistCompany').focus();
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server"
                    DynamicLayout="true">
                    <ProgressTemplate>
                        <div id="updPleaseWait" style="text-align: right; color: White; vertical-align: top; position: fixed; top: 300px; left: 600px">
                            <table style="background-color: #7cc3ef; width: 100%">
                                <tr>
                                    <td colspan="2" style="text-align: center; width: 100%">
                                        <b><font color="white">Please Wait...</font></b>
                                        <br />
                                        <img src="../vendor/lightbox2/dist/images/loading.gif" alt="Please wait...." title="Please wait...." />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div class="col-md-12" style="background: linear-gradient(45deg, #b74aef 0%, #da4726 100%);">
                    <div class="col-md-5">
                        <ol class="breadcrumb">
                            <li>
                                <asp:Button Text="Save" ID="btnSave" runat="server" CssClass="btn btn-primary btn-sm" OnCommand="Page_Command" CommandName="btnSave" OnClientClick="return Validation()" />
                                <asp:Button Text="Update" ID="btnUpdate" runat="server" CssClass="btn btn-primary btn-sm" OnCommand="Page_Command" CommandName="btnUpdate" OnClientClick="return Validation()" Visible="false" />
                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-primary btn-sm" OnCommand="Page_Command" CommandName="btnClear" />
                            </li>
                        </ol>
                    </div>
                    <div class="col-md-3" style="padding-top: 0px">
                        <span style="color: #fff">
                            <h4>Distributor Master</h4>
                        </span>
                    </div>
                    <div class="col-md-3" style="padding-top: 7px; margin-left: 100px;"><span style="color: #fff">Date/ Time :</span><span style="color: #fff; font-size: 12px;"> <%=System.DateTime.Now.ToString("dd-MMM-yyyy hh:mm") %></span></div>
                    <div class="col-md-1" style="padding-top: 7px"><span style="color: #fff"></span><span style="color: #fff; font-size: 12px;" runat="server" id="TimeMinu"></span></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Distibutor Code :</label>
                                    <asp:TextBox ID="txtDistCode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Distibutor Name :</label>
                                    <asp:TextBox ID="txtDistName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <ajaxToolkit:ComboBox runat="server" ID="cmdCompany" CssClass="form-control" Width="100%" DataSourceID="aGetCompany"></ajaxToolkit:ComboBox>
                                </div>
                                <div class="box-body">
                                    <div class="textwithselect form-group" id="Div1" runat="server">
                                        <label>Distibutor Company :</label>
                                        <asp:TextBox ID="txtDistCompany" runat="server" CssClass="form-control" AutoPostBack="true" ToolTip="Distibutor Company"></asp:TextBox>
                                        <a href="" id="SelectDistibutorCompany" class="iframe">
                                            <asp:Button ID="btnDistCompany" runat="server" CssClass="btn btn-primary btn-sm" Text="Select"
                                                TabIndex="6" ToolTip="Select Distibutor Company"></asp:Button></a>
                                        <asp:HiddenField ID="HIDDistCompany" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Address :</label>
                                    <asp:TextBox ID="txtDistAddress" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>State :</label>
                                    <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="0" Text="--Select State--"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Andaman and Nicobar Islands"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Andhra Pradesh"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Arunachal Pradesh"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Assam"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="Bihar"></asp:ListItem>
                                        <asp:ListItem Value="6" Text="Chandigarh"></asp:ListItem>
                                        <asp:ListItem Value="7" Text="Chhattisgarh"></asp:ListItem>
                                        <asp:ListItem Value="8" Text="Dadra and Nagar Haveli and Daman and Diu"></asp:ListItem>
                                        <asp:ListItem Value="9" Text="Delhi"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="Goa"></asp:ListItem>
                                        <asp:ListItem Value="11" Text="Gujarat"></asp:ListItem>
                                        <asp:ListItem Value="12" Text="Haryana"></asp:ListItem>
                                        <asp:ListItem Value="13" Text="Himachal Pradesh"></asp:ListItem>
                                        <asp:ListItem Value="14" Text="Jammu and Kashmir"></asp:ListItem>
                                        <asp:ListItem Value="15" Text="Jharkhand"></asp:ListItem>
                                        <asp:ListItem Value="16" Text="Karnataka"></asp:ListItem>
                                        <asp:ListItem Value="17" Text="Kerala"></asp:ListItem>
                                        <asp:ListItem Value="18" Text="Ladakh"></asp:ListItem>
                                        <asp:ListItem Value="19" Text="Lakshadweep"></asp:ListItem>
                                        <asp:ListItem Value="20" Text="Madhya Pradesh"></asp:ListItem>
                                        <asp:ListItem Value="21" Text="Maharashtra"></asp:ListItem>
                                        <asp:ListItem Value="22" Text="Manipur"></asp:ListItem>
                                        <asp:ListItem Value="23" Text="Meghalaya"></asp:ListItem>
                                        <asp:ListItem Value="24" Text="Mizoram"></asp:ListItem>
                                        <asp:ListItem Value="25" Text="Nagaland"></asp:ListItem>
                                        <asp:ListItem Value="26" Text="Odisha"></asp:ListItem>
                                        <asp:ListItem Value="27" Text="Puducherry"></asp:ListItem>
                                        <asp:ListItem Value="28" Text="Punjab"></asp:ListItem>
                                        <asp:ListItem Value="29" Text="Rajasthan"></asp:ListItem>
                                        <asp:ListItem Value="30" Text="Sikkim"></asp:ListItem>
                                        <asp:ListItem Value="31" Text="Tamil Nadu"></asp:ListItem>
                                        <asp:ListItem Value="32" Text="Telangana"></asp:ListItem>
                                        <asp:ListItem Value="33" Text="Tripura"></asp:ListItem>
                                        <asp:ListItem Value="34" Text="Uttar Pradesh"></asp:ListItem>
                                        <asp:ListItem Value="35" Text="Uttarakhand"></asp:ListItem>
                                        <asp:ListItem Value="36" Text="	West Bengal"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>City :</label>
                                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Distibutor Warehouse :</label>
                                    <asp:TextBox ID="txtDistWarehouse" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Phone No :</label>
                                    <asp:TextBox ID="txtDistPhone" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Mobile No :</label>
                                    <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Email :</label>
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>FAX :</label>
                                    <asp:TextBox ID="txtFAX" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>PIN :</label>
                                    <asp:TextBox ID="txtPINCode" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Close :</label>
                                    <asp:CheckBox ID="chkClose" runat="server" CssClass="form-control" OnCheckedChanged="chkClose_CheckedChanged" AutoPostBack="true" />
                                </div>
                            </div>
                        </div>
                        <div id="divClose" runat="server" visible="false">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body">
                                        <label>Close Date :</label>
                                        <asp:TextBox ID="txtCloseDate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" ImageUrl="../images/CALENDAR.GIF" ImageAlign="Bottom" runat="server" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="imgToDate"
                                            TargetControlID="txtCloseDate" Format="dd/MMM/yyyy" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body">
                                        <label>Close Remark :</label>
                                        <asp:TextBox ID="txtCloseRemark" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <asp:HiddenField ID="HIDDistId" runat="server" />
                <asp:LinkButton ID="LinkButton1" runat="server"></asp:LinkButton>
                <div runat="server" id="printdiv"></div>
            </ContentTemplate>
            <%-- <Triggers>
                <asp:PostBackTrigger ControlID="btnPrint" />
            </Triggers>--%>
        </asp:UpdatePanel>
    </form>
</body>
</html>
