﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks; 
using System.Web; 
using System.IO;
using System.Diagnostics;

namespace BusinessLayer
{
   public class ExceptionHandler
    {
       public static void LogError(Exception ex)
       {
           string errorDetails = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm");
           errorDetails += "\t" + ex.GetType().ToString();
           errorDetails += "\t" + requestURL();
           errorDetails += "\t" + ex.TargetSite.ToString();
           errorDetails += "\t" + ex.Message;
           errorDetails += "\t" + GetStackInfo();
           WriteToFile(errorDetails);
       }

       private static string GetLogFilePathName()
       {
           string logFilePathName = GetPathInfo(PathVariables.ErrorLogFile, true);
           logFilePathName += System.DateTime.Now.ToString("yyyy_MM_dd");
           logFilePathName += ".log";
           return logFilePathName;
       }

       public enum PathVariables
       {
           AppRoot,
           ThemeFolder,
           ErrorLogFile
       }

       public static string PhysicalApplicationPath()
       {
           try
           {
               return HttpContext.Current.Request.PhysicalApplicationPath;
           }
           catch (Exception Exception)
           {               
               return string.Empty;
           }
       }

       public static string ApplicationPath()
       {
           try
           {
               return HttpContext.Current.Request.ApplicationPath;
           }
           catch (Exception Exception)
           {               
               return string.Empty;
           }
       }

       public static string GetPathInfo(PathVariables pathName, bool returnFullPath)
       {
           string applicationPath = string.Empty;
           if (returnFullPath)
               applicationPath = PhysicalApplicationPath();
           else
               applicationPath = ApplicationPath();

           if (!(applicationPath.EndsWith(@"\") || applicationPath.EndsWith(@"/")))
               applicationPath += @"/"; // Forward Slash put instead of Backward Slash For 

           string sPathName = string.Empty;
           switch (pathName)
           {
               case PathVariables.AppRoot:
                   sPathName = string.Empty;
                   break;

               case PathVariables.ErrorLogFile:
                   sPathName = "Error/";
                   break;
           }

           sPathName = applicationPath + sPathName;

           if (!(sPathName.EndsWith(@"\") || sPathName.EndsWith(@"/")))
               sPathName += @"\";

           return sPathName;
       }

       private static void WriteToFile(string errorDetails)
       {
           try
           {
               string logFilePathName = GetLogFilePathName();
               using (StreamWriter sw = new StreamWriter(logFilePathName, true))
               {
                   sw.WriteLine(errorDetails);
                   sw.Flush();
                   sw.Close();
               }
           }
           catch (Exception ex)
           {
               //_common.ExceptionHandler.LogError(ex);
               //exception handler to exception handler !!!!????
           }
       }

       private static string requestURL()
       {
           try
           {
               if (requestURL1() == null)
                   return string.Empty;
               else
                   return requestURL1();
           }
           catch (Exception ex)
           {              
               return string.Empty;
           }
       }

       public static string requestURL1()
       {
           try
           {
               return HttpContext.Current.Request.Url.AbsoluteUri;
           }
           catch (Exception Exception)
           {               
               return string.Empty;
           }
       }

       private static string GetStackInfo()
       {
           string stackInfo = string.Empty;
           try
           {
               StackTrace st = new StackTrace(false);
               StackFrame sf = st.GetFrame(1);    // just going one level above in the stacktrace             
               stackInfo = sf.GetMethod().DeclaringType.Name + ":" + sf.GetMethod().Name;

               sf = st.GetFrame(2);
               stackInfo += Environment.NewLine;
               stackInfo = sf.GetMethod().DeclaringType.Name + ":" + sf.GetMethod().Name;

               sf = st.GetFrame(3);
               stackInfo += Environment.NewLine;
               stackInfo = sf.GetMethod().DeclaringType.Name + ":" + sf.GetMethod().Name;
           }
           catch (Exception ex)
           {
               //_common.ExceptionHandler.LogError(ex);
               // do nothing
           }
           return stackInfo;
       }
    }
}
