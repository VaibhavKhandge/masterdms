﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using BusinessLayer;

namespace eBILL.UI
{
    public partial class frmWorkFlow : System.Web.UI.Page
    {
        protected Library objLibrary = new Library();
        protected AlertQueryConstant ObjAlertQueryConstant = new AlertQueryConstant();
        protected MasterCommotion ObjMasterComm = new MasterCommotion();
        protected static int i = 0;
        protected static int count = 0;

        # region [Branch_ID Property]
        protected int Branch_ID
        {
            get { return ViewState["Branch_ID"] != null ? (Int32)ViewState["Branch_ID"] : 0; }
            set { ViewState["Branch_ID"] = value; }
        }
        # endregion

        # region [Company_ID Property]
        protected int Company_ID
        {
            get { return ViewState["Company_ID"] != null ? (Int32)ViewState["Company_ID"] : 0; }
            set { ViewState["Company_ID"] = value; }
        }
        # endregion

        # region [FY_ID Property]
        protected int FY_ID
        {
            get { return ViewState["FY_ID"] != null ? (Int32)ViewState["FY_ID"] : 0; }
            set { ViewState["FY_ID"] = value; }
        }
        # endregion

        # region [Role_ID Property]
        protected int Role_ID
        {
            get { return ViewState["Role_ID"] != null ? (Int32)ViewState["Role_ID"] : 0; }
            set { ViewState["Role_ID"] = value; }
        }
        # endregion

        # region [UserName Property]
        protected string UserName
        {
            get { return ViewState["UserName"] != null ? (string)ViewState["UserName"] : ""; }
            set { ViewState["UserName"] = value; }
        }
        # endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    UserName = Session["UserName"].ToString();
                    Branch_ID = Convert.ToInt32(Session["BranchID"].ToString());
                    Company_ID = Convert.ToInt32(Session["CompanyID"].ToString());
                    FY_ID = Convert.ToInt32(Session["FinancialYearId"].ToString());
                    Role_ID = Convert.ToInt32(Session["RoleId"].ToString());

                    HiddenField hdnModuleNameDisplay = (HiddenField)Master.FindControl("hdnModuleNameDisplay");
                    hdnModuleNameDisplay.Value = "0";

                    Session["chkViewsVal"] = "false";
                    if (Session["ModuleName"] != null && Session["ModuleName"].ToString() != "")
                    {
                        CheckBox chkMasterForms = (CheckBox)Master.FindControl("chkMasterForms");
                        CheckBox chkTranFroms = (CheckBox)Master.FindControl("chkTranFroms");
                        CheckBox chkAlerts = (CheckBox)Master.FindControl("chkAlerts");
                        CheckBox chkReports = (CheckBox)Master.FindControl("chkReports");
                        CheckBox chkViews = (CheckBox)Master.FindControl("chkViews");

                        if (chkMasterForms.Checked)
                            Session["MasterFormChkVal"] = "true";
                        else
                            Session["MasterFormChkVal"] = "false";

                        if (chkTranFroms.Checked)
                            Session["TranFormChkVal"] = "true";
                        else
                            Session["TranFormChkVal"] = "false";

                        if (chkAlerts.Checked)
                            Session["chkAlertsVal"] = "true";
                        else
                            Session["chkAlertsVal"] = "false";

                        if (chkReports.Checked)
                            Session["chkReportsVal"] = "true";
                        else
                            Session["chkReportsVal"] = "false";

                        if (chkViews.Checked)
                            Session["chkViewsVal"] = "false";
                        else
                            Session["chkViewsVal"] = "false";

                        bindHomePage();
                        Timer1.Interval = 600000;
                    }
                    else
                    {
                        Session["MasterFormChkVal"] = "true";
                        Session["TranFormChkVal"] = "true";
                        Session["chkAlertsVal"] = "true";
                        Session["chkReportsVal"] = "true";
                        Session["chkViewsVal"] = "false";
                        Session["ModuleName"] = "All";
                        bindHomePage();
                    }
                }
            }
            catch (Exception vx)
            {
                ExceptionHandler.LogError(vx);
            }
        }

        /// <summary>
        /// Bind Workflow (Home Page Content) on direct page load without session..
        /// </summary>
        protected void bindHomePage()
        {
            try
            {
                // code to get Module Wise Master Pages Data Table
                DataTable masterFormsDt = new DataTable();
                DataSet masterDS = new DataSet();

                masterDS = objLibrary.FillDataSet("ssp_GetDisplayModulewiseMasterForm", Company_ID.ToString(), Branch_ID.ToString(), Role_ID.ToString(), Session["ModuleName"].ToString());
                masterFormsDt = masterDS.Tables[0];
                Session["MasterFormDataTable"] = masterFormsDt;

                // Code to get Module Wise Transaction Pages
                DataTable TransactionFormsTable = new DataTable();
                DataSet tranDS = new DataSet();
                tranDS = objLibrary.FillDataSet("ssp_GetDisplayMasterwiseTransactionNameNew", Company_ID.ToString(), Branch_ID.ToString(), Role_ID.ToString(), Session["ModuleName"].ToString());
                TransactionFormsTable = tranDS.Tables[0];
                Session["TransactionFormsDataTable"] = TransactionFormsTable;

                // Code to get Favorite Menu
                DataTable aDtFavoritemenu = new DataTable();
                aDtFavoritemenu = ObjMasterComm.GetTblFavoriteMenu();
                Session["FavoriteFormsHtmlDataTable"] = aDtFavoritemenu;

                // Code to get Role wise  Alerts
                DataTable alertsDataTable = new DataTable();
                DataSet alertDS = new DataSet();
                alertDS = objLibrary.FillDataSet("ssp_BindAlertQueryTypeWithOutBranchForNew", Company_ID.ToString(), Branch_ID.ToString(), FY_ID.ToString(), Role_ID.ToString(), Session["UserID"].ToString());
                alertsDataTable = alertDS.Tables[0];
                Session["AlertsDataTable"] = alertsDataTable;

                // code to get Module Wise Report Pages Data Table
                DataTable reportPagesDt = new DataTable();
                DataSet reportDS = new DataSet();
                reportDS = objLibrary.FillDataSet("ssp_GetDisplayModulewiseReports", Company_ID.ToString(), Branch_ID.ToString(), Role_ID.ToString(), Session["ModuleName"].ToString());
                reportPagesDt = reportDS.Tables[0];
                Session["reportPagesDataTable"] = reportPagesDt;

                // code to get Module Wise Report Pages Data Table
                DataTable ViewsDt = new DataTable();
                DataSet viewsDS = new DataSet();
                viewsDS = objLibrary.FillDataSet("ssp_GetMdReportNameForDasBoared", Company_ID.ToString(), Branch_ID.ToString(), Role_ID.ToString(), Session["ModuleName"].ToString());
                ViewsDt = viewsDS.Tables[0];
                Session["ViewsDataTable"] = ViewsDt;

                //********************************************************************
                string aMasterFormsHtml = string.Empty;
                string aMasterFormsHtmlTable = string.Empty;
                CheckBox chkMasterForms = (CheckBox)Master.FindControl("chkMasterForms");
                if (aMasterFormsHtml == string.Empty)
                {
                    if (Session["MasterFormChkVal"].ToString() == "true")
                    {
                        if (masterFormsDt.Rows.Count > 0)
                        {
                            aMasterFormsHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='divMasterDiv'>" + // changed 4 to 12
                                                 "<div id='DivT2' runat='server' class='x_panel tile fixed_height_320' style='height: auto;'>" +
                                                 "<div style='border-radius:5px;border-color:black'>" +
                                                 "<div class='x_title HeaderInnerMenu' >" +
                                                 "<h2 style='font-color:White;margin-top:2px;'>Masters </h2>" +
                                                 "<ul class='nav navbar-right panel_toolbox'>" +
                                                 "<li><a class='collapse-link' onclick='javascript:return OpenClosed(2)'><i class='fa fa-chevron-up'></i></a></li>" +
                                                 "<li><a href='#' id='OpenMaster'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                                 "<li><a class='close-link' ID='Master' onclick='javascript:return setDivPosition(divMasterDiv)' ><i><lable class='fa fa-close' runat ='server'></lable></i></a>" +
                                                 "</li>" +
                                                 "</ul>" +
                                                 "<div class='clearfix'></div>" +

                                                 "</div></div>" +
                                                  "<div class='x_content' style='display : none;' id='divS2'>" +
                                                  "<div runat='server' id='divTable' class='divtable table-responsive'>" +
                                                  "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            string mainLinkFromDb = string.Empty;

                            for (int i = 0; i < masterFormsDt.Rows.Count; i++)
                            {
                                mainLinkFromDb = masterFormsDt.Rows[i]["strNavigateUrl"].ToString();
                                mainLinkFromDb = mainLinkFromDb.Substring(5);
                                aMasterFormsHtmlTable += "<tr>" +
                                                         "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'><a href='" + masterFormsDt.Rows[i]["strFormUserManualFile"].ToString() + "' target='_blank'>" + (i + 1).ToString() + "</a></div></td>" +
                                                         "<td><a href='" + mainLinkFromDb + "' target='_blank'>" + masterFormsDt.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                                         "</tr>";
                            }
                            aMasterFormsHtml += aMasterFormsHtmlTable +
                                            "</tbody></table></div></div></div></div>";
                            chkMasterForms.Enabled = true;
                            Session["MasterFormChkVal"] = "true";
                        }
                        else
                        {
                            aMasterFormsHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='divMasterDiv'>" + // changed 4 to 12
                                                 "<div id='DivT2' runat='server' class='x_panel tile fixed_height_320' style='height: auto;'>" +
                                                 "<div style='border-radius:5px;border-color:black'>" +
                                                 "<div class='x_title HeaderInnerMenu' >" +
                                                 "<h2 style='font-color:White;margin-top:2px;'>Masters </h2>" +
                                                 "<ul class='nav navbar-right panel_toolbox'>" +
                                                 "<li><a class='collapse-link' onclick='javascript:return OpenClosed(2)'><i class='fa fa-chevron-up'></i></a></li>" +
                                                 "<li><a href='#' id='OpenMaster'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                                 "<li><a class='close-link' ID='Master' onclick='javascript:return setDivPosition(divMasterDiv)' ><i ><lable class='fa fa-close' runat ='server' ></lable></i></a>" +
                                                 "</li>" +
                                                 "</ul>" +
                                                 "<div class='clearfix'></div>" +
                                                  "</div></div>" +
                                                  "<div class='x_content' style='display : none;' id='divS2'>" +
                                                  "<div runat='server' id='divTable' class='divtable table-responsive'>" +
                                                  "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            aMasterFormsHtml += "</tbody></table></div></div></div></div>";
                            chkMasterForms.Checked = false;
                            chkMasterForms.Enabled = false;
                            Session["MasterFormChkVal"] = "false";
                        }
                    }
                    else
                    {
                        aMasterFormsHtml = "";
                        Session["MasterFormChkVal"] = "false";
                    }
                    Session["MasterForms"] = aMasterFormsHtml;

                    //****************Preparation of Favorite Menu***************//
                    string aFavoriteFormsHtml = string.Empty;
                    string aFavoriteFormsHtmlTable = string.Empty;
                    if (aDtFavoritemenu.Rows.Count > 0)
                    {
                        aFavoriteFormsHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='Divfavorite'>" + // changed 4 to 12
                                             "<div id='MasterFormdiv' runat='server' class='x_panel tile fixed_height_560' >" +
                                             "<div style='border-radius:5px;border-color:black'>" +
                                             "<div class='x_title HeaderInnerMenu' >" +
                                             "<h2 style='font-color:White;margin-top:2px;'>Favorite </h2>" +
                                             "<ul class='nav navbar-right panel_toolbox'>" +
                                             "<li><a href='frmManageFavorite.aspx' class='iframe'><i class='fa fa-wrench'></i></a></li>" +
                                             "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                             "<li><a href='#' id='OpenMaster'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                             "<li><a class='close-link' ID='Master' onclick='javascript:return setDivPosition(Divfavorite)' ><i ><lable class='fa fa-close' runat ='server' ></lable></i></a>" +
                                             "</li>" +
                                             "</ul>" +
                                             "<div class='clearfix'></div>" +
                                              "</div></div>" +
                                              "<div class='x_content'>" +
                                              "<div runat='server' id='divTable' class='divtableNew2 table-responsive'>" +
                                              "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";

                        string mainLinkFromDb = string.Empty;
                        string aFavoriteHeader = string.Empty;
                        int j = 0;
                        for (int i = 0; i < aDtFavoritemenu.Rows.Count; i++)
                        {
                            mainLinkFromDb = aDtFavoritemenu.Rows[i]["strUrl"].ToString();
                            if (i == 0)
                            {
                                aFavoriteFormsHtmlTable += "<tr>" +
                                                     "<td colspan='2'><h1>User Favorite</h1></td>" +
                                                     "</tr>";
                                aFavoriteHeader = aDtFavoritemenu.Rows[i]["StrType"].ToString();
                                aFavoriteFormsHtmlTable += "<tr>" +
                                                     "<td colspan='2' >" + aDtFavoritemenu.Rows[i]["StrType"].ToString() + "</td>" +
                                                     "</tr>";
                                j = j + 1;
                            }
                            else
                            {
                                if (aFavoriteHeader == aDtFavoritemenu.Rows[i]["StrType"].ToString())
                                {
                                    j = j + 1;
                                }
                                else
                                {
                                    aFavoriteHeader = aDtFavoritemenu.Rows[i]["StrType"].ToString();
                                    aFavoriteFormsHtmlTable += "<tr>" +
                                                         "<td colspan='2' >" + aDtFavoritemenu.Rows[i]["StrType"].ToString() + "</td>" +
                                                         "</tr>";
                                    j = 0;
                                    j = j + 1;
                                }
                            }
                            aFavoriteFormsHtmlTable += "<tr>" +
                                                     "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'><a href='" + aDtFavoritemenu.Rows[i]["strFormUserManualFile"].ToString() + "' target='_blank'>" + (j).ToString() + "</a></div></td>" +
                                                     "<td><a href='" + mainLinkFromDb + "' target='_blank'>" + aDtFavoritemenu.Rows[i]["strMenu"].ToString() + "</a></td>" +
                                                     "</tr>";
                        }
                        aFavoriteFormsHtml += aFavoriteFormsHtmlTable +
                                        "</tbody></table></div></div></div></div>";
                    }
                    else
                    {
                        aFavoriteFormsHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='Divfavorite'>" + // changed 4 to 12
                                              "<div id='MasterFormdiv' runat='server' class='x_panel tile fixed_height_560' style='height: auto;' >" +
                                              "<div style='border-radius:5px;border-color:black'>" +
                                              "<div class='x_title HeaderInnerMenu' >" +
                                              "<h2 style='font-color:White;margin-top:2px;'>Favorite </h2>" +
                                              "<ul class='nav navbar-right panel_toolbox'>" +
                                              "<li><a href='frmManageFavorite.aspx' class='iframe'><i class='fa fa-wrench'></i></a></li>" +
                                              "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                              "<li><a href='#' id='OpenMaster'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                              "<li><a class='close-link' ID='Master' onclick='javascript:return setDivPosition(Divfavorite)' ><i ><lable class='fa fa-close' runat ='server' ></lable></i></a>" +
                                              "</li>" +
                                              "</ul>" +
                                              "<div class='clearfix'></div>" +
                                               "</div></div>" +
                                               "<div class='x_content' style='display : none;'>" +
                                               "<div runat='server' id='divTable' class='divtableNew2 table-responsive'>" +
                                               "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                        aFavoriteFormsHtml +=
                                       "</tbody></table></div></div></div></div>";
                    }
                    Session["aFavoriteFormsHtml"] = aFavoriteFormsHtml;

                    //========================== Inner html preparation for transaction page Table
                    string transactionFormsInnerHtml = string.Empty;
                    string transactionFormsHtmlTable = string.Empty;
                    CheckBox chkTranFroms = (CheckBox)Master.FindControl("chkTranFroms");
                    if (Session["TranFormChkVal"].ToString() == "true")
                    {
                        if (TransactionFormsTable.Rows.Count > 0)
                        {
                            transactionFormsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12 ' style='padding:0px;' id='divTransaction'>" +
                                                          "<div class='x_panel transheight fixed_height_340' id='DivT1'>" +
                                                          "<div style='border-radius:5px;border-color:black'>" +
                                                          "<div class='x_title HeaderInnerMenu' >" +
                                                          "<h2 style='font-color:White;margin-top:2px;'>Transaction </h2>" +
                                                          "<ul class='nav navbar-right panel_toolbox'>" +
                                                          "<li><a class='collapse-link' onclick='javascript:return OpenClosed(1)' ><i class='fa fa-chevron-up'></i></a></li>" +
                                                          "<li><a href='#' id='OpenTran'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                                          "<li><a class='close-link' ID='Transaction' onclick='javascript:return setDivPosition(divTransaction)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                                          "</li>" +
                                                          "</ul>" +
                                                          "<div class='clearfix'></div>" +
                                                          "</div></div>" +
                                                          "<div class='x_content' id='divS1'>" +
                                                          "<div runat='server' id='divTable'  class='divtableNew table-responsive'>" +
                                                          "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            string mainLinkFromDbforTran = string.Empty;

                            for (int i = 0; i < TransactionFormsTable.Rows.Count; i++)
                            {
                                mainLinkFromDbforTran = TransactionFormsTable.Rows[i]["strNavigateUrl"].ToString();
                                mainLinkFromDbforTran = mainLinkFromDbforTran.Substring(5);
                                transactionFormsHtmlTable += "<tr>" +
                                                             "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'><a href='" + TransactionFormsTable.Rows[i]["strFormUserManualFile"].ToString() + "' target='_blank'>" + (i + 1).ToString() + "</a></div></td>" +
                                                             "<td><a href='" + mainLinkFromDbforTran + "' target='_blank'>" + TransactionFormsTable.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                                             "</tr>";
                            }
                            transactionFormsInnerHtml += transactionFormsHtmlTable +
                                               "</tbody></table></div></div></div></div>";
                            chkTranFroms.Enabled = true;
                            Session["TranFormChkVal"] = "true";
                        }
                        else
                        {
                            transactionFormsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12 ' style='padding:0px;' id='divTransaction'>" +
                                                           "<div class='x_panel transheight fixed_height_340' style='height: auto;' id='DivT1'>" +
                                                           "<div style='border-radius:5px;border-color:black'>" +
                                                           "<div class='x_title HeaderInnerMenu' >" +
                                                           "<h2 style='font-color:White;margin-top:2px;'>Transaction </h2>" +
                                                           "<ul class='nav navbar-right panel_toolbox'>" +
                                                           "<li><a class='collapse-link' onclick='javascript:return OpenClosed(1)'><i class='fa fa-chevron-up'></i></a></li>" +
                                                           "<li><a href='#' id='OpenTran'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                                           "<li><a class='close-link' ID='Transaction' onclick='javascript:return setDivPosition(divTransaction)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                                           "</li>" +
                                                           "</ul>" +
                                                           "<div class='clearfix'></div>" +
                                                           "</div></div>" +
                                                           "<div class='x_content' style='display : none;' id='divS1'>" +
                                                           "<div runat='server' id='divTable'  class='divtableNew table-responsive'>" +
                                                           "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            transactionFormsInnerHtml +=
                                              "</tbody></table></div></div></div></div>";
                            chkTranFroms.Checked = false;
                            chkTranFroms.Enabled = false;
                            Session["TranFormChkVal"] = "false";
                        }
                    }
                    else
                    {
                        transactionFormsInnerHtml = "";
                        Session["TranFormChkVal"] = "false";
                    }
                    Session["TransactionForms"] = transactionFormsInnerHtml;

                    // ======================================== preparation of innerhtml of Alerts
                    string alertsInnerHtml = string.Empty;
                    string alertsHtmlTable = string.Empty;
                    CheckBox chkAlerts = (CheckBox)Master.FindControl("chkAlerts");
                    if (Session["chkAlertsVal"].ToString() == "true")
                    {
                        if (alertsDataTable.Rows.Count > 0)
                        {
                            alertsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='DivAlerts'>" +
                                              "<div class='x_panel tile fixed_height_560'>" +
                                              "<div style='border-radius:5px;border-color:black'>" +
                                              "<div class='x_title HeaderInnerMenu' >" +
                                              "<h2 style='font-color:White;margin-top:2px;'>Alerts </h2>" +
                                              "<ul class='nav navbar-right panel_toolbox'>" +
                                              "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                              "<li><a href='#' id='OpenAlerts'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                              "<li><a class='close-link' ID='Alerts' onclick='javascript:return setDivPosition(DivAlerts)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                              "</li>" +
                                              "</ul>" +
                                                "<div class='clearfix'></div>" +
                                              "</div></div>" +
                                              "<div class='x_content'>" +
                                              "<div runat='server' id='divTable'  class='divtableNew2 table-responsive'>" +
                                              "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            for (int i = 0; i < alertsDataTable.Rows.Count; i++)
                            {
                                alertsHtmlTable += "<tr>" +
                                                   "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'>" + (i + 1).ToString() + "</div></td>" +
                                                   "<td width='10px'><img style='cursor:pointer' src='../Images/print1.gif' height='20px' width='20px' id='" + alertsDataTable.Rows[i]["intAlertQueryId"].ToString() + "' onclick='javascript:return getAlertPrint(this)' /></td>" +
                                                   "<td><a href='#' onclick='javascript:return getAlertDetails(this)' style='cursor:pointer'  id='" + alertsDataTable.Rows[i]["intAlertQueryId"].ToString() + "'>" + alertsDataTable.Rows[i]["strAlertType"].ToString() + "</a></td>" +
                                                   "</tr>";
                            }
                            alertsInnerHtml += alertsHtmlTable +
                                               "</tbody></table></div></div></div></div>";
                            chkAlerts.Enabled = true;
                            Session["chkAlertsVal"] = "true";
                        }
                        else
                        {
                            alertsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='DivAlerts'>" +
                                               "<div class='x_panel tile fixed_height_560' style='height: auto;'>" +
                                               "<div style='border-radius:5px;border-color:black'>" +
                                               "<div class='x_title HeaderInnerMenu' >" +
                                               "<h2 style='font-color:White;margin-top:2px;'>Alerts </h2>" +
                                               "<ul class='nav navbar-right panel_toolbox'>" +
                                               "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                               "<li><a href='#' id='OpenAlerts'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                               "<li><a class='close-link' ID='Alerts' onclick='javascript:return setDivPosition(DivAlerts)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                               "</li>" +
                                               "</ul>" +
                                               "<div class='clearfix'></div>" +
                                               "</div></div>" +
                                               "<div class='x_content' style='display : none;'>" +
                                               "<div runat='server' id='divTable'  class='divtableNew2 table-responsive'>" +
                                               "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            chkAlerts.Enabled = false;
                            chkAlerts.Checked = false;
                            Session["chkAlertsVal"] = "false";
                        }
                    }
                    else
                    {
                        alertsInnerHtml = "";
                        Session["chkAlertsVal"] = "false";
                    }
                    Session["AlertsForms"] = alertsInnerHtml;

                    //================= Concatination of Upper Row Divs.....(Master Forms/Transactions/Alerts)
                    string firstRowEndString = //"</div>" + // Commented to get all div in sequence 
                                         " <div class='modal fade bs-example-modal-lg' tabindex='-1' role='dialog' aria-hidden='true'>" +
                                         " <div class='modal-dialog modal-lg'>" +
                                         "<div class='modal-content'>" +
                                         "<div class='modal-header'>" +
                                         "<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>×</span></button>" +
                                         "<h4 class='modal-title' id='myModalLabel'>" + "Sample Form Name" + "</h4>" +
                                         "</div>" +
                                         "<div class='modal-body'>" +
                                         "<h4>Text in a modal</h4>" +
                                         "<p>Aenean lacinia bibendum,sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>" +
                                         "</div>" +
                                         "<div class='modal-footer'>" +
                                         "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
                                         "</div>" +
                                         "</div>" +
                                         "</div>" +
                                         "</div>";
                    alertsInnerHtml += "</tbody></table></div></div></div></div>";

                    Session["FirstRowEndString"] = firstRowEndString;
                    string endString = firstRowEndString;

                    // preparation of innerhtml of Reports
                    string reportsInnerHtml = string.Empty;
                    string reportsHtmlTable = string.Empty;

                    CheckBox chkReports = (CheckBox)Master.FindControl("chkReports");
                    if (Session["chkReportsVal"].ToString() == "true")
                    {
                        if (reportPagesDt.Rows.Count > 0)
                        {
                            reportsInnerHtml = "<div  class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='Divreport'>" +
                                               "<div  id='DivT3' class='x_panel tile fixed_height_320' style='height: auto;'>" +
                                               "<div style='border-radius:5px;border-color:black'>" +
                                               "<div class='x_title HeaderInnerMenu' >" +
                                               "<h2 style='font-color:White;margin-top:2px;'>Reports </h2>" +
                                               "<ul class='nav navbar-right panel_toolbox'>" +
                                               "<li><a class='collapse-link' onclick='javascript:return OpenClosed(3)'><i class='fa fa-chevron-up'></i></a></li>" +
                                               "<li><a href='#' ID='OpenReports'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                               "<li><a class='close-link' ID='Reports' onclick='javascript:return setDivPosition(Divreport)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                               "</li>" +
                                               "</ul>" +
                                               "<div class='clearfix'></div>" +
                                               "</div></div>" +
                                               "<div class='x_content' style='display : none;' id='divS3'>" +
                                               "<div runat='server' id='divTable'  class='divtable table-responsive'>" +
                                               "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            for (int i = 0; i < reportPagesDt.Rows.Count; i++)
                            {
                                reportsHtmlTable += "<tr>" +
                                                         "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'>" + (i + 1).ToString() + "</div></td>" +
                                                         "<td><a href='#' onclick='javascript:return OpenReport(" + i + ")'>" + reportPagesDt.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                                         "</tr>";
                            }
                            reportsInnerHtml += reportsHtmlTable +
                                               "</tbody></table></div></div></div></div>";
                            chkReports.Enabled = true;
                            Session["chkReportsVal"] = "true";
                        }
                        else
                        {
                            reportsInnerHtml = "<div  class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='Divreport'>" +
                                                "<div  id='DivT3' class='x_panel tile fixed_height_320' style='height: auto;'>" +
                                                "<div style='border-radius:5px;border-color:black'>" +
                                                "<div class='x_title HeaderInnerMenu' >" +
                                                "<h2 style='font-color:White;margin-top:2px;'>Reports </h2>" +
                                                "<ul class='nav navbar-right panel_toolbox'>" +
                                                "<li><a class='collapse-link' onclick='javascript:return OpenClosed(3)'><i class='fa fa-chevron-up'></i></a></li>" +
                                                "<li><a href='#' ID='OpenReports'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                                "<li><a class='close-link' ID='Reports' onclick='javascript:return setDivPosition(Divreport)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                                "</li>" +
                                                "</ul>" +
                                                "<div class='clearfix'></div>" +
                                                "</div></div>" +
                                                "<div class='x_content' style='display : none;' id='divS3'>" +
                                                "<div runat='server' id='divTable'  class='divtable table-responsive'>" +
                                                "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            reportsInnerHtml +=
                                               "</tbody></table></div></div></div></div>";
                            chkReports.Enabled = false;
                            chkReports.Checked = false;
                            Session["chkReportsVal"] = "false";
                        }
                    }
                    else
                    {
                        reportsInnerHtml = "";
                        Session["chkReportsVal"] = "false";
                    }
                    Session["ReportsForms"] = reportsInnerHtml;

                    //================== Preparation of Views Forms Div ============================
                    string viewsInnerHtml = string.Empty;
                    string viewsHtmlTable = string.Empty;
                    CheckBox chkViews = (CheckBox)Master.FindControl("chkViews");
                    if (Session["chkViewsVal"].ToString() == "true")
                    {
                        if (ViewsDt.Rows.Count > 0)
                        {
                            viewsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='DivView'>" +
                                             "<div class='x_panel tile fixed_height_320' style='height: auto;' id='DivT4'>" +
                                             "<div style='border-radius:5px;border-color:black'>" +
                                             "<div class='x_title HeaderInnerMenu' >" +
                                             "<h2 style='font-color:White;margin-top:2px;'>Views </h2>" +
                                             "<ul class='nav navbar-right panel_toolbox'>" +
                                             "<li><a class='collapse-link' onclick='javascript:return OpenClosed(4)'><i class='fa fa-chevron-up'></i></a></li>" +
                                             "<li><a href='#' id='OpenViews'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                             "<li><a class='close-link' ID='Views' onclick='javascript:return setDivPosition(DivView)'  ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                             "</li>" +
                                             "</ul>" +
                                             "<div class='clearfix'></div>" +
                                             "</div></div>" +
                                             "<div class='x_content' style='display : none;' id='divS4'>" +
                                             "<div runat='server' id='divTable'  class='divtable table-responsive'>" +
                                             "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            for (int i = 0; i < ViewsDt.Rows.Count; i++)
                            {
                                viewsHtmlTable += "<tr>" +
                                                  "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'>" + (i + 1).ToString() + "</div></td>" +
                                                  "<td><a href='#'>" + ViewsDt.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                                  "</tr>";
                            }
                            viewsInnerHtml += viewsHtmlTable +
                                               "</tbody></table></div></div></div></div>";
                            chkViews.Enabled = true;
                            Session["chkViewsVal"] = "false";
                        }
                        else
                        {
                            viewsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='DivView'>" +
                                              "<div class='x_panel tile fixed_height_320' style='height: auto;' id='DivT4'>" +
                                              "<div style='border-radius:5px;border-color:black'>" +
                                              "<div class='x_title HeaderInnerMenu' >" +
                                              "<h2 style='font-color:White;margin-top:2px;'>Views </h2>" +
                                              "<ul class='nav navbar-right panel_toolbox'>" +
                                              "<li><a class='collapse-link' onclick='javascript:return OpenClosed(4)'><i class='fa fa-chevron-up'></i></a></li>" +
                                              "<li><a href='#' id='OpenViews'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                              "<li><a class='close-link' ID='Views' onclick='javascript:return setDivPosition(DivView)'  ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                              "</li>" +
                                              "</ul>" +
                                              "<div class='clearfix'></div>" +
                                              "</div></div>" +
                                              "<div class='x_content' style='display : none;' id='divS4'>" +
                                              "<div runat='server' id='divTable'  class='divtable table-responsive'>" +
                                              "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            viewsInnerHtml +=
                                               "</tbody></table></div></div></div></div>";
                            chkViews.Enabled = false;
                            Session["chkViewsVal"] = "false";
                            chkViews.Checked = false;
                        }
                    }
                    else
                    {
                        viewsInnerHtml = "";
                        Session["chkViewsVal"] = "false";
                    }
                    Session["ViewsForms"] = viewsInnerHtml;

                    string FCStrat = "<div id='column1MainDiv' runat='server'>" +
                                                                     "<div class='col-md-4 col-sm-4 col-xs-12 fixed_height_560' style='overflow:auto;'>" +
                                                                        " <div id='column1Div' >";
                    Session["FirstColumnStart"] = FCStrat;

                    string FCEnd = "</div></div></div>";
                    Session["FirstColumnEnd"] = FCEnd;

                    string SCStart = "<div id='column2Div' runat='server'>" +
                                                                    "<div class='col-md-4 col-sm-4 col-xs-12 fixed_height_560' style='overflow:auto;'>";
                    Session["SecondColumnStart"] = SCStart;
                    string SCEnd = "</div></div>";
                    Session["SecondColumnEnd"] = SCEnd;

                    string TCStart = "<div id='column2Div' runat='server'>" +
                                     "<div class='col-md-4 col-sm-4 col-xs-12 fixed_height_560' style='overflow:auto;'>" +
                                     "<div >";
                    Session["ThirdColumnStart"] = TCStart;
                    string TCEnd = "</div></div></div>";
                    Session["ThirdColumnEnd"] = TCEnd;
                    setDivPosition(aMasterFormsHtml, transactionFormsInnerHtml, alertsInnerHtml, endString, reportsInnerHtml, viewsInnerHtml, FCStrat, FCEnd, SCStart, SCEnd, TCStart, TCEnd, aFavoriteFormsHtml);
                }
            }
            catch (Exception vx)
            {
                ExceptionHandler.LogError(vx);
            }
        }

        /// <summary>
        /// Bind Workflow page content using session values (Comes From Master Page)
        /// </summary>
        protected void bindMasterFormsDiv()
        {
            // ============== Preparation for Master Forms Div =========================
            string aMasterFormsHtml = string.Empty;
            if (Session["MasterForms"] != null && Session["MasterForms"].ToString() != "")
            {
                aMasterFormsHtml = Session["MasterForms"].ToString();
            }

            //============== Preparation for transaction page ==========================
            string transactionFormsInnerHtml = string.Empty;
            if (Session["TransactionForms"] != null && Session["TransactionForms"].ToString() != "")
            {
                transactionFormsInnerHtml = Session["TransactionForms"].ToString();
            }

            //============== preparation of Alerts Page ================================
            string alertsInnerHtml = string.Empty;
            if (Session["AlertsForms"] != null && Session["AlertsForms"].ToString() != "")
            {
                alertsInnerHtml = Session["AlertsForms"].ToString();
            }

            //================= END STRING =============================================
            string endString = string.Empty;
            if (Session["FirstRowEndString"] != null && Session["FirstRowEndString"].ToString() != "")
            {
                endString = Session["FirstRowEndString"].ToString();
            }

            //================= Preparation of Reports ====================
            string reportsInnerHtml = string.Empty;
            string secondRowStart = string.Empty;
            string secondRowEnd = string.Empty;

            secondRowStart = "<div class='row'>";
            if (Session["ReportsForms"] != null && Session["ReportsForms"].ToString() != "")
            {
                reportsInnerHtml = Session["ReportsForms"].ToString();
            }

            // preparation of innerhtml of Views
            string viewsInnerHtml = string.Empty;
            if (Session["ViewsForms"] != null && Session["ViewsForms"].ToString() != "")
            {
                viewsInnerHtml = Session["ViewsForms"].ToString();
            }
            secondRowEnd = "</div>";

            //===================== GET Column Strings ================
            string FCStrat = string.Empty;
            if (Session["FirstColumnStart"] != null && Session["FirstColumnStart"].ToString() != "")
            {
                FCStrat = Session["FirstColumnStart"].ToString();
            }

            string FCEnd = string.Empty;
            if (Session["FirstColumnEnd"] != null && Session["FirstColumnEnd"].ToString() != "")
            {
                FCEnd = Session["FirstColumnEnd"].ToString();
            }

            string SCStart = string.Empty;
            if (Session["SecondColumnStart"] != null && Session["SecondColumnStart"].ToString() != "")
            {
                SCStart = Session["SecondColumnStart"].ToString();
            }

            string SCEnd = string.Empty;
            if (Session["SecondColumnEnd"] != null && Session["SecondColumnEnd"].ToString() != "")
            {
                SCEnd = Session["SecondColumnEnd"].ToString();
            }

            string TCStart = string.Empty;
            if (Session["ThirdColumnStart"] != null && Session["ThirdColumnStart"].ToString() != "")
            {
                TCStart = Session["ThirdColumnStart"].ToString();
            }

            string TCEnd = string.Empty;
            if (Session["ThirdColumnEnd"] != null && Session["ThirdColumnEnd"].ToString() != "")
            {
                TCEnd = Session["ThirdColumnEnd"].ToString();
            }

            string aFavoriteFormsHtml = string.Empty;
            if (Session["aFavoriteFormsHtml"] != null && Session["aFavoriteFormsHtml"].ToString() != "")
            {
                aFavoriteFormsHtml = Session["aFavoriteFormsHtml"].ToString();
            }
            //================= Set Div Position ========================
            setDivPosition(aMasterFormsHtml, transactionFormsInnerHtml, alertsInnerHtml, endString, reportsInnerHtml, viewsInnerHtml, FCStrat, FCEnd, SCStart, SCEnd, TCStart, TCEnd, aFavoriteFormsHtml);
        }

        /// <summary>
        /// Method to sort Master Forms div and set to main div..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkForMasterFormSort_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable masterFormsDt = new DataTable();
                DataTable TransactionFormsTable = new DataTable();
                DataTable aDtFavoritemenu = new DataTable();
                DataTable alertsDataTable = new DataTable();
                DataTable reportPagesDt = new DataTable();
                DataTable ViewsDt = new DataTable();
                DataTable TempData = new DataTable();

                TempData = (DataTable)Session["MasterFormDataTable"];
                DataRow[] strConditionArr = TempData.Select("strMenuName LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'");
                if (strConditionArr.Length > 0)
                    masterFormsDt = TempData.Select("strMenuName LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'").CopyToDataTable();
                else
                    masterFormsDt = TempData.Clone();

                TempData = (DataTable)Session["TransactionFormsDataTable"];
                DataRow[] strConditionArr1 = TempData.Select("strMenuName LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'");
                if (strConditionArr1.Length > 0)
                    TransactionFormsTable = TempData.Select("strMenuName LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'").CopyToDataTable();
                else
                    TransactionFormsTable = TempData.Clone();

                aDtFavoritemenu = (DataTable)Session["FavoriteFormsHtmlDataTable"];
                TempData = (DataTable)Session["AlertsDataTable"];
                DataRow[] strConditionArr2 = (TempData.Select("strAlertType LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'"));
                if (strConditionArr2.Length > 0)
                    alertsDataTable = (TempData.Select("strAlertType LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'").CopyToDataTable());
                else
                    alertsDataTable = TempData.Clone();

                TempData = (DataTable)Session["reportPagesDataTable"];
                DataRow[] strConditionArr3 = TempData.Select("strMenuName LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'");
                if (strConditionArr3.Length > 0)
                    reportPagesDt = TempData.Select("strMenuName LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'").CopyToDataTable();
                else
                    reportPagesDt = TempData.Clone();

                TempData = (DataTable)Session["ViewsDataTable"];
                DataRow[] strConditionArr4 = TempData.Select("strMenuName LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'");
                if (strConditionArr4.Length > 0)
                    ViewsDt = TempData.Select("strMenuName LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'").CopyToDataTable();
                else
                    ViewsDt = TempData.Clone();

                string aMasterFormsHtml = string.Empty;
                string aMasterFormsHtmlTable = string.Empty;
                CheckBox chkMasterForms = (CheckBox)Master.FindControl("chkMasterForms");
                if (aMasterFormsHtml == string.Empty)
                {
                    if (Session["MasterFormChkVal"].ToString() == "true")
                    {
                        if (masterFormsDt.Rows.Count > 0)
                        {
                            aMasterFormsHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='divMasterDiv'>" + // changed 4 to 12
                                                 "<div id='MasterFormdiv' runat='server' class='x_panel tile fixed_height_320' style='height: auto;'>" +
                                                 "<div style='border-radius:5px;border-color:black'>" +
                                                 "<div class='x_title HeaderInnerMenu' >" +
                                                 "<h2 style='font-color:White;margin-top:2px;'>Masters </h2>" +
                                                 "<ul class='nav navbar-right panel_toolbox'>" +
                                                 "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                                 "<li><a href='#' id='OpenMaster'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                                 "<li><a class='close-link' ID='Master' onclick='javascript:return setDivPosition(divMasterDiv)' ><i ><lable class='fa fa-close' runat ='server' ></lable></i></a>" +
                                                 "</li>" +
                                                 "</ul>" +
                                                 "<div class='clearfix'></div>" +
                                                 "</div></div>" +
                                                  "<div class='x_content' style='display : none;'>" +
                                                  "<div runat='server' id='divTable' class='divtable table-responsive'>" +
                                                  "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            string mainLinkFromDb = string.Empty;
                            for (int i = 0; i < masterFormsDt.Rows.Count; i++)
                            {
                                mainLinkFromDb = masterFormsDt.Rows[i]["strNavigateUrl"].ToString();
                                mainLinkFromDb = mainLinkFromDb.Substring(5);
                                aMasterFormsHtmlTable += "<tr>" +
                                                         "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'><a href='" + masterFormsDt.Rows[i]["strFormUserManualFile"].ToString() + "' target='_blank'>" + (i + 1).ToString() + "</a></div></td>" +
                                                         "<td><a href='" + mainLinkFromDb + "' target='_blank'>" + masterFormsDt.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                                         "</tr>";
                            }
                            aMasterFormsHtml += aMasterFormsHtmlTable + "</tbody></table></div></div></div></div>";
                            chkMasterForms.Enabled = true;
                            Session["MasterFormChkVal"] = "true";
                        }
                        else
                        {
                            aMasterFormsHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='divMasterDiv'>" + // changed 4 to 12
                                                 "<div id='MasterFormdiv' runat='server' class='x_panel tile fixed_height_320' style='height: auto;'>" +
                                                 "<div style='border-radius:5px;border-color:black'>" +
                                                 "<div class='x_title HeaderInnerMenu' >" +
                                                 "<h2 style='font-color:White;margin-top:2px;'>Masters </h2>" +
                                                 "<ul class='nav navbar-right panel_toolbox'>" +
                                                 "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                                 "<li><a href='#' id='OpenMaster'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                                 "<li><a class='close-link' ID='Master' onclick='javascript:return setDivPosition(divMasterDiv)' ><i ><lable class='fa fa-close' runat ='server' ></lable></i></a>" +
                                                 "</li>" +
                                                 "</ul>" +
                                                 "<div class='clearfix'></div>" +
                                                 "</div></div>" +
                                                  "<div class='x_content' style='display : none;'>" +
                                                  "<div runat='server' id='divTable' class='divtable table-responsive'>" +
                                                  "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            aMasterFormsHtml += "</tbody></table></div></div></div></div>";
                            chkMasterForms.Checked = false;
                            chkMasterForms.Enabled = false;
                            Session["MasterFormChkVal"] = "false";
                        }
                    }
                    else
                    {
                        aMasterFormsHtml = "";
                        Session["MasterFormChkVal"] = "false";
                    }

                    //****************Preparation of Favorite Menu***************//
                    string aFavoriteFormsHtml = string.Empty;
                    string aFavoriteFormsHtmlTable = string.Empty;
                    if (aDtFavoritemenu != null)
                    {
                        aFavoriteFormsHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='Divfavorite'>" + // changed 4 to 12
                                             "<div id='MasterFormdiv' runat='server' class='x_panel tile fixed_height_560' >" +
                                             "<div style='border-radius:5px;border-color:black'>" +
                                             "<div class='x_title HeaderInnerMenu' >" +
                                             "<h2 style='font-color:White;margin-top:2px;'>Favorite </h2>" +
                                             "<ul class='nav navbar-right panel_toolbox'>" +
                                             "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                             "<li><a href='#' id='OpenMaster'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                             "<li><a class='close-link' ID='Master' onclick='javascript:return setDivPosition(Divfavorite)' ><i ><lable class='fa fa-close' runat ='server' ></lable></i></a>" +
                                             "</li>" +
                                             "</ul>" +
                                             "<div class='clearfix'></div>" +
                                              "</div></div>" +
                                              "<div class='x_content'>" +
                                              "<div runat='server' id='divTable' class='divtableNew2 table-responsive'>" +
                                              "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                        string mainLinkFromDb = string.Empty;
                        string aFavoriteHeader = string.Empty;

                        //For Search Master 
                        for (int i = 0; i < masterFormsDt.Rows.Count; i++)
                        {
                            mainLinkFromDb = masterFormsDt.Rows[i]["strNavigateUrl"].ToString();
                            mainLinkFromDb = mainLinkFromDb.Substring(5);
                            if (i == 0)
                            {
                                aFavoriteFormsHtmlTable += "<tr>" +
                                                         "<td colspan='2'><h1>Masters Search Result</h1></td>" +
                                                         "</tr>";
                            }
                            aFavoriteFormsHtmlTable += "<tr>" +
                                                     "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'><a href='" + masterFormsDt.Rows[i]["strFormUserManualFile"].ToString() + "' target='_blank'>" + (i + 1).ToString() + "</a></div></td>" +
                                                     "<td><a href='" + mainLinkFromDb + "' target='_blank'>" + masterFormsDt.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                                     "</tr>";
                        }
                        //End

                        //For Search Transaction 
                        string mainLinkFromDbforTran = string.Empty;
                        for (int i = 0; i < TransactionFormsTable.Rows.Count; i++)
                        {
                            mainLinkFromDbforTran = TransactionFormsTable.Rows[i]["strNavigateUrl"].ToString();
                            mainLinkFromDbforTran = mainLinkFromDbforTran.Substring(5);
                            if (i == 0)
                            {
                                aFavoriteFormsHtmlTable += "<tr>" +
                                                         "<td colspan='2'><h1>Transaction Search Result</h1></td>" +
                                                         "</tr>";
                            }
                            aFavoriteFormsHtmlTable += "<tr>" +
                                                         "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'>" + (i + 1).ToString() + "</div></td>" +
                                                         "<td><a href='" + mainLinkFromDbforTran + "' target='_blank'>" + TransactionFormsTable.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                                         "</tr>";
                        }
                        //End

                        int j = 0;
                        for (int i = 0; i < aDtFavoritemenu.Rows.Count; i++)
                        {
                            mainLinkFromDb = aDtFavoritemenu.Rows[i]["strUrl"].ToString();
                            if (i == 0)
                            {
                                aFavoriteFormsHtmlTable += "<tr>" +
                                                     "<td colspan='2'><h1>User Favorite</h1></td>" +
                                                     "</tr>";
                                aFavoriteHeader = aDtFavoritemenu.Rows[i]["StrType"].ToString();
                                aFavoriteFormsHtmlTable += "<tr>" +
                                                     "<td colspan='2' >" + aDtFavoritemenu.Rows[i]["StrType"].ToString() + "</td>" +
                                                     "</tr>";
                                j = j + 1;
                            }
                            else
                            {
                                if (aFavoriteHeader == aDtFavoritemenu.Rows[i]["StrType"].ToString())
                                {
                                    j = j + 1;
                                }
                                else
                                {
                                    aFavoriteHeader = aDtFavoritemenu.Rows[i]["StrType"].ToString();
                                    aFavoriteFormsHtmlTable += "<tr>" +
                                                         "<td colspan='2' >" + aDtFavoritemenu.Rows[i]["StrType"].ToString() + "</td>" +
                                      "</tr>";
                                    j = 0;
                                    j = j + 1;
                                }
                            }
                            aFavoriteFormsHtmlTable += "<tr>" +
                                                     "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'><a href='" + aDtFavoritemenu.Rows[i]["strFormUserManualFile"].ToString() + "' target='_blank'>" + (j).ToString() + "</a></div></td>" +
                                                     "<td><a href='" + mainLinkFromDb + "' target='_blank'>" + aDtFavoritemenu.Rows[i]["strMenu"].ToString() + "</a></td>" +
                                                     "</tr>";
                        }
                        aFavoriteFormsHtml += aFavoriteFormsHtmlTable +
                                        "</tbody></table></div></div></div></div>";
                    }
                    else
                    {
                        aFavoriteFormsHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='Divfavorite'>" + // changed 4 to 12
                                              "<div id='MasterFormdiv' runat='server' class='x_panel tile fixed_height_560' style='height: auto;' >" +
                                              "<div style='border-radius:5px;border-color:black'>" +
                                              "<div class='x_title HeaderInnerMenu' >" +
                                              "<h2 style='font-color:White;margin-top:2px;'>Favorite </h2>" +
                                              "<ul class='nav navbar-right panel_toolbox'>" +
                                              "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                              "<li><a href='#' id='OpenMaster'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                              "<li><a class='close-link' ID='Master' onclick='javascript:return setDivPosition(Divfavorite)' ><i ><lable class='fa fa-close' runat ='server' ></lable></i></a>" +
                                              "</li>" +
                                              "</ul>" +
                                              "<div class='clearfix'></div>" +
                                               "</div></div>" +
                                               "<div class='x_content' style='display : none;'>" +
                                               "<div runat='server' id='divTable' class='divtableNew2 table-responsive'>" +
                                               "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                        aFavoriteFormsHtml +=
                                       "</tbody></table></div></div></div></div>";
                    }

                    //========================== Inner html preparation for transaction page Table
                    string transactionFormsInnerHtml = string.Empty;
                    string transactionFormsHtmlTable = string.Empty;
                    CheckBox chkTranFroms = (CheckBox)Master.FindControl("chkTranFroms");
                    if (Session["TranFormChkVal"].ToString() == "true")
                    {
                        if (TransactionFormsTable.Rows.Count > 0)
                        {
                            transactionFormsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12 ' style='padding:0px;' id='divTransaction'>" +
                                                          "<div class='x_panel transheight fixed_height_340'>" +
                                                          "<div style='border-radius:5px;border-color:black'>" +
                                                          "<div class='x_title HeaderInnerMenu' >" +
                                                          "<h2 style='font-color:White;margin-top:2px;'>Transaction </h2>" +
                                                          "<ul class='nav navbar-right panel_toolbox'>" +
                                                          "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                                          "<li><a href='#' id='OpenTran'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                                          "<li><a class='close-link' ID='Transaction' onclick='javascript:return setDivPosition(divTransaction)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                                          "</li>" +
                                                          "</ul>" +
                                                          "<div class='clearfix'></div>" +
                                                          "</div></div>" +
                                                          "<div class='x_content'>" +
                                                          "<div runat='server' id='divTable'  class='divtableNew table-responsive'>" +
                                                          "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            string mainLinkFromDbforTran = string.Empty;

                            for (int i = 0; i < TransactionFormsTable.Rows.Count; i++)
                            {
                                mainLinkFromDbforTran = TransactionFormsTable.Rows[i]["strNavigateUrl"].ToString();
                                mainLinkFromDbforTran = mainLinkFromDbforTran.Substring(5);
                                transactionFormsHtmlTable += "<tr>" +
                                                             "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'><a href='" + TransactionFormsTable.Rows[i]["strFormUserManualFile"].ToString() + "' target='_blank'>" + (i + 1).ToString() + "</a></div></td>" +
                                                             "<td><a href='" + mainLinkFromDbforTran + "' target='_blank'>" + TransactionFormsTable.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                                             "</tr>";
                            }
                            transactionFormsInnerHtml += transactionFormsHtmlTable +
                                               "</tbody></table></div></div></div></div>";
                            chkTranFroms.Enabled = true;
                            Session["TranFormChkVal"] = "true";
                        }
                        else
                        {
                            transactionFormsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12 ' style='padding:0px;' id='divTransaction'>" +
                                                           "<div class='x_panel transheight fixed_height_340' style='height: auto;'>" +
                                                           "<div style='border-radius:5px;border-color:black'>" +
                                                           "<div class='x_title HeaderInnerMenu' >" +
                                                           "<h2 style='font-color:White;margin-top:2px;'>Transaction </h2>" +
                                                           "<ul class='nav navbar-right panel_toolbox'>" +
                                                           "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                                           "<li><a href='#' id='OpenTran'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                                           "<li><a class='close-link' ID='Transaction' onclick='javascript:return setDivPosition(divTransaction)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                                           "</li>" +
                                                           "</ul>" +
                                                           "<div class='clearfix'></div>" +
                                                           "</div></div>" +
                                                           "<div class='x_content' style='display : none;'>" +
                                                           "<div runat='server' id='divTable'  class='divtableNew table-responsive'>" +
                                                           "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            transactionFormsInnerHtml +=
                                              "</tbody></table></div></div></div></div>";
                            chkTranFroms.Checked = false;
                            chkTranFroms.Enabled = false;
                            Session["TranFormChkVal"] = "false";
                        }
                    }
                    else
                    {
                        transactionFormsInnerHtml = "";
                        Session["TranFormChkVal"] = "false";
                    }

                    // ======================================== preparation of innerhtml of Alerts
                    string alertsInnerHtml = string.Empty;
                    string alertsHtmlTable = string.Empty;
                    CheckBox chkAlerts = (CheckBox)Master.FindControl("chkAlerts");
                    if (Session["chkAlertsVal"].ToString() == "true")
                    {
                        if (alertsDataTable.Rows.Count > 0)
                        {
                            alertsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='DivAlerts'>" +
                                              "<div class='x_panel tile fixed_height_560'>" +
                                              "<div style='border-radius:5px;border-color:black'>" +
                                              "<div class='x_title HeaderInnerMenu' >" +
                                              "<h2 style='font-color:White;margin-top:2px;'>Alerts </h2>" +
                                              "<ul class='nav navbar-right panel_toolbox'>" +
                                              "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                              "<li><a href='#' id='OpenAlerts'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                              "<li><a class='close-link' ID='Alerts' onclick='javascript:return setDivPosition(DivAlerts)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                              "</li>" +
                                              "</ul>" +
                                              "<div class='clearfix'></div>" +
                                              "</div></div>" +
                                              "<div class='x_content'>" +
                                              "<div runat='server' id='divTable'  class='divtableNew2 table-responsive'>" +
                                              "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";

                            for (int i = 0; i < alertsDataTable.Rows.Count; i++)
                            {
                                alertsHtmlTable += "<tr>" +
                                                   "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'>" + (i + 1).ToString() + "</div></td>" +
                                                   "<td width='10px'><img style='cursor:pointer' src='../Images/print1.gif' height='20px' width='20px' id='" + alertsDataTable.Rows[i]["intAlertQueryId"].ToString() + "' onclick='javascript:return getAlertPrint(this)' /></td>" +
                                                   "<td><a href='#' onclick='javascript:return getAlertDetails(this)' style='cursor:pointer'  id='" + alertsDataTable.Rows[i]["intAlertQueryId"].ToString() + "'>" + alertsDataTable.Rows[i]["strAlertType"].ToString() + "</a></td>" +
                                                   "</tr>";
                            }
                            alertsInnerHtml += alertsHtmlTable +
                                               "</tbody></table></div></div></div></div>";
                            chkAlerts.Enabled = true;
                            Session["chkAlertsVal"] = "true";
                        }
                        else
                        {
                            alertsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='DivAlerts'>" +
                                               "<div class='x_panel tile fixed_height_560' style='height: auto;'>" +
                                               "<div style='border-radius:5px;border-color:black'>" +
                                               "<div class='x_title HeaderInnerMenu' >" +
                                               "<h2 style='font-color:White;margin-top:2px;'>Alerts </h2>" +
                                               "<ul class='nav navbar-right panel_toolbox'>" +
                                               "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                               "<li><a href='#' id='OpenAlerts'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                               "<li><a class='close-link' ID='Alerts' onclick='javascript:return setDivPosition(DivAlerts)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                               "</li>" +
                                               "</ul>" +
                                                "<div class='clearfix'></div>" +
                                               "</div></div>" +
                                               "<div class='x_content' style='display : none;'>" +
                                               "<div runat='server' id='divTable'  class='divtableNew2 table-responsive'>" +
                                               "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            chkAlerts.Enabled = false;
                            chkAlerts.Checked = false;
                            Session["chkAlertsVal"] = "false";
                        }
                    }
                    else
                    {
                        alertsInnerHtml = "";
                        Session["chkAlertsVal"] = "false";
                    }
                    //================= Concatination of Upper Row Divs.....(Master Forms/Transactions/Alerts)
                    string firstRowEndString = //"</div>" + // Commented to get all div in sequence 
                                         " <div class='modal fade bs-example-modal-lg' tabindex='-1' role='dialog' aria-hidden='true'>" +
                                         " <div class='modal-dialog modal-lg'>" +
                                         "<div class='modal-content'>" +
                                         "<div class='modal-header'>" +
                                         "<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>×</span></button>" +
                                         "<h4 class='modal-title' id='myModalLabel'>" + "Sample Form Name" + "</h4>" +
                                         "</div>" +
                                         "<div class='modal-body'>" +
                                         "<h4>Text in a modal</h4>" +
                                         "<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>" +
                                         "<p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>" +
                                         "</div>" +
                                         "<div class='modal-footer'>" +
                                         "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
                                         "</div>" +
                                         "</div>" +
                                         "</div>" +
                                         "</div>";
                    alertsInnerHtml += "</tbody></table></div></div></div></div>";
                    Session["FirstRowEndString"] = firstRowEndString;
                    string endString = firstRowEndString;

                    // preparation of innerhtml of Reports
                    string reportsInnerHtml = string.Empty;
                    string reportsHtmlTable = string.Empty;

                    CheckBox chkReports = (CheckBox)Master.FindControl("chkReports");
                    if (Session["chkReportsVal"].ToString() == "true")
                    {
                        if (reportPagesDt.Rows.Count > 0)
                        {
                            reportsInnerHtml = "<div  class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='Divreport'>" +
                                               "<div  id='ReportsDiv' class='x_panel tile fixed_height_320' style='height: auto;'>" +
                                               "<div style='border-radius:5px;border-color:black'>" +
                                               "<div class='x_title HeaderInnerMenu' >" +
                                               "<h2 style='font-color:White;margin-top:2px;'>Reports </h2>" +
                                               "<ul class='nav navbar-right panel_toolbox'>" +
                                               "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                               "<li><a href='#' ID='OpenReports'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                               "<li><a class='close-link' ID='Reports' onclick='javascript:return setDivPosition(Divreport)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                               "</li>" +
                                               "</ul>" +
                                               "<div class='clearfix'></div>" +
                                               "</div></div>" +
                                               "<div class='x_content' style='display : none;'>" +
                                               "<div runat='server' id='divTable'  class='divtable table-responsive'>" +
                                               "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";

                            for (int i = 0; i < reportPagesDt.Rows.Count; i++)
                            {
                                reportsHtmlTable += "<tr>" +
                                                         "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'>" + (i + 1).ToString() + "</div></td>" +
                                                         "<td><a href='#'>" + reportPagesDt.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                                         "</tr>";
                            }
                            reportsInnerHtml += reportsHtmlTable +
                                               "</tbody></table></div></div></div></div>";
                            chkReports.Enabled = true;
                            Session["chkReportsVal"] = "true";
                        }
                        else
                        {
                            reportsInnerHtml = "<div  class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='Divreport'>" +
                                                "<div  id='ReportsDiv' class='x_panel tile fixed_height_320' style='height: auto;'>" +
                                                "<div style='border-radius:5px;border-color:black'>" +
                                                "<div class='x_title HeaderInnerMenu' >" +
                                                "<h2 style='font-color:White;margin-top:2px;'>Reports </h2>" +
                                                "<ul class='nav navbar-right panel_toolbox'>" +
                                                "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                                "<li><a href='#' ID='OpenReports'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                                "<li><a class='close-link' ID='Reports' onclick='javascript:return setDivPosition(Divreport)' ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                                "</li>" +
                                                "</ul>" +
                                                "<div class='clearfix'></div>" +
                                                "</div></div>" +
                                                "<div class='x_content' style='display : none;'>" +
                                                "<div runat='server' id='divTable'  class='divtable table-responsive'>" +
                                                "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            reportsInnerHtml +=
                                               "</tbody></table></div></div></div></div>";
                            chkReports.Enabled = false;
                            chkReports.Checked = false;
                            Session["chkReportsVal"] = "false";
                        }
                    }
                    else
                    {
                        reportsInnerHtml = "";
                        Session["chkReportsVal"] = "false";
                    }

                    //================== Preparation of Views Forms Div ============================
                    string viewsInnerHtml = string.Empty;
                    string viewsHtmlTable = string.Empty;
                    CheckBox chkViews = (CheckBox)Master.FindControl("chkViews");
                    if (Session["chkViewsVal"].ToString() == "true")
                    {
                        if (ViewsDt.Rows.Count > 0)
                        {
                            viewsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='DivView'>" +
                                             "<div class='x_panel tile fixed_height_320' style='height: auto;'>" +
                                             "<div style='border-radius:5px;border-color:black'>" +
                                             "<div class='x_title HeaderInnerMenu' >" +
                                             "<h2 style='font-color:White;margin-top:2px;'>Views </h2>" +
                                             "<ul class='nav navbar-right panel_toolbox'>" +
                                             "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                             "<li><a href='#' id='OpenViews'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                             "<li><a class='close-link' ID='Views' onclick='javascript:return setDivPosition(DivView)'  ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                             "</li>" +
                                             "</ul>" +
                                             "<div class='clearfix'></div>" +
                                             "</div></div>" +
                                             "<div class='x_content' style='display : none;'>" +
                                             "<div runat='server' id='divTable'  class='divtable table-responsive'>" +
                                             "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";

                            for (int i = 0; i < ViewsDt.Rows.Count; i++)
                            {
                                viewsHtmlTable += "<tr>" +
                                                  "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'>" + (i + 1).ToString() + "</div></td>" +
                                                  "<td><a href='#'>" + ViewsDt.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                                  "</tr>";
                            }
                            viewsInnerHtml += viewsHtmlTable +
                                               "</tbody></table></div></div></div></div>";
                            chkViews.Enabled = true;
                            Session["chkViewsVal"] = "false";
                        }
                        else
                        {
                            viewsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px;' id='DivView'>" +
                                              "<div class='x_panel tile fixed_height_320' style='height: auto;'>" +
                                              "<div style='border-radius:5px;border-color:black'>" +
                                              "<div class='x_title HeaderInnerMenu' >" +
                                              "<h2 style='font-color:White;margin-top:2px;'>Views </h2>" +
                                              "<ul class='nav navbar-right panel_toolbox'>" +
                                              "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                              "<li><a href='#' id='OpenViews'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                              "<li><a class='close-link' ID='Views' onclick='javascript:return setDivPosition(DivView)'  ><i ><lable class='fa fa-close' ></lable></i></a>" +
                                              "</li>" +
                                              "</ul>" +
                                              "<div class='clearfix'></div>" +
                                              "</div></div>" +
                                              "<div class='x_content' style='display : none;'>" +
                                              "<div runat='server' id='divTable'  class='divtable table-responsive'>" +
                                              "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                            viewsInnerHtml +=
                                               "</tbody></table></div></div></div></div>";
                            chkViews.Enabled = false;
                            Session["chkViewsVal"] = "false";
                            chkViews.Checked = false;
                        }
                    }
                    else
                    {
                        viewsInnerHtml = "";
                        Session["chkViewsVal"] = "false";
                    }
                    string FCStrat = "<div id='column1MainDiv' runat='server'>" +
                                    "<div class='col-md-4 col-sm-4 col-xs-12 fixed_height_560' style='overflow:auto;'>" +
                                    " <div id='column1Div' >";
                    Session["FirstColumnStart"] = FCStrat;
                    string FCEnd = "</div></div></div>";
                    Session["FirstColumnEnd"] = FCEnd;
                    string SCStart = "<div id='column2Div' runat='server'>" +
                                    "<div class='col-md-4 col-sm-4 col-xs-12 fixed_height_560' style='overflow:auto;'>";

                    Session["SecondColumnStart"] = SCStart;
                    string SCEnd = "</div></div>";
                    Session["SecondColumnEnd"] = SCEnd;
                    string TCStart = "<div id='column2Div' runat='server'>" +
                                    "<div class='col-md-4 col-sm-4 col-xs-12 fixed_height_560' style='overflow:auto;'>" +
                                    " <div >";
                    Session["ThirdColumnStart"] = TCStart;
                    string TCEnd = "</div></div></div>";
                    Session["ThirdColumnEnd"] = TCEnd;
                    setDivPosition(aMasterFormsHtml, transactionFormsInnerHtml, alertsInnerHtml, endString, reportsInnerHtml, viewsInnerHtml, FCStrat, FCEnd, SCStart, SCEnd, TCStart, TCEnd, aFavoriteFormsHtml);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Function to set position of div's...
        /// </summary>
        /// <param name="aMasterFormsHtml"></param>
        /// <param name="transactionFormsInnerHtml"></param>
        /// <param name="alertsInnerHtml"></param>
        /// <param name="endString"></param>
        /// <param name="reportsInnerHtml"></param>
        /// <param name="viewsInnerHtml"></param>
        protected void setDivPosition(string aMasterFormsHtml, string transactionFormsInnerHtml, string alertsInnerHtml, string endString, string reportsInnerHtml, string viewsInnerHtml, string FCStart, string FCEnd, string SCStart, string SCEnd, string TCStart, string TCEnd, string aFavoriteFormsHtml)
        {
            string innerHtmlAfterSetPosition = string.Empty;
            if (hdnDivName.Value.ToString() == "Master")
            {
                hdnMasterIsClose.Value = "true";
            }
            if (hdnDivName.Value.ToString() == "Transaction")
            {
                hdnTranIsClose.Value = "true";
            }
            if (hdnDivName.Value.ToString() == "Alerts")
            {
                hdnAlertIsClose.Value = "true";
            }
            if (hdnDivName.Value.ToString() == "Views")
            {
                hdnViewIsClose.Value = "true";
            }
            if (hdnDivName.Value.ToString() == "Reports")
            {
                hdnReportsIsClose.Value = "true";
            }
            string[] arr = setCheckedDiv(aMasterFormsHtml, transactionFormsInnerHtml, alertsInnerHtml, endString, reportsInnerHtml, viewsInnerHtml, FCStart, FCEnd, SCStart, SCEnd, TCStart, TCEnd, aFavoriteFormsHtml);
            for (int i = 0; i < arr.Length; i++)
            {
                innerHtmlAfterSetPosition = innerHtmlAfterSetPosition + (arr[i].ToString());
            }
            divMain.InnerHtml = string.Empty;
            divMain.InnerHtml = innerHtmlAfterSetPosition;
            lblModuleName.Text = Session["ModuleName"] != null ? Session["ModuleName"].ToString() : "All";
        }

        protected string[] setCheckedDiv(string aMasterFormsHtml, string transactionFormsInnerHtml, string alertsInnerHtml, string endString, string reportsInnerHtml, string viewsInnerHtml, string FCStart, string FCEnd, string SCStart, string SCEnd, string TCStart, string TCEnd, string aFavoriteFormsHtml)
        {
            if (Session["MasterFormChkVal"] != null && Session["MasterFormChkVal"].ToString() != string.Empty)
            {
                if (Session["MasterFormChkVal"].ToString() == "false")
                {
                    aMasterFormsHtml = "";
                }
            }

            if (Session["TranFormChkVal"] != null && Session["TranFormChkVal"].ToString() != string.Empty)
            {
                if (Session["TranFormChkVal"].ToString() == "false")
                {
                    transactionFormsInnerHtml = "";
                }
            }

            if (Session["chkAlertsVal"] != null && Session["chkAlertsVal"].ToString() != string.Empty)
            {
                if (Session["chkAlertsVal"].ToString() == "false")
                {
                    alertsInnerHtml = "";
                }
            }

            if (Session["chkReportsVal"] != null && Session["chkReportsVal"].ToString() != string.Empty)
            {
                if (Session["chkReportsVal"].ToString() == "false")
                {
                    reportsInnerHtml = "";
                }
            }

            if (Session["chkViewsVal"] != null && Session["chkViewsVal"].ToString() != string.Empty)
            {
                if (Session["chkViewsVal"].ToString() == "false")
                {
                    viewsInnerHtml = "";
                }
            }
            string[] checkedStringArr = new string[15];
            checkedStringArr[0] = "<div class='row'>";
            checkedStringArr[1] = aFavoriteFormsHtml == "" && aFavoriteFormsHtml == "" ? FCStart : FCStart;
            checkedStringArr[2] = aFavoriteFormsHtml;
            checkedStringArr[3] = aFavoriteFormsHtml == "" && aFavoriteFormsHtml == "" ? FCEnd : FCEnd;
            checkedStringArr[4] = transactionFormsInnerHtml == "" ? SCStart : SCStart;
            checkedStringArr[5] = transactionFormsInnerHtml;
            checkedStringArr[6] = aMasterFormsHtml;
            checkedStringArr[7] = reportsInnerHtml;
            checkedStringArr[8] = viewsInnerHtml;
            checkedStringArr[9] = transactionFormsInnerHtml == "" ? SCEnd : SCEnd;
            checkedStringArr[10] = alertsInnerHtml == "" && viewsInnerHtml == "" ? TCStart : TCStart;
            checkedStringArr[11] = alertsInnerHtml;
            checkedStringArr[12] = alertsInnerHtml == "" && viewsInnerHtml == "" ? TCEnd : TCEnd;
            checkedStringArr[13] = endString;
            checkedStringArr[14] = "</div>";
            return checkedStringArr;
        }

        /// <summary>
        /// Method to sort Transaction Forms div and set to main div..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkForTransactionFormSort_Click(object sender, EventArgs e)
        {
            try
            {
                //Data Table for All Transaction Pages
                DataTable TransactionFormsTable = (DataTable)Session["TransactionFormsDataTable"];
                DataTable sortedDataTable = new DataTable();

                if (!string.IsNullOrEmpty(hdnSearchValueForTranForm.Value.ToString()))
                {
                    DataRow[] strConditionArr = TransactionFormsTable.Select("strMenuName LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'").ToArray();
                    if (strConditionArr.Length > 0)
                    {
                        sortedDataTable = TransactionFormsTable.Select("strMenuName LIKE '%" + hdnSearchValueForTranForm.Value.ToString().Trim() + "%'").CopyToDataTable();
                    }
                }
                else
                {
                    sortedDataTable = (DataTable)Session["TransactionFormsDataTable"];
                }

                //================== Preparation of Master Form Div ===================
                string aMasterFormsHtml = string.Empty;
                if (Session["MasterForms"] != null && Session["MasterForms"].ToString() != "")
                {
                    aMasterFormsHtml = Session["MasterForms"].ToString();
                }

                // Inner html preparation for transaction page Table
                string transactionFormsInnerHtml = string.Empty;
                string transactionFormsHtmlTable = string.Empty;
                transactionFormsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12 ' style='padding:0px;'>" +
                                            "<div class='x_panel tile' style='height:540px'>" +
                                            "<div style='border-radius:5px;border-color:black'>" +
                                            "<div class='x_title HeaderInnerMenu' >" +
                                            "<h2 style='font-color:White;margin-top:2px;'>Transaction </h2>" +
                                            "<ul class='nav navbar-right panel_toolbox'>" +
                                            "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                            "<li><a href='#' id='OpenTran'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                            "<li><a class='close-link' ID='Transaction' onclick='javascript:return setDivPosition(this)'><i ><lable class='fa fa-close' ></lable></i></a>" +
                                            "</li>" +
                                            "</ul>" +
                                            "<div class='input-group' style='float:left;'><input type='text' width='50%' class='form-control' runat='server' id='txtSearchTran' placeholder='Search for...' value='" + hdnSearchValueForTranForm.Value.ToString() + "'><span class='input-group-btn'><button class='btn btn-primary' type='button' onclick='javascript:return getSortedDataforTransaction()'>Go!</button></span></div>" +
                                            "<div class='clearfix'></div>" +
                                            "</div></div>" +
                                            "<div class='x_content'>" +
                                            "<div runat='server' id='divTable'  class='divtableNew table-responsive'>" +
                                            "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";

                string mainLinkFromDbforTran = string.Empty;
                if (sortedDataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < sortedDataTable.Rows.Count; i++)
                    {
                        mainLinkFromDbforTran = TransactionFormsTable.Rows[i]["strNavigateUrl"].ToString();
                        mainLinkFromDbforTran = mainLinkFromDbforTran.Substring(5);
                        transactionFormsHtmlTable += "<tr>" +
                                                 "<td width='20px'><div style='cursor:pointer;border-radius: 15px;background-color: lightblue;color: black;text-align: center;'>" + (i + 1).ToString() + "</div></td>" +
                                                 "<td><a href='" + mainLinkFromDbforTran + "' target='_blank'>" + sortedDataTable.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                                 "</tr>";
                    }
                }
                else
                {
                    transactionFormsHtmlTable += "<tr>" + "<td> No Data Found !</td></tr>";
                }
                transactionFormsInnerHtml += transactionFormsHtmlTable + "</tbody></table></div></div></div></div>";

                // preparation of innerhtml of Alerts
                string alertsInnerHtml = string.Empty;
                if (Session["AlertsForms"] != null && Session["AlertsForms"].ToString() != "")
                {
                    alertsInnerHtml = Session["AlertsForms"].ToString();
                }
                //================= END STRING

                string endString = string.Empty;
                if (Session["FirstRowEndString"] != null && Session["FirstRowEndString"].ToString() != "")
                {
                    endString = Session["FirstRowEndString"].ToString();
                }

                // preparation of innerhtml of Reports
                string reportsInnerHtml = string.Empty;
                string secondRowStart = string.Empty;
                string secondRowEnd = string.Empty;
                secondRowStart = "<div class='row'>";
                if (Session["ReportsForms"] != null && Session["ReportsForms"].ToString() != "")
                {
                    reportsInnerHtml = Session["ReportsForms"].ToString();
                }

                // preparation of innerhtml of Views
                string viewsInnerHtml = string.Empty;
                if (Session["ViewsForms"] != null && Session["ViewsForms"].ToString() != "")
                {
                    viewsInnerHtml = Session["ViewsForms"].ToString();
                }
                secondRowEnd = "</div>";

                //===================== GET Column Strings ================
                string FCStrat = string.Empty;
                if (Session["FirstColumnStart"] != null && Session["FirstColumnStart"].ToString() != "")
                {
                    FCStrat = Session["FirstColumnStart"].ToString();
                }

                string FCEnd = string.Empty;
                if (Session["FirstColumnEnd"] != null && Session["FirstColumnEnd"].ToString() != "")
                {
                    FCEnd = Session["FirstColumnEnd"].ToString();
                }

                string SCStart = string.Empty;
                if (Session["SecondColumnStart"] != null && Session["SecondColumnStart"].ToString() != "")
                {
                    SCStart = Session["SecondColumnStart"].ToString();
                }

                string SCEnd = string.Empty;
                if (Session["SecondColumnEnd"] != null && Session["SecondColumnEnd"].ToString() != "")
                {
                    SCEnd = Session["SecondColumnEnd"].ToString();
                }

                string TCStart = string.Empty;
                if (Session["ThirdColumnStart"] != null && Session["ThirdColumnStart"].ToString() != "")
                {
                    TCStart = Session["ThirdColumnStart"].ToString();
                }

                string TCEnd = string.Empty;
                if (Session["ThirdColumnEnd"] != null && Session["ThirdColumnEnd"].ToString() != "")
                {
                    TCEnd = Session["ThirdColumnEnd"].ToString();
                }

                string aFavoriteFormsHtml = string.Empty;
                if (Session["aFavoriteFormsHtml"] != null && Session["aFavoriteFormsHtml"].ToString() != "")
                {
                    aFavoriteFormsHtml = Session["aFavoriteFormsHtml"].ToString();
                }

                //================= Set Div Position ========================
                if (hdnIsMax.Value.ToString() != "true")
                {
                    setDivPosition(aMasterFormsHtml, transactionFormsInnerHtml, alertsInnerHtml, endString, reportsInnerHtml, viewsInnerHtml, FCStrat, FCEnd, SCStart, SCEnd, TCStart, TCEnd, aFavoriteFormsHtml);
                }
                else
                {
                    transactionFormsInnerHtml = transactionFormsInnerHtml.Replace("fixed_height_320", "fixed_height_520");
                    transactionFormsInnerHtml = transactionFormsInnerHtml.Replace("divtable", "divtableNew");
                    divMain.InnerHtml = "<div class='row'>" + transactionFormsInnerHtml +
                                        "<div  class='col-md-8 col-sm-8 col-xs-12'>" +
                                        "<div class='x_panel tile fixed_height_520'>" +
                                        "<h2> Sample information about Div</h2>" +
                                        "</div></div></div>";
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Method to sort Alerts Forms div and set to main div..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkForAlerts_Click(object sender, EventArgs e)
        {
            try
            {
                // Data Table for all alerts
                DataTable alertsDataTable = (DataTable)Session["AlertsDataTable"];
                DataTable sortedDataTable = new DataTable();

                if (!string.IsNullOrEmpty(hdnSearchValueForAlerts.Value.ToString()))
                {
                    DataRow[] strConditionArr = alertsDataTable.Select("strAlertType LIKE '%" + hdnSearchValueForAlerts.Value.ToString().Trim() + "%'").ToArray();
                    if (strConditionArr.Length > 0)
                    {
                        sortedDataTable = alertsDataTable.Select("strAlertType LIKE '%" + hdnSearchValueForAlerts.Value.ToString().Trim() + "%'").CopyToDataTable();
                    }
                }
                else
                {
                    sortedDataTable = (DataTable)Session["AlertsDataTable"];
                }

                //================== Preparation of Master forms Div =============================
                string aMasterFormsHtml = string.Empty;
                if (Session["MasterForms"] != null && Session["MasterForms"].ToString() != "")
                {
                    aMasterFormsHtml = Session["MasterForms"].ToString();
                }

                //Preparation for transaction page Table
                string transactionFormsInnerHtml = string.Empty;
                if (Session["TransactionForms"] != null && Session["TransactionForms"].ToString() != "")
                {
                    transactionFormsInnerHtml = Session["TransactionForms"].ToString();
                }

                // preparation of innerhtml of Alerts
                string alertsInnerHtml = string.Empty;
                string alertsHtmlTable = string.Empty;
                alertsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12 ' style='padding:0px;'>" +
                                    "<div class='x_panel tile fixed_height_320'>" +
                                    "<div style='border-radius:5px;border-color:black'>" +
                                    "<div class='x_title HeaderInnerMenu' >" +
                                    "<h2 style='font-color:White;margin-top:2px;'>Alerts </h2>" +
                                    "<ul class='nav navbar-right panel_toolbox'>" +
                                    "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                    "<li><a href='#' id='OpenAlerts'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                    "<li><a class='close-link' ID='Alerts'  onclick='javascript:return setDivPosition(this)'><i ><lable class='fa fa-close' ></lable></i></a>" +
                                    "</li>" +
                                    "</ul>" +
                                    "<div class='input-group' style='float:left;'><input type='text' width='50%' class='form-control' runat='server' id='txtSearchAlerts' placeholder='Search for...' value ='" + hdnSearchValueForAlerts.Value.ToString() + "'><span class='input-group-btn'><button class='btn btn-primary' type='button' onclick='javascript:return getSortedDataforAlerts()'>Go!</button></span></div>" +
                                    "<div class='clearfix'></div>" +
                                    "</div></div>" +
                                    "<div class='x_content'>" +
                                    "<div runat='server' id='divTable'  class='divtable table-responsive'>" +
                                    "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";

                if (sortedDataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < sortedDataTable.Rows.Count; i++)
                    {
                        alertsHtmlTable += "<tr>" +
                                "<td width='20px' style='cursor:pointer'><div style='border-radius: 15px;background-color: lightblue;color: black;text-align: center;'>" + (i + 1).ToString() + "</div></td>" +
                                "<td width='10px'><img style='cursor:pointer' src='../Images/print1.gif' height='20px' width='20px' id='" + sortedDataTable.Rows[i]["intAlertQueryId"].ToString() + "' onclick='javascript:return getAlertPrint(this)' /></td>" +
                                "<td><a href='#' onclick='javascript:return getAlertDetails(this)' style='cursor:pointer'  id='" + sortedDataTable.Rows[i]["intAlertQueryId"].ToString() + "'>" + sortedDataTable.Rows[i]["strAlertType"].ToString() + "</a></td>" +
                                "</tr>";
                    }
                }
                else
                {
                    alertsHtmlTable += "<tr>" +
                                    "<td> No Data Found !</td></tr>";
                }

                alertsInnerHtml += alertsHtmlTable +
                                   "</tbody></table></div></div></div></div>";
                //================= END STRING

                string endString = string.Empty;
                if (Session["FirstRowEndString"] != null && Session["FirstRowEndString"].ToString() != "")
                {
                    endString = Session["FirstRowEndString"].ToString();
                }

                // preparation of innerhtml of Reports
                string reportsInnerHtml = string.Empty;
                string secondRowStart = string.Empty;
                string secondRowEnd = string.Empty;
                secondRowStart = "<div class='row'>";
                if (Session["ReportsForms"] != null && Session["ReportsForms"].ToString() != "")
                {
                    reportsInnerHtml = Session["ReportsForms"].ToString();
                }

                // preparation of innerhtml of Views
                string viewsInnerHtml = string.Empty;
                if (Session["ViewsForms"] != null && Session["ViewsForms"].ToString() != "")
                {
                    viewsInnerHtml = Session["ViewsForms"].ToString();
                }
                secondRowEnd = "</div>";

                //===================== GET Column Strings ================
                string FCStrat = string.Empty;
                if (Session["FirstColumnStart"] != null && Session["FirstColumnStart"].ToString() != "")
                {
                    FCStrat = Session["FirstColumnStart"].ToString();
                }

                string FCEnd = string.Empty;
                if (Session["FirstColumnEnd"] != null && Session["FirstColumnEnd"].ToString() != "")
                {
                    FCEnd = Session["FirstColumnEnd"].ToString();
                }

                string SCStart = string.Empty;
                if (Session["SecondColumnStart"] != null && Session["SecondColumnStart"].ToString() != "")
                {
                    SCStart = Session["SecondColumnStart"].ToString();
                }

                string SCEnd = string.Empty;
                if (Session["SecondColumnEnd"] != null && Session["SecondColumnEnd"].ToString() != "")
                {
                    SCEnd = Session["SecondColumnEnd"].ToString();
                }

                string TCStart = string.Empty;
                if (Session["ThirdColumnStart"] != null && Session["ThirdColumnStart"].ToString() != "")
                {
                    TCStart = Session["ThirdColumnStart"].ToString();
                }

                string TCEnd = string.Empty;
                if (Session["ThirdColumnEnd"] != null && Session["ThirdColumnEnd"].ToString() != "")
                {
                    TCEnd = Session["ThirdColumnEnd"].ToString();
                }

                string aFavoriteFormsHtml = string.Empty;
                if (Session["aFavoriteFormsHtml"] != null && Session["aFavoriteFormsHtml"].ToString() != "")
                {
                    aFavoriteFormsHtml = Session["aFavoriteFormsHtml"].ToString();
                }

                //================= Set Div Position ========================
                if (hdnIsMax.Value.ToString() != "true")
                {
                    setDivPosition(aMasterFormsHtml, transactionFormsInnerHtml, alertsInnerHtml, endString, reportsInnerHtml, viewsInnerHtml, FCStrat, FCEnd, SCStart, SCEnd, TCStart, TCEnd, aFavoriteFormsHtml);
                }
                else
                {
                    alertsInnerHtml = alertsInnerHtml.Replace("fixed_height_320", "fixed_height_520");
                    alertsInnerHtml = alertsInnerHtml.Replace("divtable", "divtableNew");
                    divMain.InnerHtml = "<div class='row'>" + alertsInnerHtml +
                                        "<div  class='col-md-8 col-sm-8 col-xs-12'>" +
                                        "<div class='x_panel tile fixed_height_520'>" +
                                        "<h2> Sample information about Div</h2>" +
                                        "</div></div></div>";
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Method to set div positions and set to main div..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkForSetPosition_Click(object sender, EventArgs e)
        {
            try
            {
                // ============== Preparation for Master Forms Div =========================
                string aMasterFormsHtml = string.Empty;
                if (Session["MasterForms"] != null && Session["MasterForms"].ToString() != "")
                {
                    aMasterFormsHtml = Session["MasterForms"].ToString();
                }

                //============== Preparation for transaction page ==========================
                string transactionFormsInnerHtml = string.Empty;
                if (Session["TransactionForms"] != null && Session["TransactionForms"].ToString() != "")
                {
                    transactionFormsInnerHtml = Session["TransactionForms"].ToString();
                }

                //============== preparation of Alerts Page ================================
                string alertsInnerHtml = string.Empty;
                if (Session["AlertsForms"] != null && Session["AlertsForms"].ToString() != "")
                {
                    alertsInnerHtml = Session["AlertsForms"].ToString();
                }

                //================= END STRING =============================================
                string endString = string.Empty;
                if (Session["FirstRowEndString"] != null && Session["FirstRowEndString"].ToString() != "")
                {
                    endString = Session["FirstRowEndString"].ToString();
                }

                //================= Preparation of Reports ====================
                string reportsInnerHtml = string.Empty;
                string secondRowStart = string.Empty;
                string secondRowEnd = string.Empty;
                secondRowStart = "<div class='row'>";
                if (Session["ReportsForms"] != null && Session["ReportsForms"].ToString() != "")
                {
                    reportsInnerHtml = Session["ReportsForms"].ToString();
                }

                // preparation of innerhtml of Views
                string viewsInnerHtml = string.Empty;
                if (Session["ViewsForms"] != null && Session["ViewsForms"].ToString() != "")
                {
                    viewsInnerHtml = Session["ViewsForms"].ToString();
                }
                secondRowEnd = "</div>";

                //===================== GET Column Strings ================
                string FCStrat = string.Empty;
                if (Session["FirstColumnStart"] != null && Session["FirstColumnStart"].ToString() != "")
                {
                    FCStrat = Session["FirstColumnStart"].ToString();
                }

                string FCEnd = string.Empty;
                if (Session["FirstColumnEnd"] != null && Session["FirstColumnEnd"].ToString() != "")
                {
                    FCEnd = Session["FirstColumnEnd"].ToString();
                }

                string SCStart = string.Empty;
                if (Session["SecondColumnStart"] != null && Session["SecondColumnStart"].ToString() != "")
                {
                    SCStart = Session["SecondColumnStart"].ToString();
                }

                string SCEnd = string.Empty;
                if (Session["SecondColumnEnd"] != null && Session["SecondColumnEnd"].ToString() != "")
                {
                    SCEnd = Session["SecondColumnEnd"].ToString();
                }

                string TCStart = string.Empty;
                if (Session["ThirdColumnStart"] != null && Session["ThirdColumnStart"].ToString() != "")
                {
                    TCStart = Session["ThirdColumnStart"].ToString();
                }

                string TCEnd = string.Empty;
                if (Session["ThirdColumnEnd"] != null && Session["ThirdColumnEnd"].ToString() != "")
                {
                    TCEnd = Session["ThirdColumnEnd"].ToString();
                }

                string aFavoriteFormsHtml = string.Empty;
                if (Session["aFavoriteFormsHtml"] != null && Session["aFavoriteFormsHtml"].ToString() != "")
                {
                    aFavoriteFormsHtml = Session["aFavoriteFormsHtml"].ToString();
                }

                //================= Set Div Position ========================
                setDivPosition(aMasterFormsHtml, transactionFormsInnerHtml, alertsInnerHtml, endString, reportsInnerHtml, viewsInnerHtml, FCStrat, FCEnd, SCStart, SCEnd, TCStart, TCEnd, aFavoriteFormsHtml);
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void btnReload_Click(object sender, EventArgs e)
        {
            Session["MasterFormChkVal"] = "true";
            Session["TranFormChkVal"] = "true";
            Session["chkAlertsVal"] = "true";
            Session["chkReportsVal"] = "true";
            Session["chkViewsVal"] = "false";
            Server.Transfer("frmWorkFlowNew.aspx");
        }

        protected void ResetLink_Click(object sender, EventArgs e)
        {
            Session["MasterFormChkVal"] = "true";
            Session["TranFormChkVal"] = "true";
            Session["chkAlertsVal"] = "true";
            Session["chkReportsVal"] = "true";
            Session["chkViewsVal"] = "false";
            Session["ModuleName"] = "All";
            Server.Transfer("frmWorkFlowNew.aspx");
        }

        /// <summary>
        /// Method to sort Report Forms div and set to main div..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkForReports_Click(object sender, EventArgs e)
        {
            try
            {
                // Data Table For All Reports
                DataTable reportPagesDt = (DataTable)Session["reportPagesDataTable"];
                DataTable sortedDataTable = new DataTable();
                if (!string.IsNullOrEmpty(hdnSearchValueForReports.Value.ToString()))
                {
                    DataRow[] strConditionArr = reportPagesDt.Select("strMenuName LIKE '%" + hdnSearchValueForReports.Value.ToString().Trim() + "%'").ToArray();
                    if (strConditionArr.Length > 0)
                    {
                        sortedDataTable = reportPagesDt.Select("strMenuName LIKE '%" + hdnSearchValueForReports.Value.ToString().Trim() + "%'").CopyToDataTable();
                    }
                }
                else
                {
                    sortedDataTable = (DataTable)Session["reportPagesDataTable"];
                }

                //================ Preparation Of Master Form ==============================
                string aMasterFormsHtml = string.Empty;
                if (Session["MasterForms"] != null && Session["MasterForms"].ToString() != "")
                {
                    aMasterFormsHtml = Session["MasterForms"].ToString();
                }

                //================ Preparation of transaction ==============================
                string transactionFormsInnerHtml = string.Empty;
                if (Session["TransactionForms"] != null && Session["TransactionForms"].ToString() != "")
                {
                    transactionFormsInnerHtml = Session["TransactionForms"].ToString();
                }

                //================ Preparation of Alerts ===================================
                string alertsInnerHtml = string.Empty;
                if (Session["AlertsForms"] != null && Session["AlertsForms"].ToString() != "")
                {
                    alertsInnerHtml = Session["AlertsForms"].ToString();
                }

                //================ END STRING ==============================================
                string endString = string.Empty;
                if (Session["FirstRowEndString"] != null && Session["FirstRowEndString"].ToString() != "")
                {
                    endString = Session["FirstRowEndString"].ToString();
                }

                //================ Preparation of Reports ==================================
                string reportsInnerHtml = string.Empty;
                string reportsHtmlTable = string.Empty;
                string secondRowStart = string.Empty;
                string secondRowEnd = string.Empty;
                secondRowStart = "<div class='row'>";
                reportsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12 ' style='padding:0px;'>" +
                                "<div class='x_panel tile fixed_height_320'>" +
                                "<div style='border-radius:5px;border-color:black'>" +
                                "<div class='x_title HeaderInnerMenu' >" +
                                "<h2 style='font-color:White;margin-top:2px;'>Reports </h2>" +
                                "<ul class='nav navbar-right panel_toolbox'>" +
                                "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                                "<li><a href='#' ID='OpenReports'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                                "<li><a class='close-link' ID='Reports'  onclick='javascript:return setDivPosition(this)'><i ><lable class='fa fa-close' ></lable></i></a>" +
                                "</li>" +
                                "</ul>" +
                                "<div class='input-group' style='float:left;'><input type='text' width='50%' class='form-control' runat='server' id='txtSearchReports' placeholder='Search for...' value='" + hdnSearchValueForReports.Value.ToString() + "'><span class='input-group-btn'><button class='btn btn-primary' type='button' onclick='javascript:return getSortedDataforReports()'>Go!</button></span></div>" +
                                "<div class='clearfix'></div>" +
                                "</div></div>" +
                                "<div class='x_content'>" +
                                "<div runat='server' id='divTable'  class='divtable table-responsive'>" +
                                "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";

                if (sortedDataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < sortedDataTable.Rows.Count; i++)
                    {
                        reportsHtmlTable += "<tr>" +
                                        "<td width='20px'><div style='cursor:pointer;border-radius: 15px;background-color: lightblue;color: black;text-align: center;'>" + (i + 1).ToString() + "</div></td>" +
                                        "<td><a href='#'>" + sortedDataTable.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                        "</tr>";
                    }
                }
                else
                {
                    reportsHtmlTable += "<tr>" +
                                     "<td> No Data Found !</td></tr>";
                }
                reportsInnerHtml += reportsHtmlTable +
                        "</tbody></table></div></div></div></div>";

                //================== Preparation of Views ===================================
                string viewsInnerHtml = string.Empty;
                if (Session["ViewsForms"] != null && Session["ViewsForms"].ToString() != "")
                {
                    viewsInnerHtml = Session["ViewsForms"].ToString();
                }
                secondRowEnd = "</div>";

                //===================== GET Column Strings ================
                string FCStrat = string.Empty;
                if (Session["FirstColumnStart"] != null && Session["FirstColumnStart"].ToString() != "")
                {
                    FCStrat = Session["FirstColumnStart"].ToString();
                }

                string FCEnd = string.Empty;
                if (Session["FirstColumnEnd"] != null && Session["FirstColumnEnd"].ToString() != "")
                {
                    FCEnd = Session["FirstColumnEnd"].ToString();
                }

                string SCStart = string.Empty;
                if (Session["SecondColumnStart"] != null && Session["SecondColumnStart"].ToString() != "")
                {
                    SCStart = Session["SecondColumnStart"].ToString();
                }

                string SCEnd = string.Empty;
                if (Session["SecondColumnEnd"] != null && Session["SecondColumnEnd"].ToString() != "")
                {
                    SCEnd = Session["SecondColumnEnd"].ToString();
                }

                string TCStart = string.Empty;
                if (Session["ThirdColumnStart"] != null && Session["ThirdColumnStart"].ToString() != "")
                {
                    TCStart = Session["ThirdColumnStart"].ToString();
                }

                string TCEnd = string.Empty;
                if (Session["ThirdColumnEnd"] != null && Session["ThirdColumnEnd"].ToString() != "")
                {
                    TCEnd = Session["ThirdColumnEnd"].ToString();
                }

                string aFavoriteFormsHtml = string.Empty;
                if (Session["aFavoriteFormsHtml"] != null && Session["aFavoriteFormsHtml"].ToString() != "")
                {
                    aFavoriteFormsHtml = Session["aFavoriteFormsHtml"].ToString();
                }

                //================= Set Div Position ========================
                if (hdnIsMax.Value.ToString() != "true")
                {
                    setDivPosition(aMasterFormsHtml, transactionFormsInnerHtml, alertsInnerHtml, endString, reportsInnerHtml, viewsInnerHtml, FCStrat, FCEnd, SCStart, SCEnd, TCStart, TCEnd, aFavoriteFormsHtml);
                }
                else
                {
                    reportsInnerHtml = reportsInnerHtml.Replace("fixed_height_320", "fixed_height_520");
                    reportsInnerHtml = reportsInnerHtml.Replace("divtable", "divtableNew");
                    divMain.InnerHtml = "<div class='row'>" + reportsInnerHtml +
                                        "<div  class='col-md-8 col-sm-8 col-xs-12'>" +
                                        "<div class='x_panel tile fixed_height_520'>" +
                                        "<h2> Sample information about Div</h2>" +
                                        "</div></div></div>";
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Method to sort Views Forms div and set to main div..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkForViews_Click(object sender, EventArgs e)
        {
            try
            {
                //Data Table For All Views
                DataTable ViewsDt = (DataTable)Session["ViewsDataTable"];
                DataTable sortedDataTable = new DataTable();
                if (!string.IsNullOrEmpty(hdnSearchValueForViews.Value.ToString()))
                {
                    DataRow[] strConditionArr = ViewsDt.Select("strMenuName LIKE '%" + hdnSearchValueForViews.Value.ToString().Trim() + "%'").ToArray();
                    if (strConditionArr.Length > 0)
                    {
                        sortedDataTable = ViewsDt.Select("strMenuName LIKE '%" + hdnSearchValueForViews.Value.ToString().Trim() + "%'").CopyToDataTable();
                    }
                }
                else
                {
                    sortedDataTable = (DataTable)Session["ViewsDataTable"];
                }

                //====================== Preparation of Master Forms =======================
                string aMasterFormsHtml = string.Empty;
                if (Session["MasterForms"] != null && Session["MasterForms"].ToString() != "")
                {
                    aMasterFormsHtml = Session["MasterForms"].ToString();
                }

                //====================== Preparation of transaction ========================
                string transactionFormsInnerHtml = string.Empty;
                if (Session["TransactionForms"] != null && Session["TransactionForms"].ToString() != "")
                {
                    transactionFormsInnerHtml = Session["TransactionForms"].ToString();
                }

                //====================== Preaparation of Alert Div =========================
                string alertsInnerHtml = string.Empty;
                if (Session["AlertsForms"] != null && Session["AlertsForms"].ToString() != "")
                {
                    alertsInnerHtml = Session["AlertsForms"].ToString();
                }

                //================= END STRING ========================================
                string endString = string.Empty;
                if (Session["FirstRowEndString"] != null && Session["FirstRowEndString"].ToString() != "")
                {
                    endString = Session["FirstRowEndString"].ToString();
                }

                //================ Preparation of Reports ==================================
                string reportsInnerHtml = string.Empty;
                string secondRowStart = string.Empty;
                string secondRowEnd = string.Empty;
                secondRowStart = "<div class='row'>";
                if (Session["ReportsForms"] != null && Session["ReportsForms"].ToString() != "")
                {
                    reportsInnerHtml = Session["ReportsForms"].ToString();
                }

                //================ Preparation of Views ====================================
                string viewsInnerHtml = string.Empty;
                string viewsHtmlTable = string.Empty;
                viewsInnerHtml = "<div class='col-md-12 col-sm-12 col-xs-12 ' style='padding:0px;'>" +
                            "<div class='x_panel tile fixed_height_320'>" +
                            "<div style='border-radius:5px;border-color:black'>" +
                            "<div class='x_title HeaderInnerMenu' >" +
                            "<h2 style='font-color:White;margin-top:2px;'>Views</h2>" +
                            "<ul class='nav navbar-right panel_toolbox'>" +
                            "<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>" +
                            "<li><a href='#' id='OpenViews'  onclick='javascript:return openDivFull(this)'><i class='glyphicon glyphicon-fullscreen'></i></a></li>" +
                            "<li><a class='close-link' ID='Views'  onclick='javascript:return setDivPosition(this)'><i ><lable class='fa fa-close' ></lable></i></a>" +
                            "</li>" +
                            "</ul>" +
                            "<div class='input-group' style='float:left;'><input type='text' width='50%' class='form-control' runat='server' id='txtSearchViews' placeholder='Search for...' value ='" + hdnSearchValueForViews.Value.ToString() + "'><span class='input-group-btn'><button class='btn btn-primary' type='button' onclick='javascript:return getSortedDataforViews()'>Go!</button></span></div>" +
                            "<div class='clearfix'></div>" +
                            "</div></div>" +
                            "<div class='x_content'>" +
                            "<div runat='server' id='divTable'  class='divtable table-responsive'>" +
                            "<table class='table table-bordered table-striped jambo_table bulk_action'><tbody>";
                if (sortedDataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < sortedDataTable.Rows.Count; i++)
                    {
                        viewsHtmlTable += "<tr>" +
                                        "<td width='20px' ><div style='cursor:pointer;border-radius: 15px;background-color: lightblue;color: black;text-align: center;'>" + (i + 1).ToString() + "</div></td>" +
                                        "<td><a href='#'>" + sortedDataTable.Rows[i]["strMenuName"].ToString() + "</a></td>" +
                                        "</tr>";
                    }
                }
                else
                {
                    viewsHtmlTable += "<tr>" +
                                    "<td> No Data Found !</td></tr>";
                }
                viewsInnerHtml += viewsHtmlTable +
                                   "</tbody></table></div></div></div></div>";
                secondRowEnd = "</div>";

                //===================== GET Column Strings ================
                string FCStrat = string.Empty;
                if (Session["FirstColumnStart"] != null && Session["FirstColumnStart"].ToString() != "")
                {
                    FCStrat = Session["FirstColumnStart"].ToString();
                }

                string FCEnd = string.Empty;
                if (Session["FirstColumnEnd"] != null && Session["FirstColumnEnd"].ToString() != "")
                {
                    FCEnd = Session["FirstColumnEnd"].ToString();
                }

                string SCStart = string.Empty;
                if (Session["SecondColumnStart"] != null && Session["SecondColumnStart"].ToString() != "")
                {
                    SCStart = Session["SecondColumnStart"].ToString();
                }

                string SCEnd = string.Empty;
                if (Session["SecondColumnEnd"] != null && Session["SecondColumnEnd"].ToString() != "")
                {
                    SCEnd = Session["SecondColumnEnd"].ToString();
                }

                string TCStart = string.Empty;
                if (Session["ThirdColumnStart"] != null && Session["ThirdColumnStart"].ToString() != "")
                {
                    TCStart = Session["ThirdColumnStart"].ToString();
                }

                string TCEnd = string.Empty;
                if (Session["ThirdColumnEnd"] != null && Session["ThirdColumnEnd"].ToString() != "")
                {
                    TCEnd = Session["ThirdColumnEnd"].ToString();
                }

                string aFavoriteFormsHtml = string.Empty;
                if (Session["aFavoriteFormsHtml"] != null && Session["aFavoriteFormsHtml"].ToString() != "")
                {
                    aFavoriteFormsHtml = Session["aFavoriteFormsHtml"].ToString();
                }

                //================= Set Div Position ========================
                if (hdnIsMax.Value.ToString() != "true")
                {
                    setDivPosition(aMasterFormsHtml, transactionFormsInnerHtml, alertsInnerHtml, endString, reportsInnerHtml, viewsInnerHtml, FCStrat, FCEnd, SCStart, SCEnd, TCStart, TCEnd, aFavoriteFormsHtml);
                }
                else
                {
                    viewsInnerHtml = viewsInnerHtml.Replace("fixed_height_320", "fixed_height_520");
                    viewsInnerHtml = viewsInnerHtml.Replace("divtable", "divtableNew");
                    divMain.InnerHtml = "<div class='row'>" + viewsInnerHtml +
                                        "<div  class='col-md-8 col-sm-8 col-xs-12'>" +
                                        "<div class='x_panel tile fixed_height_520'>" +
                                        "<h2> Sample information about Div</h2>" +
                                        "</div></div></div>";
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Method to set div fullscreen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkOpenDivFullScreen_Click(object sender, EventArgs e)
        {
            string divInnerHtml = string.Empty;
            if (hdnDivName.Value.ToString() == "OpenMaster")
            {
                if (Session["MasterForms"] != null && Session["MasterForms"].ToString() != "")
                {
                    divInnerHtml = Session["MasterForms"].ToString();
                }
            }
            if (hdnDivName.Value.ToString() == "OpenTran")
            {
                if (Session["TransactionForms"] != null && Session["TransactionForms"].ToString() != "")
                {
                    divInnerHtml = Session["TransactionForms"].ToString();
                }
            }
            if (hdnDivName.Value.ToString() == "OpenAlerts")
            {
                if (Session["AlertsForms"] != null && Session["AlertsForms"].ToString() != "")
                {
                    divInnerHtml = Session["AlertsForms"].ToString();
                }
            }
            if (hdnDivName.Value.ToString() == "OpenReports")
            {
                if (Session["ReportsForms"] != null && Session["ReportsForms"].ToString() != "")
                {
                    divInnerHtml = Session["ReportsForms"].ToString();
                }
            }
            if (hdnDivName.Value.ToString() == "OpenViews")
            {
                if (Session["ViewsForms"] != null && Session["ViewsForms"].ToString() != "")
                {
                    divInnerHtml = Session["ViewsForms"].ToString();
                }
            }

            divInnerHtml = divInnerHtml.Replace("fixed_height_320", "fixed_height_520");
            divInnerHtml = divInnerHtml.Replace("divtable", "divtableNew");
            divInnerHtml = divInnerHtml.Replace("col-md-12 col-sm-12 col-xs-12", "col-md-4 col-sm-4 col-xs-12");
            divMain.InnerHtml = "<div class='row'>" + divInnerHtml +
                                "<div  class='col-md-8 col-sm-8 col-xs-12'>" +
                                "<div class='x_panel tile fixed_height_520'>" +
                                "<h2> Sample information about Div</h2>" +
                                "<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>" +
                                "<p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>" +
                                "</div></div></div>";
        }

        /// <summary>
        /// Method to Export excel on print click from Alerts..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkPrintAlert_Click(object sender, EventArgs e)
        {
            try
            {
                Session["strExcelReportflag"] = "Y";
                Session["CompanyHeaderFlag"] = "Y";
                Session["format"] = "xls";

                string AlertId = hdnPrintAlertID.Value.ToString();
                DataTable alertsDataTable = (DataTable)Session["AlertsDataTable"];
                DataView dv = alertsDataTable.AsDataView();
                dv.RowFilter = "intAlertQueryId = " + Convert.ToInt32(AlertId.ToString());

                string alertType = dv[0]["strAlertType"].ToString();
                DataSet dsHeader = new DataSet();
                dsHeader = objLibrary.FillDataSet(ObjAlertQueryConstant.SP_FillGrid, Session["CompanyID"].ToString(), Session["BranchID"].ToString(), Session["FinancialYearId"].ToString(), AlertId.ToString(), Session["RoleId"].ToString(), Session["UserID"].ToString());
                Session["ExcelCriteria"] = "";
                Session["ExcelDS"] = dsHeader;
                Session["ExcelRptName"] = alertType;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "openWindow();", true);
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Method to display all alerts details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkViewAlerts_Click(object sender, EventArgs e)
        {
            try
            {
                string AlertId = hdnPrintAlertID.Value.ToString();
                Session["AlertID"] = AlertId.ToString();
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "window.open('frmAlerts.aspx','_newtab','width=830px,height=600px,scrollbars=1,left=100px,top=100px',1);", true);
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Type", "OpenClosed(3);", true);
        }

        protected void lnkOpenView_Click(object sender, EventArgs e)
        {
            try
            {
                //Int32 rptID;
                //Int32 i = Convert.ToInt32(hdnOpenViewIndex.Value);
                //DataSet ViewDs = (DataSet)Session["ViewsDataSet"];
                //DataSet dstemp = new DataSet();
                //dstemp = ViewDs.Clone();
                //if (dstemp == null) dstemp = ViewDs.Clone();
                //else dstemp.Clear();
                //rptID = Convert.ToInt32(hdnOpenViewId .Value );
                //dstemp.Tables[0].ImportRow(ViewDs.Tables[0].Rows[i]);
                //    Session["SESSION_dstemp"] = dstemp;
                //    Session["RptId"] = rptID;
                //    Session["MISRptId"] = rptID;
                //    Session["AutoMISReportID"] = Convert.ToInt32(ViewDs.Tables[0].Rows[i]["intMDReportAutoId"].ToString());
                //    Session["btnTempSave_Click"] ="Y";
                //    string Control1 = "btnTempSaveundefined";
                //    System.Text.StringBuilder SB = new System.Text.StringBuilder();
                //    SB.Append("<script language='Javascript'>");
                //    if (HIDBrowser.Value == "Microsoft Internet Explorer") SB.Append("var _opener = window.dialogArguments;");
                //    else SB.Append("var _opener = opener;");
                //    SB.Append("_opener.$get('" + Control1 + "').click();");
                //    SB.Append("_opener.focus();");
                //    //if (!btnFlag) SB.Append("window.close();");
                //    SB.Append("</script>");
                //    if (!Page.ClientScript.IsClientScriptBlockRegistered("abc"))
                //    {
                //        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "abc", SB.ToString().Trim());
                //    }
                //    Session["CallFromWorkFlow"] = "Yes";
                //    Response.Redirect("frmMISReport.aspx?Module=General");
                // //   ScriptManager.RegisterStartupScript(Page, Page.GetType(), Page.ToString(), "timeout();", true);
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            bindHomePage();
        }
    }
}