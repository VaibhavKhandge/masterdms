﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BusinessLayer
{
    public class ManageDistributor
    {
        public int intCompanyID, intCloseByID, intPINCode, intDistributerID, intFYID, intBranchID;
        public string strDistributerName, strDistributerAddress, strDistributerWarehouse, strDistributerPhoneNo, strDistributerMobileNo,
        strDistributerEmail, strDistributerFAX, strCloseFlag, strCloseDate, strCloseRemark, strDistributorCity, strDistributorState;

        public string ManagetblDistributerMaster()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[16];
                int i = 0;
                parm[i++] = new SqlParameter("@intDistributerID", intDistributerID);
                parm[i++] = new SqlParameter("@intCompanyID", intCompanyID);
                parm[i++] = new SqlParameter("@strDistributerName", strDistributerName);
                parm[i++] = new SqlParameter("@strDistributerAddress", strDistributerAddress);
                parm[i++] = new SqlParameter("@strDistributerWarehouse", strDistributerWarehouse);
                parm[i++] = new SqlParameter("@strDistributerPhoneNo", strDistributerPhoneNo);
                parm[i++] = new SqlParameter("@strDistributerMobileNo", strDistributerMobileNo);
                parm[i++] = new SqlParameter("@strDistributerEmail", strDistributerEmail);
                parm[i++] = new SqlParameter("@strDistributerFAX", strDistributerFAX);
                parm[i++] = new SqlParameter("@strCloseFlag", strCloseFlag);
                parm[i++] = new SqlParameter("@intCloseByID", intCloseByID);
                parm[i++] = new SqlParameter("@strCloseRemark", strCloseRemark);
                parm[i++] = new SqlParameter("@strCloseDate", strCloseDate);
                parm[i++] = new SqlParameter("@intPINCode", intPINCode);
                parm[i++] = new SqlParameter("@strDistributorState", strDistributorState);
                parm[i++] = new SqlParameter("@strDistributorCity", strDistributorCity);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblDistributerMaster", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GettblDistributerMaster()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intDistributerID", intDistributerID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblDistributerMaster", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GettblDistributerMaster_View()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intDistributerID", intDistributerID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblDistributerMaster_View", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        //aGetCompanyDetails
        public DataTable aGetCompanyDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intCompanyID", intCompanyID);
                parm[i++] = new SqlParameter("@intBranchID", intBranchID);
                parm[i++] = new SqlParameter("@intFYID", intFYID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("aGetCompanyDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }
    }
}