﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLayer;

namespace eBILL.UI
{
    public partial class frmSalesExecutiveMaster : System.Web.UI.Page
    {
        # region [Branch_ID Property]
        protected int Branch_ID
        {
            get { return ViewState["Branch_ID"] != null ? (Int32)ViewState["Branch_ID"] : 0; }
            set { ViewState["Branch_ID"] = value; }
        }
        # endregion

        # region [Company_ID Property]
        protected int Company_ID
        {
            get { return ViewState["Company_ID"] != null ? (Int32)ViewState["Company_ID"] : 0; }
            set { ViewState["Company_ID"] = value; }
        }
        # endregion

        # region [FY_ID Property]
        protected int FY_ID
        {
            get { return ViewState["FY_ID"] != null ? (Int32)ViewState["FY_ID"] : 0; }
            set { ViewState["FY_ID"] = value; }
        }
        # endregion

        # region [Role_ID Property]
        protected int Role_ID
        {
            get { return ViewState["Role_ID"] != null ? (Int32)ViewState["Role_ID"] : 0; }
            set { ViewState["Role_ID"] = value; }
        }
        # endregion

        # region [dsHeader Property]
        public DataTable dsHeader
        {
            get { return ViewState["dsHeader"] != null ? (DataTable)ViewState["dsHeader"] : null; }
            set { ViewState["dsHeader"] = value; }
        }

        #endregion

        ManageSalesExecutive OBJManageSalesExec = new ManageSalesExecutive();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["intSalesExecutiveID"] != null && Request.QueryString["intSalesExecutiveID"]!="")
                    {
                        Branch_ID = Convert.ToInt32(Session["BranchID"].ToString());
                        Company_ID = Convert.ToInt32(Session["CompanyID"].ToString());
                        FY_ID = Convert.ToInt32(Session["FinancialYearId"].ToString());
                        Role_ID = Convert.ToInt32(Session["RoleId"].ToString());
                        txtExeCloseDate.Text = System.DateTime.Now.ToString("dd/MMM/yyyy");

                        ControlEnableDisable(false);
                        btnSave.Visible = false;
                        btnUpdate.Visible = true;

                        HIDExecutive.Value = Request.QueryString["intSalesExecutiveID"].ToString();
                        DataTable aGetDistrbuterDetails = new DataTable();
                        OBJManageSalesExec.intSalesExecutiveID = Convert.ToInt32(Request.QueryString["intSalesExecutiveID"].ToString());
                        aGetDistrbuterDetails = OBJManageSalesExec.GettblSalesExecutiveMaster_View();

                        if (aGetDistrbuterDetails.Rows.Count > 0)
                        {
                            HIDExecutive.Value = aGetDistrbuterDetails.Rows[0]["intSalesExecutiveID"].ToString();
                            txtExeCode.Text = aGetDistrbuterDetails.Rows[0]["strSalesExecutiveCode"].ToString();
                            txtExeName.Text = aGetDistrbuterDetails.Rows[0]["strSalesExecutiveName"].ToString();

                            HIDExeCompID.Value = aGetDistrbuterDetails.Rows[0]["intCompanyID"].ToString();
                            txtExeCompany.Text = aGetDistrbuterDetails.Rows[0]["strCompanyName"].ToString();
                            HIDExecutiveDistID.Value = aGetDistrbuterDetails.Rows[0]["intDistributerID"].ToString();
                            txtExecutiveDist.Text = aGetDistrbuterDetails.Rows[0]["strDistributerName"].ToString();

                            txtExeAddress.Text = aGetDistrbuterDetails.Rows[0]["strSalesExecutiveAddress"].ToString();
                            txtExePhone.Text = aGetDistrbuterDetails.Rows[0]["strSalesExecutivePhoneNo"].ToString();
                            txtExeEmail.Text = aGetDistrbuterDetails.Rows[0]["strSalesExecutiveEmail"].ToString();
                            txtExeMobile.Text = aGetDistrbuterDetails.Rows[0]["strSalesExecutiveMobileNo"].ToString();

                            chkExeClose.Checked = aGetDistrbuterDetails.Rows[0]["strCloseFlag"].ToString() == "Y" ? true : false;
                            if (chkExeClose.Checked == true)
                            {
                                chkExeClose_CheckedChanged(null, null);
                                txtExeCloseDate.Text = aGetDistrbuterDetails.Rows[0]["strCloseDate"].ToString();
                                txtExeCloseRemark.Text = aGetDistrbuterDetails.Rows[0]["strCloseRemark"].ToString();
                            }
                            else
                            {
                                txtExeCloseDate.Text = "";
                                txtExeCloseRemark.Text = "";
                            }
                        }
                    }
                }
                setSearchButton();
            }
            catch(Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        private void setSearchButton()
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "setsearchButton('frmSalesExecutiveMaster','" + HIDExeCompID.ClientID + "','" + txtExeCompany.ClientID + "','strCompanyName','SelectExecutiveCompany');", true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "setsearchButton('frmSalesExecutiveMaster','" + HIDExecutiveDistID.ClientID + "','" + txtExecutiveDist.ClientID + "','strDistributerName','SelectExecutiveDistibutor');", true);
        }

        protected void ControlEnableDisable(bool bVal)
        {
            txtExeName.Enabled = bVal;
            btnExecutiveCompany.Visible = bVal;
            txtExeCompany.Enabled = bVal;

            btnExeDistributor.Visible = bVal;
            txtExecutiveDist.Enabled = bVal;
        }

        protected void Page_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string aStatus = string.Empty;
                if (e.CommandName == "btnSave")
                {
                    OBJManageSalesExec.intSalesExecutiveID = HIDExecutive.Value == "" ? 0 : Convert.ToInt32(HIDExecutive.Value);
                    OBJManageSalesExec.intCompanyID = HIDExeCompID.Value == "" ? 0 : Convert.ToInt32(HIDExeCompID.Value);
                    OBJManageSalesExec.intDistributerID = HIDExecutiveDistID.Value == "" ? 0 : Convert.ToInt32(HIDExecutiveDistID.Value);
                    OBJManageSalesExec.strSalesExecutiveName = txtExeName.Text != "" ? txtExeName.Text.Trim() : "";
                    OBJManageSalesExec.strSalesExecutiveAddress = txtExeAddress.Text != "" ? txtExeAddress.Text.Trim() : "";
                    OBJManageSalesExec.strSalesExecutivePhoneNo = txtExePhone.Text != "" ? txtExePhone.Text.Trim() : "";
                    OBJManageSalesExec.strSalesExecutiveMobileNo = txtExeMobile.Text != "" ? txtExeMobile.Text.Trim() : "";
                    OBJManageSalesExec.strSalesExecutiveEmail = txtExeEmail.Text != "" ? txtExeEmail.Text.Trim() : "";
                    OBJManageSalesExec.strCloseFlag = chkExeClose.Checked ? "Y" : "N";
                    if (chkExeClose.Checked == true)
                    {
                        OBJManageSalesExec.intCloseByID = Convert.ToInt32(Role_ID);
                        OBJManageSalesExec.strCloseDate = txtExeCloseDate.Text.Trim();
                        OBJManageSalesExec.strCloseRemark = txtExeCloseRemark.Text != "" ? txtExeCloseRemark.Text.Trim() : "";
                    }
                    else
                    {
                        OBJManageSalesExec.intCloseByID = 0;
                        OBJManageSalesExec.strCloseDate = "";
                        OBJManageSalesExec.strCloseRemark = "";
                    }
                    aStatus = OBJManageSalesExec.ManagetblSalesExecutiveMaster();
                    if (aStatus == "Duplicate")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(3);", true);
                        return;
                    }
                    else if (aStatus != "")
                    {
                        if (aStatus == HIDExecutive.Value)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(1);", true);
                        }
                        else if (aStatus != "")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(2);", true);
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "CloseDiv();", true);
                    }
                }
                else if (e.CommandName == "btnClear")
                {
                    txtExeName.Text = string.Empty;
                    txtExeName.Text = string.Empty;
                    txtExeCompany.Text = string.Empty;
                    HIDExeCompID.Value = "0";
                    txtExecutiveDist.Text = string.Empty;
                    HIDExecutiveDistID.Value = "0";

                    txtExeAddress.Text = string.Empty;
                    txtExePhone.Text = string.Empty;
                    txtExeMobile.Text = string.Empty;
                    txtExeEmail.Text = string.Empty;

                    chkExeClose.Checked = false;
                    txtExeCloseDate.Text = string.Empty;
                    txtExeCloseRemark.Text = string.Empty;
                }
                else if (e.CommandName == "btnUpdate")
                {
                    OBJManageSalesExec.intSalesExecutiveID = HIDExecutive.Value == "" ? 0 : Convert.ToInt32(HIDExecutive.Value);
                    OBJManageSalesExec.intCompanyID = HIDExeCompID.Value == "" ? 0 : Convert.ToInt32(HIDExeCompID.Value);
                    OBJManageSalesExec.intDistributerID = HIDExecutiveDistID.Value == "" ? 0 : Convert.ToInt32(HIDExecutiveDistID.Value);
                    OBJManageSalesExec.strSalesExecutiveName = txtExeName.Text != "" ? txtExeName.Text.Trim() : "";
                    OBJManageSalesExec.strSalesExecutiveAddress = txtExeAddress.Text != "" ? txtExeAddress.Text.Trim() : "";
                    OBJManageSalesExec.strSalesExecutivePhoneNo = txtExePhone.Text != "" ? txtExePhone.Text.Trim() : "";
                    OBJManageSalesExec.strSalesExecutiveMobileNo = txtExeMobile.Text != "" ? txtExeMobile.Text.Trim() : "";
                    OBJManageSalesExec.strSalesExecutiveEmail = txtExeEmail.Text != "" ? txtExeEmail.Text.Trim() : "";
                    OBJManageSalesExec.strCloseFlag = chkExeClose.Checked ? "Y" : "N";
                    if (chkExeClose.Checked == true)
                    {
                        OBJManageSalesExec.intCloseByID = Convert.ToInt32(Role_ID);
                        OBJManageSalesExec.strCloseDate = txtExeCloseDate.Text.Trim();
                        OBJManageSalesExec.strCloseRemark = txtExeCloseRemark.Text != "" ? txtExeCloseRemark.Text.Trim() : "";
                    }
                    else
                    {
                        OBJManageSalesExec.intCloseByID = 0;
                        OBJManageSalesExec.strCloseDate = "";
                        OBJManageSalesExec.strCloseRemark = "";
                    }
                    aStatus = OBJManageSalesExec.ManagetblSalesExecutiveMaster();
                    if (aStatus == "Duplicate")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(3);", true);
                        return;
                    }
                    else if (aStatus != "")
                    {
                        if (aStatus == HIDExecutive.Value)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(1);", true);
                        }
                        else if (aStatus != "")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ClosePopup(2);", true);
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "CloseDiv();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void chkExeClose_CheckedChanged(object sender, EventArgs e)
        {
            if (chkExeClose.Checked == true)
                divClose.Visible = true;
            else
                divClose.Visible = false;
        }
    }
}