﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace eBILL.UI.UserControls
{
    public partial class MyMessageBox : System.Web.UI.UserControl
    {
        #region Properties
        public bool ShowCloseButton { get; set; }
        #endregion

        #region [ Delegate Event declaration ]
        public delegate void OnYesButtonClick(object sender, EventArgs e);
        public delegate void OnNoButtonClick(object sender, EventArgs e);
        public delegate void OnCancelButtonClick(object sender, EventArgs e);
        #endregion

        #region [ Event declaration ]
        public event OnYesButtonClick btnYesHandler;
        public event OnNoButtonClick btnNoHandler;
        public event OnCancelButtonClick btnCancelHandler;
        #endregion

        #region [ Button Events ]
        protected void btnYes_Click(object sender, EventArgs e)
        {
            if (btnYesHandler != null)
            {
                btnYesHandler(this, e);
            }
        }
        protected void btnNo_Click(object sender, EventArgs e)
        {
            if (btnNoHandler != null)
            {
                btnNoHandler(this, e);
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (btnCancelHandler != null)
            {
                btnCancelHandler(this, e);
            }
        }
        #endregion

        #region Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ShowCloseButton)
                CloseButton.Attributes.Add("onclick", "document.getElementById('" + MessageBox.ClientID + "').style.display = 'none'");
            btnYes.Click += new EventHandler(btnYes_Click);
            btnNo.Click += new EventHandler(btnNo_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
        }
        #endregion

        #region Wrapper methods
        public void ShowError(string message, int height, int width, int Yes, int No, int Cancel)
        {
            Show(MessageType.Error, message, height, width, Yes, No, Cancel);
        }

        public void ShowInfo(string message, int height, int width, int Yes, int No, int Cancel)
        {
            Show(MessageType.Info, message, height, width, Yes, No, Cancel);
        }

        public void ShowSuccess(string message, int height, int width, int Yes, int No, int Cancel)
        {
            Show(MessageType.Success, message, height, width, Yes, No, Cancel);
        }

        public void ShowWarning(string message, int height, int width, int Yes, int No, int Cancel)
        {
            Show(MessageType.Warning, message, height, width, Yes, No, Cancel);
        }
        #endregion

        #region Show control
        public void Show(MessageType messageType, string message, int height, int width, int Yes, int No, int Cancel)
        {
            CloseButton.Visible = ShowCloseButton;
            PID.InnerText = message;
            MessageBox.Height = height;
            MessageBox.Width = width;

            btnYes.Visible = Yes == 0 ? false : true;
            btnNo.Visible = No == 0 ? false : true;
            btnCancel.Visible = Cancel == 0 ? false : true;

            MessageBox.CssClass = messageType.ToString().ToLower();
            ModalPopupExtenderMessage.Show();
            this.Visible = true;
        }
        
        public void Show(MessageType messageType, string message, int height, int width, int Yes, int No, int Cancel, string YesCap, string NoCap, string CancelCap)
        {
            CloseButton.Visible = ShowCloseButton;
            PID.InnerText = message;
            MessageBox.Height = height;
            MessageBox.Width = width;

            btnYes.Visible = Yes == 0 ? false : true;
            btnNo.Visible = No == 0 ? false : true;
            btnCancel.Visible = Cancel == 0 ? false : true;

            btnYes.Text = YesCap == "" ? btnYes.Text : YesCap;
            btnNo.Text = NoCap == "" ? btnNo.Text : NoCap;
            btnCancel.Text = CancelCap == "" ? btnCancel.Text : CancelCap;

            MessageBox.CssClass = messageType.ToString().ToLower();
            ModalPopupExtenderMessage.Show();
            this.Visible = true;
        }
        #endregion

        #region Enum
        public enum MessageType
        {
            Error = 1,
            Info = 2,
            Success = 3,
            Warning = 4
        }
        #endregion
    }
}