﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using BusinessLayer;
using System.IO;

namespace eBILL.UI
{
    public partial class frmSalesmanMasterView : System.Web.UI.Page
    {
        # region [Branch_ID Property]
        protected int Branch_ID
        {
            get { return ViewState["Branch_ID"] != null ? (Int32)ViewState["Branch_ID"] : 0; }
            set { ViewState["Branch_ID"] = value; }
        }
        # endregion

        # region [Company_ID Property]
        protected int Company_ID
        {
            get { return ViewState["Company_ID"] != null ? (Int32)ViewState["Company_ID"] : 0; }
            set { ViewState["Company_ID"] = value; }
        }
        # endregion

        # region [FY_ID Property]
        protected int FY_ID
        {
            get { return ViewState["FY_ID"] != null ? (Int32)ViewState["FY_ID"] : 0; }
            set { ViewState["FY_ID"] = value; }
        }
        # endregion

        # region [Role_ID Property]
        protected int Role_ID
        {
            get { return ViewState["Role_ID"] != null ? (Int32)ViewState["Role_ID"] : 0; }
            set { ViewState["Role_ID"] = value; }
        }
        # endregion

        # region [dsHeader Property]
        public DataTable dsHeader
        {
            get { return ViewState["dsHeader"] != null ? (DataTable)ViewState["dsHeader"] : null; }
            set { ViewState["dsHeader"] = value; }
        }
        #endregion

        # region [dsExcel Property]
        public DataTable dsExcel
        {
            get { return ViewState["dsExcel"] != null ? (DataTable)ViewState["dsExcel"] : null; }
            set { ViewState["dsExcel"] = value; }
        }
        #endregion

        ManageSalesmanMaster OBJManageSalesmanMaster = new ManageSalesmanMaster();

        protected void Page_Load(object sender, EventArgs e)
        {
            Branch_ID = Convert.ToInt32(Session["BranchID"].ToString());
            Company_ID = Convert.ToInt32(Session["CompanyID"].ToString());
            FY_ID = Convert.ToInt32(Session["FinancialYearId"].ToString());
            Role_ID = Convert.ToInt32(Session["RoleId"].ToString());
            try
            {
                if (!IsPostBack)
                {
                    OBJManageSalesmanMaster.intSalesmanID = 0;
                    dsHeader = OBJManageSalesmanMaster.GettblSalesmanMaster();
                    GRDDist.DataSource = dsHeader;
                    GRDDist.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {
                OBJManageSalesmanMaster.intSalesmanID = 0;
                dsHeader = OBJManageSalesmanMaster.GettblSalesmanMaster();
                GRDDist.DataSource = dsHeader;
                GRDDist.DataBind();
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void Page_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string aFileName = string.Empty;
                if (e.CommandName == "btnPrint")
                {
                    StringWriter aPrintHTML = new StringWriter();
                    aPrintHTML.Write("<br>");
                    aPrintHTML.Write("<table style='width:900px'>");
                    aPrintHTML.Write("<tr><td colspan='5' style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'></td>");
                    aPrintHTML.Write("<td colspan='3' style='text-align:center;font-family:Verdana;font-weight:bold; font-size:20px;color:#000;border:1px solid #000;width:900px;'>Sales Person Master View</td>");
                    aPrintHTML.Write("<td colspan='5' style='text-align:center;font-family:Verdana;font-weight:bold; font-size:20px;color:#000;border:1px solid #000;width:900px;'></td>");
                    aPrintHTML.Write("</tr>");
                    aPrintHTML.Write("</table>");
                    aPrintHTML.Write("<br>");
                    aPrintHTML.Write(" <table style='width:900px'>");
                    aPrintHTML.Write("<tr><td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Sr. No</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Code</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Sales Person Name</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Distributor</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Sales Executive</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Company</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Adderss</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Phone No.</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Mobile No.</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Email</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Close</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Close Date</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Close Remark</td>");
                    aPrintHTML.Write("</tr>");

                    OBJManageSalesmanMaster.intSalesmanID = 0;
                    dsExcel = OBJManageSalesmanMaster.GettblSalesmanMaster();

                    if (dsExcel.Rows.Count > 0)
                    {
                        for (int k = 0; k < dsExcel.Rows.Count; k++)
                        {
                            aPrintHTML.Write("<tr>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["SrNO"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strSalesmanCode"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strSalesmanName"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strDistributerName"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strSalesExecutiveName"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strCompanyName"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strSalesmanAddress"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strSalesmanPhoneNo"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strSalesmanMobileNo"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strSalesmanEmail"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strCloseFlag"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strCloseDate"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strCloseRemark"].ToString() + "</td>");

                            aPrintHTML.Write("</tr>");
                        }
                    }
                    aPrintHTML.Write(" </table>");
                    DateTime aDate = DateTime.Now;
                    aFileName = aDate.Month + "_" + aDate.Day + "_" + aDate.Hour + "_" + aDate.Minute + "_" + aDate.Second + "_" + "SalesPersonMaster";

                    Response.ClearContent();
                    Response.AddHeader("content-disposition", "attachment; filename=" + aFileName + ".xls");
                    Response.ContentType = "application/excel";
                    HtmlTextWriter htw = new HtmlTextWriter(aPrintHTML);
                    Response.Write(aPrintHTML.ToString());
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }
    }
}