﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BusinessLayer;
using System.Globalization;

namespace eBILL.UI.UserControls
{
    public partial class frmListViewControl : System.Web.UI.UserControl
    {
        public Library objLibrary = new Library();

        # region [dtBindGrid Property]
        public DataSet dtBindGrid
        {
            get { return ViewState["dtBindGrid"] != null ? (DataSet)ViewState["dtBindGrid"] : null; }
            set { ViewState["dtBindGrid"] = value; }
        }
        # endregion

        #region [ Delegate Event declaration ]
        public delegate void OnForwardImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnTextChanged(object sender, EventArgs e);
        #endregion

        #region [ Event declaration ]
        public event OnTextChanged OnTextChangedHandler;
        public event OnForwardImageButtonClick btnFirstImageHandler;
        #endregion

        # region [CurrPage Property]
        public string CurrPage
        {
            get { return ViewState["CurrPage"] != null ? (string)ViewState["CurrPage"] : "0"; }
            set { ViewState["CurrPage"] = value; }
        }
        # endregion

        # region [TotPage Property]
        public string TotPage
        {
            get { return ViewState["TotPage"] != null ? (string)ViewState["TotPage"] : "0"; }
            set { ViewState["TotPage"] = value; }
        }
        # endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void InsertSrNo(DataTable Table, int TopSkipped)
        {
            if (!Table.Columns.Contains("SRNo"))
            {
                Table.Columns.Add("SRNo");
            }
            for (int i = 0; i < Table.Rows.Count; i++)
            {
                Table.Rows[i]["SRNo"] = (TopSkipped + (i + 1)).ToString();

            }
        }

        public DataSet BindGrid(ref DataGrid GR, string BindSPName, string CompanyID, string BranchID, string txtFromDate, string txtToDate, string Top, string TopSkipped, string Where)
        {
            dtBindGrid = objLibrary.FillDataSet(BindSPName, CompanyID, BranchID, txtFromDate, txtToDate, Top, TopSkipped, Where);

            if (dtBindGrid.Tables[0].Rows.Count == 0)
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Record Not Found');", true);

            InsertSrNo(dtBindGrid.Tables[0], Convert.ToInt32(TopSkipped));
            GR.DataSource = dtBindGrid;
            GR.DataBind();
            GetTotalPages(GR);
            return dtBindGrid;
        }

        public void BindGrid(ref GridView GR, string BindSPName, string CompanyID, string BranchID, string FyID, string txtFromDate, string txtToDate, string Top, string TopSkipped, string Role, string AllFlag, string Where)
        {
            dtBindGrid = objLibrary.FillDataSet(BindSPName, CompanyID, BranchID, FyID, txtFromDate, txtToDate, Top, TopSkipped, Role, AllFlag, Where);

            if (dtBindGrid.Tables[0].Rows.Count == 0)
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Record Not Found');", true);

            GR.DataSource = dtBindGrid;
            GR.DataBind();
            GetTotalPages(GR);
        }

        public void BindGrid(ref GridView GR, string BindSPName, string CompanyID, string BranchID, string txtFromDate, string txtToDate, string Top, string TopSkipped, string Where)
        {
            dtBindGrid = objLibrary.FillDataSet(BindSPName, CompanyID, BranchID, txtFromDate, txtToDate, Top, TopSkipped, Where);
            if (dtBindGrid.Tables[0].Rows.Count == 0)
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Record Not Found');", true);

            GR.DataSource = dtBindGrid;
            GR.DataBind();
            GetTotalPages(GR);
        }

        public void BindGridNew(ref GridView GR, string BindSPName, string CompanyID, string BranchID, string txtFromDate, string txtToDate, string Top, string TopSkipped, string Role, string AllFlag, string Where)
        {
            dtBindGrid = objLibrary.FillDataSet(BindSPName, CompanyID, BranchID, txtFromDate, txtToDate, Top, TopSkipped, Role, AllFlag, Where);

            if (dtBindGrid.Tables[0].Rows.Count == 0)
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Record Not Found');", true);

            GR.DataSource = dtBindGrid;
            GR.DataBind();
            GetTotalPages(GR);
            objLibrary.EmptyGridFix(ref GR);
        }

        public void BindGrid(ref GridView GR, string BindSPName, string CompanyID, string BranchID, string txtFromDate, string txtToDate, string Top, string TopSkipped, string Role, string AllFlag, string Where)
        {
            dtBindGrid = objLibrary.FillDataSet(BindSPName, CompanyID, BranchID, txtFromDate, txtToDate, Top, TopSkipped, Role, AllFlag, Where);

            if (dtBindGrid.Tables[0].Rows.Count == 0)
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Record Not Found');", true);

            GR.DataSource = dtBindGrid;
            GR.DataBind();
            GetTotalPages(GR);
        }

        public void BindGrid(ref GridView GR, string BindSPName, string CompanyID, string BranchID, string Top, string TopSkipped, string Where)
        {
            dtBindGrid = objLibrary.FillDataSet(BindSPName, CompanyID, BranchID, Top, TopSkipped, Where);

            if (dtBindGrid.Tables[0].Rows.Count == 0)
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Record Not Found');", true);

            GR.DataSource = dtBindGrid;
            GR.DataBind();
            GetTotalPages(GR);
        }

        public void BindGrid(ref GridView GR, string BindSPName, string CompanyID, string BranchID, string FYId, string Top, string TopSkipped, string Where)
        {
            dtBindGrid = objLibrary.FillDataSet(BindSPName, CompanyID, BranchID, FYId, Top, TopSkipped, Where);

            if (dtBindGrid.Tables[0].Rows.Count == 0)
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Record Not Found');", true);

            GR.DataSource = dtBindGrid;
            GR.DataBind();
            GetTotalPages(GR);
        }

        public void BindGridOrderby(ref GridView GR, string BindSPName, string CompanyID, string BranchID, string FYId, string Top, string TopSkipped, string Where, string OrderBy)
        {
            dtBindGrid = objLibrary.FillDataSet(BindSPName, CompanyID, BranchID, FYId, Top, TopSkipped, Where, OrderBy);

            if (dtBindGrid.Tables[0].Rows.Count == 0)
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Record Not Found');", true);

            GR.DataSource = dtBindGrid;
            GR.DataBind();
            GetTotalPages(GR);
        }

        public void BindGridOrderby(ref GridView GR, DataSet dttempBindGrid, int totalrows)
        {
            dtBindGrid = dttempBindGrid.Clone();
            dtBindGrid = dttempBindGrid.Copy();

            if (dtBindGrid.Tables[0].Rows.Count == 0)
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Record Not Found');", true);

            GR.DataSource = dtBindGrid;
            GR.DataBind();
            GetTotalPagesNew(GR, totalrows);
        }

        public void BindDataGridOrderby(ref DataGrid GR, DataSet dttempBindGrid, int totalrows)
        {
            dtBindGrid = dttempBindGrid.Clone();
            dtBindGrid = dttempBindGrid.Copy();

            if (dtBindGrid.Tables[0].Rows.Count == 0)
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Record Not Found');", true);

            GR.DataSource = dtBindGrid;
            GR.DataBind();
            GetTotalPagesNewDataGrid(GR, totalrows);
        }

        public void GetTotalPagesNewDataGrid(DataGrid Grid, int totalrows)
        {
            Label2.Text = totalrows.ToString();
            int r = Convert.ToInt32(Label2.Text.ToString());
            double page;
            string[] st;
            if (r > 0 && r > Grid.PageSize)
            {
                page = Convert.ToDouble(r) / Convert.ToDouble(Grid.PageSize);
                st = Convert.ToString(page).Split('.');

                if (st.Length > 1)
                    Label3.Text = (Convert.ToInt32(st[0]) + 1).ToString();
                else
                    Label3.Text = st[0];
            }
            else
                Label3.Text = "1";

            TotPage = Label3.Text.ToString();

            TXTPageNumber.Text =
                TXTPageNumber.Text.ToString() == "0" || TXTPageNumber.Text.ToString() == "" ? "1" :
                Convert.ToInt32(TXTPageNumber.Text.ToString()) > Convert.ToInt32(TotPage) ? TotPage :
                TXTPageNumber.Text.ToString();
        }

        public DataSet BindGridDel(ref DataGrid GR, string BindSPName, string BranchID, string FYID, string txtFromDate, string txtToDate, string Top, string TopSkipped, string HeaderTableName, string Field_ID, string Type, string Where, string FormName)
        {
            dtBindGrid = objLibrary.FillDataSet(BindSPName, BranchID, FYID, txtFromDate, txtToDate, Top, TopSkipped, HeaderTableName, Field_ID, Type, Where, FormName);

            if (dtBindGrid.Tables[0].Rows.Count == 0)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Record Not Found');", true);
            }
            else
            {
            }
            GR.DataSource = dtBindGrid;
            GR.DataBind();
            GetTotalPagesDel(GR);
            return dtBindGrid;
        }

        private void GetTotalPagesDel(DataGrid Grid)
        {
            Label2.Text = dtBindGrid.Tables[0].Rows.Count > 0 ? dtBindGrid.Tables[0].Rows[0]["RowC"].ToString() : "0";
            int r = Convert.ToInt32(Label2.Text.ToString());
            double page;
            string[] st;
            if (r > 0 && r > Grid.PageSize)
            {
                page = Convert.ToDouble(r) / Convert.ToDouble(Grid.PageSize);
                st = Convert.ToString(page).Split('.');

                if (st.Length > 1)
                    Label3.Text = (Convert.ToInt32(st[0]) + 1).ToString();
                else
                    Label3.Text = st[0];
            }
            else //if (dtBindGrid.Tables[2].Rows.Count > GRDPurchaseList.PageSize)
                Label3.Text = "1";

            TotPage = Label3.Text.ToString();

            TXTPageNumber.Text =
                TXTPageNumber.Text.ToString() == "0" || TXTPageNumber.Text.ToString() == "" ? "1" :
                Convert.ToInt32(TXTPageNumber.Text.ToString()) > Convert.ToInt32(TotPage) ? TotPage :
                TXTPageNumber.Text.ToString();
        }

        public void GetTotalPagesNew(GridView Grid, int totalrows)
        {
            Label2.Text = totalrows.ToString();
            int r = Convert.ToInt32(Label2.Text.ToString());
            double page;
            string[] st;
            if (r > 0 && r > Grid.PageSize)
            {
                page = Convert.ToDouble(r) / Convert.ToDouble(Grid.PageSize);
                st = Convert.ToString(page).Split('.');

                if (st.Length > 1)
                    Label3.Text = (Convert.ToInt32(st[0]) + 1).ToString();
                else
                    Label3.Text = st[0];
            }
            else //if (dtBindGrid.Tables[2].Rows.Count > GRDPurchaseList.PageSize)
                Label3.Text = "1";

            TotPage = Label3.Text.ToString();

            TXTPageNumber.Text =
                TXTPageNumber.Text.ToString() == "0" || TXTPageNumber.Text.ToString() == "" ? "1" :
                Convert.ToInt32(TXTPageNumber.Text.ToString()) > Convert.ToInt32(TotPage) ? TotPage :
                TXTPageNumber.Text.ToString();
        }

        public void GetTotalPages(GridView Grid)
        {
            Label2.Text = dtBindGrid.Tables[0].Rows.Count > 0 ? dtBindGrid.Tables[0].Rows[0]["RowC"].ToString() : "0";
            int r = Convert.ToInt32(Label2.Text.ToString());
            double page;
            string[] st;
            if (r > 0 && r > Grid.PageSize)
            {
                page = Convert.ToDouble(r) / Convert.ToDouble(Grid.PageSize);
                st = Convert.ToString(page).Split('.');

                if (st.Length > 1)
                    Label3.Text = (Convert.ToInt32(st[0]) + 1).ToString();
                else
                    Label3.Text = st[0];
            }
            else //if (dtBindGrid.Tables[2].Rows.Count > GRDPurchaseList.PageSize)
                Label3.Text = "1";

            TotPage = Label3.Text.ToString();

            TXTPageNumber.Text =
                TXTPageNumber.Text.ToString() == "0" || TXTPageNumber.Text.ToString() == "" ? "1" :
                Convert.ToInt32(TXTPageNumber.Text.ToString()) > Convert.ToInt32(TotPage) ? TotPage :
                TXTPageNumber.Text.ToString();
        }

        public void GetTotalPages(DataGrid Grid)
        {
            Label2.Text = dtBindGrid.Tables[0].Rows.Count > 0 ? dtBindGrid.Tables[0].Rows[0]["RowC"].ToString() : "0";
            int r = Convert.ToInt32(Label2.Text.ToString());
            double page;
            string[] st;
            if (r > 0 && r > Grid.PageSize)
            {
                page = Convert.ToDouble(r) / Convert.ToDouble(Grid.PageSize);
                st = Convert.ToString(page).Split('.');

                if (st.Length > 1)
                    Label3.Text = (Convert.ToInt32(st[0]) + 1).ToString();
                else
                    Label3.Text = st[0];
            }
            else //if (dtBindGrid.Tables[2].Rows.Count > GRDPurchaseList.PageSize)
                Label3.Text = "1";

            TotPage = Label3.Text.ToString();

            TXTPageNumber.Text =
                TXTPageNumber.Text.ToString() == "0" || TXTPageNumber.Text.ToString() == "" ? "1" :
                Convert.ToInt32(TXTPageNumber.Text.ToString()) > Convert.ToInt32(TotPage) ? TotPage :
                TXTPageNumber.Text.ToString();
        }

        public void GetTotalPagesForSelection(DataGrid Grid, DataSet dsCommanData)
        {
            //dtBindGrid.Clear();
            dtBindGrid = dsCommanData.Copy();
            Label2.Text = dtBindGrid.Tables[0].Rows.Count.ToString();
            int r = Convert.ToInt32(Label2.Text.ToString());
            double page;
            string[] st;
            if (r > 0 && r > Grid.PageSize)
            {
                page = Convert.ToDouble(r) / Convert.ToDouble(Grid.PageSize);
                st = Convert.ToString(page).Split('.');

                if (st.Length > 1)
                    Label3.Text = (Convert.ToInt32(st[0]) + 1).ToString();
                else
                    Label3.Text = st[0];
            }
            else //if (dtBindGrid.Tables[2].Rows.Count > GRDPurchaseList.PageSize)
                Label3.Text = "1";

            TotPage = Label3.Text.ToString();

            TXTPageNumber.Text = (Convert.ToInt32(TotPage) - (Convert.ToInt32(TotPage) - 1)).ToString();
        }

        #region [ Button Events ]
        protected void btnImage_Click(object sender, ImageClickEventArgs e)
        {
            if (dtBindGrid.Tables[0].Rows.Count > 0)
            {
                string imgID = ((ImageButton)sender).ID.ToString();
                if (imgID == "imgFirst") TXTPageNumber.Text = "1";
                else if (imgID == "imgLast") TXTPageNumber.Text = Label3.Text.ToString();
                else if (imgID == "imgForword") TXTPageNumber.Text = (Convert.ToInt32(Label3.Text.ToString()) > Convert.ToInt32(TXTPageNumber.Text.ToString()) + 1 ? Convert.ToInt32(TXTPageNumber.Text.ToString()) + 1 : Convert.ToInt32(Label3.Text.ToString())).ToString();
                else if (imgID == "imgBack") TXTPageNumber.Text = (0 >= Convert.ToInt32(TXTPageNumber.Text.ToString()) - 1 ? 1 : Convert.ToInt32(TXTPageNumber.Text.ToString()) - 1).ToString();

                CurrPage = TXTPageNumber.Text.ToString();

                if (btnFirstImageHandler != null)
                    btnFirstImageHandler(this, e);
            }
        }
        #endregion

        protected void TXTPageNumber_OnTextChanged(object sender, EventArgs e)
        {
            if (dtBindGrid.Tables[0].Rows.Count > 0)
            {
                CurrPage = TXTPageNumber.Text.ToString();
                if (OnTextChangedHandler != null)
                {
                    OnTextChangedHandler(this, e);
                }
            }
        }
    }
}