﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using BusinessLayer;
using eBILL.UI.UserControls;

namespace eBILL.UI
{
    public partial class frmSelection : System.Web.UI.Page
    {
        public Library ObjLibrary = new Library();
        public string controlname = "";

        # region [PurchaseListPageIndex Property]
        public int PageIndex
        {
            get { return ViewState["PageIndex"] != null ? (Int32)ViewState["PageIndex"] : 0; }
            set { ViewState["PageIndex"] = value; }
        }
        # endregion

        #region [dsCommanData Property]
        public DataSet dsCommanData
        {
            get { return ViewState["dsCommanData"] != null ? (DataSet)ViewState["dsCommanData"] : null; }
            set { ViewState["dsCommanData"] = value; }
        }
        # endregion

        # region [ToBeSelect Property]
        public bool ToBeSelect
        {
            get { return ViewState["ToBeSelect"] != null ? (bool)ViewState["ToBeSelect"] : false; }
            set { ViewState["ToBeSelect"] = value; }
        }
        # endregion

        #region [dsToBeFill Property]
        public DataSet dsToBeFill
        {
            get { return ViewState["dsToBeFill"] != null ? (DataSet)ViewState["dsToBeFill"] : null; }
            set { ViewState["dsToBeFill"] = value; }
        }
        # endregion

        #region [dt Property]
        public DataTable dt
        {
            get { return ViewState["dt"] != null ? (DataTable)ViewState["dt"] : null; }
            set { ViewState["dt"] = value; }
        }
        #endregion

        #region [Filter Property]
        public string Filter
        {
            get { return ViewState["Filter"] != null ? (string)ViewState["Filter"] : ""; }
            set { ViewState["Filter"] = value; }
        }
        #endregion

        #region [SpForHelp Property]
        public string SpForHelp
        {
            get { return ViewState["SpForHelp"] != null ? (string)ViewState["SpForHelp"] : ""; }
            set { ViewState["SpForHelp"] = value; }
        }
        # endregion

        # region [FieldName Property]
        public string FieldName
        {
            get { return ViewState["FieldName"] != null ? (string)ViewState["FieldName"] : ""; }
            set { ViewState["FieldName"] = value; }
        }
        # endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    SpForHelp = "";
                    FieldName = "";
                    BindThisGrid(false);
                    ButtCancel.Attributes.Add("onclick", "javascript:window.close()");
                    if (Request.QueryString["SearchFire"] != null)
                    {
                        if (Request.QueryString["SearchFire"].ToString() == "Y")
                            ButtCancel.Visible = false;
                    }
                    else
                        ButtCancel.Visible = true;
                    for (int i = 1; i < Request.QueryString.Count; i++)
                    {
                        string Control1 = Request.QueryString[i].ToString().Trim();
                        if (Control1.ToLower().Contains("btn"))
                            LBLBtnText.Text = "Note : This Selection Auto Fills some details, if it is not done, then Click On right most button of Selection.";
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "GetBrowser();", true);
                }
                else
                {
                    //LSFilter.OnTextChangedHandler += new frmListViewControl.OnTextChanged(TXTPageNumber_OnTextChanged);
                    //LSFilter.btnFirstImageHandler += new frmListViewControl.OnForwardImageButtonClick(btnFirstBakFrdLast_Click);
                }
            }
            catch (Exception Ex)
            {
                Response.Write(Ex.ToString());
            }
        }

        protected void TXTPageNumber_OnTextChanged(object sender, EventArgs e)
        {
            if (LSFilter.CurrPage != "")
            {
                if (Convert.ToInt32(LSFilter.TotPage) >= Convert.ToInt32(LSFilter.CurrPage) && 0 < Convert.ToInt32(LSFilter.CurrPage))
                {
                }
            }
        }

        protected void btnFirstBakFrdLast_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(LSFilter.TotPage) >= Convert.ToInt32(LSFilter.CurrPage) && 0 < Convert.ToInt32(LSFilter.CurrPage))
                {
                    PageIndex = Convert.ToInt32(LSFilter.CurrPage) - 1;
                    GrdCommon.CurrentPageIndex = PageIndex;
                    GrdCommon.DataSource = dsCommanData;
                    GrdCommon.DataBind();
                }
            }
            catch (Exception ex)
            {
                MyMessageBoxInfo.Show(MyMessageBox.MessageType.Success, ex.Message.ToString().Trim() + ". Do you want to keep This Error In LOG File?", 75, 250, 1, 1, 1);
            }
        }

        private void BindThisGrid(bool search)
        {
            try
            {
                if ((Request.QueryString["UnitConversion"]) != null)
                #region For UnitConversion
                {
                    if (Convert.ToString(Request.QueryString["UnitConversion"]).Trim() == "UnitConversion")
                    {
                        DataSet ds = new DataSet();
                        DataSet dsTblDataSelection = new DataSet();
                        DataSet DstoFormat = new DataSet();
                        string strExec = "";

                        controlname = Convert.ToString(Request.QueryString["control"]) != null ? "control" : "control1";
                        DataTable dt = ObjLibrary.FillDT("ssp_DataSelectionParam", Request.QueryString["form"].ToString(), Request.QueryString[controlname].ToString());

                        dsTblDataSelection.Tables.Add(dt.Copy());
                        dt = Request.QueryString["ID"] == null ? ObjLibrary.BindGrid(dt.Rows[0]["ProcName"].ToString().Trim(), Session["CompanyID"].ToString(), Session["BranchID"].ToString(), Session["FinancialYearId"].ToString())
                        : ObjLibrary.BindGrid(dt.Rows[0]["ProcName"].ToString().Trim(), Session["CompanyID"].ToString(), Session["BranchID"].ToString(), Request.QueryString["ID"].ToString());
                        Filter = Convert.ToString(Request.QueryString["Filter"]);
                        ds.Tables.Add(dt.Copy());
                        if (Filter != null)
                        {
                            DataView dvFiltered = new DataView(ds.Tables[0]);
                            dvFiltered.RowFilter = Filter;
                            if (dvFiltered != null)
                            {
                                if (dvFiltered.Count > 0)
                                {
                                    ds = new DataSet();
                                    ds.Tables.Add(dvFiltered.ToTable());
                                }
                                else
                                {
                                    ds.Clear();
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Matching Record Found');", true);
                                }
                            }
                        }

                        if (search == false)
                        {
                            DataView dv1 = new DataView(ds.Tables[0]);
                            string str = "";

                            if ((Convert.ToString(Request.QueryString["Fld"]) != null) && (Convert.ToString(Request.QueryString["Val"]) != null) && (Convert.ToString(Request.QueryString["Fld1"]) != null) && (Convert.ToString(Request.QueryString["StrVal1"]) != null))
                                if ((Convert.ToString(Request.QueryString["Fld"]) != "") && (Convert.ToString(Request.QueryString["Val"]) != "") && (Convert.ToString(Request.QueryString["Fld1"]) != ""))
                                    str = Convert.ToString(Request.QueryString["StrVal1"]).Trim() == "" ? Request.QueryString["Fld"].ToString() + " =" + Request.QueryString["Val"].ToString()
                                        : Request.QueryString["Fld"].ToString() + " =" + Request.QueryString["Val"].ToString() + " and " + Request.QueryString["Fld1"].ToString() + " like '" + Request.QueryString["StrVal1"].ToString() + "%'";
                            if ((Convert.ToString(Request.QueryString["Fld"]) != null) && (Convert.ToString(Request.QueryString["StrVal"]) != null))
                                if ((Convert.ToString(Request.QueryString["Fld"]) != "") && (Convert.ToString(Request.QueryString["StrVal"]) != ""))
                                    str = Request.QueryString["Fld"].ToString() + " like '" + Request.QueryString["StrVal"].ToString() + "%'";

                            if (str != "")
                            {
                                dv1.RowFilter = str;
                                if (dv1.Count > 0)
                                {
                                    ds = new DataSet();
                                    ds.Tables.Add(dv1.ToTable());
                                }
                                else
                                {
                                    ds.Clear();
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Matching Record Found');", true);
                                }
                            }
                        }
                        DataSet dsTemp = new DataSet();

                        if (search == true)
                        {
                            ds = new DataSet();
                            ds = (DataSet)ViewState["dsFilter"];
                            DataView dv = new DataView(((DataSet)ViewState["dsFilter"]).Tables[0]);
                            if ((ds.Tables[0].Columns.Contains(ddlSearchField.SelectedItem.Value.Trim())) && ((ds.Tables[0].Columns[ddlSearchField.SelectedItem.Value.Trim()].DataType == typeof(int)) || (ds.Tables[0].Columns[ddlSearchField.SelectedItem.Value.Trim()].DataType == typeof(double)) || (ds.Tables[0].Columns[ddlSearchField.SelectedItem.Value.Trim()].DataType == typeof(float)) || (ds.Tables[0].Columns[ddlSearchField.SelectedItem.Value.Trim()].DataType == typeof(long)) || (ds.Tables[0].Columns[ddlSearchField.SelectedItem.Value.Trim()].DataType == typeof(short))))
                            {
                                if (txtSearch.Text.Trim() != "")
                                {
                                    string str = ddlSearchField.SelectedItem.Value.Trim() + "=" + txtSearch.Text.Trim();
                                    dv.RowFilter = str;
                                }
                            }
                            else
                            {
                                string str = ddlSearchField.SelectedItem.Value.Trim() + " like '" + txtSearch.Text.Trim() + "%'";
                                dv.RowFilter = str;
                            }
                            DataSet ds4 = new DataSet();
                            if (dv.Count > 0)
                            {
                                ds4.Tables.Add(dv.ToTable());
                                dsTemp.Tables.Add(dv.ToTable());
                                ViewState["ds"] = ds4;
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Matching Record Found, Showing All Records..');", true);
                                dsTemp.Merge(ds);
                                ViewState["ds"] = ds;
                            }
                        }
                        else
                        {
                            ViewState["ds"] = ds;
                            ViewState["dsFilter"] = ds;
                            dsTemp = ds.Copy();
                        }
                        strExec = "FormName='" + Request.QueryString["Form"].ToString() + "' and ControlName='" + Request.QueryString[controlname].ToString() + "'";
                        DataView dvField = new DataView(dsTblDataSelection.Tables[0]);
                        dvField.RowFilter = strExec;
                        DstoFormat.Tables.Add(dvField.ToTable());
                        if (DstoFormat.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < DstoFormat.Tables[0].Rows.Count; i++)
                            {
                                if (Convert.ToString(DstoFormat.Tables[0].Rows[i]["Length"]) == "0")
                                {
                                    if (dsTemp.Tables[0].Columns.Contains(Convert.ToString(DstoFormat.Tables[0].Rows[i]["FieldName"])) == true)
                                        dsTemp.Tables[0].Columns.Remove(Convert.ToString(DstoFormat.Tables[0].Rows[i]["FieldName"]));
                                }
                            }
                        }
                        strExec = "FormName='" + Request.QueryString["Form"].ToString() + "' and ControlName='" + Request.QueryString[controlname].ToString() + "' and length<>0";//
                        DataView dvColoumn = new DataView(dsTblDataSelection.Tables[0]);
                        dvColoumn.RowFilter = strExec;
                        DstoFormat.Clear();
                        DstoFormat.Tables.RemoveAt(0);
                        DstoFormat.Tables.Add(dvColoumn.ToTable());
                        for (int i = 0; i < DstoFormat.Tables[0].Rows.Count; i++)
                        {
                            dsTemp.Tables[0].Columns[i].ColumnName = DstoFormat.Tables[0].Rows[i]["ColumnHeader"].ToString();
                        }
                        if (dsCommanData != null)
                            dsCommanData.Clear();

                        if (!dsTemp.Tables[0].Columns.Contains("From UOM"))
                            dsTemp.Tables[0].Columns.Add("From UOM");

                        if (!dsTemp.Tables[0].Columns.Contains("To UOM"))
                            dsTemp.Tables[0].Columns.Add("To UOM");

                        if (!dsTemp.Tables[0].Columns.Contains("Conversion Rate"))
                            dsTemp.Tables[0].Columns.Add("Conversion Rate");


                        if (!ds.Tables[0].Columns.Contains("Conversion Rate"))
                            ds.Tables[0].Columns.Add("Conversion Rate");

                        if (!ds.Tables[0].Columns.Contains("From UOM"))
                            ds.Tables[0].Columns.Add("From UOM");

                        if (!ds.Tables[0].Columns.Contains("To UOM"))
                            ds.Tables[0].Columns.Add("To UOM");

                        DataSet dsUOM = new DataSet();
                        dsUOM = (DataSet)ViewState["ds"];

                        DataSet dsUOMConv = new DataSet();

                        dsUOMConv = ObjLibrary.FillDataSet("ssp_SSPSelectUOMConversion", Request.QueryString["ToUom"].ToString(), Session["CompanyID"].ToString());

                        ViewState["strUOM"] = dsUOMConv.Tables[1].Rows[0]["strUOM"].ToString();

                        if (dsUOMConv.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < dsUOM.Tables[0].Rows.Count; i++)
                            {
                                DataView dvUOM = new DataView(dsUOMConv.Tables[0]);
                                dvUOM.RowFilter = "intUOMIDFrom=" + dsUOM.Tables[0].Rows[i][0].ToString();
                                if (dvUOM.ToTable().Rows.Count > 0)
                                {
                                    dsUOM.Tables[0].Rows[i]["From UOM"] = dvUOM.ToTable().Rows[0]["From UOM"].ToString();
                                    dsUOM.Tables[0].Rows[i]["To UOM"] = dvUOM.ToTable().Rows[0]["To UOM"].ToString();
                                    dsUOM.Tables[0].Rows[i]["Conversion Rate"] = dvUOM.ToTable().Rows[0]["dblConvRate"].ToString();

                                    dsTemp.Tables[0].Rows[i]["From UOM"] = dvUOM.ToTable().Rows[0]["From UOM"].ToString();
                                    dsTemp.Tables[0].Rows[i]["To UOM"] = dvUOM.ToTable().Rows[0]["To UOM"].ToString();
                                    dsTemp.Tables[0].Rows[i]["Conversion Rate"] = dvUOM.ToTable().Rows[0]["dblConvRate"].ToString();
                                }
                            }
                        }

                        if (search == true)
                        {
                            ViewState["ds"] = dsUOM;
                        }
                        else
                        {
                            ViewState["ds"] = dsUOM;
                            ViewState["dsFilter"] = dsUOM;
                        }

                        dsCommanData = dsTemp.Copy();
                        PageIndex = 0;
                        
                        GrdCommon.DataSource = dsCommanData;
                        GrdCommon.DataBind();
                        LSFilter.GetTotalPagesForSelection(GrdCommon, dsCommanData);
                        //-------------------
                        if (Request.QueryString.AllKeys.Contains("MultSel"))
                        {
                            GrdCommon.Columns[0].Visible = true;
                            GrdCommon.Columns[1].Visible = false;
                            btnSelect.Visible = true;
                            int[] ToBeSelect = new int[dsCommanData.Tables[0].Rows.Count];
                        }
                        else
                        {
                            GrdCommon.Columns[0].Visible = false;
                            GrdCommon.Columns[1].Visible = true;
                            btnSelect.Visible = false;
                        }
                        if (search != true)
                        {
                            ddlSearchField.DataSource = DstoFormat;
                            ddlSearchField.DataTextField = "ColumnHeader";
                            ddlSearchField.DataValueField = "FieldName";
                            ddlSearchField.DataBind();
                        }
                    }
                }
                #endregion
                else
                {
                    DataSet ds = new DataSet();
                    DataSet dsTblDataSelection = new DataSet();
                    DataSet DstoFormat = new DataSet();
                    string strExec = "";

                    controlname = Convert.ToString(Request.QueryString["control"]) != null ? "control" : "control1";
                    dt = ObjLibrary.FillDT("ssp_DataSelectionParam", Request.QueryString["form"].ToString(), Request.QueryString[controlname].ToString());

                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["strHelpButtonFlag"].ToString() == "Y")
                        {
                            GrdCommon.Columns[2].Visible = true;
                        }
                        else
                        {
                            GrdCommon.Columns[2].Visible = false;
                        }
                        SpForHelp = dt.Rows[0]["strHelpSPName"].ToString();
                        FieldName = dt.Rows[0]["strParameterField"].ToString();
                    }
                    else
                    {
                        GrdCommon.Columns[2].Visible = false;
                    }

                    dsTblDataSelection.Tables.Add(dt.Copy());

                    DstoFormat.Clear();
                    DstoFormat.Merge(ObjLibrary.GetTempDSRet(dsTblDataSelection, "Length<>0"));
                    if (search != true)
                    {
                        if (ddlSearchField.SelectedItem == null)
                        {
                            ddlSearchField.DataSource = DstoFormat;
                            ddlSearchField.DataTextField = "ColumnHeader";
                            ddlSearchField.DataValueField = "FieldName";
                            ddlSearchField.DataBind();
                        }
                    }

                    SetTXTSearchControls();
                    string value = txtSearch.Text.Trim().ToString() == "" ? (Convert.ToString(Request.QueryString["StrVal"])) : txtSearch.Text.Trim().ToString();

                    if (Request.QueryString["form"].ToString() == "frmInventoryMIS3" &&
                        (Request.QueryString[controlname].ToString() == "TXTItemName"))
                    {
                        string F = "" + ddlSearchField.SelectedItem.Value.ToString() + " like '" + value + "%'";// :
                        ds = ObjLibrary.FillDataSet(dt.Rows[0]["ProcName"].ToString().Trim(),
                            Session["CompanyID"].ToString(), Session["BranchID"].ToString(), Session["FinancialYearId"].ToString(), F);// :
                    }
                    else if (Request.QueryString["form"].ToString() == "frmTechnicalBOM" &&
                        (Request.QueryString[controlname].ToString() == "TXTItemName" ||
                        Request.QueryString[controlname].ToString() == "TXTProductName"))
                    {
                        string F = "" + ddlSearchField.SelectedItem.Value.ToString() + " like '" + value + "%'";// :
                        ds = ObjLibrary.FillDataSet(dt.Rows[0]["ProcName"].ToString().Trim(),
                            Session["CompanyID"].ToString(), Session["BranchID"].ToString(), Session["FinancialYearId"].ToString(), F);// :
                    }                   
                    else if (Request.QueryString["form"].ToString() == "frmExecutiveVisitEntry" && Request.QueryString["control1"].ToString() == "txtEnquiryNo")
                    {
                        string F = "";
                        F = Request.QueryString["Filter"];
                        if (F != null)
                        {
                            ds = ObjLibrary.FillDataSet(dt.Rows[0]["ProcName"].ToString().Trim(),
                                  Session["CompanyID"].ToString(), Session["BranchID"].ToString(), Session["FinancialYearId"].ToString(), F);
                        }
                        else
                        {
                            ds = ObjLibrary.FillDataSet(dt.Rows[0]["ProcName"].ToString().Trim(),
                                  Session["CompanyID"].ToString(), Session["BranchID"].ToString(), Session["FinancialYearId"].ToString(), "");
                        }
                        Filter = null;

                    }
                    else if (Request.QueryString["form"].ToString() == "frmExecutiveVisitEntry" && Request.QueryString["control1"].ToString() == "txtComplaintNo")
                    {
                        string F = "";
                        F = Request.QueryString["Filter"];
                        if (F != null)
                        {
                            ds = ObjLibrary.FillDataSet(dt.Rows[0]["ProcName"].ToString().Trim(),
                                  Session["CompanyID"].ToString(), Session["BranchID"].ToString(), Session["FinancialYearId"].ToString(), F);
                        }
                        else
                        {
                            ds = ObjLibrary.FillDataSet(dt.Rows[0]["ProcName"].ToString().Trim(),
                                  Session["CompanyID"].ToString(), Session["BranchID"].ToString(), Session["FinancialYearId"].ToString(), "");
                        }
                        Filter = null;

                    }                    
                    else
                        ds = Request.QueryString["ID"] == null ?
                           ObjLibrary.FillDataSet(dt.Rows[0]["ProcName"].ToString().Trim(), Session["CompanyID"].ToString(), Session["BranchID"].ToString(), Session["FinancialYearId"].ToString())
                          : ObjLibrary.FillDataSet(dt.Rows[0]["ProcName"].ToString().Trim(), Session["CompanyID"].ToString(), Session["BranchID"].ToString(), Request.QueryString["ID"].ToString());



                    if (!string.IsNullOrEmpty(Filter))
                    {
                        DataSet temp = ds.Clone();
                        temp.Merge(ObjLibrary.GetTempDSRet(ds, Filter), true);
                        ds.Clear(); ds.Merge(temp);
                        if (ds.Tables[0].Rows.Count <= 0)

                            MyMessageBoxInfo.Show(MyMessageBox.MessageType.Success, "No Matching Record Found.", 100, 250, 0, 1, 0, "", "OK", "");


                    }

                    if (search == false)
                    {
                        DataView dv1 = new DataView(ds.Tables[0]);
                        string str = "";

                        if ((Convert.ToString(Request.QueryString["Fld"]) != null) && (Convert.ToString(Request.QueryString["Val"]) != null) && (Convert.ToString(Request.QueryString["Fld1"]) != null) && (Convert.ToString(Request.QueryString["StrVal1"]) != null))
                            if ((Convert.ToString(Request.QueryString["Fld"]) != "") && (Convert.ToString(Request.QueryString["Val"]) != "") && (Convert.ToString(Request.QueryString["Fld1"]) != ""))
                                str = Convert.ToString(Request.QueryString["StrVal1"]).Trim() == "" ? Request.QueryString["Fld"].ToString() + " =" + Request.QueryString["Val"].ToString()
                                    : Request.QueryString["Fld"].ToString() + " =" + Request.QueryString["Val"].ToString() + " and " + Request.QueryString["Fld1"].ToString() + " like '" + Request.QueryString["StrVal1"].ToString() + "%'";
                        if ((Convert.ToString(Request.QueryString["Fld"]) != null) && (Convert.ToString(Request.QueryString["StrVal"]) != null))
                            if ((Convert.ToString(Request.QueryString["Fld"]) != "") && (Convert.ToString(Request.QueryString["StrVal"]) != ""))
                                str = Request.QueryString["Fld"].ToString() + " like '" + Request.QueryString["StrVal"].ToString() + "%'";

                        if (str != "")
                        {
                            dv1.RowFilter = str;
                            if (dv1.Count > 0)
                            {
                                ds = new DataSet();
                                ds.Tables.Add(dv1.ToTable());
                            }
                            else
                            {
                                ds.Clear();
                                MyMessageBoxInfo.Show(MyMessageBox.MessageType.Success, "No Matching Record Found.", 100, 250, 0, 1, 0, "", "OK", "");
                            }
                        }
                        #region

                        #endregion
                    }
                    DataSet dsTemp = new DataSet();

                    if (search == true)
                    {
                        DataView dv = new DataView(ds.Tables[0]);
                        if ((ds.Tables[0].Columns.Contains(ddlSearchField.SelectedItem.Value.Trim())) && ((ds.Tables[0].Columns[ddlSearchField.SelectedItem.Value.Trim()].DataType == typeof(int)) || (ds.Tables[0].Columns[ddlSearchField.SelectedItem.Value.Trim()].DataType == typeof(double)) || (ds.Tables[0].Columns[ddlSearchField.SelectedItem.Value.Trim()].DataType == typeof(float)) || (ds.Tables[0].Columns[ddlSearchField.SelectedItem.Value.Trim()].DataType == typeof(long)) || (ds.Tables[0].Columns[ddlSearchField.SelectedItem.Value.Trim()].DataType == typeof(short))))
                        {
                            if (txtSearch.Text.Trim() != "")
                            {
                                string str = ddlSearchField.SelectedItem.Value.Trim() + "=" + txtSearch.Text.Trim();
                                dv.RowFilter = str;
                            }
                        }
                        else
                        {
                            string str = ddlSearchField.SelectedItem.Value.Trim() + " like '%" + txtSearch.Text.Trim() + "%'";
                            dv.RowFilter = str;
                        }
                        DataSet ds4 = new DataSet();
                        if (dv.Count > 0)
                        {
                            ds4.Tables.Add(dv.ToTable());
                            dsTemp.Tables.Add(dv.ToTable());
                            ViewState["ds"] = ds4;
                        }
                        else
                        {

                            MyMessageBoxInfo.Show(MyMessageBox.MessageType.Success, "No Matching Record Found.", 100, 250, 0, 1, 0, "", "OK", "");
                            dsTemp.Merge(ds);
                            ViewState["ds"] = ds;
                        }
                    }
                    else
                    {
                        ViewState["ds"] = ds;
                        ViewState["dsFilter"] = ds;
                        dsTemp = ds.Copy();
                    }

                    if (dsTblDataSelection.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsTblDataSelection.Tables[0].Rows.Count; i++)
                        {
                            if (Convert.ToString(dsTblDataSelection.Tables[0].Rows[i]["Length"]) == "0")
                                if (dsTemp.Tables[0].Columns.Contains(Convert.ToString(dsTblDataSelection.Tables[0].Rows[i]["FieldName"])))
                                    dsTemp.Tables[0].Columns.Remove(Convert.ToString(dsTblDataSelection.Tables[0].Rows[i]["FieldName"]));
                        }
                    }


                    for (int i = 0; i < DstoFormat.Tables[0].Rows.Count; i++)
                        dsTemp.Tables[0].Columns[i].ColumnName = DstoFormat.Tables[0].Rows[i]["ColumnHeader"].ToString();

                    if (dsCommanData != null)
                        dsCommanData.Clear();

                    dsCommanData = dsTemp.Copy();

                    PageIndex = 0;
                    double page;
                    string[] st;
                    if (dsCommanData.Tables[0].Rows.Count == 0 || dsCommanData.Tables[0].Rows.Count <= GrdCommon.PageSize)
                        PageIndex = 0;
                    else
                    {
                        page = Convert.ToDouble(dsCommanData.Tables[0].Rows.Count) / Convert.ToDouble(GrdCommon.PageSize);
                        st = Convert.ToString(page).Split('.');
                        if (st.Length > 1)
                        {
                            st[1] = st[1].Length > 3 ? st[1].Substring(0, 2) : st[1];
                            PageIndex = (Convert.ToInt32(st[1]) > 0 ? Convert.ToInt32(st[0]) : Convert.ToInt32(st[0]) - 1) - ((Convert.ToInt32(st[1]) > 0 ? Convert.ToInt32(st[0]) : Convert.ToInt32(st[0]) - 1));
                        }
                        else
                        {
                            PageIndex = Convert.ToInt32(st[0]) - (Convert.ToInt32(st[0]));
                        }
                    }

                    GrdCommon.CurrentPageIndex = PageIndex;
                    GrdCommon.DataSource = dsCommanData;
                    GrdCommon.DataBind();
                    LSFilter.GetTotalPagesForSelection(GrdCommon, dsCommanData);
                    if (Request.QueryString.AllKeys.Contains("MultSel"))
                    {
                        GrdCommon.Columns[0].Visible = true;
                        GrdCommon.Columns[1].Visible = false;
                        btnSelect.Visible = true;
                        DataColumn strReadyFlag = new DataColumn("strCHKFlag");
                        strReadyFlag.DataType = System.Type.GetType("System.String");
                        strReadyFlag.DefaultValue = "N";
                        if (!dsCommanData.Tables[0].Columns.Contains("strCHKFlag"))
                            dsCommanData.Tables[0].Columns.Add(strReadyFlag);
                        dsToBeFill = (DataSet)ViewState["ds"];
                        DataColumn strCHKFlag = new DataColumn("strCHKFlag");
                        strCHKFlag.DataType = System.Type.GetType("System.String");
                        strCHKFlag.DefaultValue = "N";
                        if (!dsToBeFill.Tables[0].Columns.Contains("strCHKFlag"))
                            dsToBeFill.Tables[0].Columns.Add(strCHKFlag);
                    }
                    else
                    {
                        GrdCommon.Columns[0].Visible = false;
                        GrdCommon.Columns[1].Visible = true;
                        btnSelect.Visible = false;
                    }
                    //-------------------

                }
            }
            catch (Exception Ex)
            {
                Response.Write(Ex.ToString());
            }
        }

        protected void SetTXTSearchControls()
        {
            if (Request.QueryString.AllKeys.Contains("ItemGroupForAss"))
                if (ddlSearchField.SelectedItem != null)
                {
                    if (ddlSearchField.SelectedValue.ToString() == "strItemGroupName")
                    {
                        btnSelectItemName1.Visible = true;
                        btnFill.Visible = true;
                        btnSelectItemName1.Attributes.Add("onClick", "ViewExistItem1E('frmSelection','" +
                           HIDItemGroupID.ClientID + "','" +
                           txtSearch.ClientID + "','" +
                           btnFill.ClientID + "','strItemGroupName')");
                        Filter = "intItemGroupId=" + HIDItemGroupID.Value;
                    }
                    else
                    {
                        btnSelectItemName1.Attributes.Remove("onClick");
                        btnSelectItemName1.Visible = false;
                        btnFill.Visible = false;
                        Filter = Convert.ToString(Request.QueryString["Filter"]);
                    }
                }
                else
                    Filter = Convert.ToString(Request.QueryString["Filter"]);
            else
                Filter = Convert.ToString(Request.QueryString["Filter"]);
        }

        protected void GrdCommon_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                string aReturnValue = string.Empty;
                if ((Request.QueryString["UnitConversion"]) != null)
                {
                    if (Convert.ToString(Request.QueryString["UnitConversion"]) == "UnitConversion")
                    {
                        bool btnFlag = false;
                        System.Text.StringBuilder SB = new StringBuilder();
                        DataSet ds2 = (DataSet)ViewState["ds"];
                        DataSet ds1 = new DataSet();
                        ds1.Tables.Add("TblUOM");
                        if (!ds1.Tables[0].Columns.Contains(ds2.Tables[0].Columns[0].ColumnName))
                            ds1.Tables[0].Columns.Add(ds2.Tables[0].Columns[0].ColumnName);

                        if (!ds1.Tables[0].Columns.Contains(ds2.Tables[0].Columns[1].ColumnName))
                            ds1.Tables[0].Columns.Add(ds2.Tables[0].Columns[1].ColumnName);

                        if (!ds1.Tables[0].Columns.Contains("Conversion Rate"))
                            ds1.Tables[0].Columns.Add("Conversion Rate");
                        ds1.Clear();
                        for (int i = 0; i < ds2.Tables[0].Rows.Count; i++)
                        {
                            ds1.Tables[0].Rows.Add(ds1.Tables[0].NewRow());
                            ds1.Tables[0].Rows[ds1.Tables[0].Rows.Count - 1][ds2.Tables[0].Columns[0].ColumnName] = ds2.Tables[0].Rows[i][ds2.Tables[0].Columns[0].ColumnName].ToString();
                            ds1.Tables[0].Rows[ds1.Tables[0].Rows.Count - 1][ds2.Tables[0].Columns[1].ColumnName] = ds2.Tables[0].Rows[i][ds2.Tables[0].Columns[1].ColumnName].ToString();
                            ds1.Tables[0].Rows[ds1.Tables[0].Rows.Count - 1]["Conversion Rate"] = ds2.Tables[0].Rows[i]["Conversion Rate"].ToString();
                        }

                        if (e.CommandName == "Select")
                        {
                            if (Convert.ToString(ds1.Tables[0].Rows[((PageIndex * GrdCommon.PageSize) + e.Item.ItemIndex)]["Conversion Rate"]).Trim() == "")
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Conversion is not done for " + Convert.ToString(ds1.Tables[0].Rows[((PageIndex * GrdCommon.PageSize) + e.Item.ItemIndex)][1]).Trim() + " to  " + ViewState["strUOM"].ToString().Trim() + " ');", true);
                                return;
                            }

                            SB.Append("<script language='Javascript'>");
                            if (HIDBrowser.Value == "Microsoft Internet Explorer") SB.Append("var _opener = window.dialogArguments;");
                            else SB.Append("var _opener = opener;");
                            for (int i = 1; i < Request.QueryString.Count; i++)
                            {
                                if (Request.QueryString.GetKey(i).Contains("con") && Request.QueryString.GetKey(i).ToString().Trim() != "control")
                                {
                                    string Control1 = Request.QueryString[i].ToString().Trim();
                                    if (Control1.ToUpper().Contains("CHK"))
                                    {
                                        SB.Append("_opener.$get('" + Control1 + "').checked=" + Convert.ToString(ds1.Tables[0].Rows[((PageIndex * GrdCommon.PageSize) + e.Item.ItemIndex)][i - 1]).Trim() + ";");
                                        btnFlag = true;
                                    }
                                    else if (Control1.ToLower().Contains("btn"))
                                    {
                                        SB.Append("_opener.$get('" + Control1 + "').click();");
                                        btnFlag = true;
                                    }
                                    else
                                    {
                                        if (aReturnValue == "")
                                            aReturnValue = Control1 + "$" + Convert.ToString(ds1.Tables[0].Rows[((PageIndex * GrdCommon.PageSize) + e.Item.ItemIndex)][i - 1]);
                                        else
                                            aReturnValue += "," + Control1 + "$" + Convert.ToString(ds1.Tables[0].Rows[((PageIndex * GrdCommon.PageSize) + e.Item.ItemIndex)][i - 1]);

                                        SB.Append("_opener.$get('" + Control1 + "').value='" + Convert.ToString(ds1.Tables[0].Rows[((PageIndex * GrdCommon.PageSize) + e.Item.ItemIndex)][i - 1]).Trim() + "';");
                                    }
                                }
                            }
                            SB.Append("_opener.focus();");
                            if (!btnFlag) SB.Append("window.close();");
                            SB.Append("</script>");
                            if (Request.QueryString["SearchFire"] != null)
                            {
                                if (Request.QueryString["SearchFire"].ToString() == "Y")
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "CallParent('" + aReturnValue + "');", true);
                                }
                                else
                                {
                                    if (!Page.ClientScript.IsClientScriptBlockRegistered("abc"))
                                    {
                                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "abc", SB.ToString().Trim());
                                    }
                                    if (btnFlag) ScriptManager.RegisterStartupScript(Page, Page.GetType(), Page.ToString(), "timeout();", true);
                                }
                            }
                            else
                            {
                                if (!Page.ClientScript.IsClientScriptBlockRegistered("abc"))
                                {
                                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "abc", SB.ToString().Trim());
                                }
                                if (btnFlag) ScriptManager.RegisterStartupScript(Page, Page.GetType(), Page.ToString(), "timeout();", true);
                            }
                        }
                    }
                }
                else
                {
                    bool btnFlag = false;
                    System.Text.StringBuilder SB = new StringBuilder();
                    DataSet ds1 = (DataSet)ViewState["ds"];
                    if (e.CommandName == "Select")
                    {
                        SB.Append("<script language='Javascript'>");
                        if (HIDBrowser.Value == "Microsoft Internet Explorer") SB.Append("var _opener = window.dialogArguments;");
                        else SB.Append("var _opener = opener;");
                        for (int i = 1; i < Request.QueryString.Count; i++)
                        {
                            if (Request.QueryString.GetKey(i).Contains("con") && Request.QueryString.GetKey(i).ToString().Trim() != "control")
                            {
                                string Control1 = Request.QueryString[i].ToString().Trim();
                                if (Control1.ToUpper().Contains("CHK"))
                                {
                                    SB.Append("_opener.$get('" + Control1 + "').checked=" + Convert.ToString(ds1.Tables[0].Rows[((PageIndex * GrdCommon.PageSize) + e.Item.ItemIndex)][i - 1]).Trim() + ";");
                                    btnFlag = true;
                                }
                                else if (Control1.ToLower().Contains("btn"))
                                {
                                    SB.Append("_opener.$get('" + Control1 + "').click();");
                                    btnFlag = true;
                                }
                                else
                                {
                                    if (aReturnValue == "")
                                        aReturnValue = Control1 + "$" + Convert.ToString(ds1.Tables[0].Rows[((PageIndex * GrdCommon.PageSize) + e.Item.ItemIndex)][i - 1]);
                                    else
                                        aReturnValue += "," + Control1 + "$" + Convert.ToString(ds1.Tables[0].Rows[((PageIndex * GrdCommon.PageSize) + e.Item.ItemIndex)][i - 1]);

                                    SB.Append("_opener.$get('" + Control1 + "').value='" + Convert.ToString(ds1.Tables[0].Rows[((PageIndex * GrdCommon.PageSize) + e.Item.ItemIndex)][i - 1]).Trim() + "';");
                                }
                            }
                        }
                        SB.Append("_opener.focus();");
                        if (!btnFlag) SB.Append("window.close();");
                        SB.Append("</script>");
                        if (Request.QueryString["SearchFire"] != null)
                        {
                            if (Request.QueryString["SearchFire"].ToString() == "Y")
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "CallParent('" + aReturnValue + "');", true);
                            }
                            else
                            {
                                if (!Page.ClientScript.IsClientScriptBlockRegistered("abc"))
                                {
                                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "abc", SB.ToString().Trim());
                                }
                                if (btnFlag) ScriptManager.RegisterStartupScript(Page, Page.GetType(), Page.ToString(), "timeout();", true);
                            }
                        }
                        else
                        {
                            if (!Page.ClientScript.IsClientScriptBlockRegistered("abc"))
                            {
                                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "abc", SB.ToString().Trim());
                            }
                            if (btnFlag) ScriptManager.RegisterStartupScript(Page, Page.GetType(), Page.ToString(), "timeout();", true);
                        }
                    }

                    if (e.CommandName == "Help")
                    {
                        if (!string.IsNullOrEmpty(SpForHelp.ToString()) && !string.IsNullOrEmpty(SpForHelp.ToString()) && ds1.Tables[0].Columns.Contains(FieldName))
                        {
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "DisplayHelpHeader('" + SpForHelp.ToString() + "','" + Convert.ToString(ds1.Tables[0].Rows[((PageIndex * GrdCommon.PageSize) + e.Item.ItemIndex)][FieldName]).Trim() + "','');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Help');", true);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Response.Write(Ex.ToString());
            }
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!Request.QueryString.AllKeys.Contains("ItemGroupForAss"))
                {
                    if (ddlSearchField.SelectedItem != null)
                        BindThisGrid(true);
                }
                else
                {
                    if (ddlSearchField.SelectedItem != null)
                        if (ddlSearchField.SelectedValue.ToString() != "strItemGroupName")
                            BindThisGrid(true);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }

        protected void GrdCommon_OnPageIndexChanged(object sender, DataGridPageChangedEventArgs e)
        {
            try
            {
                PageIndex = e.NewPageIndex;
                GrdCommon.CurrentPageIndex = PageIndex;
                GrdCommon.DataSource = dsCommanData;
                GrdCommon.DataBind();

                if (Request.QueryString.AllKeys.Contains("MultSel"))
                {
                    ToBeSelect = false;
                    CHKItem();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }

        protected void ddlSearchField_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!Request.QueryString.AllKeys.Contains("ItemGroupForAss"))
                    if (ddlSearchField.SelectedItem != null)
                        if (ddlSearchField.SelectedValue.ToString() != "strItemGroupName")
                        {
                            if (txtSearch.Text.ToString() != "")
                                BindThisGrid(true);
                        }
                        else
                            SetTXTSearchControls();
                    else
                        SetTXTSearchControls();
                else
                    SetTXTSearchControls();
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }

        protected void btnFill_Click(object sender, EventArgs e)
        {
            BindThisGrid(false);
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dsToBeFill.Tables[0].Rows.Count; i++)
                if (dsToBeFill.Tables[0].Rows[i]["strCHKFlag"].ToString() == "N")
                {
                    dsToBeFill.Tables[0].Rows[i].Delete();
                    dsToBeFill.AcceptChanges();
                    i--;
                }

            for (int i = 1; i < dsToBeFill.Tables[0].Columns.Count; i++)
            { dsToBeFill.Tables[0].Columns.Remove(dsToBeFill.Tables[0].Columns[i]); i--; }

            Session["SelItem"] = dsToBeFill;
            bool btnFlag = false;
            System.Text.StringBuilder SB = new StringBuilder();
            SB.Append("<script language='Javascript'>");
            #region            
            #endregion
            if (!btnFlag) SB.Append("window.close();");
            SB.Append("</script>");

            if (!Page.ClientScript.IsClientScriptBlockRegistered("abc"))
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "abc", SB.ToString());

            if (btnFlag) ScriptManager.RegisterStartupScript(Page, Page.GetType(), Page.ToString(), "timeout();", true);
        }

        protected void CHKSelect_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = ((CheckBox)sender);
            ObjLibrary.Check(ref GrdCommon, ref chk, "CHKSelectSingle");
            ToBeSelect = true;
            CHKItem();
        }

        protected void CHKSelectSingle_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = ((CheckBox)sender);
            ToBeSelect = true;
            CHKItem();
        }

        protected void CHKItem()
        {
            for (int i = 0; i < GrdCommon.Items.Count; i++)
                if (!ToBeSelect)
                    ((CheckBox)GrdCommon.Items[i].FindControl("CHKSelectSingle")).Checked =
                        dsCommanData.Tables[0].Rows[i + (GrdCommon.CurrentPageIndex * GrdCommon.PageSize)]["strCHKFlag"].ToString() == "N" ? false : true;
                else
                {
                    dsToBeFill.Tables[0].Rows[i + (GrdCommon.CurrentPageIndex * GrdCommon.PageSize)]["strCHKFlag"] =
                        ((CheckBox)GrdCommon.Items[i].FindControl("CHKSelectSingle")).Checked ? "Y" : "N";

                    dsCommanData.Tables[0].Rows[i + (GrdCommon.CurrentPageIndex * GrdCommon.PageSize)]["strCHKFlag"] =
                        ((CheckBox)GrdCommon.Items[i].FindControl("CHKSelectSingle")).Checked ? "Y" : "N";
                }
        }

        protected void btnSearchTxt_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (!Request.QueryString.AllKeys.Contains("ItemGroupForAss"))
                {
                    if (ddlSearchField.SelectedItem != null)
                        BindThisGrid(true);
                }
                else
                {
                    if (ddlSearchField.SelectedItem != null)
                        if (ddlSearchField.SelectedValue.ToString() != "strItemGroupName")
                            BindThisGrid(true);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }
    }
}