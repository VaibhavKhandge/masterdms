﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmSalesmanMaster.aspx.cs" Inherits="eBILL.UI.frmSalesmanMaster" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sales Man Master</title>
    <!-- bootstrap 3.0.2 -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="CSS/AdminLTE.css" rel="stylesheet" />
    <link href="CSS/colorbox.css" rel="stylesheet" />
    <!-- Theme style -->
    <script src="JS/jquery.min.js" type="text/javascript"></script>
    <script src="../Script/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="JS/jquery.colorbox.js" type="text/javascript"></script>
    <script src="../Script/KeyPress.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
        });

        function pageLoad(sender, args) {
            if (args.get_isPartialLoad()) {
                $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
            }
        }
        function loadPopup() {
            $(".iframe").colorbox({ iframe: true, width: "98%", height: "98%" });
        }

        function LoadPage() {
            $.colorbox.close();
            document.getElementById('LinkButton1').click();
        }

        function setValues(Values) {
            debugger
            var ActualValues = Values.split(',');
            var HdnFieldName = "";
            for (var i = 0; i < ActualValues.length; i++) {
                var result = ActualValues[i].split('$');
                if (i == 0) {
                    HdnFieldName = result[0];
                }
                if (HdnFieldName == 'HIDSalesmanExecutiveID') {
                    document.getElementById(result[0]).value = result[1];
                }
            }
            if (HdnFieldName == 'HIDSalesmanExecutiveID') {
                var s_a = document.getElementById('SelectSalesmanExecutive');
                s_a.href = 'frmSelection.aspx?form=' + 'frmSalesmanMaster' + '&control0=' + 'HIDSalesmanExecutiveID' + '&control1=' + 'txtSalesmanExecutive' + '&Fld=' + 'strSalesExecutiveName' + '&StrVal=' + document.getElementById('txtSalesmanExecutive').value + '&SearchFire=Y';
            }
            $.colorbox.close();

            if (HdnFieldName == 'HIDSalesmanExecutiveID') {
                document.getElementById('LinkButton1').click();
            }
        }
        function setsearchButton(form, control0, control1, Fld, hrefid) {
            if (hrefid == 'SelectSalesmanExecutive') {
                if (document.getElementById('SelectSalesmanExecutive') != null) {
                    var s_a = document.getElementById('SelectSalesmanExecutive');
                    s_a.href = 'frmSelection.aspx?form=' + form + '&control0=' + control0 + '&control1=' + control1 + '&Fld=' + Fld + '&StrVal=' + document.getElementById(control1).value + '&SearchFire=Y';
                }
            }
        }

        function ClosePopup(values) {
            if (values == "1") {
                alert('Record Update SuccessFully....');
            }
            if (values == "2") {
                alert('Record save SuccessFully...');
            }
            if (values == "3") {
                alert('Sales person can not be duplicated,enter another sales person...');
            }
        }

        function CloseDiv() {
            parent.LoadPage();
        }

        function Validation() {
            if (document.getElementById('txtSalesmanName').value == "") {
                alert('Please enter sales man name...!');
                return false;
            }
            else if (document.getElementById('HIDSalesmanExecutiveID').value == "") {
                alert('Please select executive...!');
                document.getElementById('txtSalesmanExecutive').focus();
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server"
                    DynamicLayout="true">
                    <ProgressTemplate>
                        <div id="updPleaseWait" style="text-align: right; color: White; vertical-align: top; position: fixed; top: 300px; left: 600px">
                            <table style="background-color: #7cc3ef; width: 100%">
                                <tr>
                                    <td colspan="2" style="text-align: center; width: 100%">
                                        <b><font color="white">Please Wait...</font></b>
                                        <br />
                                        <img src="../vendor/lightbox2/dist/images/loading.gif" alt="Please wait...." title="Please wait...." />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div class="col-md-12" style="background: linear-gradient(45deg, #b74aef 0%, #da4726 100%);">
                    <div class="col-md-5">
                        <ol class="breadcrumb">
                            <li>
                                <asp:Button Text="Save" ID="btnSave" runat="server" CssClass="btn btn-primary btn-sm" OnCommand="Page_Command" CommandName="btnSave" OnClientClick="return Validation()" />
                                <asp:Button Text="Update" ID="btnUpdate" runat="server" CssClass="btn btn-primary btn-sm" OnCommand="Page_Command" CommandName="btnUpdate" OnClientClick="return Validation()" Visible="false" />
                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-primary btn-sm" OnCommand="Page_Command" CommandName="btnClear" />
                            </li>
                        </ol>
                    </div>
                    <div class="col-md-3" style="padding-top: 0px">
                        <span style="color: #fff">
                            <h4>Sales Man Master</h4>
                        </span>
                    </div>
                    <div class="col-md-3" style="padding-top: 7px; margin-left: 100px;"><span style="color: #fff">Date/ Time :</span><span style="color: #fff; font-size: 12px;"> <%=System.DateTime.Now.ToString("dd-MMM-yyyy hh:mm") %></span></div>
                    <div class="col-md-1" style="padding-top: 7px"><span style="color: #fff"></span><span style="color: #fff; font-size: 12px;" runat="server" id="TimeMinu"></span></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Salesman Code :</label>
                                    <asp:TextBox ID="txtSalesmanCode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Salesman Name :</label>
                                    <asp:TextBox ID="txtSalesmanName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="textwithselect form-group" id="Div1" runat="server">
                                        <label>Executive :</label>
                                        <asp:TextBox ID="txtSalesmanExecutive" runat="server" CssClass="form-control" AutoPostBack="true" ToolTip="Salesman Executive"></asp:TextBox>
                                        <a href="" id="SelectSalesmanExecutive" class="iframe">
                                            <asp:Button ID="btnSalesmanExecutive" runat="server" CssClass="btn btn-primary btn-sm" Text="Select"
                                                TabIndex="6" ToolTip="Select Executive"></asp:Button></a>
                                        <asp:HiddenField ID="HIDSalesmanExecutiveID" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Distributer :</label>
                                    <asp:TextBox ID="txtSalesmanDistributor" Enabled="false" runat="server" CssClass="form-control" ToolTip="Salesman Distibutor"></asp:TextBox>
                                    <asp:HiddenField ID="HIDExecutiveDistID" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Company :</label>
                                    <asp:TextBox ID="txtCompany" Enabled="false" runat="server" CssClass="form-control" ToolTip="Salesman Distibutor"></asp:TextBox>
                                    <asp:HiddenField ID="HIDCompanyID" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Salesman Address :</label>
                                    <asp:TextBox ID="txtSalesmanAddress" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Phone No :</label>
                                    <asp:TextBox ID="txtSalesmanPhone" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Mobile No :</label>
                                    <asp:TextBox ID="txtSalesmanMobile" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Email :</label>
                                    <asp:TextBox ID="txtSalesmanEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <label>Close :</label>
                                    <asp:CheckBox ID="chkSalesmanClose" runat="server" CssClass="form-control" OnCheckedChanged="chkSalesmanClose_CheckedChanged" AutoPostBack="true" />
                                </div>
                            </div>
                        </div>
                        <div id="divClose" runat="server" visible="false">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body">
                                        <label>Close Date :</label>
                                        <asp:TextBox ID="txtCloseDate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body">
                                        <label>Close Remark :</label>
                                        <asp:TextBox ID="txtCloseRemark" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click"></asp:LinkButton>
                <asp:HiddenField runat="server" ID="HIDSalesmanID" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
