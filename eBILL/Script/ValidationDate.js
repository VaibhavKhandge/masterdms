//Function var checking valid date

function IsValidDate(source, args) 
{
            var str = args.Value;
            var rg = /(0[1-9]|1[0-9]|2[0-9]|3[0-1])[-](Jan|Mar|May|Jul|Aug|Oct|Dec|jan|mar|may|jul|aug|oct|dec)[-](19|20)\d\d/i;
            var rg1 = /(0[1-9]|1[0-9]|2[0-9]|3[0])[-](Apr|Jun|Sep|Nov|apr|jun|sep|nov)[-](19|20)\d\d/i;
            var rg2 = /(0[1-9]|1[0-9]|2[0-9])[-](feb|Feb)[-](19|20)\d\d/i;

            if (rg.test(str)) 
            {
                args.IsValid = true;
            }
            else if (rg1.test(str)) 
            {
                args.IsValid = true;    
            }
            else if (rg2.test(str)) 
            {
                var year = str.substring(7, 11);
                var day = str.substring(0, 2);

                if (((parseInt(year) % 4) != 0) && parseInt(day) == 29) 
                {
                    args.IsValid = false;
                }
                else
                    args.IsValid = true;    
            }
            else 
            {
                args.IsValid = false;
            }
}
