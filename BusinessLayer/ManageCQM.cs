﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using DataLayer;
using System.Web;

namespace BusinessLayer
{
    public class ManageCQM
    {
        public int intBranchid, theintItemId, intResponsbility, intEnergyMeterId, intCustomerComplaintsRegisterId, intCustomerId, intChampionId, intProductId,
            EmployeeId, DepartmentId, intProductCategoryId, intProductSubCategory, intAPU, intDefectCategory, intLocationId, Tasks, WorkStationId, intMachineCategoryID;
        public decimal dblQuantity;
        public string strBranchid, strMonth, strResponsibilityType, strDate, strMfgDate, strQorQTno, strCustomerComplaint, strComplaintcommunicationMode, strDefectType,
            EquipmentCategory, strComplainType, strStatus;

        public DataTable ProductWiseItemGroupDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intItemId", theintItemId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetProductWiseItemGroupDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strmanualcode, strProductType, strResponsetoreportedcomplaintDate, intCommunicationmodeId, strPPMorNonPPM, strDesignProcesstype, strPartRecivedDate, strActualComunicationDate;
        public string ManageComplainDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[33];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@strMonth", strMonth);
                parm[i++] = new SqlParameter("@strDate", strDate);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                parm[i++] = new SqlParameter("@intChampionId", intChampionId);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@intProductCategoryId", intProductCategoryId);
                parm[i++] = new SqlParameter("@intProductSubCategory", intProductSubCategory);
                parm[i++] = new SqlParameter("@dblQuantity", dblQuantity);
                parm[i++] = new SqlParameter("@strMfgDate", strMfgDate);
                parm[i++] = new SqlParameter("@intAPU", intAPU);
                parm[i++] = new SqlParameter("@strQorQTno", strQorQTno);
                parm[i++] = new SqlParameter("@intDefectCategory", intDefectCategory);
                parm[i++] = new SqlParameter("@strCustomerComplaint", strCustomerComplaint);
                parm[i++] = new SqlParameter("@strComplaintcommunicationMode", strComplaintcommunicationMode);
                parm[i++] = new SqlParameter("@intLocationId", intLocationId);
                parm[i++] = new SqlParameter("@strDefectType", strDefectType);
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@Task", Tasks);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFyId);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                parm[i++] = new SqlParameter("@intResponsbility", intResponsbility);
                parm[i++] = new SqlParameter("@strResponsibilityType", strResponsibilityType);
                parm[i++] = new SqlParameter("@strProductType", strProductType);
                parm[i++] = new SqlParameter("@strResponsetoreportedcomplaintDate", strResponsetoreportedcomplaintDate);
                parm[i++] = new SqlParameter("@intCommunicationmodeId", intCommunicationmodeId);
                parm[i++] = new SqlParameter("@strPPMorNonPPM", strPPMorNonPPM);
                parm[i++] = new SqlParameter("@strDesignProcesstype", strDesignProcesstype);
                parm[i++] = new SqlParameter("@strPartRecivedDate", strPartRecivedDate);
                parm[i++] = new SqlParameter("@strActualComunicationDate", strActualComunicationDate);
                parm[i++] = new SqlParameter("@strmanualcode", strmanualcode);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageComplainDetails", parm);
            }
            catch (Exception exp)
            {    
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public DataTable CUstomerComplainWiseDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManageComplainDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);

            }
            return ds;
        }

        public string strMSILobservationOrDefectConformation, strobservationDefectCategory, strCFTMemberDetails, strContainmentAction, strContainmentActionTargetdate, strContainmentActionActualdate,
            strRootcauseAnalysisForOccurrence, strRootcauseAnalysisForNonDetection, strRootcauseFinalizationTargetDate, strRootcauseFinalizationActualDate, strRootCauseValidationDate,
            strCorrectiveActionCountermeasureForOccurrence, strCorrectiveActionCountermeasureForDetection, strCorrectiveActionCountermeasureImplementationTargetDate, strCorrectiveActionCountermeasureImplementationActualDate,
            strCorrectiveActionCountermeasureValidationDate, str4MCategoryName;
        public int int4MCategory;

        public DataTable CUstomerComplainWiseDefactDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManageDefactDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);

            }
            return ds;
        }
        public string strOccuranceType, strNTFReason;
        public string ManageComplainDefactDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[21];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@strMSILobservationOrDefectConformation", strMSILobservationOrDefectConformation);
                parm[i++] = new SqlParameter("@strobservationDefectCategory", strobservationDefectCategory);
                parm[i++] = new SqlParameter("@strCFTMemberDetails", strCFTMemberDetails);
                parm[i++] = new SqlParameter("@strContainmentAction", strContainmentAction);
                parm[i++] = new SqlParameter("@strContainmentActionTargetdate", strContainmentActionTargetdate);
                parm[i++] = new SqlParameter("@strContainmentActionActualdate", strContainmentActionActualdate);
                parm[i++] = new SqlParameter("@strRootcauseAnalysisForOccurrence", strRootcauseAnalysisForOccurrence);
                parm[i++] = new SqlParameter("@strRootcauseAnalysisForNonDetection", strRootcauseAnalysisForNonDetection);
                parm[i++] = new SqlParameter("@strRootcauseFinalizationTargetDate", strRootcauseFinalizationTargetDate);
                parm[i++] = new SqlParameter("@strRootcauseFinalizationActualDate", strRootcauseFinalizationActualDate);
                parm[i++] = new SqlParameter("@int4MCategory", int4MCategory);
                parm[i++] = new SqlParameter("@strRootCauseValidationDate", strRootCauseValidationDate);
                parm[i++] = new SqlParameter("@strCorrectiveActionCountermeasureForOccurrence", strCorrectiveActionCountermeasureForOccurrence);
                parm[i++] = new SqlParameter("@strCorrectiveActionCountermeasureForDetection", strCorrectiveActionCountermeasureForDetection);
                parm[i++] = new SqlParameter("@strCorrectiveActionCountermeasureImplementationTargetDate", strCorrectiveActionCountermeasureImplementationTargetDate);
                parm[i++] = new SqlParameter("@strCorrectiveActionCountermeasureImplementationActualDate", strCorrectiveActionCountermeasureImplementationActualDate);
                parm[i++] = new SqlParameter("@strCorrectiveActionCountermeasureValidationDate", strCorrectiveActionCountermeasureValidationDate);
                parm[i++] = new SqlParameter("@Task", Tasks);
                parm[i++] = new SqlParameter("@strOccuranceType", strOccuranceType);
                parm[i++] = new SqlParameter("@strNTFReason", strNTFReason);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageDefactDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable CUstomerComplainWiseMonitoringDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManageMonitoringDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);

            }
            return ds;
        }

        public string strEffectivenessMonitoringM1, strEffectivenessMonitoringM2, strEffectivenessMonitoringM3, strEffectivenessMonitoringM4, 
            strEffectivenessMonitoringM5, strEffectivenessMonitoringM6, strEffectivenessMonitoringM7, strEffectivenessMonitoringM8, 
            strEffectivenessMonitoringM9, strEffectivenessMonitoringM10, strEffectivenessMonitoringM11, strEffectivenessMonitoringM12,
            strLesssonsLearnedAfter3Meffectivenessmonitioring, strPTDBAfter3Meffectivenessmonitioring, strHDAfter3Meffectivenessmonitioring,
            strEffectivenessMonitoringM1Colur, strEffectivenessMonitoringM2Colur, strEffectivenessMonitoringM3Colur,
            strEffectivenessMonitoringM4Colur, strEffectivenessMonitoringM5Colur, strEffectivenessMonitoringM6Colur,
            strEffectivenessMonitoringM7Colur, strEffectivenessMonitoringM8Colur, strEffectivenessMonitoringM9Colur,
            strEffectivenessMonitoringM10Colur, strEffectivenessMonitoringM11Colur, strEffectivenessMonitoringM12Colur;

        public string ManageComplainMonitoringDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[29];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM1", strEffectivenessMonitoringM1);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM2", strEffectivenessMonitoringM2);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM3", strEffectivenessMonitoringM3);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM4", strEffectivenessMonitoringM4);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM5", strEffectivenessMonitoringM5);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM6", strEffectivenessMonitoringM6);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM7", strEffectivenessMonitoringM7);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM8", strEffectivenessMonitoringM8);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM9", strEffectivenessMonitoringM9);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM10", strEffectivenessMonitoringM10);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM11", strEffectivenessMonitoringM11);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM12", strEffectivenessMonitoringM12);
                parm[i++] = new SqlParameter("@strLesssonsLearnedAfter3Meffectivenessmonitioring", strLesssonsLearnedAfter3Meffectivenessmonitioring);
                parm[i++] = new SqlParameter("@strPTDBAfter3Meffectivenessmonitioring", strPTDBAfter3Meffectivenessmonitioring);
                parm[i++] = new SqlParameter("@strHDAfter3Meffectivenessmonitioring", strHDAfter3Meffectivenessmonitioring);
                parm[i++] = new SqlParameter("@Task", Tasks);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM1Colur", strEffectivenessMonitoringM1Colur);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM2Colur", strEffectivenessMonitoringM2Colur);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM3Colur", strEffectivenessMonitoringM3Colur);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM4Colur", strEffectivenessMonitoringM4Colur);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM5Colur", strEffectivenessMonitoringM5Colur);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM6Colur", strEffectivenessMonitoringM6Colur);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM7Colur", strEffectivenessMonitoringM7Colur);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM8Colur", strEffectivenessMonitoringM8Colur);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM9Colur", strEffectivenessMonitoringM9Colur);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM10Colur", strEffectivenessMonitoringM10Colur);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM11Colur", strEffectivenessMonitoringM11Colur);
                parm[i++] = new SqlParameter("@strEffectivenessMonitoringM12Colur", strEffectivenessMonitoringM12Colur);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageMonitoringDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable CUstomerComplainWiseRemarksDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManageRemarksDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                // throw;
            }
            return ds;
        }

        public string strRemark1, strRemark2, strRemark3, strRemark4, strRemark5, strRemark6, strRemark7, strRemark8, strRemark9, strRemark10;
        public string ManageComplainRemarksDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[12];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@strRemark1", strRemark1);
                parm[i++] = new SqlParameter("@strRemark2", strRemark2);
                parm[i++] = new SqlParameter("@strRemark3", strRemark3);
                parm[i++] = new SqlParameter("@strRemark4", strRemark4);
                parm[i++] = new SqlParameter("@strRemark5", strRemark5);
                parm[i++] = new SqlParameter("@strRemark6", strRemark6);
                parm[i++] = new SqlParameter("@strRemark7", strRemark7);
                parm[i++] = new SqlParameter("@strRemark8", strRemark8);
                parm[i++] = new SqlParameter("@strRemark9", strRemark9);
                parm[i++] = new SqlParameter("@strRemark10", strRemark10);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageRemarksDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }
        public string strTypeFor;
        public DataTable GetDocumentType()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@strTypeFor", strTypeFor);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetDocumentTypeMaster", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GetDocumentsDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("CustomerComplainDocumentDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public int intDocumentTypeId, intRevisionNo;
        public string strDocumentName, strDocumentNo, strDocumentDate, strRevisionDate;

        public string ManageDocumentDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@intDocumentTypeId", intDocumentTypeId);
                parm[i++] = new SqlParameter("@strDocumentName", strDocumentName);
                parm[i++] = new SqlParameter("@intCreatedId", intCreatedId);
                parm[i++] = new SqlParameter("@strDocumentNo", strDocumentNo);
                parm[i++] = new SqlParameter("@strDocumentDate", strDocumentDate);
                parm[i++] = new SqlParameter("@intRevisionNo", intRevisionNo);
                parm[i++] = new SqlParameter("@strRevisionDate", strRevisionDate);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageDocumentDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intWarentyInfoId, intCustomerLocationId, intCustomerChampionId;
        public string strRejectedAccepted, strDataanalysisDate, strClaimsettlementwarrantySignoffDate, strProductSegment, strClaimno, strDealerCode, strDealerLocation, strDelaerZone, strChasisNoorVINno, strVehicleSoldDate, strVehicleFailRepairDate,
            strKMsRunHoursRun, strDealerName, strClaimMfgMonthasperchasispartMfgdate, strDefectPhenomenonFieldConcernCustomerVoice, strNTFABPBBPNewDefect, strNTFreasonIncaseofNTFonly, strRemark;
        public string ManageWarentyInformationDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[29];
                int i = 0;
                parm[i++] = new SqlParameter("@intWarentyInfoId", intWarentyInfoId);
                parm[i++] = new SqlParameter("@strDataanalysisDate", strDataanalysisDate);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                parm[i++] = new SqlParameter("@intCustomerLocationId", intCustomerLocationId);
                parm[i++] = new SqlParameter("@intCustomerChampionId", intCustomerChampionId);
                parm[i++] = new SqlParameter("@strClaimsettlementwarrantySignoffDate", strClaimsettlementwarrantySignoffDate);
                parm[i++] = new SqlParameter("@strProductSegment", strProductSegment);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@intProductCategoryId", intProductCategoryId);
                parm[i++] = new SqlParameter("@strClaimno", strClaimno);
                parm[i++] = new SqlParameter("@strDealerCode", strDealerCode);
                parm[i++] = new SqlParameter("@strDealerLocation", strDealerLocation);
                parm[i++] = new SqlParameter("@strDelaerZone", strDelaerZone);
                parm[i++] = new SqlParameter("@strChasisNoorVINno", strChasisNoorVINno);
                parm[i++] = new SqlParameter("@strVehicleSoldDate", strVehicleSoldDate);
                parm[i++] = new SqlParameter("@strVehicleFailRepairDate", strVehicleFailRepairDate);
                parm[i++] = new SqlParameter("@strKMsRunHoursRun", strKMsRunHoursRun);
                parm[i++] = new SqlParameter("@strClaimMfgMonthasperchasispartMfgdate", strClaimMfgMonthasperchasispartMfgdate);
                parm[i++] = new SqlParameter("@strDefectPhenomenonFieldConcernCustomerVoice", strDefectPhenomenonFieldConcernCustomerVoice);
                parm[i++] = new SqlParameter("@strNTFABPBBPNewDefect", strNTFABPBBPNewDefect);
                parm[i++] = new SqlParameter("@strNTFreasonIncaseofNTFonly", strNTFreasonIncaseofNTFonly);
                parm[i++] = new SqlParameter("@strRemark", strRemark);
                parm[i++] = new SqlParameter("@intCreatedId", intCreatedId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                parm[i++] = new SqlParameter("@strRejectedAccepted", strRejectedAccepted);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@strDealerName", strDealerName);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageWarrantyInformation", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public DataTable GetWarrantyInformationDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intWarentyInfoId", intWarentyInfoId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManageWarrantyInformation", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string ManageWarentyInformationDetailsInsertIntoMainTable()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intWarentyInfoId", intWarentyInfoId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFyId);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageWarrantyDetailsIntoMainTable", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string DeletetblComplainDefectDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageProductParameterDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intParameterGroupId, intParameterId, intDefectDetailsId;
        public string strDeffect, strRemarks, dblAmount;

        public string ManagetblComplainDefectDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intDefectDetailsId", intDefectDetailsId);
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@intParameterGroupId", intParameterGroupId);
                parm[i++] = new SqlParameter("@intParameterId", intParameterId);
                parm[i++] = new SqlParameter("@strDeffect", strDeffect);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@dblAmount", dblAmount);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageProductParameterDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int strFYID;
        public DataTable GetFinancialYearFirstDate()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@strFYID", strFYID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetFinancialYearFirstDate", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetHalfYearlyDates()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intFYId", strFYID);
                parm[i++] = new SqlParameter("@Task", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetHalfYearlyDates", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetQuatersDates()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intFYId", strFYID);
                parm[i++] = new SqlParameter("@Task", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetQuatersDates", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string FromDate, Todate, CheckBoxFlag, intBranchIds;
        public DataTable GetExporttoExceltblCustomerComplaintsRegister_CQM()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegister_CQM", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intAPUId, intDefectParameterId, intDefectValue, TotalRej, TotalProduction, ZeroValuesEntry;
        public decimal totalEolPPm;
        public string ManageDefectDaillyDetailsInsert()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[15];
                int i = 0;
                parm[i++] = new SqlParameter("@intAPUId", intAPUId);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                parm[i++] = new SqlParameter("@intDefectParameterId", intDefectParameterId);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@intDefectValue", intDefectValue);
                parm[i++] = new SqlParameter("@strDate", strDate);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                parm[i++] = new SqlParameter("@TotalRej", TotalRej);
                parm[i++] = new SqlParameter("@TotalProduction", TotalProduction);
                parm[i++] = new SqlParameter("@totalEolPPm", totalEolPPm);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@ZeroValuesEntry", ZeroValuesEntry);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageDefectDaillyDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManageGettblAPUEOLPPMProductionDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@strDate", strDate);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblAPUEOLPPMProductionDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strPSSFlag, strPSSRemarks;
        public string ManagePasInsertFromComplaintRegister()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[7];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@strPSSFlag", strPSSFlag);
                parm[i++] = new SqlParameter("@strPSSRemarks", strPSSRemarks);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@intCreatedId", intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagePSSFromtblCustomerComplaintsRegisterCQM", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManageGetPSSFlagAndRemarksForPSS()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetPSSFlagAndRemarksForPSS", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intProblemSolvingSheetHeaderId, intObservationId;
        public string strObservationDescription;
        public string ManagetblProblemSolvingSheetObservationDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strObservationDescription", strObservationDescription);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                parm[i++] = new SqlParameter("@intObservationId", intObservationId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetObservationDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblProblemSolvingSheetObservationDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intObservationId", intObservationId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetObservationDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManagetblProblemSolvingSheetObservationDetailsGetForEdit()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intObservationId", intObservationId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblProblemSolvingSheetObservationDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strDocumentType;
        public int intDocumentId;
        public string ManagetblProblemSolvingSheetObservationDocument()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intDocumentId", intDocumentId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strDocumentName", strDocumentName);
                parm[i++] = new SqlParameter("@strDocumentType", strDocumentType);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetObservationDocument", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblProblemSolvingSheetObservationDocumentDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intDocumentId", intDocumentId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetObservationDocument", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GettblProblemSolvingSheetHeader()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblProblemSolvingSheetHeader", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strAnalysisdate, strTeamMembers, strkmCovered, strDOM, strRatioSoftware, strCustomerVoicecomplaint, strProblemDescription, strInterimAction,
            strRootCauseValidationDefectOccurence, strRootCauseValidationDefectOutFlow, strDefectOccuranceDate, strDefectOutFlowDate, strCorrectiveactionsValidation,
            strdetectiveactionsValidation, strCorrectiveactionsValidationDate, strdetectiveactionsValidationDate, strPokayokeImplementation, strReasonstobementioned,
            strConclusion, str8DStatus, strConclusionDate, strCurrentstatus, strCFTMembersDetails, strPartReceiptdate, strOccuranceDetailsForPss, strOccuranceOutFlowDetailsForPss;
        public string ManagetblProblemSolvingSheetHeader()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[27];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strAnalysisdate", strAnalysisdate);
                parm[i++] = new SqlParameter("@strTeamMembers", strTeamMembers);
                parm[i++] = new SqlParameter("@strkmCovered", strkmCovered);
                parm[i++] = new SqlParameter("@strDOM", strDOM);
                parm[i++] = new SqlParameter("@strRatioSoftware", strRatioSoftware);
                parm[i++] = new SqlParameter("@strCustomerVoicecomplaint", strCustomerVoicecomplaint);
                parm[i++] = new SqlParameter("@strProblemDescription", strProblemDescription);
                parm[i++] = new SqlParameter("@strInterimAction", strInterimAction);
                parm[i++] = new SqlParameter("@strRootCauseValidationDefectOccurence", strRootCauseValidationDefectOccurence);
                parm[i++] = new SqlParameter("@strRootCauseValidationDefectOutFlow", strRootCauseValidationDefectOutFlow);
                parm[i++] = new SqlParameter("@strDefectOccuranceDate", strDefectOccuranceDate);
                parm[i++] = new SqlParameter("@strDefectOutFlowDate", strDefectOutFlowDate);
                parm[i++] = new SqlParameter("@strCorrectiveactionsValidation", strCorrectiveactionsValidation);
                parm[i++] = new SqlParameter("@strdetectiveactionsValidation", strdetectiveactionsValidation);
                parm[i++] = new SqlParameter("@strCorrectiveactionsValidationDate", strCorrectiveactionsValidationDate);
                parm[i++] = new SqlParameter("@strdetectiveactionsValidationDate", strdetectiveactionsValidationDate);
                parm[i++] = new SqlParameter("@strPokayokeImplementation", strPokayokeImplementation);
                parm[i++] = new SqlParameter("@strReasonstobementioned", strReasonstobementioned);
                parm[i++] = new SqlParameter("@strConclusion", strConclusion);
                parm[i++] = new SqlParameter("@str8DStatus", str8DStatus);
                parm[i++] = new SqlParameter("@strConclusionDate", strConclusionDate);
                parm[i++] = new SqlParameter("@strCurrentstatus", strCurrentstatus);
                parm[i++] = new SqlParameter("@strCFTMembersDetails", strCFTMembersDetails);
                parm[i++] = new SqlParameter("@strPartReceiptdate", strPartReceiptdate);
                parm[i++] = new SqlParameter("@strOccuranceDetailsForPss", strOccuranceDetailsForPss);
                parm[i++] = new SqlParameter("@strOccuranceOutFlowDetailsForPss", strOccuranceOutFlowDetailsForPss);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetHeader", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intQuantityId;
        public string strPARTCHECKEDLOCATION, strDATE, strActiononNG;
        public decimal dblOKQty, dblNGQty;

        public string ManagetblProblemSolvingSheetObservationQuantityDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@intQuantityId", intQuantityId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strPARTCHECKEDLOCATION", strPARTCHECKEDLOCATION);
                parm[i++] = new SqlParameter("@strDATE", strDATE);
                parm[i++] = new SqlParameter("@dblOKQty", dblOKQty);
                parm[i++] = new SqlParameter("@dblNGQty", dblNGQty);
                parm[i++] = new SqlParameter("@strActiononNG", strActiononNG);
                parm[i++] = new SqlParameter("@strREMARK", strRemark);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetObservationQuantityDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblProblemSolvingSheetObservationQuantityDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intQuantityId", intQuantityId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetObservationQuantityDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManagetblProblemSolvingSheetObservationQuantityDetailsGet()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intQuantityId", intQuantityId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblProblemSolvingSheetObservationQuantityDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public int intdefectOccurenceId;
        public string strDefectOccurenceDetails, strValidFlag, Str4Mtype, strObservation;
        public string ManagetblProblemSolvingSheetdefectOccurenceDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[7];
                int i = 0;
                parm[i++] = new SqlParameter("@intdefectOccurenceId", intdefectOccurenceId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strDefectOccurenceDetails", strDefectOccurenceDetails);
                parm[i++] = new SqlParameter("@strValidFlag", strValidFlag);
                parm[i++] = new SqlParameter("@Str4Mtype", Str4Mtype);
                parm[i++] = new SqlParameter("@strObservation", strObservation);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetdefectOccurenceDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblProblemSolvingSheetdefectOccurenceDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intdefectOccurenceId", intdefectOccurenceId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetdefectOccurenceDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManagetblProblemSolvingSheetdefectOccurenceDetailsGet()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intdefectOccurenceId", intdefectOccurenceId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblProblemSolvingSheetdefectOccurenceDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }
               
        public string ManagetblProblemSolvingSheetdefectOutflowDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[7];
                int i = 0;
                parm[i++] = new SqlParameter("@intdefectOutflowId", intdefectOccurenceId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strDefectOutflowDetails", strDefectOccurenceDetails);
                parm[i++] = new SqlParameter("@strValidFlag", strValidFlag);
                parm[i++] = new SqlParameter("@Str4Mtype", Str4Mtype);
                parm[i++] = new SqlParameter("@strObservation", strObservation);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetdefectOutflowDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblProblemSolvingSheetdefectOutflowDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intdefectOutflowId", intdefectOccurenceId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetdefectOutflowDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManagetblProblemSolvingSheetdefectOutflowDetailsGet()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intdefectOutflowId", intdefectOccurenceId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblProblemSolvingSheetdefectOutflowDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public int intanalysisforDefectOccurrenceId;
        public string stranalysisDefectOccurenceDetails;

        public string ManagetblProblemSolvingSheetdefectanalysisforDefectOccurrenceDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intanalysisforDefectOccurrenceId", intanalysisforDefectOccurrenceId);
                parm[i++] = new SqlParameter("@intdefectOccurenceId", intdefectOccurenceId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@stranalysisDefectOccurenceDetails", stranalysisDefectOccurenceDetails);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetdefectanalysisforDefectOccurrenceDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblProblemSolvingSheetdefectanalysisforDefectOccurrenceDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intanalysisforDefectOccurrenceId", intanalysisforDefectOccurrenceId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetdefectanalysisforDefectOccurrenceDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManagetblProblemSolvingSheetdefectanalysisforDefectOccurrenceDetailsGet()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intanalysisforDefectOccurrenceId", intanalysisforDefectOccurrenceId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblProblemSolvingSheetdefectanalysisforDefectOccurrenceDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public string ManagetblProblemSolvingSheetdefectanalysisforDefectOutflowDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intanalysisforDefectOutflowId", intanalysisforDefectOccurrenceId);
                parm[i++] = new SqlParameter("@intdefectOutflowId", intdefectOccurenceId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@stranalysisDefectOutflowDetails", stranalysisDefectOccurenceDetails);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetdefectanalysisforDefectOutflowDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblProblemSolvingSheetdefectanalysisforDefectOutflowDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intanalysisforDefectOutflowId", intanalysisforDefectOccurrenceId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetdefectanalysisforDefectOutflowDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManagetblProblemSolvingSheetdefectanalysisforDefectOutflowDetailsGet()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intanalysisforDefectOutflowId", intanalysisforDefectOccurrenceId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblProblemSolvingSheetdefectanalysisforDefectOutflowDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public int intTemporaryCorrectiveactionId, intResponsibilityId;
        public string strDefectDetails, strTargetDate, strCorrectiveType, strActionType;
        public string ManagetblProblemSolvingSheetTemporaryCorrectiveactionDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intTemporaryCorrectiveactionId", intTemporaryCorrectiveactionId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strDefectDetails", strDefectDetails);
                parm[i++] = new SqlParameter("@intResponsibilityId", intResponsibilityId);
                parm[i++] = new SqlParameter("@strTargetDate", strTargetDate);
                parm[i++] = new SqlParameter("@strStatus", strStatus);
                parm[i++] = new SqlParameter("@strCorrectiveType", strCorrectiveType);
                parm[i++] = new SqlParameter("@strActionType", strActionType);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                parm[i++] = new SqlParameter("@strActualDate", strActualDate);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetTemporaryCorrectiveactionDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblProblemSolvingSheetTemporaryCorrectiveactionDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intTemporaryCorrectiveactionId", intTemporaryCorrectiveactionId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetTemporaryCorrectiveactionDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManagetblProblemSolvingSheetTemporaryCorrectiveactionDetailsGet()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intTemporaryCorrectiveactionId", intTemporaryCorrectiveactionId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblProblemSolvingSheetTemporaryCorrectiveactionDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public string strMonth1, strMonth2, strMonth3, strMonth4, strMonth5, strMonth6, strMonth7, strMonth8, strMonth9, strMonth10, strMonth11, strMonth12;
        public string ManagetblProblemSolvingSheetEffectivenessMonitioringDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[13];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strMonth1", strMonth1);
                parm[i++] = new SqlParameter("@strMonth2", strMonth2);
                parm[i++] = new SqlParameter("@strMonth3", strMonth3);
                parm[i++] = new SqlParameter("@strMonth4", strMonth4);
                parm[i++] = new SqlParameter("@strMonth5", strMonth5);
                parm[i++] = new SqlParameter("@strMonth6", strMonth6);
                parm[i++] = new SqlParameter("@strMonth7", strMonth7);
                parm[i++] = new SqlParameter("@strMonth8", strMonth8);
                parm[i++] = new SqlParameter("@strMonth9", strMonth9);
                parm[i++] = new SqlParameter("@strMonth10", strMonth10);
                parm[i++] = new SqlParameter("@strMonth11", strMonth11);
                parm[i++] = new SqlParameter("@strMonth12", strMonth12);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetEffectivenessMonitioringDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intStandardizationDocumentId, intDocumentsType;
        public string strApplicable, strActualDate;
        public string ManagetblProblemSolvingSheetStandardizationDocumentDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[11];
                int i = 0;
                parm[i++] = new SqlParameter("@intStandardizationDocumentId", intStandardizationDocumentId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@intDocumentsType", intDocumentsType);
                parm[i++] = new SqlParameter("@strApplicable", strApplicable);
                parm[i++] = new SqlParameter("@strTargetDate", strTargetDate);
                parm[i++] = new SqlParameter("@strActualDate", strActualDate);
                parm[i++] = new SqlParameter("@strStatus", strStatus);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                parm[i++] = new SqlParameter("@strDocumentName", strDocumentName);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetStandardizationDocumentDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblProblemSolvingSheetStandardizationDocumentDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intStandardizationDocumentId", intStandardizationDocumentId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetStandardizationDocumentDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManagetblProblemSolvingSheetStandardizationDocumentDetailsGet()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intStandardizationDocumentId", intStandardizationDocumentId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblProblemSolvingSheetStandardizationDocumentDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public string strApplicableModels;
        public string ManagetblProblemSolvingSheetHorizontalDeploymentDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@intStandardizationDocumentId", intStandardizationDocumentId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@intDocumentsType", strApplicableModels);
                parm[i++] = new SqlParameter("@strTargetDate", strTargetDate);
                parm[i++] = new SqlParameter("@strActualDate", strActualDate);
                parm[i++] = new SqlParameter("@strStatus", strStatus);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetHorizontalDeploymentDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblProblemSolvingSheetHorizontalDeploymentDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intStandardizationDocumentId", intStandardizationDocumentId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblProblemSolvingSheetHorizontalDeploymentDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManagetblProblemSolvingSheetHorizontalDeploymentDetailsGet()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intStandardizationDocumentId", intStandardizationDocumentId);
                parm[i++] = new SqlParameter("@Tasks", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManagetblProblemSolvingSheetHorizontalDeploymentDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable get_tblProblemSolvingSheetObservationDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("get_tblProblemSolvingSheetObservationDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblProblemSolvingSheetObservationDocumentForReport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strDocumentType", strDocumentType);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblProblemSolvingSheetObservationDocumentForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblProblemSolvingSheetObservationQuantityDetailsForReport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblProblemSolvingSheetObservationQuantityDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblProblemSolvingSheetdefectOccurenceDetailsForReport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblProblemSolvingSheetdefectOccurenceDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblProblemSolvingSheetdefectOutflowDetailsForReport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblProblemSolvingSheetdefectOutflowDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblProblemSolvingSheetdefectanalysisforDefectOccurrenceDetailsForReport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblProblemSolvingSheetdefectanalysisforDefectOccurrenceDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblProblemSolvingSheetdefectanalysisforDefectOutflowDetailsForReport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblProblemSolvingSheetdefectanalysisforDefectOutflowDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblProblemSolvingSheetTemporaryCorrectiveactionDetailsForReport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strCorrectiveType", strCorrectiveType);
                parm[i++] = new SqlParameter("@strActionType", strActionType);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblProblemSolvingSheetTemporaryCorrectiveactionDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblProblemSolvingSheetEffectivenessMonitioringDetailsForReport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblProblemSolvingSheetEffectivenessMonitioringDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblProblemSolvingSheetStandardizationDocumentDetailsForReport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblProblemSolvingSheetStandardizationDocumentDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblCQMDocumentDetailsForUpdate()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@intDocumentTypeId", intDocumentTypeId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblCQMDocumentDetailsForUpdate", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public int intStatusId;

        public string updateComplaintStatus()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@intStatusId", intStatusId);
                parm[i++] = new SqlParameter("@intStausById", intCreatedId);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("updateComplaintStatus", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetCurrentStatusDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCurrentStatusDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public int intQCFMId;
        public decimal dblValues;
        public string ManagetblCostofPoorQualityDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@intQCFMId", intQCFMId);
                parm[i++] = new SqlParameter("@dblValues", dblValues);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCostofPoorQualityDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetCustomerLocationandChampion()
        {
            DataTable ds = new DataTable();
            try
            {
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCustomerLocationandChampion", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblCQMDocumentDetailsForReport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@strTypeFor", strTypeFor);
                parm[i++] = new SqlParameter("@intDocumentTypeId", intDocumentTypeId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblCQMDocumentDetailsForReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable ManageProductParameterDetailsGet()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intDefectDetailsId", intDefectDetailsId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManageProductParameterDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public string ManageProductParameterDetailsGetDelete()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intDefectDetailsId", intDefectDetailsId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageProductParameterDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intApuId;
        public string strFromDate, strType;
        public DataTable GetProductParameterDetailsForDaillyEntryDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intApuId", intApuId);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                parm[i++] = new SqlParameter("@strFromDate", strFromDate);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intBranchd", intBranchId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetProductParameterDetailsForDaillyEntryDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataSet Get4MDetails()
        {
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataSet("GettblProblemSolvingSheetdefectOccurenceDetailsForPSSReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataSet Get4MOutFlowDetails()
        {
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataSet("GettblProblemSolvingSheetdefectOutflowDetailsForPSSReport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GettblProblemSolvingSheetHorizontalDeploymentDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblProblemSolvingSheetHorizontalDeploymentDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GetCustomerContactDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCustomerContactDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);

            }
            return ds;
        }

        public DataTable GetCustomerWisePssExport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@PssHId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCustomerWisePssExport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public string FId, BranchId, WhereClause, ViewName, FormDate, ToDate;
        public string ExecuteMSTReport(string SPName)
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@FId", FId);
                parm[i++] = new SqlParameter("@BranchId", BranchId);
                parm[i++] = new SqlParameter("@WhereClause", WhereClause);
                parm[i++] = new SqlParameter("@ViewName", ViewName);
                parm[i++] = new SqlParameter("@FormDate", FormDate);
                parm[i++] = new SqlParameter("@ToDate", ToDate);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar(SPName, parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetUserDetailsForSendEmail()
        {
            DataTable ds = new DataTable();
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@strUserId", intCreatedId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetUserDetailsForSendEmail", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataSet GetGraphCriteriaTopOne()
        {
            DataSet ds = new DataSet();
            try
            {
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                ds = DataLayer.DataLinker.ExecuteSPDataSet("GetGraphCriteriaTopOne", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public int intTPPDocumentId;
        public string ManagetblPSSTPPDocument()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@intTPPDocumentId", intTPPDocumentId);
                parm[i++] = new SqlParameter("@intTemporaryCorrectiveactionId", intTemporaryCorrectiveactionId);
                parm[i++] = new SqlParameter("@strDocumentName", strDocumentName);
                parm[i++] = new SqlParameter("@strDocumentType", strDocumentType);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblPSSTPPDocument", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intQualityFormatStructureHeaderId, intVeriableId, intSequenceNo;
        public string strScheduleFormatName, strCompanyShortName, strParticular, strShortName, strBasedOn, strFormula, strBoldFlag, strVisibleFlag, SystemSequence, strFor;
        public string ManagetblQualityFormatStructureHeader()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intQualityFormatStructureHeaderId", intQualityFormatStructureHeaderId);
                parm[i++] = new SqlParameter("@intCompanyID", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFyId);
                parm[i++] = new SqlParameter("@strDate", strDate);
                parm[i++] = new SqlParameter("@strScheduleFormatName", strScheduleFormatName);
                parm[i++] = new SqlParameter("@strCompanyShortName", strCompanyShortName);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intVeriableId", intVeriableId);
                parm[i++] = new SqlParameter("@strFor", strFor);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblQualityFormatStructureHeader", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string strConditionFlag;
        public string ManagetblQualityFormatStructureDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[12];
                int i = 0;
                parm[i++] = new SqlParameter("@intQualityFormatStructureHeaderId", intQualityFormatStructureHeaderId);
                parm[i++] = new SqlParameter("@intSequenceNo", intSequenceNo);
                parm[i++] = new SqlParameter("@strParticular", strParticular);
                parm[i++] = new SqlParameter("@strShortName", strShortName);
                parm[i++] = new SqlParameter("@strBasedOn", strBasedOn);
                parm[i++] = new SqlParameter("@strFormula", strFormula);
                parm[i++] = new SqlParameter("@dblAmount", dblAmount);
                parm[i++] = new SqlParameter("@strBoldFlag", strBoldFlag);
                parm[i++] = new SqlParameter("@strVisibleFlag", strVisibleFlag);
                parm[i++] = new SqlParameter("@SystemSequence", SystemSequence);
                parm[i++] = new SqlParameter("@Task", Tasks);
                parm[i++] = new SqlParameter("@strConditionFlag", strConditionFlag);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblQualityFormatStructureDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblQualityFormatStructureDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intQualityFormatStructureHeaderId", intQualityFormatStructureHeaderId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblQualityFormatStructureDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblQualityFormatParameterDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intQualityFormatStructureHeaderId", intQualityFormatStructureHeaderId);
                parm[i++] = new SqlParameter("@strFurmula", strFormula);
                parm[i++] = new SqlParameter("@intVeriableId", intVeriableId);
                parm[i++] = new SqlParameter("@SystemSequence", SystemSequence);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblQualityFormatParameterDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblQualityFormatParameterDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intQualityFormatStructureHeaderId", intQualityFormatStructureHeaderId);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblQualityFormatParameterDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManageDeleteOfQualityFormatStructure()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intQualityFormatStructureHeaderId", intQualityFormatStructureHeaderId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageDeleteOfQualityFormatStructure", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intFyId, intBranchId, FormatId;
        public string Types;
        public DataTable ManageAllCSICalCulation()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@ToDate", ToDate);
                parm[i++] = new SqlParameter("@FormatId", FormatId);
                parm[i++] = new SqlParameter("@Type", Types);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManageAllCSICalCulation", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GetCSIFormatDescription()
        {
            DataTable ds = new DataTable();
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@FormatId", FormatId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCSIFormatDescription", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GetCSIFormatDescriptionBasedOnFormula()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@FormatId", FormatId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCSIFormatDescriptionBasedOnFormula", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GetCSIFormatDescriptionAll()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@FormatId", FormatId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCSIFormatDescriptionAll", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public string stringValues;
        public DataTable GetAllintegerFormulaValues()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@Values", stringValues);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("fourmulaexexe", parm);
            }
            catch (Exception e)
            {
                throw;
            }
            return ds;
        }

        public DataTable GetAllParameterForConditionValues()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@FormatId", FormatId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetAllParameterForConditionValues", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GetSystemWiseCondition()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@FormatId", FormatId);
                parm[i++] = new SqlParameter("@SystemSequence", SystemSequence);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetSystemWiseCondition", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public int intMonthId, intYearId;
        public string strProcessType;
        public string ManagetblCSICalculationHeader()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intMonthId", intMonthId);
                parm[i++] = new SqlParameter("@intYearId", intYearId);
                parm[i++] = new SqlParameter("@strProcessType", strProcessType);
                parm[i++] = new SqlParameter("@intProcessedById", intCreatedId);
                parm[i++] = new SqlParameter("@intFormatId", FormatId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCSICalculationHeader", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string strVariableName, strVariableValues, strVariableRemarks, strColorCode;
        public int intCSICalculationId, intRangeId;
        public string ManagetblCSICalculationHeaderDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intCSICalculationId", intCSICalculationId);
                parm[i++] = new SqlParameter("@intDocumentId", intDocumentId);
                parm[i++] = new SqlParameter("@strVariableName", strVariableName);
                parm[i++] = new SqlParameter("@strVariableValues", strVariableValues);
                parm[i++] = new SqlParameter("@intRangeId", intRangeId);
                parm[i++] = new SqlParameter("@strVariableRemarks", strVariableRemarks);
                parm[i++] = new SqlParameter("@strColorCode", strColorCode);
                parm[i++] = new SqlParameter("@Task", Tasks);

                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCSICalculationHeaderDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblCSICalculationHeaderDetailsDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intCSICalculationId", intCSICalculationId);
                parm[i++] = new SqlParameter("@strVariableName", strVariableName);
                parm[i++] = new SqlParameter("@Task", Tasks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCSICalculationHeaderDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intYears, IntBranchId;

        public DataTable GetInterNalProductINComplaintList()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intMonthId", intMonthId);
                parm[i++] = new SqlParameter("@intYears", intYears);
                parm[i++] = new SqlParameter("@IntBranchId", IntBranchId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetInterNalProductINComplaintList", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public string ManageInterNalDetailsIntoMainTable()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[7];
                int i = 0;
                parm[i++] = new SqlParameter("@ProductId", intProductId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFyId);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                parm[i++] = new SqlParameter("@dblQuantity", dblQuantity);
                parm[i++] = new SqlParameter("@ParameterId", intParameterId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageInterNalDetailsIntoMainTable", parm);
            }
            catch (Exception exp)
            {

                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblComplainDefectDetailsFromInterNal()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFyId);
                parm[i++] = new SqlParameter("@ParameterId", intParameterId);
                parm[i++] = new SqlParameter("@dblQuantity", dblQuantity);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblComplainDefectDetails", parm);
            }
            catch (Exception exp)
            {

                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetDefectDetailsForPSS()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intPSSHrId", intProblemSolvingSheetHeaderId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetDefectDetailsForPSS", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public string ManagetblMonthWiseInternalTop3Defectandtop5ProductDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[11];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intFYId", intFyId);
                parm[i++] = new SqlParameter("@intMonthId", intMonthId);
                parm[i++] = new SqlParameter("@intYear", intYears);
                parm[i++] = new SqlParameter("@intParameterId", intParameterId);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@intInterNalResisterId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@dblQuntity", dblQuantity);
                parm[i++] = new SqlParameter("@intApuId", intApuId);
                parm[i++] = new SqlParameter("@intCreatedId", intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblMonthWiseInternalTop3Defectandtop5ProductDetails", parm);
            }
            catch (Exception exp)
            {

                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string DeleteDefectDailyDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[14];
                int i = 0;
                parm[i++] = new SqlParameter("@intAPUId", intAPUId);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                parm[i++] = new SqlParameter("@intDefectParameterId", intDefectParameterId);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@intDefectValue", intDefectValue);
                parm[i++] = new SqlParameter("@strDate", strDate);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                parm[i++] = new SqlParameter("@TotalRej", TotalRej);
                parm[i++] = new SqlParameter("@TotalProduction", TotalProduction);
                parm[i++] = new SqlParameter("@totalEolPPm", totalEolPPm);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("DeleteDefectDailyDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string FormName, strControleName;
        public DataTable GettblControlewiseStatusMaster()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@FormName", FormName);
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblControlewiseStatusMaster", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GettblControlewiseStatusMasterALL()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@FormName", FormName);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblControlewiseStatusMasterALL", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string ManageSubStatusDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@strFormName", FormName);
                parm[i++] = new SqlParameter("@strControleName", strControleName);
                parm[i++] = new SqlParameter("@intCreatedBYId", intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageSubStatusDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManageComplaintCancelDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@intCreatedBYId", intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageComplaintCancelDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string strFormName, strEvent, strTransactionDate;
        public int intAuditHistoryTranId;
        public string ausp_SaveTblAuditHistoryNew()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);

                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFyId);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@strFormName", strFormName);
                parm[i++] = new SqlParameter("@strEvent", strEvent);
                parm[i++] = new SqlParameter("@intAuditHistoryTranId", intAuditHistoryTranId);
                parm[i++] = new SqlParameter("@strDocumentNo", strDocumentNo);
                parm[i++] = new SqlParameter("@strTransactionDate", strTransactionDate);
                parm[i++] = new SqlParameter("@intUserId", intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ausp_SaveTblAuditHistoryNew", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string strFieldExistsTabName;
        public DataTable GetFieldEntryRights()
        {
            DataTable ds = new DataTable();
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[7];
                int i = 0;
                parm[i++] = new SqlParameter("@intUserId", intCreatedId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strFormName", strFormName);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@strFieldExistsTabName", strFieldExistsTabName);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetFieldEntryRights", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intTabGroupId;
        public string strControlName, NewValues, OldValues, MatricxFlag;
        public string ManagetblFieldAccessEntryTransactionDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);

                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intTabGroupId", intTabGroupId);
                parm[i++] = new SqlParameter("@intSequenceNo", intSequenceNo);
                parm[i++] = new SqlParameter("@strControlName", strControlName);
                parm[i++] = new SqlParameter("@strFormName", strFormName);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@NewValues", NewValues);
                parm[i++] = new SqlParameter("@OldValues", OldValues);
                parm[i++] = new SqlParameter("@MatricxFlag", MatricxFlag);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblFieldAccessEntryTransactionDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetUnConfigureRightAccess()
        {
            DataTable ds = new DataTable();
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[7];
                int i = 0;
                parm[i++] = new SqlParameter("@intUserId", intCreatedId);
                parm[i++] = new SqlParameter("@intProblemSolvingSheetHeaderId", intProblemSolvingSheetHeaderId);
                parm[i++] = new SqlParameter("@strFormName", strFormName);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@strFieldExistsTabName", strFieldExistsTabName);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetUnConfigureRightAccess", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intTabSequenceNo, intApproval1, intApproval2, intApproval3;
        public string ManagetblTabAccessApprovalDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);

                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@intDocumentId", intDocumentId);
                parm[i++] = new SqlParameter("@strFormName", strFormName);
                parm[i++] = new SqlParameter("@strType", strType);
                parm[i++] = new SqlParameter("@intTabGroupId", intTabGroupId);
                parm[i++] = new SqlParameter("@intTabSequenceNo", intTabSequenceNo);
                parm[i++] = new SqlParameter("@intApproval1", intApproval1);
                parm[i++] = new SqlParameter("@intApproval2", intApproval2);
                parm[i++] = new SqlParameter("@intApproval3", intApproval3);
                parm[i++] = new SqlParameter("@strFieldExistsTabName", strFieldExistsTabName);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblTabAccessApprovalDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblCustomerComplaintRemoveHeader()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);

                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intProcessById", intCreatedId);
                parm[i++] = new SqlParameter("@strProcessDate", System.DateTime.Now.ToString("dd-MMM-yyyy"));
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intFYId", intFyId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCustomerComplaintRemoveHeader", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intRemoveFlagId, intCustomerComplaintId;
        public string strCurrentFlag;
        public string ManagetblCustomerComplaintRemoveDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intRemoveFlagId", intRemoveFlagId);
                parm[i++] = new SqlParameter("@intCustomerComplaintId", intCustomerComplaintId);
                parm[i++] = new SqlParameter("@strCurrentFlag", strCurrentFlag);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblCustomerComplaintRemoveDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ExportWarrantyInformationExport()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExportWarrantyInformationExport", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intChampionid;
        public DataTable ExportWarrantyInformationExportFilterWise()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[7];
                int i = 0;
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                parm[i++] = new SqlParameter("@intChampionid", intChampionid);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExportWarrantyInformationExportFilterWise", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQM()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQM", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegister_CQMFilterWise()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                parm[i++] = new SqlParameter("@intChampionid", intChampionId);
                parm[i++] = new SqlParameter("@intApuId", intApuId);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegister_CQMFilterWise", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQMFilterWise()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                parm[i++] = new SqlParameter("@intChampionid", intChampionId);
                parm[i++] = new SqlParameter("@intApuId", intApuId);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQMFilterWise", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strTragetType, strTargetFor, strEntryType;
        public int intTypes, intCQMSalesPPMtargetId;
        public string ManageSalesPPMTargetDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[7];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYID", intFyId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@strTragetType", strTragetType);
                parm[i++] = new SqlParameter("@strTargetFor", strTargetFor);
                parm[i++] = new SqlParameter("@strEntryType", strEntryType);
                parm[i++] = new SqlParameter("@intCreatedId", intCreatedId);

                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageSalesPPMTargetDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetTotalValues()
        {
            DataTable ds = new DataTable();
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYID", intFyId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@strTragetType", strTragetType);
                parm[i++] = new SqlParameter("@strTargetFor", strTargetFor);
                parm[i++] = new SqlParameter("@strEntryType", strEntryType);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTotalValues", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string ManageSalesPPMTargetDelete()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intCQMSalesPPMtargetId", intCQMSalesPPMtargetId);
                parm[i++] = new SqlParameter("@Type", intTypes);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageSalesPPMTarget", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intCustomerGroupId, intProductGroupId;
        public decimal dblAPRSales_Rej, dblMAYSales_Rej, dblJUNESales_Rej, dblJULYSales_Rej, dblAUGSales_Rej, dblSEPSales_Rej, dblOCTSales_Rej, dblNOVSales_Rej, dblDECSales_Rej,
            dblJANSales_Rej, dblFEBSales_Rej, dblMARSales_Rej, dblAPRSCH_PPM, dblMAYSCH_PPM, dblJUNESCH_PPM, dblJULYSCH_PPM, dblAUGSCH_PPM, dblSEPSCH_PPM, dblOCTSCH_PPM,
            dblNOVSCH_PPM, dblDECSCH_PPM, dblJANSCH_PPM, dblFEBSCH_PPM, dblMARSCH_PPM;
        public string ManageSalesPPMTarget()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[29];
                int i = 0;
                parm[i++] = new SqlParameter("@intCQMSalesPPMtargetId", intCQMSalesPPMtargetId);
                parm[i++] = new SqlParameter("@intCustomerGroupId", intCustomerGroupId);
                parm[i++] = new SqlParameter("@intProductGroupId", intProductGroupId);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@dblAPRSales_Rej", dblAPRSales_Rej);
                parm[i++] = new SqlParameter("@dblMAYSales_Rej", dblMAYSales_Rej);
                parm[i++] = new SqlParameter("@dblJUNESales_Rej", dblJUNESales_Rej);
                parm[i++] = new SqlParameter("@dblJULYSales_Rej", dblJULYSales_Rej);
                parm[i++] = new SqlParameter("@dblAUGSales_Rej", dblAUGSales_Rej);
                parm[i++] = new SqlParameter("@dblSEPSales_Rej", dblSEPSales_Rej);
                parm[i++] = new SqlParameter("@dblOCTSales_Rej", dblOCTSales_Rej);
                parm[i++] = new SqlParameter("@dblNOVSales_Rej", dblNOVSales_Rej);
                parm[i++] = new SqlParameter("@dblDECSales_Rej", dblDECSales_Rej);
                parm[i++] = new SqlParameter("@dblJANSales_Rej", dblJANSales_Rej);
                parm[i++] = new SqlParameter("@dblFEBSales_Rej", dblFEBSales_Rej);
                parm[i++] = new SqlParameter("@dblMARSales_Rej", dblMARSales_Rej);
                parm[i++] = new SqlParameter("@dblAPRSCH_PPM", dblAPRSCH_PPM);
                parm[i++] = new SqlParameter("@dblMAYSCH_PPM", dblMAYSCH_PPM);
                parm[i++] = new SqlParameter("@dblJUNESCH_PPM", dblJUNESCH_PPM);
                parm[i++] = new SqlParameter("@dblJULYSCH_PPM", dblJULYSCH_PPM);
                parm[i++] = new SqlParameter("@dblAUGSCH_PPM", dblAUGSCH_PPM);
                parm[i++] = new SqlParameter("@dblSEPSCH_PPM", dblSEPSCH_PPM);
                parm[i++] = new SqlParameter("@dblOCTSCH_PPM", dblOCTSCH_PPM);
                parm[i++] = new SqlParameter("@dblNOVSCH_PPM", dblNOVSCH_PPM);
                parm[i++] = new SqlParameter("@dblDECSCH_PPM", dblDECSCH_PPM);
                parm[i++] = new SqlParameter("@dblJANSCH_PPM", dblJANSCH_PPM);
                parm[i++] = new SqlParameter("@dblFEBSCH_PPM", dblFEBSCH_PPM);
                parm[i++] = new SqlParameter("@dblMARSCH_PPM", dblMARSCH_PPM);
                parm[i++] = new SqlParameter("@Type", intTypes);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageSalesPPMTarget", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetFinancialYearStartDateandEndDate()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetFinancialYearStartDateandEndDate", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string MonthlyManageMonitoringProcess()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageMonitoringFromPage", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string theroleid, theModuleName;
        public int theintMDReportId;
        public DataTable GetGraphMenuReportForOnePage()
        {
            string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@companyId", intCompanyId);
                parm[i++] = new SqlParameter("@roleid", theroleid);
                parm[i++] = new SqlParameter("@ModuleName", theModuleName);
                parm[i++] = new SqlParameter("@intMDReportId", theintMDReportId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetGraphMenuReportForOnePage", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetGraphMenuReportForOnePageCustomWise()
        {
            string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@companyId", intCompanyId);
                parm[i++] = new SqlParameter("@roleid", theroleid);
                parm[i++] = new SqlParameter("@ModuleName", theModuleName);
                parm[i++] = new SqlParameter("@intMDReportId", theintMDReportId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetGraphMenuReportForOnePageCustomWise", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetCompanyDetailsForLicensMatch()
        {
            DataTable ds = new DataTable();
            try
            {
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCompanyDetailsForLicensMatch");
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetDrilldownReportDetails()
        {
            string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intReportId", theintMDReportId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetDrilldownReportDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strMavedInfo;
        public string ManageMavedInfo()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter(RBEDS.RBDE.dod("GSbzlH9xxHE/No5xArOn5Q=="), strMavedInfo);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar(RBEDS.RBDE.dod("z+4JJr2vwe+w6uP7r0/cjg=="), parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GettblMavedInfo()
        {
            DataTable ds = new DataTable();
            try
            {
                ds = DataLayer.DataLinker.ExecuteSPDataTable(RBEDS.RBDE.dod("z0WJwHA+FPbPPJDBism9OQ=="));
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetUserEmployeeIDsandName()
        {
            string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intUserId", intCreatedId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetUserEmployeeIDsandName", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetUserEmployeeIDNameandDepartment()
        {
            string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ssp_GetBindEmpDepartmentForBreakDownGeneral", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intEmployeeId;
        public DataTable GetEmployeeDetailsForAutoFill()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetEmployeeDetailsForAutoFill", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strModuleName;
        public string ManagetblTargetDaysSetingHeader()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                parm[i++] = new SqlParameter("@intCustomerGroupId", intCustomerGroupId);
                parm[i++] = new SqlParameter("@strModuleName", strModuleName);
                parm[i++] = new SqlParameter("@strFormName", strFormName);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblTargetDaysSetingHeader", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public int type;
        public string DeleteManagetblTargetDaysSetingDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intDayTargetHeaderId", intDayTargetHeaderId);
                parm[i++] = new SqlParameter("@type", type);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblTargetDaysSetingDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public int intDayTargetHeaderId, intDays;
        public string strWeaklyFlag, strHolidayFlag, strDateCalculationFieldName, strDateForFieldName, strDateCalculationCaption, strDateForCaption;
        public string ManagetblTargetDaysSetingDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                SqlParameter[] parm = new SqlParameter[15];
                int i = 0;
                parm[i++] = new SqlParameter("@intDayTargetHeaderId", intDayTargetHeaderId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                parm[i++] = new SqlParameter("@intCustomerGroupId", intCustomerGroupId);
                parm[i++] = new SqlParameter("@strWeaklyFlag", strWeaklyFlag);
                parm[i++] = new SqlParameter("@strHolidayFlag", strHolidayFlag);
                parm[i++] = new SqlParameter("@strDateCalculationFieldName", strDateCalculationFieldName);
                parm[i++] = new SqlParameter("@strDateForFieldName", strDateForFieldName);
                parm[i++] = new SqlParameter("@intDays", intDays);
                parm[i++] = new SqlParameter("@strModuleName", strModuleName);
                parm[i++] = new SqlParameter("@strFormName", strFormName);
                parm[i++] = new SqlParameter("@type", type);
                parm[i++] = new SqlParameter("@strDateCalculationCaption", strDateCalculationCaption);
                parm[i++] = new SqlParameter("@strDateForCaption", strDateForCaption);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblTargetDaysSetingDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }


        public DataTable GetDetailsDayTarget()
        {

            DataTable ds = new DataTable();
            try
            {
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                parm[i++] = new SqlParameter("@intCustomerGroupId", intCustomerGroupId);
                parm[i++] = new SqlParameter("@strModuleName", strModuleName);
                parm[i++] = new SqlParameter("@strFormName", strFormName);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetDetailsDayTarget", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }




        public int intBreakDownID, intEquipmentId, intDepartment, intWorkStationId, intBDCategory, intBDReason, intRepotedId;
        public string strBDEntryDate, strBDEntryTime, strBDDate, strBDTime, strProductionStatus, strBDDetails, strBDType, strReportedDate, strReportedTime, stBDImage;
        public string ManageTblBreakDownEntry()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[21];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                parm[i++] = new SqlParameter("@intCompanyID", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@strBDEntryDate", strBDEntryDate);
                parm[i++] = new SqlParameter("@strBDEntryTime", strBDEntryTime);
                parm[i++] = new SqlParameter("@strBDDate", strBDDate);
                parm[i++] = new SqlParameter("@strBDTime", strBDTime);
                parm[i++] = new SqlParameter("@intEquipmentId", intEquipmentId);
                parm[i++] = new SqlParameter("@intDepartment", intDepartment);
                parm[i++] = new SqlParameter("@intWorkStationId", intWorkStationId);
                parm[i++] = new SqlParameter("@intBDCategory", intBDCategory);
                parm[i++] = new SqlParameter("@strProductionStatus", strProductionStatus);
                parm[i++] = new SqlParameter("@intBDReason", intBDReason);
                parm[i++] = new SqlParameter("@strBDDetails", strBDDetails);
                parm[i++] = new SqlParameter("@strBDType", strBDType);
                parm[i++] = new SqlParameter("@intRepotedId", intRepotedId);
                parm[i++] = new SqlParameter("@strReportedDate", strReportedDate);
                parm[i++] = new SqlParameter("@strReportedTime", strReportedTime);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                parm[i++] = new SqlParameter("@stBDImage", stBDImage);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageTblBreakDownEntry", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }
        public string ManageTblBreakDownEntry_General()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[20];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                parm[i++] = new SqlParameter("@intCompanyID", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@strBDEntryDate", strBDEntryDate);
                parm[i++] = new SqlParameter("@strBDEntryTime", strBDEntryTime);
                parm[i++] = new SqlParameter("@strBDDate", strBDDate);
                parm[i++] = new SqlParameter("@strBDTime", strBDTime);
                parm[i++] = new SqlParameter("@intEquipmentId", intEquipmentId);
                parm[i++] = new SqlParameter("@intDepartment", intDepartment);
                parm[i++] = new SqlParameter("@intWorkStationId", intWorkStationId);
                parm[i++] = new SqlParameter("@intBDCategory", intBDCategory);
                parm[i++] = new SqlParameter("@strProductionStatus", strProductionStatus);
                parm[i++] = new SqlParameter("@intBDReason", intBDReason);
                parm[i++] = new SqlParameter("@strBDDetails", strBDDetails);
                parm[i++] = new SqlParameter("@strBDType", strBDType);
                parm[i++] = new SqlParameter("@intRepotedId", intRepotedId);
                parm[i++] = new SqlParameter("@strReportedDate", strReportedDate);
                parm[i++] = new SqlParameter("@strReportedTime", strReportedTime);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageTblBreakDownEntry_General", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }
        
        public DataTable GetBreakDownDetailsForEdit()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBreakDownDetailsForEdit", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intMachineId, intCheckPointId;
        public string strStandardValue, strStandardDeviation, strActualDeviation, strTool;
        public string ManagetblMachineCheckPointObservationDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@intMachineId", intMachineId);
                parm[i++] = new SqlParameter("@intCheckPointId", intCheckPointId);
                parm[i++] = new SqlParameter("@strStandardValue", strStandardValue);
                parm[i++] = new SqlParameter("@strStandardDeviation", strStandardDeviation);
                parm[i++] = new SqlParameter("@strObservation", strObservation);
                parm[i++] = new SqlParameter("@strActualDeviation", strActualDeviation);
                parm[i++] = new SqlParameter("@strTool", strTool);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblMachineCheckPointObservationDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }


        public int intAllocatedById, intAllocatedTo, intBDAllocationId;
        public string strTargetTime, strAllocationTime, strAllocationDate, strMaintanenceTeam, strAllocationType;
        public string ManagetblBDAllocationDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[14];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                parm[i++] = new SqlParameter("@intBDAllocationId", intBDAllocationId);
                parm[i++] = new SqlParameter("@strAllocationType", strAllocationType);
                parm[i++] = new SqlParameter("@intAllocatedTo", intAllocatedTo);
                parm[i++] = new SqlParameter("@strMaintanenceTeam", strMaintanenceTeam);
                parm[i++] = new SqlParameter("@strAllocationDate", strAllocationDate);
                parm[i++] = new SqlParameter("@strAllocationTime", strAllocationTime);
                parm[i++] = new SqlParameter("@strTargetDate", strTargetDate);
                parm[i++] = new SqlParameter("@strTargetTime", strTargetTime);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@intAllocatedById", intAllocatedById);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFyId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblBDAllocationDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }


        public DataTable GetBDAllocationDetails()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBDAllocationDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }


        public DataTable GetBDAllocationDetailsByAllocatedId()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBDAllocationId", intBDAllocationId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBDAllocationDetailsByAllocatedId", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetTop0tblRepairSpareDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTop0tblRepairSpareDetails");
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intItemId;
        public DataTable GetUomOfItemId()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intItemId", intItemId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetUomOfItemId", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intBDRepairId, intRepairById;
        public string strRepairEntryDate, strRepairEntryTime, strRepairDate, strRepairTime, strActivitesorServices;
        public decimal dblManHours, dblServiceCost, dblSpareCost, dblTotalCost, dblAdditionalServiceCost, dblAdtitionSpareCost;
        public string ManageBDRepair()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[19];
                int i = 0;
                parm[i++] = new SqlParameter("@intBDRepairId", intBDRepairId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFyId);
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                parm[i++] = new SqlParameter("@strRepairEntryDate", strRepairEntryDate);
                parm[i++] = new SqlParameter("@strRepairEntryTime", strRepairEntryTime);
                parm[i++] = new SqlParameter("@intRepairById", intRepairById);
                parm[i++] = new SqlParameter("@strRepairDate", strRepairDate);
                parm[i++] = new SqlParameter("@strRepairTime", strRepairTime);
                parm[i++] = new SqlParameter("@strActivitesorServices", strActivitesorServices);
                parm[i++] = new SqlParameter("@strStatus", strStatus);
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                parm[i++] = new SqlParameter("@dblManHours", dblManHours);
                parm[i++] = new SqlParameter("@dblServiceCost", dblServiceCost);
                parm[i++] = new SqlParameter("@dblSpareCost", dblSpareCost);
                parm[i++] = new SqlParameter("@dblTotalCost", dblTotalCost);
                parm[i++] = new SqlParameter("@dblAdditionalServiceCost", dblAdditionalServiceCost);
                parm[i++] = new SqlParameter("@dblAdtitionSpareCost", dblAdtitionSpareCost);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageBDRepair", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public int intItemName, intUOM, intSafetyCheckPointID;
        public decimal dblQty, dblCost;
        public string ManageRepairSpare()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intBDRepairId", intBDRepairId);
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFyId);
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                parm[i++] = new SqlParameter("@intItemName", intItemName);
                parm[i++] = new SqlParameter("@dblQty", dblQty);
                parm[i++] = new SqlParameter("@intUOM", intUOM);
                parm[i++] = new SqlParameter("@dblCost", dblCost);
                parm[i++] = new SqlParameter("@type", type);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageRepairSpare", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }

        public string ManageRepairSpareDelete()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intBDRepairId", intBDRepairId);
                parm[i++] = new SqlParameter("@type", type);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageRepairSpare", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }


        public DataTable GetBDRepairDetails()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBDRepairDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetBDRepairSpareDetails()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBDRepairSpareDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strSafetyCheckPointName, strSafetyCheckPointCode, strSafetyCheckDecs, strCriticalFlag, strGeneralFlag;
        public string SaveTblSafetyCheckPoint()
        {
            string intCompanyID = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@intSafetyCheckPointID", intSafetyCheckPointID);
                parm[i++] = new SqlParameter("@intCompanyID", intCompanyID);
                parm[i++] = new SqlParameter("@strSafetyCheckPointCode", strSafetyCheckPointCode);
                parm[i++] = new SqlParameter("@strSafetyCheckPointName", strSafetyCheckPointName);
                parm[i++] = new SqlParameter("@strSafetyCheckDecs", strSafetyCheckDecs);
                parm[i++] = new SqlParameter("@strShortName", strShortName);
                parm[i++] = new SqlParameter("@strCriticalFlag", strCriticalFlag);
                parm[i++] = new SqlParameter("@strGeneralFlag", strGeneralFlag);
               // aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ssp_SaveTblSafetyCheckPoint", parm);
                if (strShortName == null || strShortName == "")
                {
                    parm[i++] = new SqlParameter("@Operation", "ExeNull");
                    aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ssp_SaveTblSafetyCheckPoint", parm);
                }
                else
                {
                    parm[i++] = new SqlParameter("@Operation", "NotExeNull");
                    aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ssp_SaveTblSafetyCheckPoint", parm);
                }
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }
        public string DeleteTblSafetyCheckPoint()
        {
            string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intSafetyCheckPointID", intSafetyCheckPointID);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ssp_DeleteTblSafetyCheckPoint", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = null;
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetTblSafetyCheckPoint()
        {
            string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intSafetyCheckPointID", intSafetyCheckPointID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ssp_GetSafetyCheckPoint", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string ReportedDateTime, BDDateTime;
        public DataTable CheckDateandTimeNotBiggerFromReportedDate()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@ReportedDateTime", ReportedDateTime);
                parm[i++] = new SqlParameter("@BDDateTime", BDDateTime);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("CheckDateandTimeNotBiggerFromReportedDate", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable CheckDateandTimeNotBiggerFromReportedDateCalculateFromBDSeting()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@ReportedDateTime", ReportedDateTime);
                parm[i++] = new SqlParameter("@BDDateTime", BDDateTime);
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("CheckDateandTimeNotBiggerFromReportedDateCalculateFromBDSeting", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GettblUserVQMSSeting()
        {
            string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intUserId", intCreatedId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblUserVQMSSeting", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intType;
        public DataTable GetManageCloseStatusOfBD()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownId", intBreakDownID);
                parm[i++] = new SqlParameter("@intType", intType);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManageCloseStatusOfBD", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strClosedTime, strClosedStatus, strClosedRemarks, strMachinehandoverdate, strMachinehandoverTime;
        public string ManageCloseStatusOfBD()
        {
            string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserEmployeeId"]);
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownId", intBreakDownID);
                parm[i++] = new SqlParameter("@strClosedTime", strClosedTime);
                parm[i++] = new SqlParameter("@strClosedStatus", strClosedStatus);
                parm[i++] = new SqlParameter("@strClosedRemarks", strClosedRemarks);
                parm[i++] = new SqlParameter("@strClosedById", intCreatedId);
                parm[i++] = new SqlParameter("@intType", intType);
                parm[i++] = new SqlParameter("@strMachinehandoverdate", strMachinehandoverdate);
                parm[i++] = new SqlParameter("@strMachinehandoverTime", strMachinehandoverTime);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageCloseStatusOfBD", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = null;
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable CheckDateandTimeNotBiggerInRepaired()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@ReportedDateTime", ReportedDateTime);
                parm[i++] = new SqlParameter("@BDDateTime", BDDateTime);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("CheckDateandTimeNotBiggerInRepaired", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strReadTime;
        public string ManageBdReadOrNNot()
        {
            string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intReadById", intCreatedId);
                parm[i++] = new SqlParameter("@strReadTime", strReadTime);
                parm[i++] = new SqlParameter("@intBreakDownId", intBreakDownID);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageBdReadOrNNot", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = null;
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string strCategoryType;
        public DataTable GetRecoredDetails()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@ToDate", ToDate);
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                parm[i++] = new SqlParameter("@strCategoryType", strCategoryType);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetRecoredDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetCountForCustomerEnquiry()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@ToDate", ToDate);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCountForCustomerEnquiry", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }
        public int intBDReasonDetailID;
        public DataTable GetCategoryOfBDReason()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBDReasonDetailID", intBDReasonDetailID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCategoryOfBDReason", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string strPMFor, strPMPeriodType, strPMYearType, strPMDayType;
        public int intMachineCategoryId, intMachineTypeId;
        public string ManagetblPMSetting()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@strPMFor", strPMFor);
                parm[i++] = new SqlParameter("@strPMPeriodType", strPMPeriodType);
                parm[i++] = new SqlParameter("@intDays", intDays);
                parm[i++] = new SqlParameter("@strPMYearType", strPMYearType);
                parm[i++] = new SqlParameter("@strPMDayType", strPMDayType);
                parm[i++] = new SqlParameter("@intMachineCategoryId", intMachineCategoryId);
                parm[i++] = new SqlParameter("@intMachineTypeId", intMachineTypeId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblPMSetting", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intPMId;
        public string DeletePMSetting()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intPMId", intPMId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("DeletePMSetting", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public int intFYId, intYear, intCategory;
        public string strDefaultNextValue;
        public string ManageProcessandReprocessofSchdlue()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFYId);
                parm[i++] = new SqlParameter("@intYear", intYear);
                parm[i++] = new SqlParameter("@intCategory", intCategory);
                parm[i++] = new SqlParameter("@intMachineTypeId", intMachineTypeId);
                parm[i++] = new SqlParameter("@intEquipmentId", intEquipmentId);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                parm[i++] = new SqlParameter("@strDefaultNextValue", strDefaultNextValue);

                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageProcessandReprocessofSchdlue", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable ManageViewScheduleDetails()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCategory", intCategory);
                parm[i++] = new SqlParameter("@intMachineTypeId", intMachineTypeId);
                parm[i++] = new SqlParameter("@intEquipmentId", intEquipmentId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ManageViewScheduleDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable CheckCategoryProcessedOrNot()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId",intBranchId );   
                parm[i++] = new SqlParameter("@intFYId", intFYId); 
                parm[i++] = new SqlParameter("@intYear",intYear );
                parm[i++] = new SqlParameter("@intCategory", intCategory); 
                ds = DataLayer.DataLinker.ExecuteSPDataTable("CheckCategoryProcessedOrNot", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetCategoryTypeIdForSelection()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[3];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFYId);
                parm[i++] = new SqlParameter("@intYear", intYear);               
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCategoryTypeIdForSelection", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intPreventiveSchduleId;
        public string strNextPreventiveNextDate,strOperation;
        public string UpdatetblPreventiveScheduleDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@strOperation", strOperation);
                parm[i++] = new SqlParameter("@intPreventiveSchduleId", intPreventiveSchduleId);
                parm[i++] = new SqlParameter("@strNextPreventiveNextDate", strNextPreventiveNextDate);
                parm[i++] = new SqlParameter("@intEditById", intCreatedId);               

                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("UpdatetblPreventiveScheduleDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }
        public int intBDCategoryId;
        public string ManageTblBreakDownEntryForPriventive()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intPreventiveSchduleId", intPreventiveSchduleId);
                parm[i++] = new SqlParameter("@strBDDate", strBDDate);
                parm[i++] = new SqlParameter("@strBDTime", strBDTime);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                parm[i++] = new SqlParameter("@intBDCategoryId", intBDCategoryId);

                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageTblBreakDownEntryForPriventive", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }


        public DataTable GetTotalCount()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[7];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCategory",intCategory );
                parm[i++] = new SqlParameter("@intMachineTypeId",intMachineTypeId );
                parm[i++] = new SqlParameter("@intEquipmentId",intEquipmentId );
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate",Todate );
                parm[i++] = new SqlParameter("@intDays", intDays);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTotalCount", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetTop0FortblMaterialInwardDetails()
        {

            DataTable ds = new DataTable();
            try
            {

                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTop0FortblMaterialInwardDetails");
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intMaterialinwardId, intVendorId, intUOMId;
        public string strMategialInWardDate, strPONo, strPODate, strGINNo, strGINDate, strCloseFlag;
        public string ManagetblMaterialInward()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                 string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[13];
                int i = 0;
                parm[i++] = new SqlParameter("@intMaterialinwardId",intMaterialinwardId);
                parm[i++] = new SqlParameter("@intCompanyId",intCompanyId);
                parm[i++] = new SqlParameter("@intBranchId",intBranchId);
                parm[i++] = new SqlParameter("@intFYId",intFyId);
                parm[i++] = new SqlParameter("@strMategialInWardDate",strMategialInWardDate);
                parm[i++] = new SqlParameter("@strPONo",strPONo);
                parm[i++] = new SqlParameter("@strPODate",strPODate);
                parm[i++] = new SqlParameter("@strGINNo",strGINNo);
                parm[i++] = new SqlParameter("@strGINDate",strGINDate);
                parm[i++] = new SqlParameter("@intVendorId",intVendorId);
                parm[i++] = new SqlParameter("@strRemarks",strRemarks);
                parm[i++] = new SqlParameter("@strCloseFlag",strCloseFlag);
                parm[i++] = new SqlParameter("@intCreatedById", intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblMaterialInward", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string ManagetblMaterialInwardDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intMaterialinwardId", intMaterialinwardId);
                parm[i++] = new SqlParameter("@intItemId", intItemId);
                 parm[i++] = new SqlParameter("@dblQty",dblQty );
                parm[i++] = new SqlParameter("@intUOMId",intUOMId );
                parm[i++] = new SqlParameter("@strRemarks", strRemarks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblMaterialInwardDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }


        public DataTable GettblMaterialInward()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intMaterialinwardId", intMaterialinwardId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblMaterialInward", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetFortblMaterialInwardDetails()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intMaterialinwardId", intMaterialinwardId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetFortblMaterialInwardDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable SPExecutionUsingSP(string SPName,int intMachineIds)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intMachineId", intMachineIds);                
                ds = DataLayer.DataLinker.ExecuteSPDataTable(SPName, parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);

            }
            return ds;
        }

        public int intCategoryId;
        public DataTable GetCategoryPMFlag()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intCategoryId", intCategoryId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetCategoryPMFlag", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetItemForRepairMultipule()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetItemForRepairMultipule", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string IntItemIds;
        public DataTable GetInserttblRepairSpareDetails()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@IntItemIds", IntItemIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetInserttblRepairSpareDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intToDepartmentId, intToWSId, intToTransferId;
        public string ManagetblEquipmentTransDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intEquipmentId",intEquipmentId);    
                parm[i++] = new SqlParameter("@intToDepartmentId",intToDepartmentId);
                parm[i++] = new SqlParameter("@intToWSId",intToWSId);
                parm[i++] = new SqlParameter("@intToTransferId",intToTransferId);
                parm[i++] = new SqlParameter("@intTransById", intCreatedId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblEquipmentTransDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetSchduleCharecterColor()
        {

            DataTable ds = new DataTable();
            try
            {
                
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetSchduleCharecterColor");
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetTop0ServicesofBDDetails()
        {

            DataTable ds = new DataTable();
            try
            {

                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTop0ServicesofBDDetails");
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public int intBDId, intServiceId;
        public decimal dblHours, dblRate;
        public string strServiceRemarks;
        public string ManageBDRepairServiceDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intBDId",intBDId);
               parm[i++] = new SqlParameter("@intServiceId",intServiceId);
                parm[i++] = new SqlParameter("@dblHours",dblHours);
                parm[i++] = new SqlParameter("@dblRate",dblRate);
                parm[i++] = new SqlParameter("@dblServiceCost",dblServiceCost);
                parm[i++] = new SqlParameter("@strServiceRemarks", strServiceRemarks);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManageBDRepairServiceDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetTblBDMaintainanceOperationMasterRate()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBDOperationId", intServiceId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTblBDMaintainanceOperationMasterRate", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GETBDServiceDetails()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBreakDownID", intBreakDownID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GETBDServiceDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public string DeleteBDServiceDetails()
        {
            string aReturnValue = string.Empty;
            try
            {

                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intBDId", intBDId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("DeleteBDServiceDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }


        public int intComplaintId;
        public string ManagetblComplaintEntryInfo()
        {
            string aReturnValue = string.Empty;
            try
            {
                  string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                string intCompanyId = Convert.ToString(HttpContext.Current.Session["CompanyID"]);
                string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                string intFyId = Convert.ToString(HttpContext.Current.Session["FinancialYearId"]);
                SqlParameter[] parm = new SqlParameter[17];
                int i = 0;
                parm[i++] = new SqlParameter("@intComplaintId", intComplaintId);
                parm[i++] = new SqlParameter("@intCustomerId",intCustomerId );               
                parm[i++] = new SqlParameter("@intLocationId",intLocationId );                 
                parm[i++] = new SqlParameter("@intChampionId",intChampionId );             
                parm[i++] = new SqlParameter("@strComplaintcommunicationMode",strComplaintcommunicationMode ); 
                parm[i++] = new SqlParameter("@strCustomerComplaint",strCustomerComplaint ); 
                parm[i++] = new SqlParameter("@intProductCategoryId",intProductCategoryId );
                parm[i++] = new SqlParameter("@intProductId", intProductId);
           
                parm[i++] = new SqlParameter("@intProductSubCategory",intProductSubCategory ); 
                parm[i++] = new SqlParameter("@dblQuantity",dblQuantity ); 
                parm[i++] = new SqlParameter("@intAPU",intAPU );
                parm[i++] = new SqlParameter("@strProductType",strProductType );
                parm[i++] = new SqlParameter("@strPartRecivedDate", strPartRecivedDate); 
                parm[i++] = new SqlParameter("@intBranchId",intBranchId ); 
                parm[i++] = new SqlParameter("@intFYId",intFyId ); 
                parm[i++] = new SqlParameter("@intCompanyId", intCompanyId);
                 parm[i++] = new SqlParameter("@intCreatedById",intCreatedId );
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblComplaintEntryInfo", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }



        public DataTable GettblComplaintEntryInfoForEdit()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intComplaintId", intComplaintId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblComplaintEntryInfoForEdit", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);

            }
            return ds;
        }

        public string intBranchids;
        public DataTable GetComplaintInfoCounts()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchid", intBranchids);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetComplaintInfoCounts", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);

            }
            return ds;
        }


        public string ManagetblComplaintEntryInfoToMainTable()
        {
            string aReturnValue = string.Empty;
            try
            {
               
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intComplaintId", intComplaintId);
                parm[i++] = new SqlParameter("@strStatus", strStatus);                
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblComplaintEntryInfoToMainTable", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);

            }
            return aReturnValue;
        }


        // Pradeep On 01-May-2019
        public int intAutoScreenId;
        public string strUserName, strLocalMachineIP, strLocalMachineMacAdd, strLocalMachineName, strPageName1, strPageName2, strbrowserName;

        public DataTable GetAlltblAutoScreenHeader()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intAutoScreenId", intAutoScreenId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetAlltblAutoScreenHeader", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }


        public DataTable GetAlltblAutoScreenDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                if (intAutoScreenId > 0)
                {
                    SqlParameter[] parm = new SqlParameter[1];
                    int i = 0;
                    parm[i++] = new SqlParameter("@intAutoScreenId", intAutoScreenId);
                    ds = DataLayer.DataLinker.ExecuteSPDataTable("GetAlltblAutoScreenDetails", parm);
                }
                else if (intAutoScreenDetailsId > 0)
                {
                    SqlParameter[] parm = new SqlParameter[1];
                    int i = 0;
                    parm[i++] = new SqlParameter("@intAutoScreenDetailsId", intAutoScreenDetailsId);
                    ds = DataLayer.DataLinker.ExecuteSPDataTable("GetAlltblAutoScreenDetails", parm);
                }

            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

      

        public string ManagetblAutoScreenHeader()
        {
            string aReturnValue = string.Empty;
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intAutoScreenId", intAutoScreenId);
                parm[i++] = new SqlParameter("@strUserName", strUserName);
                parm[i++] = new SqlParameter("@strLocalMachineIP", strLocalMachineIP);
                parm[i++] = new SqlParameter("@strLocalMachineMacAdd", strLocalMachineMacAdd);
                parm[i++] = new SqlParameter("@strLocalMachineName", strLocalMachineName);
                parm[i++] = new SqlParameter("@strPageNameA", strPageName1);
                parm[i++] = new SqlParameter("@strPageNameB", strPageName2);
                parm[i++] = new SqlParameter("@strbrowserName", strbrowserName);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblAutoScreenHeader", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }


        public int intAutoScreenDetailsId, intAutoScreenId1, intSequenceNo1;
        public double intIntervalInSecound;
        public string strDivName, strCaptionName, strPDFFileName, strFileType, strActiveFlag;

        public string ManagetblAutoScreenDetails()
        {
            string aReturnValue = "";
            try
            {
                string intCreatedId = Convert.ToString(HttpContext.Current.Session["UserID"]);
                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@intAutoScreenDetailsId", intAutoScreenDetailsId);
                parm[i++] = new SqlParameter("@intAutoScreenId", intAutoScreenId1);
                parm[i++] = new SqlParameter("@intSequenceNo", intSequenceNo1);
                parm[i++] = new SqlParameter("@intIntervalInSecound", intIntervalInSecound);
                parm[i++] = new SqlParameter("@strDivName", strDivName);
                parm[i++] = new SqlParameter("@strCaptionName", strCaptionName);
                parm[i++] = new SqlParameter("@strPDFFileName", strPDFFileName);
                parm[i++] = new SqlParameter("@strFileType", strFileType);
                parm[i++] = new SqlParameter("@strActiveFlag", strActiveFlag);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblAutoScreenDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }
        public DataTable GetMaxSequenceNotblAutoScreenDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intAutoScreenId", intAutoScreenId);
                parm[i++] = new SqlParameter("@intSequenceNo", intSequenceNo);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetMaxSequenceNotblAutoScreenDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        //public string GetMaxSequenceNotblAutoScreenDetails()
        //{
        //    string aReturnValue = "";
        //    try
        //    {
        //        SqlParameter[] parm = new SqlParameter[2];
        //        int i = 0;
        //        parm[i++] = new SqlParameter("@intAutoScreenId", intAutoScreenId);
        //        parm[i++] = new SqlParameter("@intSequenceNo", intSequenceNo);
        //        aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("GetMaxSequenceNotblAutoScreenDetails", parm);
        //    }
        //    catch (Exception exp)
        //    {
        //        aReturnValue = "";
        //        ExceptionHandler.LogError(exp);
        //    }
        //    return aReturnValue;
        //}

        public string GetMaxSequenceNotblAutoScreenDetails1()
        {
            string aReturnValue = "";
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intAutoScreenId", intAutoScreenId);
                parm[i++] = new SqlParameter("@intSequenceNo", intSequenceNo);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("GetMaxSequenceNotblAutoScreenDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GetTblAutoScreeanDivName()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intAutoScreenId", intAutoScreenId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetTblAutoScreeanDivName", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable SelectViewAllUserNames()
        {
            DataTable ds = new DataTable();
            try
            {
                ds = DataLayer.DataLinker.ExecuteSPDataTable("SelectViewAllUserNames");
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }           
      
        //Created By Vaibhav
        /// <summary>
        /// //----Product Master-----
        /// //----Product Master Details----
        /// //----Product Master SubDetails-
        /// </summary>
        /// <returns></returns>
        /// 
        public int intTaskFormatId,intTaskFormatDetailsId,intProductModuleId,intProductMoudleFormId,intProductMoudleFormTypeId;
        public string strFormCode, strFormDescription;
        public DataTable GetProjectTaskDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intTaskFormatId", intTaskFormatId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetProjectTaskDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }


        public DataTable GetProductModuleDetails()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProductModuleId", intProductModuleId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetProductModuleDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        //Delete Product Details
        public string dsp_DeleteTblProductModuleDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProductModuleId", intProductModuleId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("dsp_DeleteTblProductModuleDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public string dsp_DeleteTblProductModuleFormDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProductMoudleFormId", intProductMoudleFormId);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("dsp_DeleteTblProductModuleFormDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        //Save Details    

        public int intImplementationDuration;
        public string strProductModule, strProductModuleDescription, strProductMoudleCode;

        public string ausp_SaveProductModuleDetails()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[7];
                int i = 0;
                parm[i++] = new SqlParameter("@intItemId", intItemId);
                parm[i++] = new SqlParameter("@strProductModule", strProductModule);
                parm[i++] = new SqlParameter("@strProductModuleDescription", strProductModuleDescription);
                parm[i++] = new SqlParameter("@strProductMoudleCode", strProductMoudleCode);
                parm[i++] = new SqlParameter("@intImplementationDuration", intImplementationDuration);
                parm[i++] = new SqlParameter("@Task", "insert");
                parm[i++] = new SqlParameter("@strCloseFlag", strCloseFlag);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ausp_SaveProductModuleDetails_New", parm);
                return aReturnValue;

                //if (intProductModuleId == null)
                //{


                //}
                //else
                //{
                //    parm[i++] = new SqlParameter("@intProductModuleId", intProductModuleId);
                //    parm[i++] = new SqlParameter("@Task", "update");
                //    aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ausp_SaveProductModuleDetails", parm);
                //    return aReturnValue;
                //}
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        //public string ausp_SaveProductModuleDetails()
        //{
        //    string aReturnValue = string.Empty;
        //    try
        //    {
        //        SqlParameter[] parm = new SqlParameter[8];
        //        int i = 0;
        //        parm[i++] = new SqlParameter("@intItemId", intItemId);
        //        parm[i++] = new SqlParameter("@strProductModule", strProductModule);
        //        parm[i++] = new SqlParameter("@strProductModuleDescription", strProductModuleDescription);
        //        parm[i++] = new SqlParameter("@strProductMoudleCode", strProductMoudleCode);
        //        parm[i++] = new SqlParameter("@intImplementationDuration", intImplementationDuration);
        //        parm[i++] = new SqlParameter("@strCloseFlag", strCloseFlag);
              
        //        if (intProductModuleId == null)
        //        {
        //            parm[i++] = new SqlParameter("@Task", "insert");
        //            aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ausp_SaveProductModuleDetails", parm);
        //            return aReturnValue;
        //        }
        //        else
        //        {
        //            parm[i++] = new SqlParameter("@intProductModuleId", intProductModuleId);
        //            parm[i++] = new SqlParameter("@Task", "update");
        //            aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ausp_SaveProductModuleDetails", parm);
        //            return aReturnValue;
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        aReturnValue = "";
        //        ExceptionHandler.LogError(exp);
        //    }
        //    return aReturnValue;
        //}

        public string ausp_SaveTblProductModuleFormTypeMaster()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@intProductModuleId", intProductModuleId);
                parm[i++] = new SqlParameter("@intItemId", intItemId);
                parm[i++] = new SqlParameter("@strFormName", strFormName);
                parm[i++] = new SqlParameter("@strFormDescription", strFormDescription);
                parm[i++] = new SqlParameter("@strFormCode", strFormCode);
                parm[i++] = new SqlParameter("@intProductMoudleFormTypeId", intProductMoudleFormTypeId);
                parm[i++] = new SqlParameter("@strCloseFlag", strCloseFlag);

                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ausp_SaveProductModuleDetails", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        //Get project details for edit.
        public DataTable GetProjectModuleDetailsForEdit()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intProductModuleId", intProductModuleId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetProjectModuleDetailsForEdit", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }
        //public string getItemName()
        //{
        //    string name="";
        //  //  int intItemId;
        //    SqlParameter[] parm = new SqlParameter[1];
        //    int i = 0;
        //    parm[i++] = new SqlParameter("@intItemId", intItemId);
        //    name = DataLayer.DataLinker.ExecuteSPScalar("getItemName", parm);
        //    return name;
        //}

        public DataTable GetAllDatewiseCustomerEnquiryDetails()
        {

            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFyId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@ToDate", ToDate);
                parm[i++] = new SqlParameter("@strStatus", strStatus);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetAllAndDateWiseCustomerEnquiry", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }
        //Added on 07-Sep-2019 for bind grid Customer Complaint Analysis[Customer/Wareenty/Internal] in .cs file 

        public DataTable GetCustomerComplaintsRegister_CQM()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[6];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@ToDate", ToDate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchid", strBranchid);
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("SP_GetCustomerComplaintsRegister_CQM", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetFinancialYearCurrentMonth_FirstDate()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@strFYID", strFYID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetFinancialYearCurrentMonth_FirstDate", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable MeterReadingMasterView()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@intEnergyMeterId", intEnergyMeterId);                                
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetDetailsTblEnergyMeterReadingDetailForView", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GetPMSlipGeneration()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intCategory", intCategory);
                parm[i++] = new SqlParameter("@intMachineTypeId", intMachineTypeId);
                parm[i++] = new SqlParameter("@intEquipmentId", intEquipmentId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@intDays", intDays);
                parm[i++] = new SqlParameter("@intType", intType);                                                
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetScheduleDetailsForSlipGenerate", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable BreakDownGeneral()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[7];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFYId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@EmployeeId", EmployeeId);
                parm[i++] = new SqlParameter("@DepartmentId", DepartmentId);
                parm[i++] = new SqlParameter("@WorkStationId", WorkStationId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetEquipmentDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable BreakDownGeneral_Details()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFYId", intFYId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@EmployeeId", EmployeeId);
                parm[i++] = new SqlParameter("@DepartmentId", DepartmentId);
                parm[i++] = new SqlParameter("@WorkStationId", WorkStationId);
                parm[i++] = new SqlParameter("@Status", strStatus);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBreakDownDetails_General", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable BreakDownAllocationStatus()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[10];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intFyId", intFYId);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@ToDate", ToDate);
                parm[i++] = new SqlParameter("@Type", strType);
                parm[i++] = new SqlParameter("@intEmployeeId", intEmployeeId);
                parm[i++] = new SqlParameter("@intDepartment", intDepartment);
                parm[i++] = new SqlParameter("@intWorkStationId", intWorkStationId);
                parm[i++] = new SqlParameter("@Status", strStatus);
                parm[i++] = new SqlParameter("@EquipmentCategory", EquipmentCategory);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetBreakDownAllocationstatusDetails", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable PreventiveMainSetting()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@intMachineTypeId", intMachineTypeId);
                parm[i++] = new SqlParameter("@intMachineCategoryID", intMachineCategoryID);
                parm[i++] = new SqlParameter("@intDays", intDays);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetMachineTypeForPMSetting", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable PreventiveMainSetting_View()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[8];
                int i = 0;
                parm[i++] = new SqlParameter("@intBranchId", intBranchId);
                parm[i++] = new SqlParameter("@strPMFor", strPMFor);
                parm[i++] = new SqlParameter("@strPMPeriodType", strPMPeriodType);
                parm[i++] = new SqlParameter("@strPMYearType", strPMYearType);
                parm[i++] = new SqlParameter("@strPMDayType", strPMDayType);
                parm[i++] = new SqlParameter("@intMachineCategoryId", intMachineCategoryId);
                parm[i++] = new SqlParameter("@intMachineTypeId", intMachineTypeId);
                parm[i++] = new SqlParameter("@intType", intType);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetMachineTypeForPMSettingView", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }
        public string strBranchId;
        public DataTable aGetWarrentyInfo()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[4];
                int i = 0;
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", strBranchId);
                dt = DataLayer.DataLinker.ExecuteSPDataTable("GetWarrantyInformation", parm);
            }
            catch (Exception exp)
            {
                dt = null;
                ExceptionHandler.LogError(exp);
            }
            return dt;
        }


        public string AutoUpdateofPriviousComplaint(int intCustomerComplaintsRegisterId, int intCustomerComplaintsRegisterIdP)
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[2];
                int i = 0;
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterId", intCustomerComplaintsRegisterId);
                parm[i++] = new SqlParameter("@intCustomerComplaintsRegisterIdP", intCustomerComplaintsRegisterIdP);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("AutoUpdateofPriviousComplaint", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }


        public DataTable ExporttoExceltblCustomerComplaintsRegister_CQMFilterWiseForPrevious45()
        {
            DataTable ds = new DataTable();
            try
            {
                // string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                SqlParameter[] parm = new SqlParameter[9];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                parm[i++] = new SqlParameter("@intChampionid", intChampionId);
                parm[i++] = new SqlParameter("@intApuId", intApuId);
                parm[i++] = new SqlParameter("@intProductId", intProductId);
                parm[i++] = new SqlParameter("@intCustomerId", intCustomerId);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegister_CQMFilterWiseForPrevious45", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegister_CQM_Plant_QRQC_A()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegister_CQM_Plant_QRQC_A", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegister_CQM_Plant_QRQC_A_B()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegister_CQM_Plant_QRQC_A_B", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegister_CQM_Plant_QRQC_A_BForPrevious45()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegister_CQM_Plant_QRQC_A_BForPrevious45", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegister_CQM_Plant_QRQC_AForPrevious45()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegister_CQM_Plant_QRQC_AForPrevious45", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQMForPrevious45()
        {
            DataTable ds = new DataTable();
            try
            {
                //string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQMForPrevious45", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQM_Information()
        {
            DataTable ds = new DataTable();
            try
            {
                //string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQM_Information", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQM_InformationForPrevious45()
        {
            DataTable ds = new DataTable();
            try
            {
                //string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQM_InformationForPrevious45", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQM_QRQC()
        {
            DataTable ds = new DataTable();
            try
            {
                //string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQM_QRQC", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQM_QRQCForPrevious45()
        {
            DataTable ds = new DataTable();
            try
            {
                //string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("ExporttoExceltblCustomerComplaintsRegisterForWarenty_CQM_QRQCForPrevious45", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }

        public DataTable GetExporttoExceltblCustomerComplaintsRegister_CQMInternal45Days()
        {
            DataTable ds = new DataTable();
            try
            {
                // string intBranchId = Convert.ToString(HttpContext.Current.Session["BranchID"]);
                SqlParameter[] parm = new SqlParameter[5];
                int i = 0;
                parm[i++] = new SqlParameter("@strComplainType", strComplainType);
                parm[i++] = new SqlParameter("@FromDate", FromDate);
                parm[i++] = new SqlParameter("@Todate", Todate);
                parm[i++] = new SqlParameter("@CheckBoxFlag", CheckBoxFlag);
                parm[i++] = new SqlParameter("@intBranchId", intBranchIds);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GetExporttoExceltblCustomerComplaintsRegister_CQMInternal45Days", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
                //throw;
            }
            return ds;
        }
    }
}