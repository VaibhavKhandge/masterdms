﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataLayer
{
    public class DataBusiness
    {
        // public static string sqlConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"].ToString();

        public static string sqlConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        #region [GETDS()]
        public DataSet GetDs(string SP_Name)
        {
            DataSet ds;
            try
            {
                ds = SqlHelper.ExecuteDataset(sqlConnectionString, CommandType.StoredProcedure, SP_Name);
            }
            catch (Exception)
            {
                throw;
            }
            return ds;
        }
        #endregion

        #region [GETDS()]

        public DataSet GetDs(string SP_Name, object[] param)
        {
            DataSet ds;
            try
            {
                ds = SqlHelper.ExecuteDataset(sqlConnectionString, SP_Name, param);
            }
            catch (Exception)
            {
                throw;
            }
            return ds;
        }
        #endregion

        #region [SaveQuery()]
        public int SaveQuery(string SP_Name, object[] param)
        {
            int Count;
            try
            {
                Count = SqlHelper.ExecuteNonQuery(sqlConnectionString, SP_Name, param);
            }
            catch (Exception)
            {
                throw;
            }
            return Count;
        }
        #endregion

        public static string ExecuteSPScalar(string sp_name, params SqlParameter[] cmdParams)
        {
            string sRetVal = "";
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sp_name;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = cmd;
                if (cmdParams != null)
                {
                    foreach (SqlParameter param in cmdParams)
                        cmd.Parameters.Add(param);
                }
                System.Data.DataSet ds = new DataSet();
                adapter.Fill(ds);
                cmd.Dispose();
                conn.Close();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                        sRetVal = Convert.ToString(ds.Tables[0].Rows[0][0]);
                    else
                        sRetVal = "";
                }
                else
                    sRetVal = "";
                ds.Dispose();
                return sRetVal;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }

        public static System.Data.DataTable ExecuteSPDataTable(string sp_name, params SqlParameter[] cmdParams)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sp_name;
                cmd.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = cmd;

                if (cmdParams != null)
                {
                    foreach (SqlParameter param in cmdParams)
                        cmd.Parameters.Add(param);
                }

                System.Data.DataTable ds = new DataTable();
                adapter.Fill(ds);
                cmd.Dispose();
                conn.Close();
                return ds;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }
        public static System.Data.DataTable ExecuteSPDataTable(string sp_name)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sp_name;
                cmd.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = cmd;

                System.Data.DataTable ds = new DataTable();
                adapter.Fill(ds);
                cmd.Dispose();
                conn.Close();
                return ds;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }

        public static System.Data.DataSet ExecuteSPDataSet(string sp_name, params SqlParameter[] cmdParams)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            System.Data.SqlClient.SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sp_name;
                cmd.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand = cmd;

                if (cmdParams != null)
                {
                    foreach (SqlParameter param in cmdParams)
                        cmd.Parameters.Add(param);
                }

                System.Data.DataSet ds = new DataSet();
                adapter.Fill(ds);
                cmd.Dispose();
                conn.Close();
                return ds;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }
    }
}