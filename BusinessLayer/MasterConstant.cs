﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BusinessLayer
{
   public class MasterConstant
    {
       # region [SP_GetRolewiseMenus Property]
       public string SP_GetRolewiseMenus
       {
           get
           {
               return "ssp_GetDisplayRoleWiseMenu";
           }
       }
       # endregion
    }
}
