﻿ 
        Chart.defaults.global.legend = {
            enabled: false
        };
        
        
        // Bar chart
        var aApr = "Apr";
        var aMay = "May";
        var aJune = "June";
        var aJuly = "July";
        var aAug = "Aug";
        var aSep = "Sep";
        var aOct = "Oct";
        var aNov = "Nov";
        var aDec = "Dec";
        var aJan = 'Jan';
        var aFeb = 'Feb';

     
        
        var ctx = document.getElementById("mybarChart");
        var mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
            labels: [aApr, aMay, aJune, aJuly, aAug, aSep, aOct, aNov, aDec, aJan, aFeb],
                datasets: [{
                    label: '2014-15',
                    backgroundColor: "#8e1d9a",
                    data: [30, 42, 40, 49, 53, 68, 55, 45, 49, 0, 0]

                }, {
                    label: '2015-16',
                    backgroundColor: "#03586A",
                    data: [35, 50, 48, 50, 55, 75, 60, 30, 55, 0, 0]
                },
          {
              label: '2016-17',
              backgroundColor: "#26B99A",
              data: [52, 64, 70, 62, 75, 82, 62, 58, 62, 0, 0]
          }
          ]
            },

            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
}]
                    }
                }
            });


            // Pie chart
            var ctx = document.getElementById("pieChart");
            var data = {
                datasets: [{
                data: [52, 64, 70, 62, 75, 82, 62, 58, 62, 69, 65],
                    backgroundColor: [
            "#455C73",
            "#9B59B6",
            "#BDC3C7",
            "#26B99A",
            "#3498DB",
             "#455C73",
            "#9B59B6",
            "#BDC3C7",
            "#26B99A",
            "#3498DB",
            "#3498DB"
          ],
                    label: 'My dataset' // for legend
}],
                    labels: [
       aApr, aMay, aJune, aJuly, aAug, aSep, aOct, aNov, aDec, aJan, aFeb
        ]
                };

                var pieChart = new Chart(ctx, {
                    data: data,
                    type: 'pie',
                    otpions: {
                        legend: false
                    }
                });

                 
   
       
 