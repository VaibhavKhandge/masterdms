﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace eBILL.UI
{
    public partial class frmDistributerMasterView : System.Web.UI.Page
    {
        # region [Branch_ID Property]
        protected int Branch_ID
        {
            get { return ViewState["Branch_ID"] != null ? (Int32)ViewState["Branch_ID"] : 0; }
            set { ViewState["Branch_ID"] = value; }
        }
        # endregion

        # region [Company_ID Property]
        protected int Company_ID
        {
            get { return ViewState["Company_ID"] != null ? (Int32)ViewState["Company_ID"] : 0; }
            set { ViewState["Company_ID"] = value; }
        }
        # endregion

        # region [FY_ID Property]
        protected int FY_ID
        {
            get { return ViewState["FY_ID"] != null ? (Int32)ViewState["FY_ID"] : 0; }
            set { ViewState["FY_ID"] = value; }
        }
        # endregion

        # region [Role_ID Property]
        protected int Role_ID
        {
            get { return ViewState["Role_ID"] != null ? (Int32)ViewState["Role_ID"] : 0; }
            set { ViewState["Role_ID"] = value; }
        }
        # endregion

        # region [dsHeader Property]
        public DataTable dsHeader
        {
            get { return ViewState["dsHeader"] != null ? (DataTable)ViewState["dsHeader"] : null; }
            set { ViewState["dsHeader"] = value; }
        }

        #endregion

        # region [dsExcel Property]
        public DataTable dsExcel
        {
            get { return ViewState["dsExcel"] != null ? (DataTable)ViewState["dsExcel"] : null; }
            set { ViewState["dsExcel"] = value; }
        }

        #endregion


        ManageDistributor OBJDistributor = new ManageDistributor();

        protected void Page_Load(object sender, EventArgs e)
        {
            Branch_ID = Convert.ToInt32(Session["BranchID"].ToString());
            Company_ID = Convert.ToInt32(Session["CompanyID"].ToString());
            FY_ID = Convert.ToInt32(Session["FinancialYearId"].ToString());
            Role_ID = Convert.ToInt32(Session["RoleId"].ToString());
            try
            {
                if (!IsPostBack)
                {
                    OBJDistributor.intDistributerID = 0;
                    dsHeader = OBJDistributor.GettblDistributerMaster_View();
                    GRDDist.DataSource = dsHeader;
                    GRDDist.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }

        protected void GRDDist_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            OBJDistributor.intDistributerID = 0;
            dsHeader = OBJDistributor.GettblDistributerMaster_View();
            GRDDist.DataSource = dsHeader;
            GRDDist.DataBind();
        }

        protected void Page_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string aFileName = string.Empty;
                if (e.CommandName == "btnPrint")
                {
                    StringWriter aPrintHTML = new StringWriter();
                    aPrintHTML.Write("<br>");
                    aPrintHTML.Write("<table style='width:900px'>");
                    aPrintHTML.Write("<tr><td colspan='9' style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'></td>");
                    aPrintHTML.Write("<td colspan='2' style='text-align:center;font-family:Verdana;font-weight:bold; font-size:20px;color:#000;border:1px solid #000;width:900px;'>Distributor Master View</td>");
                    aPrintHTML.Write("<td colspan='4' style='text-align:center;font-family:Verdana;font-weight:bold; font-size:20px;color:#000;border:1px solid #000;width:900px;'></td>");
                    aPrintHTML.Write("</tr>");
                    aPrintHTML.Write("</table>");
                    aPrintHTML.Write("<br>");
                    aPrintHTML.Write(" <table style='width:900px'>");
                    aPrintHTML.Write("<tr><td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Sr. No</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Code</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Distributor Name</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Company</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Adderss</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>State</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>City</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Warehouse</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>PIN</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Phone No.</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Mobile No.</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Email</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>FAX</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Close</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Close Date</td>");
                    aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000; background-color:#d4d1d1;'>Close Remark</td>");
                    aPrintHTML.Write("</tr>");

                    OBJDistributor.intDistributerID = 0;
                    dsExcel = OBJDistributor.GettblDistributerMaster_View();

                    if (dsExcel.Rows.Count > 0)
                    {
                        for (int k = 0; k < dsExcel.Rows.Count; k++)
                        {
                            aPrintHTML.Write("<tr>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["SrNO"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strDistributerCode"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strDistributerName"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strCompanyName"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strDistributerAddress"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strDistributorState"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strDistributorCity"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strDistributerWarehouse"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["intPINCode"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strDistributerPhoneNo"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strDistributerMobileNo"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strDistributerEmail"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strDistributerFAX"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strCloseFlag"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strCloseDate"].ToString() + "</td>");
                            aPrintHTML.Write("<td style='text-align:center;font-family:Verdana;font-weight:bold; font-size:12px;color:#000;border:1px solid #000;'>" + dsExcel.Rows[k]["strCloseRemark"].ToString() + "</td>");

                            aPrintHTML.Write("</tr>");
                        }
                    }
                    aPrintHTML.Write(" </table>");
                    DateTime aDate = DateTime.Now;
                    aFileName = aDate.Month + "_" + aDate.Day + "_" + aDate.Hour + "_" + aDate.Minute + "_" + aDate.Second + "_" + "DistributorMaster";

                    Response.ClearContent();
                    Response.AddHeader("content-disposition", "attachment; filename="+ aFileName +".xls");
                    Response.ContentType = "application/excel";
                    HtmlTextWriter htw = new HtmlTextWriter(aPrintHTML);
                    Response.Write(aPrintHTML.ToString());
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogError(ex);
            }
        }
    }
}