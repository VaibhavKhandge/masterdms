﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLayer
{
    public class ManageSalesExecutive
    {
        public int intSalesExecutiveID, intCloseByID, intCompanyID, intDistributerID;
        public string strSalesExecutiveName, strSalesExecutiveAddress, strSalesExecutivePhoneNo, strSalesExecutiveMobileNo,
        strSalesExecutiveEmail, strCloseFlag, strCloseDate, strCloseRemark;

        //ManagetblSalesExecutiveMaster
        public string ManagetblSalesExecutiveMaster()
        {
            string aReturnValue = string.Empty;
            try
            {
                SqlParameter[] parm = new SqlParameter[12];
                int i = 0;
                parm[i++] = new SqlParameter("@intSalesExecutiveID", intSalesExecutiveID);
                parm[i++] = new SqlParameter("@intCompanyID", intCompanyID);
                parm[i++] = new SqlParameter("@intDistributerID", intDistributerID);
                parm[i++] = new SqlParameter("@strSalesExecutiveName", strSalesExecutiveName);
                parm[i++] = new SqlParameter("@strSalesExecutiveAddress", strSalesExecutiveAddress);
                parm[i++] = new SqlParameter("@strSalesExecutivePhoneNo", strSalesExecutivePhoneNo);
                parm[i++] = new SqlParameter("@strSalesExecutiveMobileNo", strSalesExecutiveMobileNo);
                parm[i++] = new SqlParameter("@strSalesExecutiveEmail", strSalesExecutiveEmail);
                parm[i++] = new SqlParameter("@strCloseFlag", strCloseFlag);
                parm[i++] = new SqlParameter("@intCloseByID", intCloseByID);
                parm[i++] = new SqlParameter("@strCloseDate", strCloseDate);
                parm[i++] = new SqlParameter("@strCloseRemark", strCloseRemark);
                aReturnValue = DataLayer.DataLinker.ExecuteSPScalar("ManagetblSalesExecutiveMaster", parm);
            }
            catch (Exception exp)
            {
                aReturnValue = "";
                ExceptionHandler.LogError(exp);
            }
            return aReturnValue;
        }

        public DataTable GettblSalesExecutiveMaster_View()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intSalesExecutiveID", intSalesExecutiveID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblSalesExecutiveMaster_View", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }

        public DataTable GettblSalesExecutiveMaster()
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] parm = new SqlParameter[1];
                int i = 0;
                parm[i++] = new SqlParameter("@intSalesExecutiveID", intSalesExecutiveID);
                ds = DataLayer.DataLinker.ExecuteSPDataTable("GettblSalesExecutiveMaster", parm);
            }
            catch (Exception exp)
            {
                ds = null;
                ExceptionHandler.LogError(exp);
            }
            return ds;
        }
    }
}