﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;

namespace eBILL.UI.UserControls
{
    public partial class ImageButtonControl : System.Web.UI.UserControl
    {
        #region [ Constructor ]
        public ImageButtonControl()
        {
            //CreateControls();
        }
        #endregion

        # region [AllFalse Property]
        public bool AllFalse
        {
            get { return ViewState["AllFalse"] != null ? (bool)ViewState["AllFalse"] : false; }
            set { ViewState["AllFalse"] = value; }
        }
        # endregion

        # region [SaveAsFlag Property]
        public bool SaveAsFlag
        {
            get { return ViewState["SaveAsFlag"] != null ? (bool)ViewState["SaveAsFlag"] : false; }
            set { ViewState["SaveAsFlag"] = value; }
        }
        # endregion

        #region [ Private Variable Declaration ]
        Boolean _SaveBtnVisible = true;
        Boolean _SaveBtnEnabled = true;
        Int16 _SaveBtnOrder = 0;
        Int16 _SaveBtnTabIndex;
        ImageButton btnSaveImage;

        Boolean _SaveAsBtnVisible = false;
        Boolean _SaveAsBtnEnabled = false;
        Int16 _SaveAsBtnOrder = 0;
        Int16 _SaveAsBtnTabIndex;
        ImageButton btnSaveAsImage;

        Boolean _NewBtnVisible = true;
        Boolean _NewBtnEnabled = true;
        Int16 _NewBtnOrder = 0;
        Int16 _NewBtnTabIndex;
        ImageButton btnNewImage;

        Boolean _EditBtnVisible = true;
        Boolean _EditBtnEnabled = true;
        Int16 _EditBtnOrder = 0;
        Int16 _EditBtnTabIndex;
        ImageButton btnEditImage;

        Boolean _DeleteBtnVisible = true;
        Boolean _DeleteBtnEnabled = true;
        Int16 _DeleteBtnOrder = 0;
        Int16 _DeleteBtnTabIndex;
        ImageButton btnDeleteImage;

        Boolean _PrintBtnVisible = true;
        Boolean _PrintBtnEnabled = true;
        Int16 _PrintBtnOrder = 0;
        Int16 _PrintBtnTabIndex;
        ImageButton btnPrintImage;

        Boolean _ListBtnVisible = true;
        Boolean _ListBtnEnabled = true;
        Int16 _ListBtnOrder = 0;
        Int16 _ListBtnTabIndex;
        ImageButton btnListImage;

        Boolean _FilterBtnVisible = true;
        Boolean _FilterBtnEnabled = true;
        Int16 _FilterBtnOrder = 0;
        Int16 _FilterBtnTabIndex;
        ImageButton btnFilterImage;

        Boolean _HelpBtnVisible = true;
        Boolean _HelpBtnEnabled = true;
        Int16 _HelpBtnOrder = 0;
        Int16 _HelpBtnTabIndex;
        ImageButton btnHelpImage;

        Boolean _RefreshBtnVisible = true;
        Boolean _RefreshBtnEnabled = true;
        Int16 _RefreshBtnOrder = 0;
        Int16 _RefreshBtnTabIndex;
        ImageButton btnRefreshImage;

        Boolean _CloseBtnVisible = true;
        Boolean _CloseBtnEnabled = true;
        Int16 _CloseBtnOrder = 0;
        Int16 _CloseBtnTabIndex;
        ImageButton btnCloseImage;

        Boolean _ViewBtnVisible = true;
        Boolean _ViewBtnEnabled = true;
        Int16 _ViewBtnOrder = 0;
        Int16 _ViewBtnTabIndex;
        ImageButton btnViewImage;

        Boolean _EmailBtnVisible = true;
        Boolean _EmailBtnEnabled = true;
        Int16 _EmailBtnOrder = 0;
        Int16 _EmailBtnTabIndex;
        ImageButton btnEmailImage;

        Boolean _ExitBtnVisible = true;
        Boolean _ExitBtnEnabled = true;
        Int16 _ExitBtnOrder = 0;
        Int16 _ExitBtnTabIndex;
        ImageButton btnExitImage;

        Boolean _SMSBtnVisible = true;
        Boolean _SMSBtnEnabled = true;
        Int16 _SMSBtnOrder = 0;
        Int16 _SMSBtnTabIndex;
        ImageButton btnSMSImage;

        Boolean _PrintExcelBtnVisible = true;
        Boolean _PrintExcelBtnEnabled = true;
        Int16 _PrintExcelBtnOrder = 0;
        Int16 _PrintExcelBtnTabIndex;
        ImageButton btnPrintExcelImage;

        Boolean _FillDetailBtnVisible = true;
        Boolean _FillDetailBtnEnabled = true;
        Int16 _FillDetailBtnOrder = 0;
        Int16 _FillDetailBtnTabIndex;
        ImageButton btnFillDetailImage;

        Boolean _Role_Add = true;
        Boolean _Role_Modify = true;
        Boolean _Role_Delete = true;
        Boolean _Role_View = true;
        #endregion

        #region [ Delegate Event declaration ]
        public delegate void OnSaveImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnSaveAsImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnNewImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnEditImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnDeleteImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnPrintImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnListImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnFilterImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnHelpImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnRefreshImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnCloseImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnViewImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnEmailImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnSMSImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnExitImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnMenuClick(object sender, MenuEventArgs e);
        public delegate void OnPrintExcelImageButtonClick(object sender, ImageClickEventArgs e);
        public delegate void OnFillDetailImageButtonClick(object sender, ImageClickEventArgs e);
        #endregion

        #region [ Event declaration ]
        public event OnSaveImageButtonClick btnSaveImageHandler;
        public event OnSaveAsImageButtonClick btnSaveAsImageHandler;
        public event OnNewImageButtonClick btnNewImageHandler;
        public event OnEditImageButtonClick btnEditImageHandler;
        public event OnDeleteImageButtonClick btnDeleteImageHandler;
        public event OnPrintImageButtonClick btnPrintImageHandler;
        public event OnListImageButtonClick btnListImageHandler;
        public event OnFilterImageButtonClick btnFilterImageHandler;
        public event OnHelpImageButtonClick btnHelpImageHandler;
        public event OnRefreshImageButtonClick btnRefreshImageHandler;
        public event OnCloseImageButtonClick btnCloseImageHandler;
        public event OnViewImageButtonClick btnViewImageHandler;
        public event OnEmailImageButtonClick btnEmailImageHandler;
        public event OnSMSImageButtonClick btnSMSImageHandler;
        public event OnExitImageButtonClick btnExitImageHandler;
        public event OnMenuClick MnuFileMenuItemClick;
        public event OnPrintExcelImageButtonClick btnPrintExcelImageHandler;
        public event OnFillDetailImageButtonClick btnFillDetailImageHandler;
        #endregion

        #region [ PageEvents ]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                VisibleControls();
                EnabledControls();
                SetTabIndex();
                this .btnExitImage .Attributes .Add ("OnClick","self.close()");
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            CreateControls();
        }
        #endregion

        #region [ CreateControls at Run time ]
        protected void CreateControls()
        {
            Int16[] orderlist = new Int16[17];

            orderlist[0] = _SaveBtnOrder;
            orderlist[1] = _SaveAsBtnOrder;
            orderlist[2] = _NewBtnOrder;
            orderlist[3] = _EditBtnOrder;
            orderlist[4] = _DeleteBtnOrder;
            orderlist[5] = _PrintBtnOrder;
            orderlist[6] = _PrintExcelBtnOrder;
            orderlist[7] = _FillDetailBtnOrder;
            orderlist[8] = _ListBtnOrder;
            orderlist[9] = _FilterBtnOrder;
            orderlist[10] = _HelpBtnOrder;
            orderlist[11] = _RefreshBtnOrder;
            orderlist[12] = _CloseBtnOrder;
            orderlist[13] = _ViewBtnOrder;
            orderlist[14] = _EmailBtnOrder;
            orderlist[15] = _ExitBtnOrder;
            orderlist[16] = _SMSBtnOrder;

            //ImageClickEventArgs
            btnSaveImage = new ImageButton();
            btnSaveImage.SkinID = "sknCtrlPnlButton";
            btnSaveImage.ID = "btnSaveImage"; // Added for Authorize Controls in Master Pages
            btnSaveImage.Click += new ImageClickEventHandler(btnSaveImage_Click);
            //btnSaveImage.ImageUrl = "~/Images/Save.gif";
            btnSaveImage.ImageUrl = "~/Images/Save1.gif";
            btnSaveImage.OnClientClick = "validate1()";
            btnSaveImage.Width = 30;
            btnSaveImage.Height = 18;
            btnSaveImage.ToolTip = "Save";

            btnSaveAsImage = new ImageButton();
            btnSaveAsImage.SkinID = "sknCtrlPnlButton";
            btnSaveAsImage.ID = "btnSaveAsImage"; // Added for Authorize Controls in Master Pages
            btnSaveAsImage.Click += new ImageClickEventHandler(btnSaveAsImage_Click);
            btnSaveAsImage.ImageUrl = "~/Images/SaveAs.gif";
            btnSaveAsImage.OnClientClick = "validate1()";
            btnSaveAsImage.Width = 16;
            btnSaveAsImage.Height = 17;
            btnSaveAsImage.ToolTip = "Save As";

            btnNewImage = new ImageButton();
            btnNewImage.SkinID = "sknCtrlPnlButton";
            btnNewImage.ID = "btnNewImage"; // Added for Authorize Controls in Master Pages
            btnNewImage.Click += new ImageClickEventHandler(btnNewImage_Click);
            btnNewImage.ImageUrl = "~/Images/New1.gif";
            btnNewImage.Width = 30;
            btnNewImage.Height = 18;
            btnNewImage.ToolTip = "New";
            btnNewImage.CausesValidation = false;

            btnEditImage = new ImageButton();
            btnEditImage.SkinID = "sknCtrlPnlButton";
            btnEditImage.ID = "btnEditImage"; // Added for Authorize Controls in Master Pages
            btnEditImage.Click += new ImageClickEventHandler(btnEditImage_Click);
            btnEditImage.ImageUrl = "~/Images/Edit1.gif";
            btnEditImage.OnClientClick = "return confirm('Are You Sure to Edit?')";
            btnEditImage.Width = 26;
            btnEditImage.Height = 18;
            btnEditImage.ToolTip = "Edit";
            btnEditImage.CausesValidation = false;

            btnDeleteImage = new ImageButton();
            btnDeleteImage.SkinID = "sknCtrlPnlButton";
            btnDeleteImage.ID = "btnDeleteImage"; // Added for Authorize Controls in Master Pages
            btnDeleteImage.Click += new ImageClickEventHandler(btnDeleteImage_Click);
            btnDeleteImage.ImageUrl = "~/Images/Delete1.gif";
            btnDeleteImage.OnClientClick = "return confirm('Are You Sure to Delete?')";
            btnDeleteImage.Width = 26;
            btnDeleteImage.Height = 18;
            btnDeleteImage.ToolTip = "Delete";
            btnDeleteImage.CausesValidation = false;

            btnPrintImage = new ImageButton();
            btnPrintImage.SkinID = "sknCtrlPnlButton";
            btnPrintImage.ID = "btnPrintImage"; // Added for Authorize Controls in Master Pages
            btnPrintImage.Click += new ImageClickEventHandler(btnPrintImage_Click);
            btnPrintImage.ImageUrl = "~/Images/print1.gif";
            btnPrintImage.Width = 26;
            btnPrintImage.Height = 18;
            btnPrintImage.ToolTip = "Print";
            btnPrintImage.CausesValidation = false;

            btnPrintExcelImage = new ImageButton();
            btnPrintExcelImage.SkinID = "sknCtrlPnlButton";
            btnPrintExcelImage.ID = "btnPrintExcelImage"; // Added for Authorize Controls in Master Pages
            btnPrintExcelImage.Click += new ImageClickEventHandler(btnPrintExcelImage_Click);
            btnPrintExcelImage.ImageUrl = "~/Images/Excel-icon1.png";
            btnPrintExcelImage.Width = 22;
            btnPrintExcelImage.Height = 18;
            btnPrintExcelImage.ToolTip = "PrintExcel";
            btnPrintExcelImage.CausesValidation = false;

            btnFillDetailImage = new ImageButton();
            btnFillDetailImage.SkinID = "sknCtrlPnlButton";
            btnFillDetailImage.ID = "btnFillDetailImage"; // Added for Authorize Controls in Master Pages
            btnFillDetailImage.Click += new ImageClickEventHandler(btnFillDetailImage_Click);
            btnFillDetailImage.ImageUrl = "~/Images/PendingDataView.gif";
            btnFillDetailImage.Width = 22;
            btnFillDetailImage.Height = 18;
            btnFillDetailImage.ToolTip = "View Pending Data";
            btnFillDetailImage.CausesValidation = false;

            btnListImage = new ImageButton();
            btnListImage.SkinID = "sknCtrlPnlButton";
            btnListImage.ID = "btnListImage"; // Added for Authorize Controls in Master Pages
            btnListImage.Click += new ImageClickEventHandler(btnListImage_Click);
            btnListImage.ImageUrl = "~/Images/ListView1.gif";
            btnListImage.OnClientClick = "validate1()";
            btnListImage.Width = 26;
            btnListImage.Height = 18;
            btnListImage.ToolTip = "List";
            btnListImage.CausesValidation = false;

            btnFilterImage = new ImageButton();
            btnFilterImage.SkinID = "sknCtrlPnlButton";
            btnFilterImage.ID = "btnFilterImage"; // Added for Authorize Controls in Master Pages
            btnFilterImage.Click += new ImageClickEventHandler(btnFilterImage_Click);
            btnFilterImage.ImageUrl = "~/Images/Search.gif";
            btnFilterImage.OnClientClick = "validate1()";
            btnFilterImage.Width = 24;
            btnFilterImage.Height = 18;
            btnFilterImage.ToolTip = "Filter";
            btnFilterImage.CausesValidation = false;

            btnHelpImage = new ImageButton();
            btnHelpImage.SkinID = "sknCtrlPnlButton";
            btnHelpImage.ID = "btnHelpImage"; // Added for Authorize Controls in Master Pages
            btnHelpImage.Click += new ImageClickEventHandler(btnHelpImage_Click);
            btnHelpImage.ImageUrl = "~/Images/HelpMenu1.gif";
            btnHelpImage.Width = 26;
            btnHelpImage.Height = 18;
            btnHelpImage.ToolTip = "Help";
            btnHelpImage.CausesValidation = false;

            btnRefreshImage = new ImageButton();
            btnRefreshImage.SkinID = "sknCtrlPnlButton";
            btnRefreshImage.ID = "btnRefreshImage"; // Added for Authorize Controls in Master Pages
            btnRefreshImage.Click += new ImageClickEventHandler(btnRefreshImage_Click);
            btnRefreshImage.ImageUrl = "~/Images/Refresh1.gif";
            btnRefreshImage.Width = 26;
            btnRefreshImage.Height = 18;
            btnRefreshImage.ToolTip = "Refresh";
            btnRefreshImage.CausesValidation = false;

            btnCloseImage = new ImageButton();
            btnCloseImage.SkinID = "sknCtrlPnlButton";
            btnCloseImage.ID = "btnClose"; // Added for Authorize Controls in Master Pages
            btnCloseImage.Click += new ImageClickEventHandler(btnCloseImage_Click);
            btnCloseImage.ImageUrl = "~/Images/Close1.gif";
            btnCloseImage.Width = 26;
            btnCloseImage.Height = 18;
            btnCloseImage.ToolTip = "Cancel";
            btnCloseImage.CausesValidation = false;

            btnViewImage = new ImageButton();
            btnViewImage.SkinID = "sknCtrlPnlButton";
            btnViewImage.ID = "btnView"; // Added for Authorize Controls in Master Pages
            btnViewImage.Click += new ImageClickEventHandler(btnViewImage_Click);
            btnViewImage.ImageUrl = "~/Images/Gap.GIF";
            btnViewImage.Width = 26;
            btnViewImage.Height = 18;
            btnViewImage.ToolTip = "View";
            btnViewImage.CausesValidation = false;

            btnEmailImage = new ImageButton();
            btnEmailImage.SkinID = "sknCtrlPnlButton";
            btnEmailImage.ID = "btnEmail"; // Added for Authorize Controls in Master Pages
            btnEmailImage.Click += new ImageClickEventHandler(btnEmailImage_Click);
            btnEmailImage.ImageUrl = "~/Images/Email1.gif";
            btnEmailImage.OnClientClick = "validate1()";
            btnEmailImage.Width = 26;
            btnEmailImage.Height = 18;
            btnEmailImage.ToolTip = "E-Mail";
            btnEmailImage.CausesValidation = false;

            btnSMSImage = new ImageButton();
            btnSMSImage.SkinID = "sknCtrlPnlButton";
            btnSMSImage.ID = "btnSMS"; // Added for Authorize Controls in Master Pages
            btnSMSImage.Click += new ImageClickEventHandler(btnSMSImage_Click);
            btnSMSImage.ImageUrl = "~/Images/SMS.gif";
            btnSMSImage.OnClientClick = "validate1()";
            btnSMSImage.Width = 26;
            btnSMSImage.Height = 18;
            btnSMSImage.ToolTip = "SMS";
            btnSMSImage.CausesValidation = false;

            btnExitImage = new ImageButton();
            btnExitImage.SkinID = "sknCtrlPnlButton";
            btnExitImage.ID = "btnExit"; // Added for Authorize Controls in Master Pages
            btnExitImage.Click += new ImageClickEventHandler(btnExitImage_Click);
            btnExitImage.ImageUrl = "~/Images/Exit.gif";
            btnExitImage.Width = 26;
            btnExitImage.Height = 18;
            btnExitImage.ToolTip = "Close";
            btnExitImage.CausesValidation = false;


            for (int j = 0; j <= 14; j++)
            {
                for (int i = 0; i < orderlist.Length; i++)
                {
                    if (orderlist[i] == j)
                    {
                        if (i == 0)
                            PHOptions.Controls.Add(btnSaveImage);
                        if (i == 1)
                            PHOptions.Controls.Add(btnSaveAsImage);
                        if (i == 2)
                            PHOptions.Controls.Add(btnNewImage);
                        if (i == 3)
                            PHOptions.Controls.Add(btnEditImage);
                        if (i == 4)
                            PHOptions.Controls.Add(btnDeleteImage);
                        if (i == 5)
                            PHOptions.Controls.Add(btnPrintImage);
                        if (i == 6)
                            PHOptions.Controls.Add(btnPrintExcelImage);
                        if (i == 7)
                            PHOptions.Controls.Add(btnFillDetailImage);
                        if (i == 8)
                            PHOptions.Controls.Add(btnListImage);
                        if (i == 9)
                            PHOptions.Controls.Add(btnFilterImage);
                        if (i == 10)
                            PHOptions.Controls.Add(btnHelpImage);
                        if (i == 11)
                            PHOptions.Controls.Add(btnRefreshImage);
                        if (i == 12)
                            PHOptions.Controls.Add(btnCloseImage);
                        if (i == 13)
                            PHOptions.Controls.Add(btnViewImage);
                        if (i == 14)
                            PHOptions.Controls.Add(btnEmailImage);
                        if (i == 15)
                            PHOptions.Controls.Add(btnExitImage);
                        if (i == 14)
                            PHOptions.Controls.Add(btnSMSImage);
                    }
                }
            }
        }
        #endregion

        #region [ VisibleControls ]
        private void VisibleControls()
        {
            if (_SaveBtnVisible == false)
                btnSaveImage.Visible = false;
            else
                btnSaveImage.Visible = true;

            if (_SaveAsBtnVisible == false)
                btnSaveAsImage.Visible = false;
            else
                btnSaveAsImage.Visible = true;

            if (_NewBtnVisible == false)
                btnNewImage.Visible = false;
            else
                btnNewImage.Visible = true;

            if (_EditBtnVisible == false)
                btnEditImage.Visible = false;
            else
                btnEditImage.Visible = true;

            if (_DeleteBtnVisible == false)
                btnDeleteImage.Visible = false;
            else
                btnDeleteImage.Visible = true;

            if (_PrintBtnVisible == false)
                btnPrintImage.Visible = false;
            else
                btnPrintImage.Visible = true;

            if (_ListBtnVisible == false)
            {
                btnListImage.Visible = false;
                btnFilterImage.Visible = true;
            }
            else
            {
                btnListImage.Visible = true;
                btnFilterImage.Visible = false;
            }

            if (_HelpBtnVisible == false)
                btnHelpImage.Visible = false;
            else
                btnHelpImage.Visible = true;

            if (_RefreshBtnVisible == false)
                btnRefreshImage.Visible = false;
            else
                btnRefreshImage.Visible = true;

            if (_CloseBtnVisible == false)
                btnCloseImage.Visible = false;
            else
                btnCloseImage.Visible = true;

            if (_ViewBtnVisible == false)
                btnViewImage.Visible = false;
            else
                btnViewImage.Visible = true;

            if (_EmailBtnVisible == false)
                btnEmailImage.Visible = false;
            else
                btnEmailImage.Visible = true;

            if (_SMSBtnVisible == false)
                btnSMSImage.Visible = false;
            else
                btnSMSImage.Visible = false;

            if (_ExitBtnVisible == false)
                btnExitImage.Visible = false;
            else
                btnExitImage.Visible = true;

            if (_PrintExcelBtnVisible == false)
                btnPrintExcelImage.Visible = false;
            else
                btnPrintExcelImage.Visible = true;

            if (_FillDetailBtnVisible == false)
                btnFillDetailImage.Visible = false;
            else
                btnFillDetailImage.Visible = true;

            if (_ViewBtnVisible == false && _ListBtnVisible == false)
            {
                btnFilterImage.Visible =false;
            }
            else
            {
                btnFilterImage.Visible = true;
            }


        }
        #endregion

        #region [ Set Tab Index ]
        private void SetTabIndex()
        {
            btnSaveImage.TabIndex = _SaveBtnTabIndex;
            btnSaveAsImage.TabIndex = _SaveAsBtnTabIndex;
            btnNewImage.TabIndex = _NewBtnTabIndex;
            btnEditImage.TabIndex = _EditBtnTabIndex;
            btnDeleteImage.TabIndex = _DeleteBtnTabIndex;
            btnPrintImage.TabIndex = _PrintBtnTabIndex;
            btnPrintExcelImage.TabIndex = _PrintExcelBtnTabIndex;
            btnFillDetailImage.TabIndex = _FillDetailBtnTabIndex;
            btnListImage.TabIndex = _ListBtnTabIndex;
            btnFilterImage.TabIndex = _FilterBtnTabIndex;
            btnHelpImage.TabIndex = _HelpBtnTabIndex;
            btnRefreshImage.TabIndex = _RefreshBtnTabIndex;
            btnCloseImage.TabIndex = _CloseBtnTabIndex;
            btnViewImage.TabIndex = _ViewBtnTabIndex;
            btnEmailImage.TabIndex = _EmailBtnTabIndex;
            btnExitImage.TabIndex = _ExitBtnTabIndex;
            btnSMSImage.TabIndex = _SMSBtnTabIndex;

        }
        #endregion

        #region [ EnabledControls ]
        private void EnabledControls()
        {
            if (_SaveBtnEnabled == false)
                btnSaveImage.Enabled = false;
            else
                btnSaveImage.Enabled = true;

            if (_SaveAsBtnEnabled == false)
                btnSaveAsImage.Enabled = false;
            else
                btnSaveAsImage.Enabled = true;

            if (_NewBtnEnabled == false)
                btnNewImage.Enabled = false;
            else
                btnNewImage.Enabled = true;

            if (_EditBtnEnabled == false)
                btnEditImage.Enabled = false;
            else
                btnEditImage.Enabled = true;

            if (_DeleteBtnEnabled == false)
                btnDeleteImage.Enabled = false;
            else
                btnDeleteImage.Enabled = true;

            if (_PrintBtnEnabled == false)
                btnPrintImage.Enabled = false;
            else
                btnPrintImage.Enabled = true;

            if (_ListBtnEnabled == false)
            {
                btnListImage.Enabled = false;
                btnFilterImage.Enabled = true;
            }
            else
            {
                btnListImage.Enabled = true;
                btnFilterImage.Enabled = false;
            }

            if (_HelpBtnEnabled == false)
                btnHelpImage.Enabled = false;
            else
                btnHelpImage.Enabled = true;

            if (_RefreshBtnEnabled == false)
                btnRefreshImage.Enabled = false;
            else
                btnRefreshImage.Enabled = true;

            if (_CloseBtnEnabled == false)
                btnCloseImage.Enabled = false;
            else
                btnCloseImage.Enabled = true;

            if (_ViewBtnEnabled == false)
                btnViewImage.Enabled = false;
            else
                btnViewImage.Enabled = true;

            if (_EmailBtnEnabled == false)
                btnEmailImage.Enabled = false;
            else
                btnEmailImage.Enabled = true;

            if (_ExitBtnEnabled == false)
                btnExitImage.Enabled = false;
            else
                btnExitImage.Enabled = true;

            if (_SMSBtnEnabled == false)
                btnSMSImage.Enabled = false;
            else
                btnSMSImage.Enabled = true;

            if (_PrintExcelBtnEnabled == false)
                btnPrintExcelImage.Enabled = false;
            else
                btnPrintExcelImage.Enabled = true;

            if (_FillDetailBtnEnabled == false)
                btnFillDetailImage.Enabled = false;
            else
                btnFillDetailImage.Enabled = true;

            btnFilterImage.Enabled = true;
        }
        #endregion

        #region [ VisibleProperties ]
        public Boolean SaveBtnVisible
        {
            get
            {
                return _SaveBtnVisible;
            }
            set
            {
                _SaveBtnVisible = value;
                if (btnSaveImage != null)
                    btnSaveImage.Visible = value;
            }
        }
        public Boolean SaveAsBtnVisible
        {
            get
            {
                return _SaveAsBtnVisible;
            }
            set
            {
                //_SaveAsBtnVisible = value;
                if (btnSaveAsImage != null && _SaveAsBtnVisible == true)
                    btnSaveAsImage.Visible = value;
                else
                    if (btnSaveAsImage != null)
                        btnSaveAsImage.Visible = false;
            }
        }
        public Boolean NewBtnVisible
        {
            get
            {
                return _NewBtnVisible;
            }
            set
            {
                _NewBtnVisible = value;
                if (btnNewImage != null)
                    btnNewImage.Visible = value;
            }
        }
        public Boolean EditBtnVisible
        {
            get
            {
                return _EditBtnVisible;
            }
            set
            {
                _EditBtnVisible = value;
                if (btnEditImage != null)
                    btnEditImage.Visible = value;
            }
        }
        public Boolean DeleteBtnVisible
        {
            get
            {
                return _DeleteBtnVisible;
            }
            set
            {
                _DeleteBtnVisible = value;
                if (btnDeleteImage != null)
                    btnDeleteImage.Visible = value;
            }
        }
        public Boolean PrintBtnVisible
        {
            get
            {
                return _PrintBtnVisible;
            }
            set
            {
                _PrintBtnVisible = value;
                if (btnPrintImage != null)
                    btnPrintImage.Visible = value;
            }
        }
        public Boolean ListBtnVisible
        {
            get
            {
                return _ListBtnVisible;
            }
            set
            {
                _ListBtnVisible = value;
                if (btnListImage != null)
                    btnListImage.Visible = value;
            }
        }
        public Boolean FilterBtnVisible
        {
            get
            {
                return _FilterBtnVisible;
            }
            set
            {
                _FilterBtnVisible = value;
                if (btnFilterImage != null)
                    btnFilterImage.Visible = value;
            }
        }
        public Boolean HelpBtnVisible
        {
            get
            {
                return _HelpBtnVisible;
            }
            set
            {
                _HelpBtnVisible = value;
                if (btnHelpImage != null)
                    btnHelpImage.Visible = value;
            }
        }
        public Boolean RefreshBtnVisible
        {
            get
            {
                return _RefreshBtnVisible;
            }
            set
            {
                _RefreshBtnVisible = value;
                if (btnRefreshImage != null)
                    btnRefreshImage.Visible = value;
            }
        }
        public Boolean CloseBtnVisible
        {
            get
            {
                return _CloseBtnVisible;
            }
            set
            {
                _CloseBtnVisible = value;
                if (btnCloseImage != null)
                    btnCloseImage.Visible = value;
            }
        }
        public Boolean ViewBtnVisible
        {
            get
            {
                return _ViewBtnVisible;
            }
            set
            {
                _ViewBtnVisible = value;
                if (btnViewImage != null)
                    btnViewImage.Visible = value;
            }
        }

        public Boolean EmailBtnVisible
        {
            get
            {
                return _EmailBtnVisible;
            }
            set
            {
                _EmailBtnVisible = value;
                if (btnEmailImage != null)
                    btnEmailImage.Visible = value;
            }
        }

        public Boolean ExitBtnVisible
        {
            get
            {
                return _ExitBtnVisible;
            }
            set
            {
                _ExitBtnVisible = value;
                if (btnExitImage != null)
                    btnExitImage.Visible = value;
            }
        }
        public Boolean SMSBtnVisible
        {
            get
            {
                return _SMSBtnVisible;
            }
            set
            {
                _SMSBtnVisible = value;
                if (btnSMSImage != null)
                    btnSMSImage.Visible = value;
            }
        }

        public Boolean PrintExcelBtnVisible
        {
            get
            {
                return _PrintExcelBtnVisible;
            }
            set
            {
                _PrintExcelBtnVisible = value;
                if (btnPrintExcelImage != null)
                    btnPrintExcelImage.Visible = value;
            }
        }

        public Boolean FillDetailBtnVisible
        {
            get
            {
                return _FillDetailBtnVisible;
            }
            set
            {
                _FillDetailBtnVisible = value;
                if (btnFillDetailImage != null)
                    btnFillDetailImage.Visible = value;
            }
        }
        #endregion

        #region [ EnableProperties ]
        public Boolean SaveBtnEnabled
        {
            get
            {
                return _SaveBtnEnabled;
            }
            set
            {
                _SaveBtnEnabled = value;
                if (btnSaveImage != null)
                    btnSaveImage.Enabled = value;
            }
        }
        public Boolean SaveAsBtnEnabled
        {
            get
            {
                return _SaveAsBtnEnabled;
            }
            set
            {
                _SaveAsBtnEnabled = value;
                if (btnSaveAsImage != null)
                    btnSaveAsImage.Enabled = value;
            }
        }
        public Boolean NewBtnEnabled
        {
            get
            {
                return _NewBtnEnabled;
            }
            set
            {
                _NewBtnEnabled = value;
                if (btnNewImage != null)
                    btnNewImage.Enabled = value;
            }
        }
        public Boolean EditBtnEnabled
        {
            get
            {
                return _EditBtnEnabled;
            }
            set
            {
                _EditBtnEnabled = value;
                if (btnEditImage != null)
                    btnEditImage.Enabled = value;
            }
        }
        public Boolean DeleteBtnEnabled
        {
            get
            {
                return _DeleteBtnEnabled;
            }
            set
            {
                _DeleteBtnEnabled = value;
                if (btnDeleteImage != null)
                    btnDeleteImage.Enabled = value;
            }
        }
        public Boolean PrintBtnEnabled
        {
            get
            {
                return _PrintBtnEnabled;
            }
            set
            {
                _PrintBtnEnabled = value;
                if (btnPrintImage != null)
                    btnPrintImage.Enabled = value;
            }
        }
        public Boolean Print2BtnEnabled
        {
            get
            {
                return _PrintExcelBtnEnabled;
            }
            set
            {
                _PrintExcelBtnEnabled = value;
                if (btnPrintExcelImage != null)
                    btnPrintExcelImage.Enabled = value;
            }
        }
        public Boolean Print3BtnEnabled
        {
            get
            {
                return _FillDetailBtnEnabled;
            }
            set
            {
                _FillDetailBtnEnabled = value;
                if (btnFillDetailImage != null)
                    btnFillDetailImage.Enabled = value;
            }
        }

        public Boolean ListBtnEnabled
        {
            get
            {
                return _ListBtnEnabled;
            }
            set
            {
                _ListBtnEnabled = value;
                if (btnListImage != null)
                    btnListImage.Enabled = value;
            }
        }
        public Boolean FilterBtnEnabled
        {
            get
            {
                return _FilterBtnEnabled;
            }
            set
            {
                _FilterBtnEnabled = value;
                if (btnFilterImage != null)
                    btnFilterImage.Enabled = value;
            }
        }
        public Boolean HelpBtnEnabled
        {
            get
            {
                return _HelpBtnEnabled;
            }
            set
            {
                _HelpBtnEnabled = value;
                if (btnHelpImage != null)
                    btnHelpImage.Enabled = value;
            }
        }
        public Boolean RefreshBtnEnabled
        {
            get
            {
                return _RefreshBtnEnabled;
            }
            set
            {
                _RefreshBtnEnabled = value;
                if (btnRefreshImage != null)
                    btnRefreshImage.Enabled = value;
            }
        }
        public Boolean CloseBtnEnabled
        {
            get
            {
                return _CloseBtnEnabled;
            }
            set
            {
                _CloseBtnEnabled = value;
                if (btnCloseImage != null)
                    btnCloseImage.Enabled = value;
            }
        }
        public Boolean ViewBtnEnabled
        {
            get
            {
                return _ViewBtnEnabled;
            }
            set
            {
                _ViewBtnEnabled = value;
                if (btnViewImage != null)
                    btnViewImage.Enabled = value;
            }
        }

        public Boolean EmailBtnEnabled
        {
            get
            {
                return _EmailBtnEnabled;
            }
            set
            {
                _EmailBtnEnabled = value;
                if (btnEmailImage != null)
                    btnEmailImage.Enabled = value;
            }
        }
        public Boolean SMSBtnEnabled
        {
            get
            {
                return _SMSBtnEnabled;
            }
            set
            {
                _SMSBtnEnabled = value;
                if (btnSMSImage != null)
                    btnSMSImage.Enabled = value;
            }
        }
        public Boolean ExitBtnEnabled
        {
            get
            {
                return _ExitBtnEnabled;
            }
            set
            {
                _ExitBtnEnabled = value;
                if (btnExitImage != null)
                    btnExitImage.Enabled = value;
            }
        }
        #endregion

        #region [ OrderProperties ]
        public Int16 SaveBtnOrder
        {
            get { return _SaveBtnOrder; }
            set { _SaveBtnOrder = value; }
        }
        public Int16 SaveAsBtnOrder
        {
            get { return _SaveAsBtnOrder; }
            set { _SaveAsBtnOrder = value; }
        }
        public Int16 NewBtnOrder
        {
            get { return _NewBtnOrder; }
            set { _NewBtnOrder = value; }
        }
        public Int16 EditBtnOrder
        {
            get { return _EditBtnOrder; }
            set { _EditBtnOrder = value; }
        }
        public Int16 DeleteBtnOrder
        {
            get { return _DeleteBtnOrder; }
            set { _DeleteBtnOrder = value; }
        }
        public Int16 PrintBtnOrder
        {
            get { return _PrintBtnOrder; }
            set { _PrintBtnOrder = value; }
        }
        public Int16 ListBtnOrder
        {
            get { return _ListBtnOrder; }
            set { _ListBtnOrder = value; }
        }
        public Int16 FilterBtnOrder
        {
            get { return _FilterBtnOrder; }
            set { _FilterBtnOrder = value; }
        }
        public Int16 HelpBtnOrder
        {
            get { return _HelpBtnOrder; }
            set { _HelpBtnOrder = value; }
        }
        public Int16 RefreshBtnOrder
        {
            get { return _RefreshBtnOrder; }
            set { _RefreshBtnOrder = value; }
        }
        public Int16 CloseBtnOrder
        {
            get { return _CloseBtnOrder; }
            set { _CloseBtnOrder = value; }
        }
        public Int16 ViewBtnOrder
        {
            get { return _ViewBtnOrder; }
            set { _ViewBtnOrder = value; }
        }

        public Int16 EmailBtnOrder
        {
            get { return EmailBtnOrder; }
            set { _EmailBtnOrder = value; }
        }

        public Int16 ExitBtnOrder
        {
            get { return ExitBtnOrder; }
            set { _ExitBtnOrder = value; }
        }

        public Int16 SMSBtnOrder
        {
            get { return SMSBtnOrder; }
            set { _SMSBtnOrder = value; }
        }
        #endregion

        #region [ Set Visible Properties ]
        public void SetVisibleProperties(bool True)
        {
            AllFalse = True;
            MnuFile.Visible = false;
            SaveAsFlag = false;
            NewBtnVisible = false;
            //MnuFile.Items[0].ChildItems[0].Enabled = false;

            NewBtnVisible = false;
            //MnuFile.Items[0].ChildItems[0].Enabled = false;

            EditBtnVisible = false;
            //MnuFile.Items[0].ChildItems[1].Enabled = false;

            EditBtnVisible = false;
            //MnuFile.Items[0].ChildItems[1].Enabled = false;
            DeleteBtnVisible = false;
            //MnuFile.Items[0].ChildItems[2].Enabled = false;

            DeleteBtnVisible = false;
            //MnuFile.Items[0].ChildItems[2].Enabled = false;

            SaveBtnVisible = false;
            //MnuFile.Items[0].ChildItems[3].Enabled = false;
            //MnuFile.Items[0].ChildItems[4].Enabled = false;

            RefreshBtnVisible = true;
            //MnuFile.Items[0].ChildItems[10].Enabled = false;
            PrintBtnVisible = true;
            //MnuFile.Items[0].ChildItems[8].Enabled = false;
            CloseBtnVisible = false;
            //MnuFile.Items[0].ChildItems[5].Enabled = false;
            HelpBtnVisible = false;
            //MnuFile.Items[0].ChildItems[11].Enabled = false;
            ListBtnVisible = false;
            //MnuFile.Items[0].ChildItems[6].Enabled = false;
            ListBtnVisible = false;
            //MnuFile.Items[0].ChildItems[6].Enabled = false;
            //MnuFile.Items[0].ChildItems[7].Enabled = false;
            btnFilterImage.Visible = false;
            EmailBtnVisible = false;
            //MnuFile.Items[0].ChildItems[9].Enabled = false;
            ExitBtnVisible = false;
            //MnuFile.Items[0].ChildItems[12].Enabled = true;
            btnSMSImage.Visible = false;
            //modified by Rupesh 
            btnFilterImage.Visible = true;
            btnFilterImage.Enabled = true;
            //MnuFile.Items[0].ChildItems[7].Enabled = false;
        }
        #endregion

        #region [ Set Visible Properties ]
        public void SetVisibleProperties(bool _New, bool _Edit, bool _Delete, bool _Save, bool _Refresh, bool _Print, bool _Close, bool _Help, bool _List, bool _Email)
        {
            SaveAsFlag = false;
            if (_Role_Add == true)
            {
                NewBtnVisible = _New;
                MnuFile.Items[0].ChildItems[0].Enabled = _New;
            }
            else
            {
                NewBtnVisible = false;
                MnuFile.Items[0].ChildItems[0].Enabled = false;
            }

            if (_Role_Modify == true)
            {
                EditBtnVisible = _Edit;
                MnuFile.Items[0].ChildItems[1].Enabled = _Edit;
            }
            else
            {
                EditBtnVisible = false;
                MnuFile.Items[0].ChildItems[1].Enabled = false;
            }

            if (_Role_Delete == true)
            {
                DeleteBtnVisible = _Delete;
                MnuFile.Items[0].ChildItems[2].Enabled = _Delete;
            }
            else
            {
                DeleteBtnVisible = false;
                MnuFile.Items[0].ChildItems[2].Enabled = false;
            }

            SaveBtnVisible = _Save;
            MnuFile.Items[0].ChildItems[3].Enabled = _Save;
            MnuFile.Items[0].ChildItems[4].Enabled = false;

            RefreshBtnVisible = _Refresh;
            MnuFile.Items[0].ChildItems[10].Enabled = _Refresh;
            PrintBtnVisible = _Print;
            MnuFile.Items[0].ChildItems[8].Enabled = _Print;
            CloseBtnVisible = _Close;
            MnuFile.Items[0].ChildItems[5].Enabled = _Close;
            HelpBtnVisible = _Help;
            MnuFile.Items[0].ChildItems[11].Enabled = _Help;
            if (_Role_View == true)
            {
                ListBtnVisible = _List;
                MnuFile.Items[0].ChildItems[6].Enabled = _List;
                btnFilterImage.Visible = true;
                btnFilterImage.Enabled = true;
                MnuFile.Items[0].ChildItems[7].Enabled = true;
            }
            else
            {
                ListBtnVisible = false;
                MnuFile.Items[0].ChildItems[6].Enabled = false;
                btnFilterImage.Visible = false;
                btnFilterImage.Enabled = false;
                MnuFile.Items[0].ChildItems[7].Enabled = false;
            }
            MnuFile.Items[0].ChildItems[7].Enabled = false;
            btnFilterImage.Visible = false;

            EmailBtnVisible = _Email;
            MnuFile.Items[0].ChildItems[9].Enabled = _Email;

            ExitBtnVisible = true;
            MnuFile.Items[0].ChildItems[12].Enabled = true;

            if (_Edit)
                btnSMSImage.Visible = true;
            else
                btnSMSImage.Visible = false;


            //modified by Rupesh 
            btnFilterImage.Visible = true;
            btnFilterImage.Enabled = true;
            MnuFile.Items[0].ChildItems[7].Enabled = true;

            // Added By Pritesh on 9 August 2014
            MnuFile.Items[0].ChildItems[14].Enabled = false;
            PrintExcelBtnVisible = false;
            MnuFile.Items[0].ChildItems[15].Enabled = false;
            FillDetailBtnVisible = false;
        }
        #endregion
        #region [ Set Visible Properties ]

        public void SetVisibleProperties(bool _New, bool _Edit, bool _Delete, bool _Save, bool _Refresh, bool _Print, bool _Close, bool _Help, bool _List, bool _Email, bool _Exit, string str)
        {
            SaveAsFlag = false;
            if (_Role_Add == true)
            {
                NewBtnVisible = _New;
                MnuFile.Items[0].ChildItems[0].Enabled = _New;
            }
            else
            {
                NewBtnVisible = false;
                MnuFile.Items[0].ChildItems[0].Enabled = false;
            }

            if (_Role_Modify == true)
            {
                EditBtnVisible = _Edit;
                MnuFile.Items[0].ChildItems[1].Enabled = _Edit;
            }
            else
            {
                EditBtnVisible = false;
                MnuFile.Items[0].ChildItems[1].Enabled = false;
            }

            if (_Role_Delete == true)
            {
                DeleteBtnVisible = _Delete;
                MnuFile.Items[0].ChildItems[2].Enabled = _Delete;
            }
            else
            {
                DeleteBtnVisible = false;
                MnuFile.Items[0].ChildItems[2].Enabled = false;
            }

            SaveBtnVisible = _Save;
            MnuFile.Items[0].ChildItems[3].Enabled = _Save;
            MnuFile.Items[0].ChildItems[4].Enabled = false;

            RefreshBtnVisible = _Refresh;
            MnuFile.Items[0].ChildItems[10].Enabled = _Refresh;
            PrintBtnVisible = _Print;
            MnuFile.Items[0].ChildItems[8].Enabled = _Print;
            CloseBtnVisible = _Close;
            MnuFile.Items[0].ChildItems[5].Enabled = _Close;
            HelpBtnVisible = _Help;
            MnuFile.Items[0].ChildItems[11].Enabled = _Help;
            if (_Role_View == true)
            {
                ListBtnVisible = _List;
                MnuFile.Items[0].ChildItems[6].Enabled = _List;
                btnFilterImage.Visible = true;
                btnFilterImage.Enabled = true;
                MnuFile.Items[0].ChildItems[7].Enabled = true;
            }
            else
            {
                ListBtnVisible = false;
                MnuFile.Items[0].ChildItems[6].Enabled = false;
                btnFilterImage.Visible = false;
                btnFilterImage.Enabled = false;
                MnuFile.Items[0].ChildItems[7].Enabled = false;
            }
           MnuFile.Items[0].ChildItems[7].Enabled = false;
            btnFilterImage.Visible = false;

            EmailBtnVisible = _Email;
            MnuFile.Items[0].ChildItems[9].Enabled = _Email;

            ExitBtnVisible = _Exit;
            MnuFile.Items[0].ChildItems[12].Enabled = _Exit;

            if (_Edit)
                btnSMSImage.Visible = true;
            else
                btnSMSImage.Visible = false;


            //modified by Rupesh 
           btnFilterImage.Visible = true;
            btnFilterImage.Enabled = true;
            MnuFile.Items[0].ChildItems[7].Enabled = true;

            // Added By Pritesh on 9 August 2014
            MnuFile.Items[0].ChildItems[14].Enabled = false;
            PrintExcelBtnVisible = false;
            MnuFile.Items[0].ChildItems[15].Enabled = false;
            FillDetailBtnVisible = false;
        }
        #endregion
                     
        #region [ Set Visible Properties ]
        public void SetVisibleProperties(bool _New, bool _Edit, bool _Delete, bool _Save, bool _Refresh, bool _Print, bool _Close, bool _Help, bool _List, bool _Email, bool _SaveAs)
        {
            SaveAsFlag = true;
            if (_Role_Add == true)
            {
                NewBtnVisible = _New;
                MnuFile.Items[0].ChildItems[0].Enabled = _New;
            }
            else
            {
                NewBtnVisible = false;
                MnuFile.Items[0].ChildItems[0].Enabled = false;
            }

            if (_Role_Modify == true)
            {
                EditBtnVisible = _Edit;
                MnuFile.Items[0].ChildItems[1].Enabled = _Edit;
            }
            else
            {
                EditBtnVisible = false;
                MnuFile.Items[0].ChildItems[1].Enabled = false;
            }

            if (_Role_Delete == true)
            {
                DeleteBtnVisible = _Delete;
                MnuFile.Items[0].ChildItems[2].Enabled = _Delete;
            }
            else
            {
                DeleteBtnVisible = false;
                MnuFile.Items[0].ChildItems[2].Enabled = false;
            }
            MnuFile.Items[0].ChildItems[7].Enabled = false;
            btnFilterImage.Visible = false;

            SaveBtnVisible = _Save;
            MnuFile.Items[0].ChildItems[3].Enabled = _Save;

            RefreshBtnVisible = _Refresh;
            MnuFile.Items[0].ChildItems[10].Enabled = _Refresh;
            PrintBtnVisible = _Print;
            MnuFile.Items[0].ChildItems[8].Enabled = _Print;
            CloseBtnVisible = _Close;
            MnuFile.Items[0].ChildItems[5].Enabled = _Close;
            HelpBtnVisible = _Help;
            MnuFile.Items[0].ChildItems[11].Enabled = _Help;
            if (_Role_View == true)
            {
                ListBtnVisible = _List;
                MnuFile.Items[0].ChildItems[6].Enabled = _List;
                btnFilterImage.Visible = true;
                btnFilterImage.Enabled = true;
                MnuFile.Items[0].ChildItems[7].Enabled = true;

            }
            else
            {
                ListBtnVisible = false;
                MnuFile.Items[0].ChildItems[6].Enabled = false;
                btnFilterImage.Visible = false;
                btnFilterImage.Enabled = false;
                MnuFile.Items[0].ChildItems[7].Enabled = false;

            }

            EmailBtnVisible = _Email;
            MnuFile.Items[0].ChildItems[9].Enabled = _Email;

            ExitBtnVisible = true;
            MnuFile.Items[0].ChildItems[12].Enabled = true;

            if (_SaveAs == true)
                _SaveAsBtnVisible = true;
            SaveAsBtnVisible = _SaveAs;
            MnuFile.Items[0].ChildItems[4].Enabled = _SaveAs;


            //modified by Rupesh 
            btnFilterImage.Visible = true;
            btnFilterImage.Enabled = true;
            MnuFile.Items[0].ChildItems[7].Enabled = true;

            if (_Edit)
                btnSMSImage.Visible = true;
            else
                btnSMSImage.Visible = false;

            // Added By Pritesh on 9 August 2014
            MnuFile.Items[0].ChildItems[14].Enabled = false ;
            PrintExcelBtnVisible = false;
            MnuFile.Items[0].ChildItems[15].Enabled = false;
            FillDetailBtnVisible = false;
        }
        #endregion

        #region [ Set Visible Properties ]
        public void SetVisibleProperties(bool _New, bool _Edit, bool _Delete, bool _Save, bool _Refresh, bool _Print, bool _Close, bool _Help, bool _List, bool _Email, bool _SaveAs, bool _PrintExcel, bool _FillDetail)
        {
            SaveAsFlag = false;
            if (_Role_Add == true)
            {
                NewBtnVisible = _New;
                MnuFile.Items[0].ChildItems[0].Enabled = _New;
            }
            else
            {
                NewBtnVisible = false;
                MnuFile.Items[0].ChildItems[0].Enabled = false;
            }

            if (_Role_Modify == true)
            {
                EditBtnVisible = _Edit;
                MnuFile.Items[0].ChildItems[1].Enabled = _Edit;
            }
            else
            {
                EditBtnVisible = false;
                MnuFile.Items[0].ChildItems[1].Enabled = false;
            }

            if (_Role_Delete == true)
            {
                DeleteBtnVisible = _Delete;
                MnuFile.Items[0].ChildItems[2].Enabled = _Delete;
            }
            else
            {
                DeleteBtnVisible = false;
                MnuFile.Items[0].ChildItems[2].Enabled = false;
            }
            MnuFile.Items[0].ChildItems[7].Enabled = false;
            btnFilterImage.Visible = false;

            SaveBtnVisible = _Save;
            MnuFile.Items[0].ChildItems[3].Enabled = _Save;

            RefreshBtnVisible = _Refresh;
            MnuFile.Items[0].ChildItems[10].Enabled = _Refresh;
            PrintBtnVisible = _Print;
            MnuFile.Items[0].ChildItems[8].Enabled = _Print;
            CloseBtnVisible = _Close;
            MnuFile.Items[0].ChildItems[5].Enabled = _Close;
            HelpBtnVisible = _Help;
            MnuFile.Items[0].ChildItems[11].Enabled = _Help;
            if (_Role_View == true)
            {
                ListBtnVisible = _List;
                MnuFile.Items[0].ChildItems[6].Enabled = _List;
                btnFilterImage.Visible = true;
                btnFilterImage.Enabled = true;
                MnuFile.Items[0].ChildItems[7].Enabled = true;
            }
            else
            {
                ListBtnVisible = false;
                MnuFile.Items[0].ChildItems[6].Enabled = false;
                btnFilterImage.Visible =false;
                btnFilterImage.Enabled = false;
                MnuFile.Items[0].ChildItems[7].Enabled = false;
            }

            EmailBtnVisible = _Email;
            MnuFile.Items[0].ChildItems[9].Enabled = _Email;

            ExitBtnVisible = true;
            MnuFile.Items[0].ChildItems[12].Enabled = true;

            if (_SaveAs == true)
                _SaveAsBtnVisible = true;
            SaveAsBtnVisible = _SaveAs;
            MnuFile.Items[0].ChildItems[4].Enabled = _SaveAs;


            //modified by Rupesh 
            btnFilterImage.Visible = true;
            btnFilterImage.Enabled = true;
            MnuFile.Items[0].ChildItems[7].Enabled = true;

            if (_Edit)
                btnSMSImage.Visible = true;
            else
                btnSMSImage.Visible = false;

            // Added By Pritesh on 9 August 2014
            MnuFile.Items[0].ChildItems[14].Enabled = _PrintExcel;
            PrintExcelBtnVisible = _PrintExcel;
            MnuFile.Items[0].ChildItems[15].Enabled = _FillDetail;
            FillDetailBtnVisible = _FillDetail;

        }
        #endregion

        #region [ Button Events ]
        protected void btnSaveImage_Click(object sender, ImageClickEventArgs e)
        {
            //SetVisibleProperties(true, true, true, false, false, true, false, true, true, true, false);
            if (btnSaveImageHandler != null)
            {
                btnSaveImageHandler(this, e);
            }
            btnListImage.Visible = true;
            btnListImage.Enabled = true;
        }
        protected void btnSaveAsImage_Click(object sender, ImageClickEventArgs e)
        {
          //  SetVisibleProperties(true, true, true, false, false, true, false, true, true, true, false);
            if (btnSaveAsImageHandler != null)
            {
                btnSaveAsImageHandler(this, e);
            }
        }
        protected void btnNewImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnNewImageHandler != null)
            {
                if (SaveAsFlag == true)
                    SetVisibleProperties(false, false, false, true, false, false, true, true, false, false, false);
                else
                    SetVisibleProperties(true, true, true, true, true, true, true, true, true, true);
                btnListImage.Visible = true;
                btnListImage.Enabled = true;
                btnFilterImage.Visible = false;
                btnFilterImage.Enabled = false;
                MnuFile.Items[0].ChildItems[7].Enabled = false;
                btnNewImageHandler(this, e);
            }
        }
        protected void btnEditImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnEditImageHandler != null)
            {
                if (SaveAsFlag == true)
                    SetVisibleProperties(false, false, false, true, false, false, true, true, false, false, true);
                else
                    SetVisibleProperties(false, false, false, true, false, false, true, true, false, false);
                btnEditImageHandler(this, e);
            }
        }
        protected void btnDeleteImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnDeleteImageHandler != null)
            {
                if (SaveAsFlag == true)
                    SetVisibleProperties(true, false, false, false, true, true, false, true, false, false, false);
                else
                    SetVisibleProperties(true, false, false, false, true, true, false, true, false, false);

                btnFilterImage.Visible = true;
                btnFilterImage.Enabled = true;
                MnuFile.Items[0].ChildItems[7].Enabled = true;
                btnDeleteImageHandler(this, e);
            }
        }
        protected void btnPrintImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnPrintImageHandler != null)
            {
                btnPrintImageHandler(this, e);
            }
        }

        protected void btnListImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnListImageHandler != null)
            {
                if (SaveAsFlag == true)
                    SetVisibleProperties(true, false, false, false, true, true, false, true, false, false, false);
                else
                    SetVisibleProperties(true, false, false, false, true, true, false, true, false, false);

                btnFilterImage.Visible = true;
                btnFilterImage.Enabled = true;
                btnDeleteImage.Visible = true;
                btnDeleteImage.Enabled = true;
                btnEditImage.Enabled = true;
                btnEditImage.Visible = true;
                MnuFile.Items[0].ChildItems[7].Enabled = true;
                btnListImageHandler(this, e);
            }
        }
        protected void btnFilterImage_Click(object sender, ImageClickEventArgs e)
        {
            if (!AllFalse)
            {
                if (btnFilterImageHandler != null)
                {
                    if (SaveAsFlag == true)
                        SetVisibleProperties(true, false, false, false, true, true, false, true, false, false, false);
                    else
                        SetVisibleProperties(true, false, false, false, true, true, false, true, false, false);
                    //Modified by Rupesh on date 26 May 2011
                    btnListImageHandler(this, e);
                    btnFilterImageHandler(this, e);
                }
            }
            else
                btnFilterImageHandler(this, e);

        }
        protected void btnHelpImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnHelpImageHandler != null)
            {
                btnHelpImageHandler(this, e);
            }
        }
        protected void btnRefreshImage_Click(object sender, ImageClickEventArgs e)
        {
            if (!AllFalse)
            {
                if (btnRefreshImageHandler != null)
                {
                    if (SaveAsFlag == true)
                        SetVisibleProperties(true, false, false, false, true, true, false, true, false, false, false);
                    else
                        SetVisibleProperties(true, false, false, false, true, true, false, true, false, false);

                    btnFilterImage.Visible = true;
                    btnFilterImage.Enabled = true;
                    MnuFile.Items[0].ChildItems[7].Enabled = true;

                    btnRefreshImageHandler(this, e);
                }
            }
            else
                btnRefreshImageHandler(this, e);
        }
        protected void btnCloseImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnCloseImageHandler != null)
            {
                if (SaveAsFlag == true)
                    SetVisibleProperties(true, false, false, false, false, false, false, true, true, false, false);
                else
                    SetVisibleProperties(true, false, false, false, false, false, false, true, true, false);
                btnCloseImageHandler(this, e);
            }
        }
        protected void btnViewImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnViewImageHandler != null)
            {
                btnViewImageHandler(this, e);
            }
           
            btnEditImage.Visible = true;
            btnEditImage.Enabled = true;
            btnDeleteImage.Visible = true;
            btnDeleteImage.Enabled = true;
          
        }
        protected void btnEmailImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnEmailImageHandler != null)
            {
                btnEmailImageHandler(this, e);
            }
        }


        protected void btnExitImage_Click(object sender, ImageClickEventArgs e)
        {
            //Response.Redirect("frmWorkFlowNew.aspx");
            Response.Redirect("<script language='javascript'>{self.close()}</script>");
           // Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "ClosePage", "window.onunload=CloseWindow();");
           Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClosePage", "window.Close();",true);
           // ScriptManager.RegisterStartupScript(this,this.GetType(), "ClosePage", "window.Close();", true);
        }

        protected void btnSMSImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnSMSImageHandler != null)
            {
                btnSMSImageHandler(this, e);
            }
        }

        protected void btnPrintExcelImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnPrintExcelImageHandler != null)
            {
                btnPrintExcelImageHandler(this, e);
            }
        }

        protected void btnFillDetailImage_Click(object sender, ImageClickEventArgs e)
        {
            if (btnFillDetailImageHandler != null)
            {
                btnFillDetailImageHandler(this, e);
            }
        }


        #endregion
        
        #region [ Button Properties ]
        public ImageButton Savebtn
        {
            get
            {
                return btnSaveImage;
            }
        }
        public ImageButton SaveAsbtn
        {
            get
            {
                return btnSaveAsImage;
            }
        }
        public ImageButton Newbtn
        {
            get
            {
                return btnNewImage;
            }
        }
        public ImageButton Editbtn
        {
            get
            {
                return btnEditImage;
            }
        }
        public ImageButton Deletebtn
        {
            get
            {
                return btnDeleteImage;
            }
        }
        public ImageButton Printbtn
        {
            get
            {
                return btnPrintImage;
            }
        }
        public ImageButton Listbtn
        {
            get
            {
                return btnListImage;
            }
        }
        public ImageButton Helpbtn
        {
            get
            {
                return btnHelpImage;
            }
        }
        public ImageButton Refreshbtn
        {
            get
            {
                return btnRefreshImage;
            }
        }
        public ImageButton Closebtn
        {
            get
            {
                return btnCloseImage;
            }
        }
        public ImageButton Viewbtn
        {
            get
            {
                return btnViewImage;
            }
        }
        public ImageButton Emailbtn
        {
            get
            {
                return btnEmailImage;
            }
        }
        public ImageButton Exitbtn
        {
            get
            {
                return btnExitImage;
            }
        }
        public ImageButton SMSbtn
        {
            get
            {
                return btnSMSImage;
            }
        }
        #endregion

        #region [ properties for tab index ]
        public Int16 SaveBtnTabIndex
        {
            set { _SaveBtnTabIndex = value; }
        }
        public Int16 SaveAsBtnTabIndex
        {
            set { _SaveAsBtnTabIndex = value; }
        }
        public Int16 NewBtnTabIndex
        {
            set { _NewBtnTabIndex = value; }
        }
        public Int16 EditBtnTabIndex
        {
            set { _EditBtnTabIndex = value; }
        }
        public Int16 DeleteBtnTabIndex
        {
            set { _DeleteBtnTabIndex = value; }
        }
        public Int16 PrintBtnTabIndex
        {
            set { _PrintBtnTabIndex = value; }
        }
        public Int16 ListBtnTabIndex
        {
            set { _ListBtnTabIndex = value; }
        }
        public Int16 HelpBtnTabIndex
        {
            set { _HelpBtnTabIndex = value; }
        }
        public Int16 RefreshBtnTabIndex
        {
            set { _RefreshBtnTabIndex = value; }
        }
        public Int16 CloseBtnTabIndex
        {
            set { _CloseBtnTabIndex = value; }
        }
        public Int16 ViewBtnTabIndex
        {
            set { _ViewBtnTabIndex = value; }
        }
        public Int16 EmailBtnTabIndex
        {
            set { _EmailBtnTabIndex = value; }
        }
        public Int16 ExitBtnTabIndex
        {
            set { _ExitBtnTabIndex = value; }
        }
        public Int16 SMSBtnTabIndex
        {
            set { _SMSBtnTabIndex = value; }
        }
        public Int16 PrintExcelBtnTabIndex
        {
            set { _PrintExcelBtnTabIndex = value; }
        }
        public Int16 FillDetailBtnTabIndex
        {
            set { _FillDetailBtnTabIndex = value; }
        }
        #endregion

        #region [ properties For Role Rights]
        public bool Role_Add
        {
            set { _Role_Add = value; }
        }
        public bool Role_Modify
        {
            set { _Role_Modify = value; }
        }
        public bool Role_Delete
        {
            set { _Role_Delete = value; }
        }
        public bool Role_View
        {
            set { _Role_View = value; }
        }

        #endregion

        #region [ Menu Event ]
        protected void MnuFile_MenuItemClick(object sender, MenuEventArgs e)
        {
            ImageClickEventArgs a = new ImageClickEventArgs(0, 0);
            switch (e.Item.Value)
            {
                case "New":
                    btnNewImage_Click(sender, a);
                    break;
                case "Edit":
                    btnEditImage_Click(sender, a);
                    break;
                case "Delete":
                    btnDeleteImage_Click(sender, a);
                    break;
                case "Save":
                    btnSaveImage_Click(sender, a);
                    break;
                case "SaveAs":
                    btnSaveAsImage_Click(sender, a);
                    break;
                case "Print":
                    btnPrintImage_Click(sender, a);
                    break;
                case "List":
                    btnListImage_Click(sender, a);
                    break;
                case "Filter":
                    btnFilterImage_Click(sender, a);
                    break;
                case "Help":
                    btnHelpImage_Click(sender, a);
                    break;
                case "Refresh":
                    btnRefreshImage_Click(sender, a);
                    break;
                case "Cancel":
                    btnCloseImage_Click(sender, a);
                    break;
                case "EMail":
                    btnEmailImage_Click(sender, a);
                    break;
                case "Close":
                    btnExitImage_Click(sender, a);
                    break;
                case "SMS":
                    btnSMSImage_Click(sender, a);
                    break;
                case "PrintExcel":
                    btnPrintExcelImage_Click(sender, a);
                    break;
                case "FillDetail":
                    btnFillDetailImage_Click(sender, a);
                    break;
            }
        }
        #endregion
    }
}