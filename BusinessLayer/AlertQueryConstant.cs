﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BusinessLayer
{
   public class AlertQueryConstant
    {
        # region [SP_FillDDLAlertType Property]
        public string SP_FillDDLAlertType
        {
            get
            {
                return "ssp_BindAlertQueryType";
            }
        }
        # endregion
        # region [SP_FillGrid Property]
        public string SP_FillGrid
        {
            get
            {
                return "ssp_DisplayAlertQuery";
            }
        }
        # endregion
        # region [ssp_DisplayAlertMessage Property]
        public string ssp_DisplayAlertMessage
        {
            get
            {
                return "ssp_DisplayAlertMessage";
            }
        }
        # endregion
    }
}
