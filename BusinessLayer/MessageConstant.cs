﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BusinessLayer
{
    public class MessageConstant
    {
        # region [sp_InsertMessageDetails Property]
        public string sp_InsertMessageDetails
        {
            get
            {
                return "sp_InsertMessageDetails";
            }
        }
        # endregion

        # region [sp_UpdateMessageDetails Property]
        public string sp_UpdateMessageDetails
        {
            get
            {
                return "sp_UpdateMessageDetails";
            }
        }
        # endregion
        # region [sp_UpdateUserLoginDetails Property]
        public string sp_UpdateUserLoginDetails
        {
            get
            {
                return "sp_UpdateUserLoginDetails";
            }
        }
        # endregion
        # region [sp_deleteMessageDetails Property]
        public string sp_deleteMessageDetails
        {
            get
            {
                return "sp_deleteMessageDetails";
            }
        }
        # endregion
    }
}
